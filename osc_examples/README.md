# AudioStellar's OSC Examples

Here you can download simple examples for many programs, including:

* Max/MSP
* PureData
* Python
* Processing
* Tidal
