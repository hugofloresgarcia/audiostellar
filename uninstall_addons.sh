#!/bin/bash
PROJECT_DIR=$(realpath "$0")
PROJECT_DIR=$(dirname "$PROJECT_DIR")

if [ -z "${OF_ROOT}" ]; then
    echo "OF_ROOT enviroment variable is undefined."

    read -p "Is your app in apps/myApps folder? " -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        OF_ROOT=$PROJECT_DIR/../../..
    else
        echo "Rerun this script setting OF_ROOT enviroment variable: $ export OF_ROOT=<path/to/of>\n "
        exit
    fi
fi


rm -rf $PROJECT_DIR/bin/data/assets

rm -rf $OF_ROOT/addons/ofxMidi
rm -rf $OF_ROOT/addons/ofxJSON
rm -rf $OF_ROOT/addons/ofxConvexHull
rm -rf $OF_ROOT/addons/ofxImGui
rm -rf $OF_ROOT/addons/ofxAudioFile
rm -rf $OF_ROOT/addons/ofxPDSP
rm -rf $OF_ROOT/addons/ofxAbletonLink
rm -rf $OF_ROOT/addons/ofxAnn

echo "DONE"
