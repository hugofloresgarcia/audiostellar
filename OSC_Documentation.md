# OSC Documentation

AudioStellar has a OSC API that can be used to control its parameters from other aplications and devices. This way, traversing latent space in custom ways is also possible without having to program a whole new mode.

We are providing examples in **PureData**, **Max**, **Tidal** and **Python** of many of these functionalities.

This document provides a reference to all implemented OSC routes. Words in brackets are parameters needed. Words in double brackets are optional

## Control parameters

### Mixer

Fixed addresses for changing basic parameters of units.

If OSC is enabled, AudioStellar will also send this routes with the current value as a parameter so both faders are in sync.

#### /units/[index]/volume [volume]

Changes the volume of the unit at [index]. Volume is a float between 0-1.

#### /units/[index]/pan [pan]

Changes the pan of the unit at [index]. Volume is a float between 0-1.

### OSC Learn

Pressing mouse right button on (almost) any fader will popup a context menu that allows for OSC Learn. A dialog will appear that will wait until a new OSC message arrive. The address of the message will be mapped to that fader. You should send a float parameter between 0 and 1 that will change values of faders in the UI. It is meant to be used with external applications and devices such as OSC controllers. Note that if OSC is enabled, AudioStellar will also send this routes with the current value as a parameter so both faders are in sync.

## Play sounds  

### Play by coordinates

For these addresses you'll need an OSC Unit or to check the "Enable OSC" checkbox in units of type Explorer or Particle.

#### /play/xy [x] [y] [[volume]]

Play a sound near x,y coordinates. x and y parameters are between [0,1]. Volume is optional and is between [0,1]. A cross located at provided coordinates will briefly appear on AudioStellar's screen as a visual feedback.

#### /play/x [x] [[volume]]

Play a sound near x coordinate and the last y coordinate sent. x parameter is between [0,1]. This endpoint is meant to be used for applications that can't send multiple parameters at once. Volume is optional and is between [0,1]. A cross located at provided coordinates will briefly appear on AudioStellar's screen as a visual feedback.

#### /play/y [y] [[volume]]

Play a sound near y coordinate and the last x coordinate sent. y parameter is between [0,1]. This endpoint is meant to be used for applications that can't send multiple parameters at once. Volume is optional and is between [0,1]. A cross located at provided coordinates will briefly appear on AudioStellar's screen as a visual feedback.

### Play by metadata

You'll need an OSC Unit to process this messages.

#### /play/id [sound_id] [[volume]]

Play sound with ID **sound_id**. Volume is optional and is between [0,1].

#### /play/cluster [clusterName] [[index]] [[volume]]

Play a sound from a cluster named **clusterName**. If **index** is not present AudioStellar will choose a random one; note that the index will cycle through the number of sounds in the cluster. Volume is optional and is between [0,1].

#### /play/clusterBySoundId [sound_id] [[index]] [[volume]]

Play a sound from the same cluster as the sound with id **sound_id**. If **index** is not present AudioStellar will choose a random one; note that the index will cycle through the number of sounds in the cluster. Volume is optional and is between [0,1].

## Get information

This routes are used to get information about currently opened dataset. After sending one of this messages, another message will arrive with the same route and with the parameters as the response.

#### /get/soundCount

Returns the number of sounds in the dataset. Available sounds ids go from 0 to the number returned by this function.

#### /get/positionByID [sound_id]

Returns x and y coordinates of the sound with id *sound_id*

#### /get/neighborsByID [sound_id] [threshold]

Returns an array of sound ids with neighbors of the sound with id *sound_id*. The threshold parameter [0,1] is up to how far a sound is considered a neighbor. It is defined as the squared distance normalized by the width of the window.

#### /get/neighborsByXY [x] [y] [threshold]

Returns an array of sound ids near coordinates x,y. The threshold parameter [0,1] is up to how far a sound is considered a neighbor. It is defined as the squared distance normalized by the width of the window.

#### /get/playedSound [enabled]

Send 1 to enable or 0 to disable. AudioStellar will send an OSC message using the same route with parameters: soundID, clusterID, x and y of any played sound. It is disabled by default.

#### /get/clusterIDByID [sound_id]

Returns cluster ID of the sound with id *sound_id*

#### /get/clusterNameByID [sound_id]

Returns cluster name of the sound with id *sound_id*

#### /get/clusterNameByClusterID [cluster_id]

Returns cluster name of the cluster with id *cluster_id*

## Control using Tidal cycles (experimental)

Current implementation allows to control the sound, n, and gain params

Timing will be a little bit off since we don't handle the timing of events, just fires messages as soon as they arrive.

Using previously named clusters you can treat them as a folder of samples.

More info in osc_examples/osc.tidal
