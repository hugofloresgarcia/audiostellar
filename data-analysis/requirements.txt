numpy==1.17.5
numba==0.48.0
scikit-learn==0.22
joblib==0.12
librosa==0.7.2
umap-learn
pyinstaller
