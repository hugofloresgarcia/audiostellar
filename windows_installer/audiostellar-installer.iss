[Setup]
AppName=AudioStellar
AppVersion=1.1
WizardStyle=modern
DefaultDirName={pf64}\AudioStellar
DefaultGroupName=AudioStellar
UninstallDisplayIcon={app}\AudioStellar.exe
Compression=lzma2
SolidCompression=yes
OutputDir=C:\Users\leandro\Desktop\AudioStellarBins
OutputBaseFilename=AudioStellar-1.1-win64

[Files]
Source: "audiostellar-1.1-win64\*"; DestDir: "{app}"; Flags: recursesubdirs

[Icons]                        
Name: "{group}\AudioStellar"; Filename: "{app}\AudioStellar.exe"; IconFilename: "{app}\data\assets\icon.ico"
Name: "{group}\Uninstall AudioStellar"; Filename: "{uninstallexe}"; IconFilename: "{app}\data\assets\icon.ico"

[Dirs]
Name: "{app}"; Permissions: everyone-full 

[Run]
Filename: {app}\{cm:AppName}.exe; Description: {cm:LaunchProgram,{cm:AppName}}; Flags: nowait postinstall skipifsilent

[CustomMessages]
AppName=AudioStellar
LaunchProgram=Start AudioStellar after finishing installation