# AudioStellar - http://audiostellar.xyz

AI powered experimental sampler

![logo](https://gitlab.com/ayrsd/audiostellar/-/raw/units/audiostellar-logo.png)

Explore your sound collection by creating your own constellations with thousands of sounds. Play in creative new ways interacting with the generated map and connecting with other software and hardware devices. Invent your own way for playing.

## Downloads

Get them from [https://audiostellar.xyz](https://audiostellar.xyz)

## Machine learning pipeline

![Pipeline](https://gitlab.com/ayrsd/audiostellar/raw/master/data-analysis/proceso.png)

## Community

* [Forums](https://forum.audiostellar.xyz/)
* [Discord server](https://discord.gg/JKDjqmEWkx)
* [Facebook Group](https://www.facebook.com/groups/3296184277128735)

## Contribute

[Buy me a coffee !](https://www.buymeacoffee.com/audiostellar)

* Make music with it
* Make your own sound map
* Make sound objects and installations
* Share your creations in the forums
* Make physical interfaces using OSC
* Fork it
* Hack it
* Browse our issues
* Open new issues
* Make it your own
* We love pull requests

### License

GNU/GPL v3

### How to compile

1. Download openFrameworks [0.11.2](https://openframeworks.cc/) (Linux, Windows) or [0.11.0](https://openframeworks.cc/versions/v0.11.0/of_v0.11.0_osx_release.zip) (macOS)
2. Follow [openFramework's install instructions](https://openframeworks.cc/download/) and compile an example.
3. Place this project in apps/myApps. (advanced users can set OF_ROOT enviroment variable to point where you want instead)
4. From a terminal run install.sh. This will download all the addons needed, link the assets and customize the oF installation.

#### Linux

5. Just use 'make' from a terminal or use [QTCreator](https://openframeworks.cc/setup/qtcreator/)

#### Mac

5. We are providing an [XCode](https://openframeworks.cc/setup/xcode/) project you can use

#### Windows

5. We are providing a Visual Studio project you can use

#### Finally

At this point AudioStellar should compile and execute properly but you won't be able to create your own sound map. For achieving that :

6. Follow [data-analysis README instructions](https://gitlab.com/ayrsd/audiostellar/tree/master/data-analysis) for compiling python's machine learning process.

## OSC Support

* [OSC documentation](https://gitlab.com/ayrsd/audiostellar/-/blob/units/OSC_Documentation.md)
* Check out our [osc-examples](https://gitlab.com/ayrsd/audiostellar/tree/units/osc_examples)

We are providing examples for:
* Python
* Puredata
* Max
* Touch osc
