#include "ExplorerUnit.h"
#include "Behaviors/UnitBehaviorOSC.h"
#include "Behaviors/UnitBehaviorDragToPlay.h"
#include "Behaviors/UnitBehaviorPolyphony.h"
#include "Behaviors/UnitBehaviorMIDIKeyboard.h"

ExplorerUnit::ExplorerUnit() {
    sounds = Sounds::getInstance();

    trajectory = new UnitBehaviorTrajectory(this);
    trajectory->onlyEvent = true;
    ofAddListener(trajectory->onTrajectory, this, &ExplorerUnit::onTrajectory);
    behaviors.push_back( trajectory );

    behaviors.push_back( new UnitBehaviorOSC(this) );
    behaviors.push_back( new UnitBehaviorDragToPlay(this) );
    behaviors.push_back( new UnitBehaviorMIDIKeyboard(this) );

    //set poly settings before creating
    setVoicesType(Voices::VoicesType::FullPolyphonic);
    setPolyphonyChoke( false );
    behaviors.push_back( new UnitBehaviorPolyphony(this) );
}

ExplorerUnit::~ExplorerUnit()
{
    ofRemoveListener(trajectory->onTrajectory, this, &ExplorerUnit::onTrajectory);
}

void ExplorerUnit::reset() {
    if(!midiMappings.empty()) {
        midiMappings.clear();
    }
}

void ExplorerUnit::mousePressed(ofVec2f p, int button) {
    shared_ptr<Sound> hoveredSound = sounds->getHoveredSound();
    
    if ( hoveredSound != nullptr ) {
        if ( button == 0 ) {
            // When using MIDI keyboard the adsr will close
            // and clicking won't be heard otherwise
            openMIDIKeyboardEnvelope();

            playSound(hoveredSound);
        }
    }
}

void ExplorerUnit::mouseDragged(ofVec2f p, int button){

}

float ExplorerUnit::getDistanceFrom(ofVec2f point)
{
    float minDistance = -1;
    if ( trajectory->isPlaying() ) {
        ofVec2f currentPosition = trajectory->getCurrentPosition();
        minDistance = ofDistSquared( point.x,
                                     point.y,
                                     currentPosition.x,
                                     currentPosition.y );
    }
    return minDistance;
}

void ExplorerUnit::onSelectedUnit()
{
    Unit::onSelectedUnit();

    if ( trajectory->isPlaying() ) {
        Units::getInstance()->revealUnit( trajectory->getCurrentPosition() );
    }
}

void ExplorerUnit::onTrajectory(ofVec2f &position)
{
    shared_ptr<Sound> nearestSound = sounds->getNearestSound(position);
    if (nearestSound != nullptr) {
        if (nearestSound != lastPlayedTrajectory) {
            if ( ofRandom(1.f) <= trajectory->probability.get() ) {
                playSound(nearestSound);
            }

            lastPlayedTrajectory = nearestSound;
        }
    }
    else {
        lastPlayedTrajectory = nullptr;
    }
}

Json::Value ExplorerUnit::save()
{
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value jsonMidiMappings = Json::Value( Json::objectValue );
    
    for ( auto midiMapping : midiMappings) {
        jsonMidiMappings[ ofToString(midiMapping.first) ] = midiMapping.second;
    }
    
    root["midiMappings"] = jsonMidiMappings;
    return root;
}

void ExplorerUnit::load(Json::Value jsonData)
{
    Json::Value jsonMidiMappings = jsonData["midiMappings"];
    if ( jsonMidiMappings != Json::nullValue ) {
        for( Json::Value::iterator itr = jsonMidiMappings.begin() ; itr != jsonMidiMappings.end() ; itr++ ) {
            ofLog() << itr.key() << ":" << *itr;
            midiMappings[ ofToInt( itr.key().asString() ) ] = (*itr).asInt();
        }
    }
}

void ExplorerUnit::drawGui() {

}
