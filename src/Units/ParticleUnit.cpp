#include "ParticleUnit.h"
#include "../GUI/ExtraWidgets.h"
#include "Behaviors/UnitBehaviorOSC.h"
#include "Behaviors/UnitBehaviorTrajectory.h"
#include "Behaviors/UnitBehaviorPolyphony.h"

ParticleUnit::ParticleUnit(){
    atModel = 0;// default is walk model

    density.format = "%.0f";
    lifespan.minFormat = "Min";
    lifespan.maxFormat = "Forever";
    respawn.setStyle(UberToggle::CHECKBOX);

    parameters.push_back(&lifespan);
    parameters.push_back(&velocityX);
    parameters.push_back(&velocityY);
    parameters.push_back(&spreadAmount);
    parameters.push_back(&emitRate);
    parameters.push_back(&emit);
    parameters.push_back(&emitAreaSize);
    parameters.push_back(&speed);
    parameters.push_back(&density);

    parameters.push_back(&probability);
    parameters.push_back(&respawn);

    behaviors.push_back( new UnitBehaviorOSC(this) );
    behaviors.push_back( new UnitBehaviorTrajectory(this) );

    behaviorKeyboard = new UnitBehaviorMIDIKeyboard(this);
    ofAddListener( behaviorKeyboard->keyboardEvent, this, &ParticleUnit::onMIDIKeyboard );
    behaviors.push_back( behaviorKeyboard );

    //set poly settings before creating
    setVoicesType(Voices::VoicesType::FullPolyphonic);
    behaviors.push_back( new UnitBehaviorPolyphony(this) );

    emit.style = UberButton::Big;
    emit.addListener( this, &ParticleUnit::emitButtonListener );
}

void ParticleUnit::update(){
    if ( !isActive() ) return;

    if( emitButtonPressed ) {
        if ( emitRandomPosition && emitAreaSize > 0 ) {
            ofVec2f randomDisplacement( ofRandom(-emitAreaSize/2, emitAreaSize/2),
                                        ofRandom(-emitAreaSize/2, emitAreaSize/2) );

            castParticleWithRate( centroid + randomDisplacement );
        } else {
            castParticleWithRate( centroid );
        }
    }

    unsigned int cantDeath = 0;
    particles.remove_if([this, &cantDeath](const SwarmParticle& p) {
        if ( p.age <= 0 && respawn ) {
            cantDeath++;
        }
        return p.age <= 0;
    });

    //rebirth
    for ( unsigned int i = 0 ; i < cantDeath; i++ ) {
        if ( swarmModels[atModel] == MODEL_EXPLOSION ) {
            castParticleWithRate();
        } else {
            castParticle();
        }
    }

    if ( !particles.empty() ) {
        ofRectangle boundingBox( particles.front().getPosition().x,
                                 particles.front().getPosition().y, 1, 1);
        bool mustCalculateBB = true;

        if ( centroid != ofVec2f(-1,-1) && emitAreaSize > 0 ) {
            mustCalculateBB = false;

            ofRectangle bb;
            bb.setFromCenter(centroid, emitAreaSize, emitAreaSize);
            setBoundingBox(bb);
        }

        for ( SwarmParticle &particle : particles ) {
            //Update params of particle
            particle.update();
            particle.lifeSpan = lifespan.get()*10.0;

            if(swarmModels[atModel] == MODEL_SWARM){
              particle.velocity.x = velocityX.get();
              particle.velocity.y = velocityY.get();
              particle.spreadAmount = spreadAmount.get();
            }else if(swarmModels[atModel] == MODEL_EXPLOSION){
              //For gui params that need to be updated every frame
            }

            ofVec2f pos = particle.getPosition();

            if ( emitAreaSize > 0 &&
                 ofDist( pos.x, pos.y,
                         centroid.x, centroid.y ) >
                 emitAreaSize.get()/2 ) {
                particle.kill();
                continue;
            }

            //Get near sound
            shared_ptr<Sound> soundNearParticle = Sounds::getInstance()->getNearestSound(pos);

            //If exists, play it
            if( soundNearParticle != nullptr && ofRandom(1.f) < probability.get() &&
                soundNearParticle != particle.lastPlayedSound) {

                particle.lastPlayedSound = soundNearParticle;
                playSound(soundNearParticle);
            }

            if ( mustCalculateBB ) {
                boundingBox.growToInclude(particle.getPosition());
            }
        }

        if ( mustCalculateBB ) {
            setBoundingBox( boundingBox );
        }
    }
}

void ParticleUnit::onMIDIKeyboard(vector<int> &m)
{
    emitButtonPressed = !m.empty();
}

void ParticleUnit::calculateBoundingBox()
{
    if ( !particles.empty() ) {
        if ( centroid != ofVec2f(-1,-1) && emitAreaSize > 0 ) {
            ofRectangle bb;
            bb.setFromCenter(centroid, emitAreaSize, emitAreaSize);
            setBoundingBox(bb);
        } else {
            ofRectangle boundingBox( particles.front().getPosition().x,
                                     particles.front().getPosition().y, 1, 1);

            for ( SwarmParticle &particle : particles ) {
                boundingBox.growToInclude(particle.getPosition());
            }

            setBoundingBox(boundingBox);
        }
    }
}

void ParticleUnit::updateParticlesSize(){
    for ( SwarmParticle &particle : particles ) {
        particle.setRandomSize();
    }
}

void ParticleUnit::draw(){
    if ( selected ) drawCentroid();
    drawArea();

    for ( SwarmParticle &particle : particles ) {
        if ( selected ) {
            particle.draw( ofColor( PARTICLE_COLOR_R, PARTICLE_COLOR_G, PARTICLE_COLOR_B ) );
        } else {
            particle.draw( ofColor( PARTICLE_COLOR_INACTIVE_R, PARTICLE_COLOR_INACTIVE_G, PARTICLE_COLOR_INACTIVE_B ) );
        }
    }
}

void ParticleUnit::drawCentroid()
{
    if ( centroid != ofVec2f(-1,-1) ) {
        ofNoFill();
        ofSetColor(255,255,255, 100);
        ofDrawLine((centroid.x), (centroid.y - 4.5f),
                   (centroid.x), (centroid.y + 4.5f));
        ofDrawLine((centroid.x - 4.5f), (centroid.y),
                   (centroid.x + 4.5f), (centroid.y));
    }
}

void ParticleUnit::drawArea()
{
    if ( emitAreaSize.get() == 0.f ) return;
    ofNoFill();

    if ( selected ) {
        ofSetColor(PARTICLE_COLOR_R,PARTICLE_COLOR_G,PARTICLE_COLOR_B, 255);
    } else {
        ofSetColor(PARTICLE_COLOR_INACTIVE_R,PARTICLE_COLOR_INACTIVE_G,PARTICLE_COLOR_INACTIVE_B, 100);
    }

    float zoomScale = CamZoomAndPan::getInstance()->getScale();
    float circleResolution = int( ofClamp(( (emitAreaSize.get() / 4.f ) * zoomScale * 1.5f) + 8.5f, 10.f, 100.f) );
    
    ofSetCircleResolution(circleResolution);
    ofDrawEllipse( centroid, emitAreaSize.get(), emitAreaSize.get() );
}

void ParticleUnit::drawGui(){

    if ( centroid != ofVec2f(-1,-1) ) {
        emit.draw();
        ImGui::NewLine();

        if (ExtraWidgets::Header("Emitter settings")) {
            emitRate.draw();
            emitAreaSize.draw();
            ImGui::NewLine();

            ImGui::Checkbox("Lock emitter position", &lockParticleEmitter);
            if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_PARTICLE_LOCK );

            if ( emitAreaSize.get() > 0.f ) {
                ImGui::Checkbox("Randomize inside area", &emitRandomPosition);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_PARTICLE_EMIT_RANDOM );
            }

            respawn.draw();

            ImGui::NewLine();
        }
    } else {
        emit.draw(true);
        ImGui::NewLine();
    }

    if (ExtraWidgets::Header("Particles behavior")) {
        // Draw model selector
        // TODO: should be an uber radio button...
        for(int i = 0; i < NUM_MODELS; i++){
           if(ImGui::RadioButton(swarmModels[i].c_str(), &atModel , i)){
               particles.clear();
               atModel = i;
           }

           if(i != NUM_MODELS - 1){
               ImGui::SameLine();
           }
        }
        ImGui::NewLine();

        lifespan.draw();
        if(swarmModels[atModel] == MODEL_SWARM){
          ImGui::PushItemWidth(97);
              velocityX.draw();
              ImGui::SameLine();
              velocityY.draw();
              ImGui::SameLine();
              ImGui::Text("Velocity");
          ImGui::PopItemWidth();
          spreadAmount.draw();
        }else if(swarmModels[atModel] == MODEL_EXPLOSION){
          speed.draw();
          density.draw();
        }

        ImGui::NewLine();

        probability.draw();

        ImGui::NewLine();
    }

//    if ( volumeSpread.draw() ) updateParticlesSize();
}

void ParticleUnit::reset(){

}

float ParticleUnit::getDistanceFrom(ofVec2f point)
{
    if ( emitAreaSize > 0 ) {
        return ofDistSquared( point.x,
                              point.y,
                              centroid.x,
                              centroid.y );
    } else {
        int maxParticles = 20;
        int i = 0;
        float minDistance = -1;
        for ( SwarmParticle &particle : particles ) {
            if ( i == 0 ) {
                minDistance = ofDistSquared( point.x,
                                             point.y,
                                             particle.getPosition().x,
                                             particle.getPosition().y );
            } else {
                float pDistance = ofDistSquared( point.x,
                                                 point.y,
                                                 particle.getPosition().x,
                                                 particle.getPosition().y );
                if ( pDistance < minDistance ) {
                    minDistance = pDistance;
                }
            }
            i++;

            if ( i == maxParticles ) break;
        }

        return minDistance;
    }
}

void ParticleUnit::emitButtonListener(float &v)
{
    if ( centroid != ofVec2f(-1,-1) ) {
        openMIDIKeyboardEnvelope();

        emitButtonPressed = v > 0;
        emitButtonVelocity = emitButtonPressed ? v : 1.f;
    }
}

void ParticleUnit::mousePressed(ofVec2f p, int button){
    if(button == 0 && !Gui::getInstance()->isAnyModifierPressed()){
        hasPressedAndNotDragged = true;
        castParticleWithRate(p);
    }
}

void ParticleUnit::mouseDragged(ofVec2f p, int button){
    if(button == 0 || button == -1){
        castParticleWithRate(p);
        hasPressedAndNotDragged = false;
    }
}

void ParticleUnit::mouseReleased(ofVec2f p)
{
    if (hasPressedAndNotDragged) {
        if ( !lockParticleEmitter ) {
            centroid = p;
            hasPressedAndNotDragged = false;
        }
    }
}

void ParticleUnit::onSelectedUnit()
{
    Unit::onSelectedUnit();

    int i = 0;
    for ( auto &p : particles ) {
        Units::getInstance()->revealUnit( p.getPosition() );
        i++;
        if ( i >= 10 ) {
            break;
        }
    }
}

Json::Value ParticleUnit::save()
{
    Json::Value params = Json::Value( Json::objectValue );

    params["lifespan"] = lifespan.get();
    params["velocityX"] = velocityX.get();
    params["velocityY"] = velocityY.get();
    params["spreadAmount"] = spreadAmount.get();
//    params["volumeSpread"] = volumeSpread.get();

    params["emitRate"] = emitRate.get();
    params["emitAreaSize"] = emitAreaSize.get();

    params["density"] = density.get();
    params["speed"] = speed.get();

    params["probability"] = probability.get();
    params["rebirth"] = respawn.get();


    params["currentCentroid_x"] = centroid.x;
    params["currentCentroid_y"] = centroid.y;

    params["selectedModel"] = atModel;
    params["emitRandomPosition"] = emitRandomPosition;
    params["lockParticleEmitter"] = lockParticleEmitter;

    Json::Value jsonParticles = Json::Value( Json::arrayValue );

    for ( SwarmParticle &particle : particles ) {
        Json::Value jsonParticle = Json::Value( Json::objectValue );

        jsonParticle["positionX"] = particle.position.x;
        jsonParticle["positionY"] = particle.position.y;

        jsonParticles.append( jsonParticle );
    }
    params["particles"] = jsonParticles;

    return params;
}

void ParticleUnit::load(Json::Value jsonData){
    lifespan.set( jsonData["lifespan"].asFloat() );
    velocityX.set( jsonData["velocityX"].asFloat() );
    velocityY.set( jsonData["velocityY"].asFloat() );
    spreadAmount.set( jsonData["spreadAmount"].asFloat() );
//    volumeSpread.set( jsonData["volumeSpread"].asFloat() );

    emitRate.set( jsonData["emitRate"].asFloat() );
    emitAreaSize.set( jsonData["emitAreaSize"].asFloat() );

    density.set(jsonData["density"].asFloat());
    speed.set(jsonData["speed"].asFloat());
    respawn.set(jsonData["rebirth"].asFloat());

    if ( jsonData["probability"] != Json::nullValue ) {
        probability.set(jsonData["probability"].asFloat());
    }

    if ( jsonData["particles"] != Json::nullValue ) {
        Json::Value jsonParticles = jsonData["particles"];
        for( unsigned int i = 0 ; i < jsonParticles.size() ; i++ ) {
            ofVec2f pos;
            pos.x = jsonParticles[i]["positionX"].asFloat();
            pos.y = jsonParticles[i]["positionY"].asFloat();

            castParticle(pos);
        }
    }

    centroid.x = jsonData["currentCentroid_x"].asFloat();
    centroid.y = jsonData["currentCentroid_y"].asFloat();

    atModel = jsonData["selectedModel"].asInt();
    emitRandomPosition = jsonData["emitRandomPosition"].asBool();
    lockParticleEmitter = jsonData["lockParticleEmitter"].asBool();

    calculateBoundingBox();
}

void ParticleUnit::castParticle()
{
    ofVec2f randomDisplacement = ofVec2f(0,0);
    if ( emitRandomPosition && emitAreaSize > 0 ) {
        randomDisplacement = ofVec2f( ofRandom(-emitAreaSize/2, emitAreaSize/2),
                                    ofRandom(-emitAreaSize/2, emitAreaSize/2) );


    }

    castParticle( centroid + randomDisplacement );
}

void ParticleUnit::castParticle(ofVec2f pos){
    if(swarmModels[atModel] == MODEL_SWARM) {
        SwarmParticle p (pos, MODEL_SWARM, emitButtonVelocity);
        particles.push_front(p);
    } else if(swarmModels[atModel]  == MODEL_EXPLOSION) {
        for(int i = 0; i< density.get(); i++) {
            SwarmParticle p (pos, MODEL_EXPLOSION, emitButtonVelocity);
            p.speed = speed.get();
            p.setup();
            particles.push_front(p);
        }
    }

//    calculateBoundingBox();
}

void ParticleUnit::castParticleWithRate()
{
    ofVec2f randomDisplacement = ofVec2f(0,0);
    if ( emitRandomPosition && emitAreaSize > 0 ) {
        randomDisplacement = ofVec2f( ofRandom(-emitAreaSize/2, emitAreaSize/2),
                                    ofRandom(-emitAreaSize/2, emitAreaSize/2) );


    }

    castParticleWithRate( centroid + randomDisplacement );
}

void ParticleUnit::castParticleWithRate(ofVec2f pos)
{
    //not exactly "rate" but close enough
    if ( ofGetElapsedTimeMillis() - millisLastEmit > (emitRate.getMax()-emitRate.get()) ) {
        castParticle(pos);
        millisLastEmit = ofGetElapsedTimeMillis();
    }
}


/* -------------- SWARM PARTICLE -------------------- */

ParticleUnit::SwarmParticle::SwarmParticle(ofVec2f position, string model, float volume){
    setRandomSize();
    this->position = position;
    lifeSpan = 10.0f;

    this->model = model;
    
    if(model == MODEL_SWARM){
      velocity = ofVec2f(0,0);
      spreadAmount = 2.9f;
    }else if(model == MODEL_EXPLOSION){
      velocity.x = ofRandom(-0.02,0.02);
      velocity.y = ofRandom(-0.02,0.02);
    }

    this->volume = volume;
}

void ParticleUnit::SwarmParticle::setup(){
  if(model == MODEL_SWARM){
  /*
   */
  }else if(model == MODEL_EXPLOSION){
    acceleration.x = velocity.x * speed;
    acceleration.y = velocity.y * speed;
  }
}

void ParticleUnit::SwarmParticle::customUpdate(){
    if(model == MODEL_SWARM){

      velocity.x = spreadAmount * ofRandom(-1.0,1.0) + velocity.x;
      velocity.y = spreadAmount * ofRandom(-1.0,1.0) - velocity.y;
      position += velocity;

    } else if(model == MODEL_EXPLOSION){
      velocity += acceleration;
      position += velocity;
    }

    if(sizeAnimStep < 1.0){
        sizeAnimStep += 0.01;
        size = ofLerp(size, tempSize, sizeAnimStep);
//        volume = size/SIZE_SCALER;
    }
}

void ParticleUnit::SwarmParticle::setRandomSize(){
    float r;
    tempSize = DEFAULT_SIZE;
    sizeAnimStep = 0;
    if(volumeSpread > 0.0f){
        r = ofRandom(0, SwarmParticle::SIZE_SCALER);
        tempSize *= r;
    }
}

