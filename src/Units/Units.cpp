#include "Units.h"
#include "ExplorerUnit.h"
#include "SequenceUnit.h"
#include "ParticleUnit.h"
#include "MorphUnit.h"
#include "OscUnit.h"
#include "../GUI/ExtraWidgets.h"
#include "../Servers/MidiServer.h"

Units* Units::instance = nullptr;
const float Units::UNIT_REVEAL_SIZE_STEP = 4.f;
const float Units::UNIT_REVEAL_OPACITY_STEP = 8.f;

Units::Units() {
    masterVolume.format = MASTER_VOLUME_FORMAT;
    masterVolume.minFormat = "-inf";

    btnMuteAll.setColor(btnMuteAll.COLOR_MUTE);
    btnMuteAll.setSmall(true);
    btnSoloAll.setColor(btnSoloAll.COLOR_SOLO);
    btnSoloAll.setSmall(true);
    btnActiveAll.setColor(btnSoloAll.COLOR_ACTIVE);
    btnActiveAll.setSmall(true);

    btnMuteAll.addListener( this, &Units::btnMuteAllListener );
    btnSoloAll.addListener( this, &Units::btnSoloAllListener );
    btnActiveAll.addListener( this, &Units::btnActiveAllListener );
    ofAddListener( AudioEngine::getInstance()->soundCardInit, this, &Units::onSoundCardInit );
}

void Units::selectUnit(shared_ptr<Unit> unit)
{
    if ( unit != selectedUnit ) {
        if ( selectedUnit != nullptr ) {
            auto itSelectedUnit = std::find( currentUnits.begin(), currentUnits.end(), selectedUnit );
            if ( itSelectedUnit != currentUnits.end() ) {
                (*itSelectedUnit)->onUnselectedUnit();
            }
        }

        selectedUnit = unit;

        auto itSelectedUnit = std::find( currentUnits.begin(), currentUnits.end(), selectedUnit );
        int idxSelectedUnit = std::distance(currentUnits.begin(), itSelectedUnit);
        MidiServer::getInstance()->onSelectedUnit(idxSelectedUnit);
    }

    unit->onSelectedUnit();
}

void Units::setIndexesToUnits()
{
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        currentUnits[i]->setIndex(i);

        OscServer::getInstance()->mixerChangedVolume(i, currentUnits[i]->volume.get());
        OscServer::getInstance()->mixerChangedPan(i, currentUnits[i]->pan.get());
        OscServer::getInstance()->mixerChangedSolo(i, currentUnits[i]->isSolo());
        OscServer::getInstance()->mixerChangedMute(i, currentUnits[i]->isMuted());

        MidiServer::getInstance()->broadcastUberControlChange( &currentUnits[i]->volume );
        MidiServer::getInstance()->broadcastUberControlChange( &currentUnits[i]->pan );
        MidiServer::getInstance()->broadcastUberControlChange( &currentUnits[i]->uberSolo );
        MidiServer::getInstance()->broadcastUberControlChange( &currentUnits[i]->uberMute );
        MidiServer::getInstance()->broadcastUberControlChange( &currentUnits[i]->uberActive );
    }
}

void Units::loadUnit(shared_ptr<Unit> unit, const Json::Value &jsonUnit)
{
    unit->load(jsonUnit["params"]);
    unit->setCustomName(jsonUnit["customName"].asString());
    unit->effects.load(jsonUnit["effects"]);

    unit->volume.set( jsonUnit["volume"].asFloat() );
    unit->pan.set( jsonUnit["pan"].asFloat() );

    unit->setSolo( jsonUnit["solo"].asBool() );
    unit->setMute( jsonUnit["mute"].asBool() );

    if ( !jsonUnit["active"].isNull() ) {
        unit->setActive( jsonUnit["active"].asBool() );
    }
    
    unit->envMode = jsonUnit["envelopeMode"].asInt();
    unit->setEnvelopeMode(unit->envMode);
    unit->envEnable.set( jsonUnit["envelopeEnable"].asBool() );
    unit->envAttack.set( jsonUnit["envelopeAttack"].asFloat() );
    unit->envHold.set( jsonUnit["envelopeHold"].asFloat() );
    unit->envRelease.set( jsonUnit["envelopeRelease"].asFloat() );
    
    unit->samplePitch.set( jsonUnit["samplePitch"].asFloat() );
    unit->sampleStart.set( jsonUnit["sampleStart"].asFloat() );
    unit->sampleReverse.set( jsonUnit["sampleReverse"].asBool() );
    
    unit->modeSpread.set( jsonUnit["spreadMode"].asFloat() );
    unit->panSpread.set( jsonUnit["spreadPan"].asFloat() );

    //MIDIKeyboardPitches
    if ( jsonUnit["requestedPitches"] != Json::nullValue ) {
        if ( jsonUnit["requestedPitches"].isArray() ) {
            unit->requestedPitches.clear();
            for ( unsigned int i = 0 ; i < jsonUnit["requestedPitches"].size() ; i++ ) {
                unit->requestedPitches.push_back( jsonUnit["requestedPitches"].get(i, 0).asInt() );
            }
        }
    }

    // was session saved with the same device that is selected?
    if ( SessionManager::getInstance()->sessionAudioDeviceName ==
         AudioEngine::getInstance()->getSelectedDeviceName() &&
         jsonUnit["output"].asInt() != 0 ) {

        unit->selectedOutput = jsonUnit["output"].asInt();
    }

    unit->loadBehaviors( jsonUnit["behaviors"] );

    if ( jsonUnit["restrictToCluster"] != Json::nullValue ) {
        int restrictToCluster = jsonUnit["restrictToCluster"].asInt();
        if ( restrictToCluster != -1 ) {
            unit->restrictToCluster = restrictToCluster;
            unit->restrictedClusterBB = Sounds::getInstance()->getClusterBB(restrictToCluster);
        }
    }

    currentUnits.push_back(unit);

    processSoloMuteActive();
}

void Units::btnMuteAllListener(float &v)
{
    if (!processingSoloMute) {
        setMuteAllUnits(v);
    }
}

void Units::btnSoloAllListener(float &v)
{
    if (!processingSoloMute) {
        setSoloAllUnits(v);
    }
}

void Units::btnActiveAllListener(float &v)
{
    if (!processingSoloMute) {
        setActiveAllUnits(v);
    }
}

void Units::setMuteAllUnits(bool mute)
{
    for (auto &unit : currentUnits) {
        unit->setMute(mute);
    }
}

void Units::setSoloAllUnits(bool solo)
{
    for (auto &unit : currentUnits) {
        unit->setSolo(solo);
    }
}

void Units::setActiveAllUnits(bool active)
{
    for (auto &unit : currentUnits) {
        unit->setActive(active);
    }
}

void Units::updateUnitsToReveal()
{
    for ( auto &u : unitsToReveal ) {
        u.size += UNIT_REVEAL_SIZE_STEP;
        u.opacity -= UNIT_REVEAL_OPACITY_STEP;
    }

    auto end = std::remove_if( unitsToReveal.begin(),
                    unitsToReveal.end(),
                    [](auto &u)  {
                       return u.opacity <= 0;
                    });
    unitsToReveal.erase(end, unitsToReveal.end());
}

void Units::beforeDraw() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        for ( auto *behavior : currentUnits[i]->behaviors ) {
            behavior->beforeDraw();
        }

        currentUnits[i]->beforeDraw();
    }
}

bool Units::needToShowConfig()
{
    return selectedUnit != nullptr;
}
void Units::draw() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        for ( auto *behavior : currentUnits[i]->behaviors ) {
            behavior->draw();
        }
        currentUnits[i]->draw();
    }

    ofNoFill();
    for ( auto &u : unitsToReveal ) {
        ofSetColor( 232, 11, 85, u.opacity );
        
        float zoomScale = CamZoomAndPan::getInstance()->getScale();
        float circleResolution = int( ofClamp(( (u.size / 4.f ) * zoomScale * 1.5f) + 8.5f, 10.f, 100.f) );
        
        ofSetCircleResolution(circleResolution);
        
        ofDrawCircle( u.centroid.x, u.centroid.y, u.size );
    }
    updateUnitsToReveal();

//    drawSelectedUnitBB(); //For debugging BB
}

void Units::drawGui()
{
    if ( currentUnits.size() > 0 ) {
        float scrollHeight = MIN( (UNITS_UNIT_HEIGHT+5)*currentUnits.size(),
                                  ofGetHeight() * 0.78f);
        float unitsWindowWidth = UNITS_WINDOW_WIDTH;
        if ( scrollHeight < ofGetHeight() * 0.78f ) {
            unitsWindowWidth = UNITS_WINDOW_WIDTH * 0.95f;
        }

        ImGuiWindowFlags window_flags = 0;

        ImGui::BeginChild("unitsScroll",
                          ImVec2(unitsWindowWidth,scrollHeight),
                          false, window_flags);

        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            auto unit = currentUnits[i];
            bool selected = selectedUnit == unit;
            string unitMenuName = "unitMenu" + ofToString(i);

            if (selected) {
                ImGui::PushStyleColor(ImGuiCol_ChildBg,  UNITS_UNIT_BGCOLOR );
                ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(.13f, .49f, .32f, 1.f) );
            }

            ImGui::BeginChild(("##unit" + ofToString(i)).c_str(),
                              ImVec2(UNITS_WINDOW_WIDTH*0.94f, UNITS_UNIT_HEIGHT),
                              true);

            if (selected) {
                ImGui::PopStyleColor(2);
            }

            ///////////////
            // Unit name
            ///////////////
            if ( unit != isRenamingUnit ) {
                string name = unit->getUnitName();
                if ( !unit->getCustomName().empty() ) {
                    name = unit->getCustomName();
                }

                ImGui::SetNextItemWidth(100);
                if (selected)
                    ImGui::Text("%s", name.c_str());
                else
                    ImGui::TextDisabled("%s", name.c_str());

                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_NAME);

            } else {
                if (!ImGui::IsAnyItemActive())
                    ImGui::SetKeyboardFocusHere();

                ImGui::PushStyleVar( ImGuiStyleVar_FramePadding, ImVec2(0,0) );
                if ( ImGui::InputText("##renameUnit", renameUnitText, RENAME_UNIT_MAX_CHARS,
                                      ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_AutoSelectAll) ) {
                    unit->setCustomName( renameUnitText );
                    isRenamingUnit = nullptr;
                    strcpy(renameUnitText,"");

                }
                ImGui::PopStyleVar();

            }



            if ( ImGui::IsItemClicked(1) ) {
                ImGui::OpenPopup( unitMenuName.c_str() );
            }

            if (ImGui::BeginPopup(unitMenuName.c_str())) {
                bool removedUnit = false;

                if (ImGui::MenuItem("Rename Unit")) {
                    renameUnit(unit);
                    ImGui::CloseCurrentPopup();
                }

                ImGui::Separator();

                if (ImGui::MenuItem("Move up")) {
                    moveUnitUp(unit);
                    ImGui::CloseCurrentPopup();
                }

                if (ImGui::MenuItem("Move down")) {
                    moveUnitDown(unit);
                    ImGui::CloseCurrentPopup();
                }

                ImGui::Separator();

                ImGui::PushStyleColor( ImGuiCol_Text, ImVec4(1,0,0,1) );
                if (ImGui::MenuItem("Remove Unit")) {
                    removeUnit(unit);
                    removedUnit = true;
                    ImGui::CloseCurrentPopup();
                }
                ImGui::PopStyleColor();
                ImGui::EndPopup();

                if ( removedUnit ) {
                    ImGui::EndChild();
                    break;
                }
            }

            ///////////////
            // Mute and Solo
            ///////////////
            float volumeWidth = UNITS_WINDOW_WIDTH * 0.80f;

            ImGui::SameLine(volumeWidth * 0.75f);
            unit->uberActive.draw();

            ImGui::SameLine(volumeWidth * 0.91f);
            unit->uberMute.draw();

            ImGui::SameLine(volumeWidth * 1.03f);
            unit->uberSolo.draw();

            ImGui::BeginGroup();
                ImGui::PushItemWidth( volumeWidth );

                unit->volume.draw();

                if ( ImGui::IsItemClicked() ) {
                    selectUnit(unit);
                }

                //outputs
                ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(8.f,0.6f));
                ImGui::PushItemWidth( volumeWidth * 0.455f );
                if ( AudioEngine::getInstance()->isConfigured() && unit->selectedOutput > -1 ) {
                    ofxImGui::AddCombo( unit->selectedOutput, AudioEngine::getInstance()->getEnabledOutputs() );
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_SELECT_OUTPUT );
                } else {
                    ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.8f, .0f, .0f, 1.f));
                    ImGui::Text("No out!");
                    ImGui::PopStyleColor();
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_SELECT_OUTPUT_NO_OUT );
                }

                ImGui::PopStyleVar(1);

                //pan
                ImGui::SameLine( volumeWidth * 0.55f );
                ImGui::PushItemWidth( volumeWidth * 0.455f );

                unit->pan.draw();
            ImGui::EndGroup();

            ImGui::SameLine( volumeWidth + 11.f);
            ExtraWidgets::VUMeter( unit->vuRMSL.meter_output(),
                                   unit->vuPeakPreL.meter_output(),
                                   ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );
            ImGui::SameLine(0.f, 1.f);
            ExtraWidgets::VUMeter( unit->vuRMSR.meter_output(),
                                   unit->vuPeakPreR.meter_output(),
                                   ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );

            ImGui::EndChild();

            if ( ImGui::IsItemClicked() ) {
                selectUnit(unit);
            }

    //        ImGui::EndGroup();
        }

        if ( updateScroll ) {
            ImGui::SetScrollHereY( 1.0f );
            updateScroll = false;
        }

        ImGui::EndChild();
    } else {
        ImGui::BeginChild("unitsScrollDummy",
                          ImVec2(UNITS_WINDOW_WIDTH,20));

        ImGui::Text("Add a unit to start.");

        ImGui::EndChild();
    }

    // Add unit
    if ( ImGui::Button("+ Add") ) {
        ImGui::OpenPopup("addUnit");
    }
    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_ADD );

    if (ImGui::BeginPopup("addUnit")) {
        if (ImGui::MenuItem("Explorer Unit")) {
            addUnit( shared_ptr<Unit>(new ExplorerUnit()) );
            ImGui::CloseCurrentPopup();
        }

        if (ImGui::MenuItem("Sequence Unit")) {
            addUnit( shared_ptr<Unit>(new SequenceUnit()) );
            ImGui::CloseCurrentPopup();
        }

        if (ImGui::MenuItem("Particle Unit")) {
            addUnit( shared_ptr<Unit>(new ParticleUnit()) );
            ImGui::CloseCurrentPopup();
        }

		if (ImGui::MenuItem("Morph Unit")) {
			addUnit(shared_ptr<Unit>(new MorphUnit()));
			ImGui::CloseCurrentPopup();
		}

        if (ImGui::MenuItem("OSC Unit")) {
            addUnit( shared_ptr<Unit>(new OscUnit()) );
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }

    if ( !currentUnits.empty() ) {
        ImGui::SameLine(UNITS_WINDOW_WIDTH * 0.65f);
        btnActiveAll.draw();
        ImGui::SameLine(UNITS_WINDOW_WIDTH * 0.765f);
        btnMuteAll.draw();
        ImGui::SameLine(UNITS_WINDOW_WIDTH * 0.86f);
        btnSoloAll.draw();
    }
}

void Units::update() {
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i]->isActive() ) {
            for ( auto *behavior : currentUnits[i]->behaviors ) {
                behavior->update();
            }
            currentUnits[i]->update();
        }
    }
}

void Units::reset()
{
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        currentUnits[i]->die();
    }
    currentUnits.clear();

//    for(unsigned int i = 0; i < removedUnits.size(); i++) {
//        removedUnits[i]->die();
//    }
//    removedUnits.clear();
}

void Units::drawSelectedUnitSettings(){
    if ( selectedUnit == nullptr ) return;
    
    ImGui::PushStyleVar(ImGuiStyleVar_TabRounding, 0);

    if (ImGui::BeginTabBar("MyTabBar", ImGuiTabBarFlags_NoTooltip))
    {
        if (ImGui::BeginTabItem("Unit"))
        {
            ImGui::Dummy(ImVec2(0,4));

            selectedUnit->drawGui();
            drawBehaviors("Unit");
            drawSpread();
            drawRestrictToCluster();
            ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Sampler"))
        {
            ImGui::Dummy(ImVec2(0,4));

            drawSampleSettings();
            drawEnvelopeSettings();
            drawBehaviors("Sampler");
            ImGui::EndTabItem();
        }
        if ( selectedUnit->getUnitName() != "OSC Unit" ) {
            if (ImGui::BeginTabItem("OSC"))
            {
                ImGui::Dummy(ImVec2(0,4));

                drawBehaviors("OSC");
                ImGui::EndTabItem();
            }
        }
        if (ImGui::BeginTabItem("Effects"))
        {
            ImGui::Dummy(ImVec2(0,4));

            drawEffectsSettings();
            ImGui::EndTabItem();
        }
        if (ImGui::BeginTabItem("Keyboard"))
        {
            ImGui::Dummy(ImVec2(0,4));
            drawBehaviors("Keyboard");
            ImGui::EndTabItem();
        }
        ImGui::EndTabBar();
    }

    ImGui::PopStyleVar();
}

void Units::drawSelectedUnitBB()
{
    if ( selectedUnit == nullptr ) return;

    ofSetColor(255,0,0);
    ofDrawRectangle(selectedUnit->getBoundingBox());
}

void Units::drawRestrictToCluster()
{
    if ( !selectedUnit->restrictToClusterEnabled ) return;
    if ( ExtraWidgets::Header("Cluster", false) ) {
        ImGui::Text("Restrict to cluster:");
        ImGui::SameLine();

        if ( selectedUnit->restrictToCluster >= 0 ) {
            ImGui::Text( "%s", ofToString( selectedUnit->restrictToCluster ).c_str() );
            ImGui::SameLine();
            if ( ImGui::SmallButton("x") ) {
                selectedUnit->restrictToCluster = -1;
                selectedUnit->restrictedClusterBB = ofRectangle();
            }
        } else {
            if ( !selectedUnit->restrictToClusterSelecting ) {
                if ( ImGui::SmallButton("Select") ) {
                    selectedUnit->restrictToClusterSelecting = true;
                }
                if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_CLUSTER_RESTRICT);

            } else {
                ImGui::Text("...");
            }
        }

        ImGui::NewLine();
    }
}

void Units::drawSpread()
{
    if ( ExtraWidgets::Header("Spread", false) ) {
        selectedUnit->modeSpread.draw();
        selectedUnit->panSpread.draw();
        ImGui::NewLine();
    }
}

void Units::drawEffectsSettings()
{
    if ( selectedUnit == nullptr ) return;

//    if ( ExtraWidgets::Header("Effects") ) {
        if ( ImGui::Button("+ Add") ) {
            ImGui::OpenPopup("addEffect");
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_EFFECT_ADD);

        selectedUnit->effects.drawGui();

        if (ImGui::BeginPopup("addEffect")) {
            if (ImGui::MenuItem("VAFilter")) {
                addEffect( new VAFilterUI() );
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::MenuItem("BasiVerb")) {
                addEffect( new BasiVerbUI() );
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::MenuItem("Delay")) {
                addEffect( new DelayUI() );
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::MenuItem("DimensionChorus")) {
                addEffect( new DimensionChorusUI() );
                ImGui::CloseCurrentPopup();
            }
            if (ImGui::MenuItem("Saturator")) {
                addEffect( new SaturatorUI() );
                ImGui::CloseCurrentPopup();
            }

            ImGui::EndPopup();
        }
//    }
}

void Units::drawEnvelopeSettings()
{
    if ( selectedUnit == nullptr ) return;
    
    if ( ExtraWidgets::Header("Envelope", false) ) {
        
        bool shouldPop = false;

        if(!selectedUnit->envEnable) {
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.4f);
            shouldPop = true;
        }

        if ( ImGui::RadioButton("ms", &selectedUnit->envMode, 0) ) {
            selectedUnit->setEnvelopeMode(selectedUnit->envMode);
        }

        if (ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_ENVELOPE_MODE_TIME);
        
        ImGui::SameLine();
        
        if ( ImGui::RadioButton("%", &selectedUnit->envMode, 1) ) {
            selectedUnit->setEnvelopeMode(selectedUnit->envMode);
        }
        
        if (ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_ENVELOPE_MODE_PERCENTAGE);
        
        ImGui::SameLine(ImGui::GetContentRegionAvail().x - 15);

        selectedUnit->envEnable.draw();
        selectedUnit->envAttack.draw();
        selectedUnit->envHold.draw();
        selectedUnit->envRelease.draw();
        
        if(shouldPop) {
            ImGui::PopStyleVar();
        }

        ImGui::NewLine();
    }
}

void Units::drawSampleSettings()
{
    if ( selectedUnit == nullptr ) return;

    if ( ExtraWidgets::Header("Sample", false) ) {
        selectedUnit->samplePitch.draw();
        selectedUnit->sampleStart.draw();
        selectedUnit->sampleReverse.draw();

        ImGui::NewLine();
    }
    
}


void Units::drawBehaviors(string tab)
{
    if ( selectedUnit == nullptr ) return;

    if ( !selectedUnit->behaviors.empty() ) {
        for ( auto * b : selectedUnit->behaviors ) {
            if ( b->tab == tab ) {
                b->drawGui();
            }
        }
    }
}

void Units::drawMasterVolume()
{ 

//        ImGui::SameLine( volumeWidth + 11.f);
//        ExtraWidgets::VUMeter( unit->vuRMSL.meter_output(),
//                               unit->vuPeakPreL.meter_output(),
//                               ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );
//        ImGui::SameLine(0.f, 1.f);
//        ExtraWidgets::VUMeter( unit->vuRMSR.meter_output(),
//                               unit->vuPeakPreR.meter_output(),
//                               ImVec2(UNITS_WINDOW_WIDTH*0.02f,38.f) );

    if ( AudioEngine::getInstance()->isConfigured() ) {
        ImGui::PushItemWidth( 100.f );

        ImGui::PushStyleColor(ImGuiCol_FrameBg, ImVec4(.13f, .24f, .17f, 1.f));
        ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, ImVec4(.17f, .35f, .24f, 1.f));
        ImGui::PushStyleColor(ImGuiCol_FrameBgActive, ImVec4(.13f, .49f, .32f, 1.f));

        ImGui::PushStyleColor(ImGuiCol_SliderGrab, ImVec4(.0f, .6f, .38f, 1.f));
        ImGui::PushStyleColor(ImGuiCol_SliderGrabActive, ImVec4(.0f, .67f, .42f, 1.f));
        masterVolume.draw();
        ImGui::PopStyleColor(5);
    } else {
        ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.8f, .0f, .0f, 1.f));
//        ImGui::Dummy(ImVec2(1.f, 0.5f));
        ImGui::Text("Audio disabled");
        ImGui::PopStyleColor();
    }
}



Units *Units::getInstance()
{
    if(instance == nullptr){
        instance = new Units();
    }
    return instance;
}

void Units::initUnits() {
    if ( currentUnits.empty() ) {
        addUnit( shared_ptr<Unit>(new ExplorerUnit()) );
    }
    selectUnit(currentUnits[0]);
    recorder.patch();
}

void Units::addUnit(shared_ptr<Unit> newUnit)
{
    currentUnits.push_back(newUnit);
    selectUnit(newUnit);
    setIndexesToUnits();
    processSoloMuteActive();

    updateScroll = true;
}

void Units::renameUnit(shared_ptr<Unit> unit)
{
    isRenamingUnit = unit;
    if ( unit->getCustomName() == "" ) {
        strcpy(renameUnitText, unit->getUnitName().substr(0,RENAME_UNIT_MAX_CHARS).c_str());
    } else {
        strcpy(renameUnitText, unit->getCustomName().substr(0,RENAME_UNIT_MAX_CHARS).c_str());
    }
}

void Units::removeUnit(shared_ptr<Unit> unit)
{
    if ( unit != nullptr ) {
        bool wasSelected = false;
        std::vector<shared_ptr<Unit>>::iterator position =
                std::find(currentUnits.begin(), currentUnits.end(), unit);

        if (position != currentUnits.end()) {
            wasSelected = *position == selectedUnit;
//            (*position)->die();
            //instead of deleting unit for real, put it in another vector
            //this is much faster and can be used for ctrl-z in the future
            removedUnits.push_back( unit );
            currentUnits.erase(position);
            unit->remove();

            setIndexesToUnits();

            if ( wasSelected ) {
                if ( !currentUnits.empty() ) {
                    if ( position != currentUnits.begin() ) {
                        selectUnit(*(position-1));
                    } else {
                        selectUnit(currentUnits[0]);
                    }
                }
                else {
                    selectedUnit = nullptr;
                }
            }

            processSoloMuteActive();
        }
    }
}

void Units::moveUnitUp(shared_ptr<Unit> unit)
{
    if ( unit != nullptr ) {
        std::vector<shared_ptr<Unit>>::iterator position =
                std::find(currentUnits.begin(), currentUnits.end(), unit);

        if ( position != currentUnits.begin() ) {
            iter_swap( position, position - 1 );
            setIndexesToUnits();
        }
    }
}

void Units::moveUnitDown(shared_ptr<Unit> unit)
{
    if ( unit != nullptr ) {
        std::vector<shared_ptr<Unit>>::iterator position =
                std::find(currentUnits.begin(), currentUnits.end(), unit);


        if ( position != currentUnits.end() - 1 ) {
            iter_swap( position, position + 1 );
            setIndexesToUnits();
        }
    }
}

void Units::addEffect(EffectUI *newEffect)
{
    if ( selectedUnit == nullptr ) return;

    // patching and unpatching doesn't seem to prevent crash
//    selectedUnit->unpatch();
    selectedUnit->effects.addEffect(newEffect);
    //    selectedUnit->patch();
}

void Units::revealUnit(ofVec2f centroid)
{
    unitsToReveal.push_back( UnitReveal{centroid} );
}

UberSlider * Units::getUnitVolume(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->volume);
    }

    return nullptr;
}

UberSlider *Units::getUnitPan(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->pan);
    }

    return nullptr;
}

UberToggle *Units::getUnitMute(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->uberMute);
    }

    return nullptr;
}

UberToggle *Units::getUnitSolo(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->uberSolo);
    }

    return nullptr;
}

UberToggle *Units::getUnitActive(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->uberActive);
    }

    return nullptr;
}

UberSlider *Units::getUnitPitch(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return &(currentUnits[index]->samplePitch);
    }

    return nullptr;
}

void Units::setUnitVolume(unsigned int index, float v)
{
    if ( index < currentUnits.size() ) {
        currentUnits[index]->setVolume(v);
    } else {
        ofLog() << "OSC Error: Unit index does not exist";
    }
}

void Units::toggleMuteAll()
{
    if ( btnMuteAll == 1.f ) {
        btnMuteAll.set(0);
    } else {
        btnMuteAll.set(1.f);
    }
}

void Units::toggleSoloAll()
{
    if ( btnSoloAll == 1.f ) {
        btnSoloAll.set(0);
    } else {
        btnSoloAll.set(1.f);
    }
}

void Units::toggleActiveAll()
{
    if ( btnActiveAll == 1.f ) {
        btnActiveAll.set(0);
    } else {
        btnActiveAll.set(1.f);
    }
}

shared_ptr<Unit> Units::getUnit(unsigned int index)
{
    if ( index < currentUnits.size() ) {
        return currentUnits[index];
    }

    return nullptr;
}

void Units::muteSound(shared_ptr<Sound> s)
{
    for ( auto &u : currentUnits ) {
        u->muteSound(s);
    }
}

Json::Value Units::save()
{
    Json::Value root = Json::Value( Json::arrayValue );
    for(unsigned int i = 0; i < currentUnits.size(); i++) {
        Json::Value jsonUnit = Json::Value( Json::objectValue );
        jsonUnit["name"] = currentUnits[i]->getUnitName();
        jsonUnit["customName"] = currentUnits[i]->getCustomName();
        jsonUnit["volume"] = currentUnits[i]->volume.get();
        jsonUnit["mute"] = currentUnits[i]->mute;
        jsonUnit["solo"] = currentUnits[i]->solo;
        jsonUnit["active"] = currentUnits[i]->isActive();
        jsonUnit["output"] = currentUnits[i]->selectedOutput.get();
        jsonUnit["pan"] = currentUnits[i]->pan.get();
        jsonUnit["params"] = currentUnits[i]->save();
        jsonUnit["effects"] = currentUnits[i]->effects.save();
        jsonUnit["behaviors"] = currentUnits[i]->saveBehaviors();
        jsonUnit["restrictToCluster"] = currentUnits[i]->restrictToCluster;
        jsonUnit["envelopeMode"] = currentUnits[i]->envMode;
        jsonUnit["envelopeEnable"] = currentUnits[i]->envEnable.get();
        jsonUnit["envelopeAttack"] = currentUnits[i]->envAttack.get();
        jsonUnit["envelopeHold"] = currentUnits[i]->envHold.get();
        jsonUnit["envelopeRelease"] = currentUnits[i]->envRelease.get();
        jsonUnit["samplePitch"] = currentUnits[i]->samplePitch.get();
        jsonUnit["sampleStart"] = currentUnits[i]->sampleStart.get();
        jsonUnit["sampleReverse"] = currentUnits[i]->sampleReverse.get();
        jsonUnit["spreadMode"] = currentUnits[i]->modeSpread.get();
        jsonUnit["spreadPan"] = currentUnits[i]->panSpread.get();


        if ( !currentUnits[i]->MIDIKeyboardSaveNotes ) {
            jsonUnit["requestedPitches"] = Json::nullValue;
        } else {
           jsonUnit["requestedPitches"] = Json::arrayValue;
           for ( auto &p : currentUnits[i]->requestedPitches ) {
               jsonUnit["requestedPitches"].append(p);
           }
        }

        root.append(jsonUnit);
    }
    return root;
}

void Units::load(Json::Value jsonUnits)
{
    for ( int i = 0 ; i < jsonUnits.size() ; i++ ) {
        Json::Value jsonUnit = jsonUnits[i];

        if ( jsonUnit["name"].asString() == "Explorer Unit" ) {
            loadUnit(shared_ptr<Unit>(new ExplorerUnit()), jsonUnit);
        }
        if ( jsonUnit["name"].asString() == "Sequence Unit" ) {
            loadUnit(shared_ptr<Unit>(new SequenceUnit()), jsonUnit);
        }
        if ( jsonUnit["name"].asString() == "Particle Unit" ) {
            loadUnit(shared_ptr<Unit>(new ParticleUnit()), jsonUnit);
        }
		if (jsonUnit["name"].asString() == "Morph Unit") {
			loadUnit(shared_ptr<Unit>(new MorphUnit()), jsonUnit);
		}
        if ( jsonUnit["name"].asString() == "OSC Unit" ) {
            loadUnit(shared_ptr<Unit>(new OscUnit()), jsonUnit);
        }
    }
    setIndexesToUnits();
}

void Units::mousePressed(int x, int y, int button) {
    if (blockInput) return;
    if ( selectedUnit == nullptr ) return;

    //this happens only when a popup is opened but the mouse is not hovering it
    if (! (Gui::getInstance()->isMouseHoveringGUI() && !ImGui::IsAnyWindowHovered()) ) {
        if ( !selectedUnit->restrictToClusterSelecting ) {
            for ( auto *behavior : selectedUnit->behaviors ) {
                behavior->mousePressed( ofVec2f(x,y), button );
            }
            selectedUnit->mousePressed( ofVec2f(x,y), button );
        } else {
            int selectedCluster = Sounds::getInstance()->getHoveredClusterID();

            if ( selectedCluster >= 0 ) {
                selectedUnit->restrictToCluster = selectedCluster;
                selectedUnit->restrictedClusterBB = Sounds::getInstance()->getClusterBB(selectedCluster);
            }

            selectedUnit->restrictToClusterSelecting = false;
        }
    }
}

void Units::keyPressed(ofKeyEventArgs & e)
{
    if (blockInput) return;
    if ( selectedUnit == nullptr ) return;

    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->keyPressed(e);
    }
    selectedUnit->keyPressed(e);
}

void Units::mouseDragged(int x, int y, int button){
    if (blockInput) return;
    if ( selectedUnit == nullptr ) return;

    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseDragged(ofVec2f(x,y), button);
    }
    selectedUnit->mouseDragged(ofVec2f(x,y), button);
}

void Units::mouseReleased(int x, int y , int button) {
    if (blockInput) return;
    if ( selectedUnit == nullptr ) return;

    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseReleased(ofVec2f(x,y));
    }
    selectedUnit->mouseReleased(ofVec2f(x,y));
}

void Units::mouseMoved(int x, int y) {
    if (blockInput) return;
    if ( selectedUnit == nullptr ) return;

    for ( auto *behavior : selectedUnit->behaviors ) {
        behavior->mouseMoved(ofVec2f(x,y));
    }
    selectedUnit->mouseMoved(ofVec2f(x,y));
}

void Units::mouseMovedOverGUI(int x, int y)
{
    if ( selectedUnit == nullptr ) return;

    selectedUnit->mouseMovedOverGUI(ofVec2f(x,y));
}

void Units::midiMessage(MIDIMessage m) {
    //Si está en midiLearn sólo le manda mensaje al modo activo
    if(MidiServer::getInstance()->getMIDILearn()){
        if ( selectedUnit == nullptr ) return;
        selectedUnit->midiMessage(m);
    }else{
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            currentUnits[i]->midiMessage(m);
        }
    }
}

shared_ptr<Unit> Units::getSelectedUnit() {
    return selectedUnit;
}

string Units::getSelectedUnitName() {
    return selectedUnit->getUnitName();
}

void Units::selectUnitAtPosition(ofVec2f point)
{
    const float MIN_DISTANCE_THRESHOLD = 2000;

    if ( currentUnits.size() ) {
        float minDistance = -1;
        shared_ptr<Unit> minDistanceUnit = nullptr;

        unsigned int i = 0;
        for ( i = 0 ; i < currentUnits.size() ; i++ ) {
            shared_ptr<Unit> unit = currentUnits[i];
            float unitDistance = unit->getDistanceFrom(point);
            if ( unitDistance != -1.f ) {
                minDistance = unitDistance;
                minDistanceUnit = unit;
                break;
            }
        }

        if ( minDistance > - 1 ) {
            for ( unsigned int j = i + 1 ; j < currentUnits.size() ; j++ ) {
                shared_ptr<Unit> unit = currentUnits[j];
                float unitDistance = unit->getDistanceFrom(point);
                if ( unitDistance != -1.f && unitDistance < minDistance ) {
                    minDistance = unitDistance;
                    minDistanceUnit = unit;
                }
            }

            if ( minDistance <= MIN_DISTANCE_THRESHOLD ) {
                selectUnit(minDistanceUnit);
            }
        }
    }
}

void Units::selectUnitAtIndex(int index)
{
    if ( index < currentUnits.size() ) {
        selectUnit( currentUnits[index] );
    }
}

void Units::setBlockInput(bool b)
{
    blockInput = b;
}

void Units::processSoloMuteActive() {
    bool isAnyUnitSoloed = false;
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i]->isSolo() ) {
            isAnyUnitSoloed = true;
            break;
        }
    }
    if ( isAnyUnitSoloed ) {
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            auto unit = currentUnits[i];
            unit->changeMute(!unit->isSolo(), true);
        }
    } else {
        for(unsigned int i = 0; i < currentUnits.size(); i++) {
            auto unit = currentUnits[i];
            unit->changeMute(false, true);
        }
    }

    bool areAllUnitsMuted = true;
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( !currentUnits[i]->isMuted() ) {
            areAllUnitsMuted = false;
            break;
        }
    }

    bool areAllUnitsActive = true;
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( !currentUnits[i]->isActive() ) {
            areAllUnitsActive = false;
            break;
        }
    }

    processingSoloMute = true;
    btnMuteAll.set( areAllUnitsMuted ? 1.f : 0.f);
    btnSoloAll.set( isAnyUnitSoloed ? 1.f : 0.f);
    btnActiveAll.set( areAllUnitsActive? 1.f : 0.f);
    processingSoloMute = false;

}

void Units::repatchOutputAll()
{
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        currentUnits[i]->repatchOutput();
    }
}

void Units::onSoundCardInit(int &deviceID)
{
    masterGain.disconnectOut();
    unsigned int countOutputs = AudioEngine::getInstance()->getAudioOutNumChannels(deviceID);
    for ( unsigned int i = 0 ; i < countOutputs ; i++ ) {
        masterGain.ch(i) >> AudioEngine::getInstance()->getAudioOut(i);
    }

    //Soundcard has changed for one with less outputs. Unit has a selected output that exists no more.
    for (unsigned int i = 0; i < currentUnits.size(); i++) {
        if ( currentUnits[i]->selectedOutput >= countOutputs/2 ) {
            currentUnits[i]->selectedOutput = 0;
        }
    }
    
    repatchOutputAll();

    recorder.reset();
}
