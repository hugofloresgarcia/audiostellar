#pragma once

#include "ofMain.h"
#include "ofxImGui.h"

#include "./Unit.h"
#include "../Utils/Utils.h"
#include "../Utils/Tooltip.h"
#include "../GUI/ExtraWidgets.h"
#include "../GUI/UberToggle.h"

#include "../Effects/VAFilterUI.h"
#include "../Effects/BasiVerbUI.h"
#include "../Effects/DimensionChorusUI.h"
#include "../Effects/SaturatorUI.h"
#include "../Effects/DelayUI.h"

#include "../GUI/UI.h"

#include "../Sound/Recorder.h"

#define UNITS_WINDOW_WIDTH 200.0f
#define UNITS_UNIT_HEIGHT 71.0f
#define UNITS_UNIT_BGCOLOR ImVec4(0.0f, 0.0f, 0.04f, 1.f)

#define MASTER_VOLUME_MIN -48.f
#define MASTER_VOLUME_MAX 12.f
#define MASTER_VOLUME_FORMAT "%.2fdB"

class MidiServer;
class Unit;

class Units {
private:
    static Units* instance;
    Units();

    shared_ptr<Unit> selectedUnit = nullptr;

    bool showOutputMenu = false;
    bool updateScroll = false; //move units scroll to bottom when needed

    void selectUnit( shared_ptr<Unit> unit );
    void setIndexesToUnits();
    void loadUnit(shared_ptr<Unit> unit, const Json::Value & jsonUnit);

    void btnMuteAllListener(float &v);
    void btnSoloAllListener(float &v);
    void btnActiveAllListener(float &v);
    void setMuteAllUnits(bool mute);
    void setSoloAllUnits(bool solo);
    void setActiveAllUnits(bool solo);
    bool processingSoloMute = false;

    shared_ptr<Unit> isRenamingUnit = nullptr;
# define RENAME_UNIT_MAX_CHARS 16
    char renameUnitText[RENAME_UNIT_MAX_CHARS] = "";

    struct UnitReveal {
        ofVec2f centroid;
        float size = 0;
        int opacity = 255;
    };
    static const float UNIT_REVEAL_SIZE_STEP;
    static const float UNIT_REVEAL_OPACITY_STEP;
    vector<UnitReveal> unitsToReveal;
    void updateUnitsToReveal();

    bool blockInput = false;

public:
    static Units* getInstance();

    vector< shared_ptr<Unit> > currentUnits;
    vector< shared_ptr<Unit> > removedUnits; //removing the units for real takes too much time

    pdsp::ParameterGain masterGain;
    UberSlider masterVolume { masterGain.set("OUT##MasterVolume", 0.f, MASTER_VOLUME_MIN, MASTER_VOLUME_MAX),
                        &Tooltip::UNITS_MASTER_VOLUME };
    UberToggle btnMuteAll {"M##uteAll", &Tooltip::UNITS_MUTE};
    UberToggle btnSoloAll {"S##oloAll", &Tooltip::UNITS_SOLO};
    UberToggle btnActiveAll {"A##ctiveAll", &Tooltip::UNITS_ACTIVE};

    void initUnits();
    void addUnit( shared_ptr<Unit> unit );
    void renameUnit( shared_ptr<Unit> unit );
    void removeUnit( shared_ptr<Unit> unit );
    void moveUnitUp( shared_ptr<Unit> unit );
    void moveUnitDown( shared_ptr<Unit> unit );
    void revealUnit( ofVec2f centroid );
    void addEffect( EffectUI *newEffect );

    UberSlider * getUnitVolume(unsigned int index);
    UberSlider * getUnitPan(unsigned int index);
    UberToggle * getUnitMute(unsigned int index);
    UberToggle * getUnitSolo(unsigned int index);
    UberToggle * getUnitActive(unsigned int index);
    UberSlider * getUnitPitch(unsigned int index);
    void setUnitVolume(unsigned int index, float v);
    void toggleMuteAll();
    void toggleSoloAll();
    void toggleActiveAll();
    shared_ptr<Unit> getUnit(unsigned int index);

    void muteSound( shared_ptr<Sound> s );

    Json::Value save();
    void load(Json::Value jsonUnits);

    bool needToShowConfig();
    void beforeDraw();
    void draw();
    void drawGui();
    void drawSelectedUnitSettings();
    void drawSelectedUnitBB(); //debugging proposes
    void drawRestrictToCluster();
    void drawSpread();
    void drawEffectsSettings();
    void drawEnvelopeSettings();
    void drawSampleSettings();
    void drawBehaviors(string tab);
    void drawMasterVolume();
    void update();
    void reset();

    void mousePressed(int x, int y, int button);
    void keyPressed(ofKeyEventArgs & e);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y , int button);
    void mouseMoved(int x, int y);
    void mouseMovedOverGUI(int x, int y);

    void midiMessage(MIDIMessage m);
    shared_ptr<Unit> getSelectedUnit();
    string getSelectedUnitName();
    void selectUnitAtPosition(ofVec2f point);
    void selectUnitAtIndex(int index);
    void setBlockInput(bool b); //This is not being used at the moment
    
    void processSoloMuteActive();
    void repatchOutputAll();

    void onSoundCardInit( int & deviceID );
    
    Recorder recorder;
    
    static int getRenameUnitMaxChars();
};
