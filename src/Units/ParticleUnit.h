#pragma once

#include "ofMain.h"
#include "Unit.h"
#include "../Sound/Sound.h"
#include "../Sound/Sounds.h"
#include "../Sound/Voices.h"
#include "Particles/Particle.h"

#include "../GUI/UberSlider.h"
#include "../GUI/UberButton.h"
#include "../GUI/UberToggle.h"

#include "Behaviors/UnitBehaviorMIDIKeyboard.h"

class ParticleUnit: virtual public Unit {

private:
    #define PARTICLE_COLOR_R 232
    #define PARTICLE_COLOR_G 11
    #define PARTICLE_COLOR_B 85
    #define PARTICLE_COLOR_INACTIVE_R 232
    #define PARTICLE_COLOR_INACTIVE_G 220
    #define PARTICLE_COLOR_INACTIVE_B 224

    //Versión global del param de cada partícula
    UberSlider lifespan {"Lifespan", 1.f, 0.f, 1.f, &Tooltip::UNIT_PARTICLE_LIFESPAN};
    UberSlider velocityX {"X", 0.f, -1.f, 1.f, &Tooltip::UNIT_PARTICLE_VELOCITY_X};
    UberSlider velocityY {"Y", 0.f, -1.f, 1.f, &Tooltip::UNIT_PARTICLE_VELOCITY_Y};
    UberSlider spreadAmount {"Spread", 0.5f, 0.f, 1.f, &Tooltip::UNIT_PARTICLE_SPREAD};
//    UberSlider volumeSpread {"Vol. Spread", 0.f, 0.f, 1.f, &Tooltip::NONE};
    //
    UberSlider emitRate{"Emit rate", 100.f, 1.f, 200.f, &Tooltip::UNIT_PARTICLE_EMIT_RATE};
    uint64_t millisLastEmit = 0;
    UberButton emit {"Emit particle", &Tooltip::UNIT_PARTICLE_EMIT_PARTICLE};
    bool emitButtonPressed = false;
    float emitButtonVelocity = 1; //MIDI velocity
    UberSlider emitAreaSize {"Area Size", 0.f, 0.f, 400.f, &Tooltip::UNIT_PARTICLE_EMIT_AREA_SIZE};
    UberSlider density { "Density", 10.0f, 1.0f, 30.0f, &Tooltip::UNIT_PARTICLE_DENSITY};
    UberSlider speed { "Speed", 4.0f, 0.01f,7.0f, &Tooltip::UNIT_PARTICLE_SPEED};

    UberSlider probability { "Probability", 1.f, 0.f, 1.f, &Tooltip::UNIT_PARTICLE_PROBABILITY };

    ofVec2f centroid = {-1,-1};
    bool emitRandomPosition = false;
    bool lockParticleEmitter = false;

    bool hasPressedAndNotDragged = false;

    UberToggle respawn { "Respawn", &Tooltip::UNIT_PARTICLE_REBIRTH};
    //
    
    #define MODEL_SWARM "Swarm"
    #define MODEL_EXPLOSION "Explosion"
    static const int NUM_MODELS = 2;
    string swarmModels[NUM_MODELS] = { MODEL_SWARM, MODEL_EXPLOSION };
    int atModel;

    class SwarmParticle: public Particle {
    public:
        SwarmParticle(ofVec2f pos, string model, float volume = 1.0f);
        /*
         * Setup is added to be able to run something once after setting
         * parameters of the instance from outside (thus, avoiding passing
         * lots of params to the constructor)
         */
        void setup();
        void customUpdate();
        void setRandomSize();

        const float DEFAULT_SIZE = 1.0;
        const static int SIZE_SCALER = 4;

        string model;
        
        //velocity is common to every model
        ofVec2f velocity;

        //swarm mode
        float spreadAmount;
        float volumeSpread = 0.0;

        //explosion mode
        ofVec2f acceleration;
        float speed = 0.5f;

        float tempSize;
        float sizeAnimStep = 0;

        float volume = 1.0f;
    };

    forward_list<SwarmParticle> particles;

    UnitBehaviorMIDIKeyboard * behaviorKeyboard;
    void onMIDIKeyboard( vector<int> &m );

    void calculateBoundingBox();
public:
    ParticleUnit();

    string getUnitName() { return "Particle Unit"; }
    void update();
    void updateParticlesSize();
    void draw();
    void drawCentroid();
    void drawArea();
    void drawGui();
    void reset();
    float getDistanceFrom(ofVec2f point);

    void emitButtonListener(float &v);

    void mousePressed(ofVec2f p, int button);
    void mouseDragged(ofVec2f p, int button);
    void mouseReleased(ofVec2f p);
    void onSelectedUnit();

    Json::Value save();
    void load(Json::Value jsonData);

    void castParticle();
    void castParticle(ofVec2f pos);
    void castParticleWithRate();
    void castParticleWithRate(ofVec2f pos);
};
