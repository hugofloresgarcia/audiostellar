#include "MorphUnit.h"
#include "../GUI/ExtraWidgets.h"
#include "Behaviors/UnitBehaviorTrajectory.h"
#include "Behaviors/UnitBehaviorPolyphony.h"
#include "Behaviors/UnitBehaviorOSC.h"
#include "Behaviors/UnitBehaviorDragToPlay.h"

MorphUnit::MorphUnit() {
	sounds = Sounds::getInstance();

	setVoicesType(Voices::VoicesType::Polyphonic);
	setPolyphonyChoke(false);

	trajectory = new UnitBehaviorTrajectory(this);
	trajectory->onlyEvent = true;
	ofAddListener(trajectory->onTrajectory, this, &MorphUnit::onTrajectory);

	behaviors.push_back(new UnitBehaviorPolyphony(this));
	behaviors.push_back(trajectory);
	behaviors.push_back(new UnitBehaviorOSC(this));

    behaviorKeyboard = new UnitBehaviorMIDIKeyboard(this);
    ofAddListener( behaviorKeyboard->keyboardEvent, this, &MorphUnit::onMIDIKeyboard );
    behaviors.push_back( behaviorKeyboard );

	parameters.push_back(&threshold);
	parameters.push_back(&mapShaper);
    parameters.push_back(&play);
    parameters.push_back(&probability);
    parameters.push_back(&centroidOffsetX);
    parameters.push_back(&centroidOffsetY);

    play.style = UberButton::Big;
    play.addListener( this, &MorphUnit::onPlayButton);

    centroidOffsetX.addListener( this, &MorphUnit::centroidOffsetListener);
    centroidOffsetY.addListener( this, &MorphUnit::centroidOffsetListener);
}

MorphUnit::~MorphUnit() {
	ofRemoveListener(trajectory->onTrajectory, this, &MorphUnit::onTrajectory);
}

void MorphUnit::onMIDIKeyboard(vector<int> &m)
{
//    if ( !trajectory->isPlaying() && !m.empty()) {
    if ( !m.empty()) {
        if ( centroid != ofVec2f(-1,-1) ) {
            morph(centroidPlusOffset);
        }
    }
}

void MorphUnit::update() {
}

void MorphUnit::draw() {
    if ( selected && mousePosition != ofVec2f(-1,-1) ) {
        if ( trajectory->isPlaying() ) {
            drawCircle(mousePosition.x, mousePosition.y);
        } else {
            if ( !mouseMovingOverGUI && !Gui::getInstance()->isAnyModifierPressed() && !Gui::getInstance()->isMouseCursorHidden() ) {
                if ( ofGetMousePressed(0) ) {
                    ofSetLineWidth(activeCircleWidth);
                } else {
                    ofSetLineWidth(1);
                }

                drawCircle(mousePosition.x, mousePosition.y);

                ofSetLineWidth(1);
            }

            if ( centroid != ofVec2f(-1,-1) ) {
                drawCircle(centroidPlusOffset.x, centroidPlusOffset.y);
            }
        }
    } else if ( !selected ){
        if ( trajectory->isPlaying() ) {
            drawCircle(mousePosition.x, mousePosition.y, true);
        } else {
            if ( centroid != ofVec2f(-1,-1) ) {
                drawCircle(centroidPlusOffset.x, centroidPlusOffset.y, true);
            }
        }
    }
}


void MorphUnit::drawGui() {
    if ( centroid != ofVec2f(-1,-1) ) {
        play.draw();
        ImGui::Dummy( ImVec2(255,1) );
        ImGui::SameLine();
        if ( ImGui::SmallButton("Remove") ) {
            centroid = {-1,-1};
        }

        ImGui::NewLine();

        ImGui::PushItemWidth(97);
            centroidOffsetX.draw();
            ImGui::SameLine();
            centroidOffsetY.draw();
        ImGui::PopItemWidth();

        ImGui::Checkbox("Lock centroid", &lockMorphCentroid);
        if (ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_MORPH_LOCK);

//        ImGui::SameLine();



        ImGui::NewLine();
    } else {
        play.draw(true);
        ImGui::NewLine();
    }

    threshold.draw();
    mapShaper.draw();
	probability.draw();
    ImGui::NewLine();

//    if ( centroid != ofVec2f(-1,-1) ) {
//        ImGui::TextDisabled("Centroid settings");

//    }

	ImGui::NewLine();
}

void MorphUnit::drawCircle(float x, float y, bool inactive) {
    ofColor c = colorActivo;
    float d = sqrt( threshold );
    
	if (inactive) c = colorInactivo;

    int maxOpacity = 200;
    if ( inactive ) {
        maxOpacity = 80;
    }

    ofSetColor(c, maxOpacity/5);
    ofNoFill();
    ofDrawCircle(x, y, d);

    static const int step = 3;

    for (int i = step; i < d-1; i+=step) {
        int diameter = (d - i);
        float opacity = (ofDistSquared(
            centroid.x + diameter, centroid.y,
            centroid.x, centroid.y)) / threshold.get();
		
        opacity = 1 - opacity;
        opacity = logMap(opacity, 0, 1, 0, maxOpacity, mapShaper);
        ofSetColor(c, opacity);
        ofDrawCircle(x, y, diameter);
    }

    ofFill();
}

void MorphUnit::centroidOffsetListener(float &v)
{
    centroidPlusOffset = centroid;
    centroidPlusOffset.x += centroidOffsetX * offsetStrength;
    centroidPlusOffset.y -= centroidOffsetY * offsetStrength;

    if ( play.get() == 1.f ) {
        morph(centroidPlusOffset);
    }
}

void MorphUnit::reset() {

}

float MorphUnit::getDistanceFrom(ofVec2f point)
{
    float minDistance = -1;
    if ( trajectory->isPlaying() ) {
        ofVec2f currentPosition = trajectory->getCurrentPosition();
        minDistance = ofDistSquared( point.x,
                                     point.y,
                                     currentPosition.x,
                                     currentPosition.y );
    } else {
        minDistance = ofDistSquared(point.x,
                                    point.y,
                                    centroid.x + centroidOffsetX,
                                    centroid.y + centroidOffsetY);
    }
    return minDistance;
}


void MorphUnit::onTrajectory(ofVec2f &position) {

	vector<shared_ptr<Sound>> neighbors = sounds->getNeighbors(position, threshold.get());

    if (position != lastPlayedTrajectoryPosition) {
        if ( !neighbors.empty() &&
             ofRandom(1.f) <= trajectory->probability.get()) {
            morph(position);
        }

        mousePosition = position;
        lastPlayedTrajectoryPosition = position;
    }
}


void MorphUnit::onPlayButton(float &v)
{
    if ( centroid != ofVec2f(-1,-1) && v > 0 ) {
        openMIDIKeyboardEnvelope();
        morph(centroidPlusOffset, v);
    }
}

void MorphUnit::mousePressed(ofVec2f p, int button) {
    if (!Gui::getInstance()->isAnyModifierPressed() &&
        (button == 0 || button == -1) && !trajectory->isPlaying() ) {
        // When using MIDI keyboard the adsr will close
        // and clicking won't be heard otherwise
        openMIDIKeyboardEnvelope();

        morph(p);
        hasPressedAndNotDragged = true;
	}
}

void MorphUnit::mouseDragged(ofVec2f p, int button) {
    if ( !Gui::getInstance()->isAnyModifierPressed() &&
         (button == 0 || button == -1 ) && !trajectory->isPlaying()) {
        morph(p);
        hasPressedAndNotDragged = false;
        mousePosition = p;
    }
}

void MorphUnit::mouseMoved(ofVec2f p) {
    if ( !trajectory->isPlaying() ) {
        mousePosition = p;
    }

    mouseMovingOverGUI = false;
}

void MorphUnit::mouseMovedOverGUI(ofVec2f p)
{
    mouseMovingOverGUI = true;
}

void MorphUnit::mouseReleased(ofVec2f p) {
    if ( hasPressedAndNotDragged ) {
		if (!lockMorphCentroid) {
            centroid = p;

            centroidOffsetX.set(0.f);
            centroidOffsetY.set(0.f);
			hasPressedAndNotDragged = false;
		}
    }
}

void MorphUnit::onSelectedUnit()
{
    Unit::onSelectedUnit();

    if ( trajectory->isPlaying() ) {
        Units::getInstance()->revealUnit( trajectory->getCurrentPosition() );
    } else if ( centroid != ofVec2f(-1, -1) ) {
        Units::getInstance()->revealUnit( centroidPlusOffset );
    }
}

void MorphUnit::morph(ofVec2f p, float volume) {
    if ( !isActive() ) return;

    neighbors = sounds->getNeighbors(p, threshold.get());

    if ( !neighbors.empty() ) {
        for (int i = 0; i < neighbors.size(); i++) {
            if (ofRandom(1.f) <= probability.get()) {
                float vol = (ofDistSquared(p.x, p.y, neighbors[i]->position.x, neighbors[i]->position.y)) / threshold.get();
                vol = 1 - vol;
                float volMap = logMap(vol, 0, 1, 0, volume, mapShaper);
                playSound(neighbors.at(i), volMap);
                playingSounds.insert(neighbors[i]);
            }
        }

        ofRectangle bb;
        float width = sqrt(threshold) * 2;
        bb.setFromCenter( p, width, width );
        setBoundingBox(bb);
    } else {
        for ( auto &s : playingSounds ) {
            muteSound(s);
        }
        playingSounds.clear();
    }

    set<shared_ptr<Sound>>::iterator it = playingSounds.begin();
    while ( it != playingSounds.end() ) {
        shared_ptr<Sound> s = *it;
        auto itExist = std::find( neighbors.begin(), neighbors.end(), s );
        if ( itExist == neighbors.end() ) {
            muteSound(s);
            playingSounds.erase(it++); //with ++ it is safe to erase (it++ returns it )
        } else {
            ++it;
        }
    }
}

Json::Value MorphUnit::save() {
	Json::Value params = Json::Value(Json::objectValue);

	params["threshold"] = threshold.get();
    params["probability"] = probability.get();
    params["currentCentroid_x"] = centroid.x;
    params["currentCentroid_y"] = centroid.y;
    params["centroidOffsetX"] = centroidOffsetX.get();
    params["centroidOffsetY"] = centroidOffsetY.get();
	params["lockMorphCentroid"] = lockMorphCentroid;
    params["shaper"] = mapShaper.get();


	return params;
}

void MorphUnit::load(Json::Value jsonData) {
	threshold.set(jsonData["threshold"].asFloat());
    centroid.x = jsonData["currentCentroid_x"].asFloat();
    centroid.y = jsonData["currentCentroid_y"].asFloat();
    centroidOffsetX.set( jsonData["centroidOffsetX"].asFloat() );
    centroidOffsetY.set( jsonData["centroidOffsetY"].asFloat() );
	lockMorphCentroid = jsonData["lockMorphCentroid"].asBool();
    mapShaper.set( jsonData["shaper"].asFloat() );
    probability.set( jsonData["probability"].asFloat() );

    if ( centroid != ofVec2f(-1,-1) ) {
        mousePosition = centroid;
    }
}


float MorphUnit::logMap(float in, float inMin, float inMax, float outMin, float outMax, float shaper) {
	// taken from: https://forum.openframeworks.cc/t/non-linear-ofmap/13508/2 
	float pct = ofMap(in, inMin, inMax, 0, 1, true);
	pct = powf(pct, shaper);
	float out = ofMap(pct, 0, 1, outMin, outMax, true);
	return out;
}
