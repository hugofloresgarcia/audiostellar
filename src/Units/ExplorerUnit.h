#pragma once

#include "ofMain.h"
#include "Unit.h"
#include "../Sound/Sounds.h"
#include "../Servers/MidiServer.h"
#include "../Sound/Voices.h"
#include "Behaviors/UnitBehaviorTrajectory.h"
#include "Behaviors/UnitBehaviorDragToPlay.h"

class ExplorerUnit: virtual public Unit {

private:
    Sounds *sounds;
    
    //nota midi -> idSonido
    std::unordered_map<int, int> midiMappings;
    shared_ptr<Sound> midiTriggeredSound = nullptr;
    shared_ptr<Sound> lastPlayedSound = nullptr;
    shared_ptr<Sound> lastPlayedTrajectory = nullptr;

    UnitBehaviorTrajectory * trajectory = nullptr;
public:
    ExplorerUnit();
    ~ExplorerUnit();

    string getUnitName() { return "Explorer Unit"; }

    void reset();
    void mousePressed(ofVec2f p, int button);
    void mouseDragged(ofVec2f p, int button);
    float getDistanceFrom(ofVec2f point);
    void onSelectedUnit();

    void onTrajectory( ofVec2f &position );
    
    Json::Value save();
    void load( Json::Value jsonData );
    
    string hoveredSoundPath;
    
    void drawGui();
};
