#pragma once

#define DEFAULT_TEMPO 120
#define QTY_BARS 7
#define QTY_STEPS 16

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxJSON.h"

#include "Unit.h"
#include "../Sound/Sounds.h"
#include "../Utils/Utils.h"
#include "../Servers/MidiServer.h"
#include "../Servers/OscServer.h"
#include "../Servers/MasterClock.h"
#include "../GUI/UI.h"
#include "../GUI/UberSlider.h"
#include "../GUI/UberCombo.h"

class MidiServer;


class SequenceUnit : virtual public Unit {
private:
    const vector<string> strBars {"1/4 bar", "1/2 bar","1 bar", "2 bars", "3 bars", "4 bars", "8 bars"};
    static int intBars[QTY_BARS];

    UberCombo cboSelectedBars {"Bars", strBars, &Tooltip::UNIT_SEQUENCE_BARS, 2};
    UberSlider probPlay {"Probability",1.f,0,1.f, &Tooltip::UNIT_SEQUENCE_PROBABILITY};
    UberSlider probGrow {"##Grow",0.f,0,1.f, &Tooltip::UNIT_SEQUENCE_PROB_GROW};
    UberSlider probShrink {"##Shrink",0.f,0,1.f, &Tooltip::UNIT_SEQUENCE_PROB_SHRINK};
    UberSlider probMutate {"##Mutate",0.f,0,1.f, &Tooltip::UNIT_SEQUENCE_PROB_MUTATE};
    UberToggle probPreserveClusters = {"Preserve clusters", &Tooltip::UNIT_SEQUENCE_USE_CLUSTERS, 1.f};
    UberToggle probPreventDeath = {"Prevent death", &Tooltip::UNIT_SEQUENCE_PREVENT_DEATH, 0.f};
    UberButton btnGrow {"Grow", &Tooltip::UNIT_SEQUENCE_BTN_GROW};
    UberButton btnShrink {"Shrink", &Tooltip::UNIT_SEQUENCE_BTN_SHRINK};
    UberButton btnMutate {"Mutate", &Tooltip::UNIT_SEQUENCE_BTN_MUTATE};
    void onBtnGrow(float & a);
    void onBtnShrink(float & a);
    void onBtnMutate(float & a);
    bool forceGrow = false;
    bool forceShrink = false;
    bool forceMutate = false;
    void grow();
    void shrink();
    void mutate();

    map<int, int> selectedClusters;

    UberSlider offset {"Offset",0,0,15.f, &Tooltip::UNIT_SEQUENCE_OFFSET};
    UberButton btnClearSequence {"Clear sequence", &Tooltip::UNIT_SEQUENCE_CLEAR};

    shared_ptr<Sound> chooseNewSound( int cluster = -2);

    vector<shared_ptr<Sound>> secuencia;
    shared_ptr<Sound> * secuenciaEnTiempo = nullptr;
    void onNeedToProcess(float & a);
    void onClearSequence(float & a);

    ofPolyline polyLine;
    bool isProcessing = false;
    bool playing = true;

    int currentStep = -1;

    void calculateBoundingBox();
public:
    SequenceUnit();
//    ~SequenceUnit();

    string getUnitName() { return "Sequence Unit"; }

    Json::Value save();
    void load( Json::Value jsonData );

    void mousePressed(ofVec2f p, int button);
    void mouseDragged(ofVec2f p, int button);

    void reset();
    void die();
    void beforeDraw();
    void draw();
    void drawGui();
    void midiMessage( MIDIMessage m );

    float getDistanceFrom(ofVec2f point);

    void onTempo(int & frame);

    void processSequence();
    void processSequenceOffset();
    void clearSequence();
    int getCantSteps();

    void toggleSound( shared_ptr<Sound> sound, bool doProcessSequence = true, bool modifySequence = true );

    void onSelectedUnit();
    void onUnselectedUnit();
    void setSelectAllSounds(bool selected = true);
    
    float stepDuration = 0.f;
    vector<float> stepsPercentage;
    float currentStepTime = 0.f;
    
    void calculateStepsPercentage();

};
