#pragma once

#include "ofMain.h"
#include "ofxImGui.h"

#include "UnitBehavior.h"

class UnitBehaviorOSC: public UnitBehavior{
private:
    bool enabled = false;

    void oscPlay(float volume);

    const string OSC_PLAY = "/play/xy";
    const string OSC_PLAY_X = "/play/x";
    const string OSC_PLAY_Y = "/play/y";

    const int cantAddresses = 4;
    vector<char *> addresses;

    vector<ofVec2f> oscPositions;
    vector<int> oscCursorOpacities;
//    ofVec2f oscPosition {0,0};
//    int oscCursorOpacity = 0;

    bool restrictingToSector = false;
    ofRectangle restrictToSector;
    ofVec2f firstPointDragged = ofVec2f(0,0);
    bool drawSector = true;

public:
    UnitBehaviorOSC(Unit * parentUnit);
    ~UnitBehaviorOSC();

    const string getName() { return "UnitBehaviorOSC"; }

    void onOSCMessage(ofxOscMessage &m);

    Json::Value save();
    void load( Json::Value jsonData );

    void update();
    void draw();
    void drawGui();

    void mouseDragged(ofVec2f p, int button);
    void mouseReleased(ofVec2f p);

    void drawCursors();
    void drawRestrictingToSector();
    void drawRestrictToSector();
};
