#include "UnitBehaviorPolyphony.h"
#include "ofxImGui.h"
#include "../../GUI/ExtraWidgets.h"

UnitBehaviorPolyphony::UnitBehaviorPolyphony(Unit *parentUnit) :
    UnitBehavior (parentUnit)
{
    currentPolyphony = parent->getVoicesType();
    numVoices = parent->getVoicesAmount();
    choke = parent->getPolyphonyChoke();
    tab = "Sampler";
}

void UnitBehaviorPolyphony::drawGui()
{
    if ( ExtraWidgets::Header("Polyphony", false) ) {
        if ( ImGui::RadioButton( "Monophonic", &currentPolyphony, 0) ) {
            parent->setVoicesType( Voices::VoicesType::Monophonic );
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_POLYPHONY_MONO );
        if ( ImGui::RadioButton( "Polyphonic", &currentPolyphony, 1) ) {
            parent->setVoicesType( Voices::VoicesType::Polyphonic );
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_POLYPHONY_POLYPHONIC );
        if ( ImGui::RadioButton( "Full Polyphonic", &currentPolyphony, 2) ) {
            parent->setVoicesType( Voices::VoicesType::FullPolyphonic );
        }
        if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_POLYPHONY_FULLPOLYPHONIC );

        if ( currentPolyphony >= 0 && currentPolyphony <= 1 ) {
            ImGui::NewLine();
            if ( ImGui::Checkbox("Choke", &choke) ) {
                parent->setPolyphonyChoke(choke);
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_POLYPHONY_CHOKE);
        }
        ImGui::NewLine();

        ImGui::SetNextItemWidth(80);
        ImGui::InputInt("# Voices", &numVoices);
        numVoices = ofClamp(numVoices, 1, 128);
        ImGui::SameLine();
        if ( ExtraWidgets::Button("Set##voices", ExtraWidgets::GREEN, numVoices != parent->getVoicesAmount()) ) {
            parent->setVoicesAmount(numVoices);
        }

        ImGui::NewLine();
    }
}

Json::Value UnitBehaviorPolyphony::save()
{
    Json::Value polyphonyConfig = Json::Value( Json::objectValue );
    polyphonyConfig["polyphony"] = currentPolyphony;
    polyphonyConfig["choke"] = choke;
    polyphonyConfig["numVoices"] = numVoices;
    return polyphonyConfig;
}

void UnitBehaviorPolyphony::load(Json::Value jsonData)
{
    if ( jsonData["polyphony"] != Json::nullValue ) {
        currentPolyphony = jsonData["polyphony"].asInt();
        parent->setVoicesType( (Voices::VoicesType)currentPolyphony );
    }
    if ( jsonData["choke"] != Json::nullValue ) {
        choke = jsonData["choke"].asBool();
        parent->setPolyphonyChoke(choke);
    }
    if ( jsonData["numVoices"] != Json::nullValue ) {
        numVoices = jsonData["numVoices"].asInt();
        parent->setVoicesAmount(numVoices);
    }
}
