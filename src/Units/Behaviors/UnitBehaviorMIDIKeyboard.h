#pragma once

#include "ofMain.h"
#include "UnitBehavior.h"

class UnitBehaviorMIDIKeyboard : public UnitBehavior
{
public:
    UnitBehaviorMIDIKeyboard(Unit * parentUnit);
    ~UnitBehaviorMIDIKeyboard();
    const string getName() { return "UnitBehaviorMIDIKeyboard"; }

    ofEvent<vector<int>> keyboardEvent;

    Json::Value save();
    void load( Json::Value jsonData );

    void onMIDIMessage(MIDIKeyboardNotes &m);

    void drawGui();
private:
    float lastTriggered = -1;

    UberToggle envEnable { " ##envEnableUnitBehaviorMIDIKeyboard", &Tooltip::UNIT_BEHAVIOR_MIDI_KEYBOARD_ENV_ENABLE };
    UberSlider envAttack { "Attack##UnitBehaviorMIDIKeyboard", 0.f, 0.f, 2.5f, &Tooltip::NONE };
    UberSlider envDecay { "Decay##UnitBehaviorMIDIKeyboard", 0.f, 0.f, 2.5f, &Tooltip::NONE };
    UberSlider envSustain { "Sustain##UnitBehaviorMIDIKeyboard", 1.f, 0.f, 1.f, &Tooltip::NONE };
    UberSlider envRelease { "Release##UnitBehaviorMIDIKeyboard", 0.f, 0.f, 2.5f, &Tooltip::NONE };

    void enableListener(float &v);
    void attackListener(float &v);
    void decayListener(float &v);
    void sustainListener(float &v);
    void releaseListener(float &v);

    void MIDIChannelChangedListener(float &v);

    // chkEmitParticles will be -1 except for Particle Units
    UberToggle chkEmitParticles { "Emit particles##EmitParticlesUnitBehaviorMIDIKeyboard", &Tooltip::UNIT_BEHAVIOR_MIDI_KEYBOARD_EMIT_PARTICLES, -1.f };

    UberCombo cboMIDIChannel {"MIDI Channel", {
        "None",
        "All",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16"}, &Tooltip::NONE};

    UberCombo cboRootNote {"Root note", {
        "C",
        "C#",
        "D",
        "D#",
        "E",
        "F",
        "F#",
        "G",
        "G#",
        "A",
        "A#",
        "B"
        }, &Tooltip::NONE};
};
