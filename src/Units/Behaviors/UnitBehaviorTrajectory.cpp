
#include "UnitBehaviorTrajectory.h"
#include "ofxImGui.h"
#include "../../GUI/ExtraWidgets.h"

UnitBehaviorTrajectory::UnitBehaviorTrajectory(Unit * parentUnit) :
    UnitBehavior (parentUnit)
{
    speed.format = "x %.2f";
    setSpeed(speed.get());

    parameters.push_back( &speed );
    parameters.push_back( &probability );

    speed.addListener(this, &UnitBehaviorTrajectory::speedListener);
}

UnitBehaviorTrajectory::~UnitBehaviorTrajectory()
{

}

void UnitBehaviorTrajectory::die()
{
    clearStroke();
    waitForThread();
}

void UnitBehaviorTrajectory::onTimer() {
    if (recording) {
        recordStroke();
    }
    else {
        if (parent->isActive() && !positions.empty()) {
            playStroke();
        }
    }
}

void UnitBehaviorTrajectory::speedListener(float &v)
{
    setSpeed( speed.get() );
}

void UnitBehaviorTrajectory::calculateBoundingBox()
{
    if ( !positions.empty() ) {
        ofRectangle boundingBox( positions[0].x, positions[0].y, 1, 1);
        for ( auto &p : positions ) {
            boundingBox.growToInclude(p);
        }
        parent->setBoundingBox( boundingBox );
    }
}

void UnitBehaviorTrajectory::recordStroke() {
    positions.push_back(currentPosition);
}

void UnitBehaviorTrajectory::playStroke() {
    // I think this is not thread safe :(
    ofVec2f position = positions[n];

    if ( !onlyEvent ) {
        if ( ofRandom(1.f) <= probability.get() ) {
            parent->mouseDragged(position, 0);
        }
    }
    onTrajectory.notify(this, position);

    n++;
    if (!positions.empty()) {
        n %= positions.size();
    }
}

void UnitBehaviorTrajectory::mouseReleased(ofVec2f position) {
    // Calculate bounding box
    if ( recording == true && !positions.empty() ) {
        calculateBoundingBox();
    }
    recording = false;
}

void UnitBehaviorTrajectory::mouseDragged(ofVec2f position, int button) {

    if ( !enabled ) return;

    if ( button == 0 ) {
        if ( !recording && positions.empty() ) {
            positions.clear();
            speed.set(1);
            setSpeed(speed.get());
            recording = true;
            n = 0;
        }

        currentPosition = position;
    }
}

void UnitBehaviorTrajectory::update() {
    // Is this really needed on update ???
    // What if this happens when user first start recording ??
    if ( Units::getInstance()->getSelectedUnit().get() == (Unit*)this->parent ) {
        if (!timerStarted) {
            startTimer();
            timerStarted = true;
        }
    }
    else {
        if (positions.empty()) {
            if (timerStarted) {
                stopTimer();
                timerStarted = false;
            }
        }
    }
}

void UnitBehaviorTrajectory::draw() {
    if (positions.size() != 0) {
        if (!recording) {

            bool selected = Units::getInstance()->getSelectedUnit().get() == (Unit*)this->parent;
            ofVec2f position = positions[n];

            if (selected) {
                ofFill();
                ofSetColor(232,11,85,255);
                ofDrawCircle(position.x, position.y, 1.5);
            } else {
                ofFill();
                ofSetColor(255,255,255);
                ofDrawCircle(position.x, position.y, 1.5);
            }
        }
    }
}

void UnitBehaviorTrajectory::drawGui() {
    if ( ExtraWidgets::Header("Trajectory", false) ) {
        if ( !enabled ) {
            ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(.17f, .35f, .24f, 1.f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(.13f, .49f, .32f, 1.f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(.0f, .67f, .42f, 1.f));

            if ( ImGui::Button("Create trajectory", ImVec2(-1,30)) ) {
                enabled = true;
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_TRAJECTORY_CREATE );

            ImGui::PopStyleColor(3);
        } else {
            if ( !recording && positions.empty() ) {
                ImGui::Dummy(ImVec2(1,4));
                ExtraWidgets::TextCentered("Awaiting coordinates...");
                if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_TRAJECTORY_AWAITING);
            } else if (recording) {
                ImGui::Dummy(ImVec2(1,4));
                ExtraWidgets::TextCentered("Recording coordinates...");
            } else {
                speed.draw();
                probability.draw();

                ImGui::NewLine();

                if ( positions.size() > 0 ) {
                    if ( ImGui::Button("Clear trajectory", ImVec2(-1,25)) ) {
                        clearStroke();
                        enabled = false;
                    }
                }
            }
        }

        ImGui::NewLine();
    }
}

bool UnitBehaviorTrajectory::isPlaying()
{
    return positions.size() > 0 && !recording;
}

ofVec2f UnitBehaviorTrajectory::getCurrentPosition()
{
    ofVec2f currentPosition = {-1,-1};
    if ( isPlaying() ) {
        currentPosition = positions[n];
    }
    return currentPosition;
}

void UnitBehaviorTrajectory::clearStroke() {
    positions.clear();
    speed.set(1);
    parent->setBoundingBox( ofRectangle() );
}


Json::Value UnitBehaviorTrajectory::save() {

    Json::Value root = Json::Value( Json::objectValue );
    Json::Value json_positions = Json::Value( Json::arrayValue );

    for (int i = 0; i < positions.size(); i++) {

        Json::Value position = Json::Value( Json::arrayValue );
        position.append( positions[i].x );
        position.append( positions[i].y );

        json_positions.append(position);

    }

    root["positions"] = json_positions;
    root["speed"] = speed.get();
    root["probability"] = probability.get();
    return root;

}

void UnitBehaviorTrajectory::load(Json::Value jsonData) {

    positions.clear();

    Json::Value json_positions = jsonData["positions"];

    if ( json_positions != Json::nullValue ) {
        for (int i = 0; i < json_positions.size(); i++) {
            Json::Value position = json_positions[i];
            ofVec2f p;
            p.x = position[0].asFloat();
            p.y = position[1].asFloat();

            positions.push_back(p);
        }

        if(positions.size() != 0) {
            calculateBoundingBox();

            enabled = true;
            startTimer();
            timerStarted = true;
            playStroke();
        }
    }

    Json::Value json_speed = jsonData["speed"];
    if ( json_speed != Json::nullValue ) {
        speed.set(json_speed.asFloat());
        setSpeed(speed.get());
    }

    if ( jsonData["probability"] != Json::nullValue ) {
        probability.set( jsonData["probability"].asFloat() );
    }
}

