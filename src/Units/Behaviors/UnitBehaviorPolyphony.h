#pragma once

#include "ofMain.h"
#include "UnitBehavior.h"

class UnitBehaviorPolyphony : public UnitBehavior
{
public:
    UnitBehaviorPolyphony(Unit * parentUnit);

    void drawGui();
private:
    int currentPolyphony = 0;
    vector<string> strPolyphony = { "Monophonic", "Polyphonic", "Full Polyphonic" };
    bool choke = true;

    int numVoices = 0;

    Json::Value save();
    void load( Json::Value jsonData );
};
