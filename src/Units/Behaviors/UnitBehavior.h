#pragma once

#include "../Unit.h"

class Unit;

class UnitBehavior {
protected:
    Unit * parent;

    ////////////////////
    // Push your UberControls here
    // It will take care of defining unique IDs
    // NOT DOING THIS WILL BREAK OSC/MIDI MAPPING
    ////////////////////
    vector<UberControl *> parameters;
public:
    UnitBehavior(Unit * parentUnit) {
        setParent(parentUnit);
    }
    virtual ~UnitBehavior() {}
    virtual void die() {}

    void setParent(Unit * parentUnit) {
        parent = parentUnit;
    }
    void setIndexesToParameters(string indexPrefix) {
        for ( auto &p : parameters ) {
            p->setName( p->originalName + "##" + indexPrefix + getName() );
        }
    }

    string tab = "Unit";

    virtual const string getName() { return ""; };

    virtual void beforeDraw() {}
    virtual void draw() {}
    virtual void drawGui(){}
    virtual void update() {}

    virtual Json::Value save() { return Json::nullValue; }
    virtual void load( Json::Value jsonData ) {}

    virtual void mousePressed(ofVec2f p, int button) {}
    virtual void mouseDragged(ofVec2f p, int button) {}
    virtual void mouseReleased(ofVec2f p){}
    virtual void mouseMoved(ofVec2f p){}
    virtual void keyPressed(ofKeyEventArgs & e) {}
};
