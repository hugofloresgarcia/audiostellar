#include "UnitBehaviorOSC.h"
#include "../../GUI/CamZoomAndPan.h"
#include "../Units.h"

UnitBehaviorOSC::UnitBehaviorOSC(Unit * parentUnit)
    : UnitBehavior(parentUnit)
{
    ofAddListener( OscServer::getInstance()->oscEvent, this, &UnitBehaviorOSC::onOSCMessage );
    tab = "OSC";

//    addresses.push_back( new char[64] );
    for ( unsigned int i = 0 ; i < cantAddresses ; i++ ) {
//        char * address = new char[64];
        addresses.push_back( new char[64] );
        strncpy(addresses[i], "", 64);
        oscPositions.push_back( ofVec2f(0,0) );
        oscCursorOpacities.push_back( 0 );
    }

    strncpy(addresses[0], "myUnit", 64);
}

UnitBehaviorOSC::~UnitBehaviorOSC()
{
    ofRemoveListener( OscServer::getInstance()->oscEvent, this, &UnitBehaviorOSC::onOSCMessage );
}

void UnitBehaviorOSC::onOSCMessage(ofxOscMessage &m)
{
    if ( !enabled || !parent->isActive() ) return;

    for ( unsigned int i = 0 ; i < cantAddresses ; i++ ) {
        char * prefix = addresses[i];
        string oscPlay;
        string oscPlayX;
        string oscPlayY;

        if ( !strncmp(prefix, "", 64) ) {
            continue;
        } else {
            string p = string(prefix);

            oscPlay = "/";
            oscPlay.append(p);
            oscPlay.append(OSC_PLAY);

            oscPlayX = "/";
            oscPlayX.append(p);
            oscPlayX.append(OSC_PLAY_X);

            oscPlayY = "/";
            oscPlayY.append(p);
            oscPlayY.append(OSC_PLAY_Y);
        }

        if(m.getAddress() != oscPlay &&
           m.getAddress() != oscPlayX &&
           m.getAddress() != oscPlayY ) {
            continue;
        }

        //volume not implemented yet
    //    float volume = 1.0f;
        ofRectangle bb;

        if ( restrictToSector.isZero() ) {
            bb = parent->getRestrictedClusterBB();
        } else {
            bb = restrictToSector;
        }

        if(m.getAddress() == oscPlay) {
            if ( m.getNumArgs() >= 2 ) {
                float x = m.getArgAsFloat(0);
                float y = m.getArgAsFloat(1);

                if ( bb.isZero() ) {
                    oscPositions[i] = ofVec2f(x * Sounds::getInstance()->getInitialWindowWidth(),
                                          y * Sounds::getInstance()->getInitialWindowHeight());
                } else {
                    oscPositions[i] = ofVec2f(ofMap(x,0,1,bb.getX(),bb.getX()+bb.getWidth()),
                                          ofMap(y,0,1,bb.getY(),bb.getY()+bb.getHeight()));
                }

    //            if (m.getNumArgs() >= 3 ) {
    //                volume = m.getArgAsFloat(2);
    //            }
            } else {
                ofLog() << "No arguments for OSC: " << m.getAddress();
            }
        } else if ( m.getAddress() == oscPlayX ) {
            if ( m.getNumArgs() >= 1 ) {
                float x = m.getArgAsFloat(0);

                if ( bb.isZero() ) {
                    x *= Sounds::getInstance()->getInitialWindowWidth();
                } else {
                    x = ofMap(x,0,1,bb.getX(),bb.getX()+bb.getWidth());
                }

                oscPositions[i] = ofVec2f(x, oscPositions[i].y);

    //            if (m.getNumArgs() >= 2) {
    //                volume = m.getArgAsFloat(1);
    //            }
            } else {
                ofLog() << "No arguments for OSC: " << m.getAddress();
            }
        } else if ( m.getAddress() == oscPlayY ) {
            if ( m.getNumArgs() >= 1 ) {
                float y = m.getArgAsFloat(0);

                if ( bb.isZero() ) {
                    y *= Sounds::getInstance()->getInitialWindowHeight();
                } else {
                    y = ofMap(y,0,1,bb.getY(),bb.getY()+bb.getHeight());
                }

                oscPositions[i] = ofVec2f(oscPositions[i].x, y);

    //            if (m.getNumArgs() >= 2) {
    //                volume = m.getArgAsFloat(1);
    //            }
            } else {
                ofLog() << "No arguments for OSC: " << m.getAddress();
            }
        }

        parent->mouseDragged(oscPositions[i], -1);
        for ( UnitBehavior * behavior : parent->behaviors ) {
            behavior->mouseDragged(oscPositions[i], -1);
        }
        oscCursorOpacities[i] = 255;
        return;
    }

}

Json::Value UnitBehaviorOSC::save()
{
    Json::Value oscConfig = Json::Value( Json::objectValue );
    oscConfig["enabled"] = enabled;
    for ( unsigned int i = 0 ; i < cantAddresses ; i++ ) {
        oscConfig["prefix_" + ofToString(i)] = addresses[i];
    }

    oscConfig["restrictToSector_x1"] = restrictToSector.getTopLeft().x;
    oscConfig["restrictToSector_y1"] = restrictToSector.getTopLeft().y;
    oscConfig["restrictToSector_x2"] = restrictToSector.getBottomRight().x;
    oscConfig["restrictToSector_y2"] = restrictToSector.getBottomRight().y;

    oscConfig["drawSector"] = drawSector;

    return oscConfig;
}

void UnitBehaviorOSC::load(Json::Value jsonData)
{
    if ( jsonData["enabled"] != Json::nullValue ) {
        enabled = jsonData["enabled"].asBool();
    }
    //Retrocompatibility
//    if ( jsonData["prefix"] != Json::nullValue ) {
//        strncpy(addresses[0], jsonData["prefix"].asCString(), 64);
//    }
    for ( unsigned int i = 0 ; i < cantAddresses ; i++ ) {
        string key = "prefix_" + ofToString(i);
        if ( jsonData[key] != Json::nullValue ) {
            strncpy(addresses[i], jsonData[key].asCString(), 64);
        }
    }

    if ( jsonData["restrictToSector_x1"] != Json::nullValue ) {
        float x1 = jsonData["restrictToSector_x1"].asFloat();
        float y1 = jsonData["restrictToSector_y1"].asFloat();
        float x2 = jsonData["restrictToSector_x2"].asFloat();
        float y2 = jsonData["restrictToSector_y2"].asFloat();

        restrictToSector = ofRectangle(
            ofVec2f(x1,y1),
            ofVec2f(x2,y2)
        );
    }

    if ( jsonData["drawSector"] != Json::nullValue ) {
        drawSector = jsonData["drawSector"].asBool();
    }

}

void UnitBehaviorOSC::update()
{
    for ( int &o : oscCursorOpacities ) {
        if ( o > 0 ) o -= 2;
    }
}

void UnitBehaviorOSC::draw()
{
    drawCursors();
    drawRestrictingToSector();
    drawRestrictToSector();
}

void UnitBehaviorOSC::drawGui()
{
    ImGui::Checkbox("Listen /play/xy OSC messages", &enabled);
    if(ImGui::IsItemHovered()) Tooltip::setTooltip( Tooltip::UNIT_BEHAVIOR_OSC_LISTEN);

    if ( enabled ) {
        ImGui::NewLine();

        for ( unsigned int i = 0 ; i < cantAddresses ; i++ ) {
            string inputLabel = "OSC Address " + ofToString(i+1) + "##" + ofToString(parent->getIndex());
            ImGui::InputText(inputLabel.c_str(), addresses[i], 64);

            if ( !strncmp(addresses[i], "", 64) )
                continue;

            string p = "Listening: /";
            p.append(string(addresses[i]));
            p.append(string("/play/xy"));
            ImGui::Text(p.c_str());
            ImGui::NewLine();
        }

        ImGui::NewLine();

        if ( restrictToSector.isZero() ) {
            if ( !restrictingToSector ) {
                if ( ImGui::Button("Restrict to sector") ) {
                    restrictingToSector = true;
                }
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_BEHAVIOR_OSC_RESTRICT_TO_SECTOR);
            } else {
                ImGui::Text("Awaiting coordinates...");
            }
        } else {
            if ( ImGui::Button("Free sector") ) {
                restrictToSector = ofRectangle();
            }
            ImGui::Checkbox("Show restricted sector", &drawSector);
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::UNIT_BEHAVIOR_OSC_RESTRICT_TO_SECTOR_SHOW);
        }
    }

    ImGui::NewLine();
}

void UnitBehaviorOSC::mouseDragged(ofVec2f p, int button)
{
    if ( restrictingToSector && button == 0 && firstPointDragged == ofVec2f(0,0) ) {
        firstPointDragged = p;
        return;
    }
}

void UnitBehaviorOSC::mouseReleased(ofVec2f p)
{
    if ( restrictingToSector && firstPointDragged != ofVec2f(0,0) ) {
        restrictToSector = ofRectangle( firstPointDragged, p );
        restrictingToSector = false;
        firstPointDragged = ofVec2f(0,0);
    }
}

void UnitBehaviorOSC::drawCursors()
{
    for ( unsigned int i = 0 ; i < cantAddresses ; i++ ) {
        int oscCursorOpacity = oscCursorOpacities[i];
        ofVec2f oscPosition = oscPositions[i];

        if ( oscCursorOpacity < 1 ) continue;

        ofNoFill();
        ofSetColor(255,255,255, oscCursorOpacity);
        ofDrawLine((oscPosition.x), (oscPosition.y - 4.5f), (oscPosition.x), (oscPosition.y + 4.5f));
        ofDrawLine((oscPosition.x - 4.5f), (oscPosition.y), (oscPosition.x + 4.5f), (oscPosition.y));
    }
}

void UnitBehaviorOSC::drawRestrictingToSector()
{
    if ( restrictingToSector && firstPointDragged != ofVec2f(0,0) ) {
        ofVec2f mouseCoordinates = CamZoomAndPan::getInstance()->screenToWorld( ofVec2f(ofGetMouseX(), ofGetMouseY()) );
        ofRectangle tmpRect(firstPointDragged, mouseCoordinates);
        ofNoFill();
        ofSetColor(55,55,55);
        ofDrawRectangle( tmpRect );
        ofSetColor(255);
    }
}

void UnitBehaviorOSC::drawRestrictToSector()
{
    if ( drawSector && parent->isSelected() && !restrictToSector.isZero() ) {
        ofNoFill();
        ofSetColor(0,100,0);
        ofDrawRectangle( restrictToSector );
        ofSetColor(255);
    }
}
