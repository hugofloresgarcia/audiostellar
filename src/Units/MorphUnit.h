#pragma once

#include "ofMain.h"
#include "Unit.h"
#include "../Sound/Sound.h"
#include "../Sound/Sounds.h"
#include "../Sound/Voices.h"
#include "./Behaviors/UnitBehaviorDragToPlay.h"
#include "./Behaviors/UnitBehaviorTrajectory.h"
#include "Behaviors/UnitBehaviorMIDIKeyboard.h"
#include "../GUI/UberSlider.h"
#include "../GUI/UberButton.h"

class MorphUnit : virtual public Unit {

private:
    const ofColor colorActivo = {232,11,85,255};
    const ofColor colorInactivo = {232,220,224,255};
    const int activeCircleWidth = 3;

	Sounds *sounds;
	UnitBehaviorTrajectory * trajectory = nullptr;
    UnitBehaviorMIDIKeyboard * behaviorKeyboard;
    void onMIDIKeyboard( vector<int> &m );
	shared_ptr<Sound> lastPlayedSound = nullptr;
    ofVec2f lastPlayedTrajectoryPosition;
	vector<shared_ptr<Sound>> neighbors;
    set<shared_ptr<Sound>> playingSounds;
	std::unordered_map<int, int> midiMappings;
    ofVec2f mousePosition = { -1, -1 };
    ofVec2f centroid = {-1,-1};
    ofVec2f centroidPlusOffset = {-1,-1};
    float offsetStrength = 100.f;

    UberSlider threshold{ "Threshold", 1000.0f, 1.0f, 8000.0f, &Tooltip::UNIT_MORPH_THRESHOLD };
    UberSlider mapShaper{ "Shaper", 1, 0, 3, &Tooltip::UNIT_MORPH_SHAPER };
	UberSlider probability{ "Probability", 1.0, 0, 1, &Tooltip::UNIT_MORPH_PROBABILITY};

    UberSlider centroidOffsetX { "X##centroid offset", 0.f, -1.f, 1, &Tooltip::NONE};
    UberSlider centroidOffsetY { "Y offset##centroid offset", 0.f, -1.f, 1, &Tooltip::NONE};
    void centroidOffsetListener(float &v);

    UberButton play {"Play", &Tooltip::UNIT_MORPH_PLAY};
    bool emitButtonPressed = false;
    float emitButtonVelocity = 1; //MIDI velocity

    bool hasPressedAndNotDragged = false;
    bool mouseMovingOverGUI = false;
	bool lockMorphCentroid = false;

public:
	MorphUnit();
	~MorphUnit();

	string getUnitName() { return "Morph Unit"; }
	void update();
	void draw();
	void drawGui();
	void reset();
    float getDistanceFrom(ofVec2f point);
    void drawCircle(float x, float y, bool inactive = false);

	void onTrajectory(ofVec2f &position);
    void onPlayButton(float &v);

	void mousePressed(ofVec2f p, int button);
	void mouseDragged(ofVec2f p, int button);
	void mouseMoved(ofVec2f p);
    void mouseMovedOverGUI(ofVec2f p);
	void mouseReleased(ofVec2f p);
    void onSelectedUnit();

    void morph(ofVec2f p, float volume = 1.f);
	float logMap(float in, float inMin, float inMax, float outMin, float outMax, float shaper);

	void shaperListener(float &shape);
	void probabilityListener(float &prob);

	Json::Value save();
	void load(Json::Value jsonData);
};
