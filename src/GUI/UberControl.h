#pragma once

#include "ofMain.h"
#include "../Utils/Tooltip.h"
/**
 * @brief The UberControl class
 *
 * This class is basically the same as an ofParameter<float> but stores
 * originalName for easier unique id
 * knows how to learn OSC and MIDI
 * knows how to draw itself
 */
class UberControl : public ofParameter<float>
{
protected:
    static UberControl * selectedControl;
    void checkUnknownMappings();
    void addUberFeatures( bool activated, const float &tmpRef );

    const Tooltip::tooltip * tooltip = nullptr;

    float defaultValue = 0.0f;
    bool defaultValueSet = false;

    vector<string> useMIDIKeyboardChannels = {
            "None",
            "All",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16"
    };

    int MIDIKeyboardChannel = -2; //-2 means that it haven't requested yet
public:
    UberControl( const std::string& name, const float& v, const float& min, const float& max, const Tooltip::tooltip * tooltip );
    UberControl( ofParameter<float>& parameter, const Tooltip::tooltip *tooltip );
    UberControl();
    ~UberControl();

    static ofEvent<UberControl> uberControlIsDead;

    string originalName = "";

    bool hasOSCMapping = false;
    bool hasMIDIMapping = false;
    bool usesMIDIKeyboard = false;


    //this could be pure virtual but
    //abstract classes cannot be instanceted
    virtual void draw() { }

    void setName( const string& name );
    string getName() const;
    virtual UberControl & set( float v );
    virtual const float & get() const;
    float getMin() const;
    float getMax() const;
    void setDefaultValue( float v );
    float getDefaultValue();
    void setMin (float v);
    void setMax (float v);
    void setTooltip (const Tooltip::tooltip * tooltip);
    bool getToggleValue();

    void removeAllMappings();
    
    enum Palette {
        BLUE_DEFAULT = 0,
        GREEN = 1,
        YELLOW = 2,
        DISABLED = 3
    };

    vector<vector<ImVec4>> colors = {
        // Blue (default)
        vector<ImVec4> {

        },
        // Green
        vector<ImVec4> {
            ImVec4(.17f, .35f, .24f, 1.f),
            ImVec4(.13f, .49f, .32f, 1.f),
            ImVec4(.0f, .67f, .42f, 1.f)
        },
        // Yellow
        vector<ImVec4> {
            ImVec4(0.2f,0.2f,0.2f,1),
            ImVec4(0.4f,0.4f,0.4f,1),
            ImVec4(207.f/255, 214.f/255, 0, 1.f)
        },
        // Disabled
        vector<ImVec4> {
            ImVec4(0.3f,0.3f,0.3f,0.7f),
            ImVec4(0.3f,0.3f,0.3f,0.7f),
            ImVec4(0.3f,0.3f,0.3f,0.7f),

            ImVec4(1.f, 1.0f, 1.f, 0.7f)
        },
    };
    Palette currentPalette = BLUE_DEFAULT;
    void setPalette(Palette p);

    static void clickOutsidePopup();

    template<class ListenerClass, typename ListenerMethod>
    void addListener( ListenerClass * listener, ListenerMethod method, int prio=OF_EVENT_ORDER_AFTER_APP ) {
        ofParameter<float>::addListener(listener, method, prio);
    }

    ofParameter<float> * getParameter();

    int getMIDIKeyboardChannel();
    void setMIDIKeyboardChannel(int newMIDIKeyboardChannel);
};
