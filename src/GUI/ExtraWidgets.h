#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "imgui.h"
#include "imgui_internal.h"

#include "../Servers/MidiServer.h"

#define COLOR_VOLUME_LOW ImVec4(0.533f, 0.772f, 0.162f, 1.0f)
#define COLOR_VOLUME_MID ImVec4(0.919f, 0.814f, 0.240f, 1.0f)
#define COLOR_VOLUME_HIGH ImVec4(0.97f, 0.40f, 0.01f, 1.0f)
#define COLOR_VOLUME_CLIP ImVec4(1.0f, 0.f, 0.f, 1.0f)

#define VOLUME_THRESHOLD_MIN 0.5
#define VOLUME_THRESHOLD_MID 0.6
#define VOLUME_THRESHOLD_CLIP 0.9

class ExtraWidgets {
public:
    static void Text(string s, bool disabled = false) {
        if ( !disabled ) {
            ImGui::TextUnformatted(s.c_str());
        } else {
            TextDisabled(s);
        }
    }
    static void TextDisabled(string s) {
        ImGui::TextDisabled("%s", s.c_str());
    }

    enum ButtonColors {
        GREEN,
        BLUE,
        DISABLED
    };

    static bool Button(string label, bool condition, const ImVec2 &size = ImVec2(0,0) ) {
        return Button(label, BLUE, condition, size);
    }

    static bool Button(string label, ButtonColors type, bool condition, const ImVec2 &size = ImVec2(0,0) ) {
        if ( condition ) {
            return Button(label, type, size);
        } else {
            return Button(label, DISABLED, size);
        }
    }

    static bool Button(string label, ButtonColors type = BLUE, const ImVec2 &size = ImVec2(0,0) ) {
        int countPops = 0;

        if ( type == GREEN ) {
            ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(.17f, .35f, .24f, 1.f));
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(.13f, .49f, .32f, 1.f));
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(.0f, .67f, .42f, 1.f));
            countPops = 3;
        } else if ( type == DISABLED ) {
            ImVec4 color = ImColor(0.3f, 0.3f, 0.3f, 0.5f);
            ImVec4 textColor = ImColor(1.f, 1.0f, 1.f, 0.5f);
            ImGui::PushStyleColor(ImGuiCol_Text, textColor);
            ImGui::PushStyleColor(ImGuiCol_Button, color);
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
            countPops = 4;
        }

        bool ret = ImGui::Button(label.c_str(), size);
        ImGui::PopStyleColor(countPops);

        if ( type == DISABLED ) {
            ret = false;
        }

        return ret;
    }
    static void VUMeter(float rms, float absolute, const ImVec2& size_arg)
    {
        ImGuiWindow* window = ImGui::GetCurrentWindow();
        if (window->SkipItems)
            return;

        ImGuiContext& g = *GImGui;
        const ImGuiStyle& style = g.Style;

        ImVec2 pos = window->DC.CursorPos;


        ImRect bb(pos, pos + ImGui::CalcItemSize(size_arg, ImGui::CalcItemWidth(), g.FontSize + style.FramePadding.y*2.0f));
//        ImRect bb(pos, pos + size_arg);
        ImGui::ItemSize(bb, style.FramePadding.y);
        if (!ImGui::ItemAdd(bb, 0))
            return;

        // Render
        rms = ImSaturate(rms);
        absolute = ImSaturate(absolute);

        ImGui::RenderFrame(bb.Min, bb.Max, ImGui::GetColorU32(ImGuiCol_FrameBg), true, style.FrameRounding);
        bb.Expand(ImVec2(-style.FrameBorderSize, -style.FrameBorderSize));

        const ImVec2 p0 = ImVec2(bb.Min.x, ImLerp(bb.Max.y, bb.Min.y, rms));
        const ImVec2 p1 = bb.Max;


        ImU32 color = ImGui::GetColorU32(COLOR_VOLUME_LOW);

        if ( rms > VOLUME_THRESHOLD_CLIP ) {
            color = ImGui::GetColorU32(COLOR_VOLUME_CLIP);
        } else if ( rms > VOLUME_THRESHOLD_MID ) {
            color = ImGui::GetColorU32(COLOR_VOLUME_HIGH);
        } else if ( rms > VOLUME_THRESHOLD_MIN ) {
            color = ImGui::GetColorU32(COLOR_VOLUME_MID);
        }

        window->DrawList->AddRectFilled(p0, p1, color);

        if ( absolute > 0 ) {
            const ImVec2 peak0 = ImVec2(bb.Min.x, ImLerp(bb.Max.y, bb.Min.y, absolute));
            const ImVec2 peak1 = peak0 + ImVec2(size_arg.x,2);

            color = ImGui::GetColorU32( ImVec4(0.8f,0.8f,0.8f,1.f) );
            /*if ( absolute > VOLUME_THRESHOLD_CLIP ) {
                color = ImGui::GetColorU32(COLOR_VOLUME_CLIP);
            } else*/ if ( absolute > VOLUME_THRESHOLD_MID ) {
                color = ImGui::GetColorU32(COLOR_VOLUME_HIGH);
            } /*else if ( absolute > VOLUME_THRESHOLD_MIN ) {
                color = ImGui::GetColorU32(COLOR_VOLUME_MID);
            }*/
            window->DrawList->AddRectFilled(peak0, peak1, color);
        }
    }

    static bool Header(string text, bool defaultOpen = true) {
        bool isOpen;
        ImGui::PushStyleColor(ImGuiCol_Header, ImVec4(0.26f, 0.59f, 0.98f, 0.12f) );
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(6.f, 10.f));

        if ( defaultOpen ) {
             isOpen = ImGui::CollapsingHeader(text.c_str(), ImGuiTreeNodeFlags_DefaultOpen);
        } else {
            isOpen = ImGui::CollapsingHeader(text.c_str());
        }

        ImGui::PopStyleVar();
        ImGui::PopStyleColor();
        return isOpen;
    }

    static void TextCentered(string text, bool disabled = false) {
        auto windowWidth = ImGui::GetWindowSize().x;
        auto textWidth   = ImGui::CalcTextSize(text.c_str()).x;

        ImGui::SetCursorPosX((windowWidth - textWidth) * 0.5f);
        Text(text, disabled);
    }
    static void TextCentered_2Cols(string text, int col = 1, bool disabled = false) {
        auto windowWidth = ImGui::GetWindowSize().x / 2;
        auto textWidth   = text.length() * 10;//ImGui::CalcTextSize(text.c_str(), 'n').x;

        ImGui::SetCursorPosX((windowWidth - textWidth) * 0.5f + (col-1)*windowWidth);

        Text(text, disabled);
    }
    static void TextJustifyRight_2Cols(string text, int margin = 5, bool disabled = false) {
        auto windowWidth = ImGui::GetWindowSize().x;
        auto textWidth   = ImGui::CalcTextSize(text.c_str()).x;

        ImGui::SetCursorPosX(windowWidth/2 - textWidth - margin);
        Text(text, disabled);
    }
};
