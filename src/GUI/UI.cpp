#include "UI.h"
#include "../Units/ExplorerUnit.h"
#include "../Units/SequenceUnit.h"
#include "../Units/ParticleUnit.h"
#include "../Units/MorphUnit.h"
#include "../Units/OscUnit.h"
#include "../Servers/AbletonLinkServer.h"
#include "CamZoomAndPan.h"
#include "../Sound/Sounds.h"

Gui* Gui::instance = nullptr;

Gui::Gui() {
    font.load( OF_TTF_MONO, 10 );

    ofImage imgWelcomeImage;
    imgWelcomeImage.setUseTexture(false);

    if ( !imgWelcomeImage.load("assets/welcome.png") ) {
        ofLog() << "Welcome image not loaded";
    }

    ofTextureData texData;
    texData.width = imgWelcomeImage.getWidth();
    texData.height = imgWelcomeImage.getHeight();
    texData.textureTarget = GL_TEXTURE_2D;
    texData.bFlipTexture = true;
    imgWelcome.allocate(texData);
    imgWelcome.loadData(imgWelcomeImage.getPixels());

    gui.setup(new GuiTheme(), true);
    drawGui = true;
}

Gui* Gui::getInstance(){

    if( instance == nullptr ){
        instance = new Gui();
    }
    return instance;
}

void Gui::newSession()
{
    haveToDrawDimReductScreen = true;
}

void Gui::drawProcessingFilesWindow()
{
    if ( isProcessingFiles ) {
        adr.checkProcess();

        if ( adr.progress >= 1.0f ) {
            isProcessingFiles = false;

            string resultJSONpath = AudioDimensionalityReduction::generatedJSONPath;
            if ( resultJSONpath != "" ) {
                SessionManager::datasetLoadOptions opts;
                opts.method = "RECENT";
                opts.path = resultJSONpath.c_str();
                SessionManager::getInstance()->loadSession(opts);

                haveToDrawMapIsDone = true;
            } else {
                showMessage( "Unexpected error", "Error" );
            }
            AudioDimensionalityReduction::end();
        } else if ( adr.progress == -1.0f ) {
            isProcessingFiles = false;
            showMessage( adr.currentProcess, "Error" );
            AudioDimensionalityReduction::end();
        } else {
            ImGuiWindowFlags window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200 ,100), ImGuiCond_Appearing);

            bool isWindowOpen = true;

            string windowTitle = "Processing " +
                    ofToString(adr.filesFound) +
                    " files...";

            if ( adr.filesFound == 0 ) {
                windowTitle = "Processing files...";
            }

            if ( ImGui::Begin( windowTitle.c_str(), &isWindowOpen, window_flags)) {
                ImGui::Text("Grab a coffee, "
                            "this can take quite a while.");


                ImGui::ProgressBar( adr.progress );
                ImGui::Text( "%s", adr.currentProcess.c_str() );
                ImGui::TextDisabled("\n\n(monitoring your RAM usage is also a good idea)");
            }
            ImGui::End();
        }
    }
}
void Gui::onControlKeyboardPressed(){
    auto &io = ImGui::GetIO();
    if ( !io.WantTextInput ) {
        CamZoomAndPan * cam= CamZoomAndPan::getInstance();

        int moveDelta =  7;
        float zoomDelta = 0.175f;
        if(ofGetKeyPressed(OF_KEY_SHIFT)){
            moveDelta =  17;
            zoomDelta = 0.4f;
        }

        //Using wasd strange things happen so we are using arrows
        //need to check it out
        if( ofGetKeyPressed(OF_KEY_UP) ){
            cam->moveUp(moveDelta);
        }
        if( ofGetKeyPressed(OF_KEY_LEFT) ){
            cam->moveLeft(moveDelta);
        }
        if( ofGetKeyPressed(OF_KEY_DOWN) ){
            cam->moveDown(moveDelta);
        }
        if( ofGetKeyPressed(OF_KEY_RIGHT) ){
            cam->moveRight(moveDelta);
        }
//        if( ofGetKeyPressed('Q') || ofGetKeyPressed('q') ){
        if( ofGetKeyPressed(OF_KEY_PAGE_DOWN) ){
             cam->zoomBy(-zoomDelta);
        }
        if( ofGetKeyPressed(OF_KEY_PAGE_UP) ){
            cam->zoomBy(zoomDelta);
        }
    }
}

void Gui::draw() {


    gui.begin();

    //Menu Superior
    drawMainMenu();

    if(drawGui) {
        //Units window
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        auto mainSettings = ofxImGui::Settings();
        mainSettings.windowPos = ImVec2(10, 30);
        mainSettings.lockPosition = true;

        if( ofxImGui::BeginWindow("Units", mainSettings, window_flags)) {
            Units::getInstance()->drawGui();
        }
        ofxImGui::EndWindow(mainSettings);

        if ( Units::getInstance()->currentUnits.size() > 0 ) {
            //Unit configuration window
            window_flags = 0;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;

            mainSettings = ofxImGui::Settings();
            mainSettings.windowPos = ImVec2(ofGetWidth()-WINDOW_TOOLS_WIDTH-10, 30);
            mainSettings.lockPosition = true;

            if( ofxImGui::BeginWindow("Selected unit configuration", mainSettings, window_flags)){
                Units::getInstance()->drawSelectedUnitSettings();
                ImGui::SetWindowSize(ImVec2(WINDOW_TOOLS_WIDTH,0)); //updating height every frame, fixed width
            }

            ofxImGui::EndWindow(mainSettings);
        }



        Tooltip::drawGui();
    }

    drawProcessingFilesWindow();
    drawAboutScreen();
    drawDimReductScreen();
    drawTutorial();
    drawLoadingSamplesScreen();
    drawShortcutsScreen();

    drawClusteringSettingsScreen();

    drawAudioSettingsScreen();
    drawPerformanceSettingsScreen();
    drawContextMenu();
    drawMidiSettingsScreen();
    drawOscSettingsScreen();
    drawOSCLearnScreen();
    OscServer::getInstance()->drawOSCLog();
    drawMIDILearnScreen();
    drawAbletonLinkSettingsScreen();

    haveToHideCursor ? ofHideCursor() : ofShowCursor();
    if ( fullscreenRequested ) {
        setFullscreen(true);
        fullscreenRequested = false;
    }

    drawWelcomeScreen();

    drawFPS();
    drawMessage();
    drawCrashMessage();
    drawStopRecordingScreen();

    hideIdleMouse(10);

    drawSegmentationTool();
    drawAudioSliceWindow();
    drawExitScreen();
    drawAudioFilesNotFound();
    drawMapIsDone();

    gui.end();
    // agregar modificador de shift

    onControlKeyboardPressed();

}

void Gui::drawWelcomeScreen(){
    if(haveToDrawWelcomeScreen){
        
        if (AudioEngine::getInstance()->isConfigured()){
            
            ImGuiWindowFlags window_flags = 0;
            int w = 380;
            window_flags |= ImGuiWindowFlags_NoScrollbar;
            window_flags |= ImGuiWindowFlags_NoMove;
            window_flags |= ImGuiWindowFlags_NoResize;
            window_flags |= ImGuiWindowFlags_NoCollapse;
            window_flags |= ImGuiWindowFlags_NoNav;
            window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
            window_flags |= ImGuiWindowFlags_NoDecoration;
            
            ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
            
            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));
            
            if ( ImGui::Begin("Welcome to AudioStellar" , &haveToDrawWelcomeScreen, window_flags) ){
                ofxImGui::AddImage(imgWelcome, ofVec2f(353,66));
                ExtraWidgets::TextCentered("v" + ofToString(VERSION), true);
                ImGui::NewLine();
                ImGui::NewLine();
                
                ImGui::PushTextWrapPos(w);
                ImGui::TextWrapped("%s", welcomeScreenText.c_str());
                ImGui::Columns(2,NULL, false);
                
                if(ImGui::Button("Yes, please.")) {
                    haveToDrawWelcomeScreen = false;
                    startTutorial();
                }
                
                ImGui::NextColumn();
                
                if(ImGui::Button("No, I can handle this.")){
                    haveToDrawWelcomeScreen = false;
                }
                
                ImGui::End();
            }
            
            ImGui::PopStyleVar();
        }
    }
}

void Gui::drawExitScreen()
{
    if ( haveToDrawExitMessage ) {
        ImGui::OpenPopup("Exit AudioStellar");
        haveToDrawExitMessage = false;
    }

    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoScrollbar;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_NoMove;

    ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f,
                  ImGui::GetIO().DisplaySize.y * 0.5f);
    ImGui::SetNextWindowSize(ImVec2(250,0));
    ImGui::SetNextWindowPos(center, 0, ImVec2(0.5f, 0.5f));

    if(ImGui::BeginPopupModal("Exit AudioStellar", NULL, window_flags)) {
        ImGui::TextWrapped("Save session before closing?");
        ImGui::NewLine();

        if ( ExtraWidgets::Button("Yes, thanks", ExtraWidgets::GREEN) ) {
            if ( !SessionManager::getInstance()->isDefaultSession ) {
               SessionManager::getInstance()->saveSession();
            } else {
               SessionManager::getInstance()->saveAsNewSession();
            }

            userWannaExit = true;
            haveToDrawExitMessage = false;
            ImGui::CloseCurrentPopup();
            ofExit(0);
        }
        ImGui::SameLine();
        if ( ImGui::Button("No") ) {
            userWannaExit = true;
            haveToDrawExitMessage = false;
            ImGui::CloseCurrentPopup();
            ofExit(0);
        }
        ImGui::SameLine();
        ImGui::Dummy(ImVec2(45,0));
        ImGui::SameLine();
        if ( ImGui::Button("Cancel") ) {
            ImGui::CloseCurrentPopup();
            haveToDrawExitMessage = false;
        }

        ImGui::EndPopup();
    }
}

void Gui::drawLoadingSamplesScreen()
{
    if ( haveToDrawLoadingSamplesScreen ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoDecoration;

        float width = 180;
        ImVec2 center(ImGui::GetIO().DisplaySize.x - width - 10,
                      ImGui::GetIO().DisplaySize.y - 70);

        ImGui::SetNextWindowSize( ImVec2(width,0) );
        ImGui::SetNextWindowPos(center);

        if ( ImGui::Begin("Initializing...", NULL, window_flags) ) {
            float progress = Sounds::getInstance()->getSoundLoaderProgress();
            string title = "Preloading " +
                    ofToString(Sounds::getInstance()->getSoundCount()) +
                    " sounds";

            ImGui::Text("%s", title.c_str());
            ImGui::ProgressBar( progress );
//            ImGui::TextDisabled("\nTip: disable preloading if low on RAM");

            if ( progress >= 1.0f ) {
                isPreloadingFiles = false;
                haveToDrawLoadingSamplesScreen = false;
            }

            ImGui::End();
        }
    }
}

void Gui::drawShortcutsScreen()
{
    if(haveToDrawShortcutScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

//        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200, ofGetHeight()/3));
        ImGui::SetNextWindowSize(ImVec2(580,510));
        if(ImGui::Begin("Controls and shortcuts", &haveToDrawShortcutScreen, window_flags)){
           ImGui::Columns(2, "tblShortcuts", true);

           int margin = 17;

           ExtraWidgets::TextJustifyRight_2Cols("Map controls", margin, true);
           ImGui::NewLine();

           ExtraWidgets::TextJustifyRight_2Cols("Alt/Space + mouse drag or Arrow keys", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Mouse scroll or PageUp/PageDown", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Shift + ArrowKeys or PageUp/PageDown", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Right click", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + C", margin);
           ImGui::NewLine();

           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + Left click", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + Right click", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + Middle click", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + Mouse scroll", margin);

           ImGui::NewLine();
           ExtraWidgets::TextJustifyRight_2Cols("Fader controls", margin, true);
           ImGui::NewLine();

           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + click over a fader", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Shift + click over a fader", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Right button over a fader", margin);

           ImGui::NewLine();
           ExtraWidgets::TextJustifyRight_2Cols("Shortcuts", margin, true);
           ImGui::NewLine();

           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + N", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + O", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + S", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + Shift + S", margin);
           ImGui::NewLine();

           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + F", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + G", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + Shift + B", margin);
           ImGui::NewLine();

           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + 1", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + 2", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + 3", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + 4", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + 5", margin);
           ImGui::NewLine();

           ExtraWidgets::TextJustifyRight_2Cols("Numbers 1 to 9", margin);
           ImGui::NewLine();

           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + M", margin);
           ExtraWidgets::TextJustifyRight_2Cols("Ctrl/Cmd + A", margin);

           ImGui::NextColumn();

           ImGui::TextDisabled("Description");
           ImGui::NewLine();

           ImGui::Text("Pan camera");
           ImGui::Text("Zoom camera");
           ImGui::Text("Pan/Zoom faster");
           ImGui::Text("Cluster/Sound settings");
           ImGui::Text("Show cluster names");
           ImGui::NewLine();

           ImGui::Text("Select unit under mouse cursor");
           ImGui::Text("Toggle mute on selected unit");
           ImGui::Text("Toggle solo on selected unit");
           ImGui::Text("Lower/Raise selected unit volume");

           ImGui::NewLine();
           ImGui::NewLine();
           ImGui::NewLine();

           ImGui::Text("Set fader value");
           ImGui::Text("Set default value");
           ImGui::Text("Fader options");

           ImGui::NewLine();
           ImGui::NewLine();
           ImGui::NewLine();

           ImGui::Text("New sound map");
           ImGui::Text("Open sound map");
           ImGui::Text("Save sound map");
           ImGui::Text("Save sound map as...");

           ImGui::NewLine();
           ImGui::Text("Fullscreen");
           ImGui::Text("Show/Hide UI");
           ImGui::Text("Show/Hide Main Menu");

           ImGui::NewLine();
           ImGui::Text("Create Explorer unit");
           ImGui::Text("Create Sequence unit");
           ImGui::Text("Create Particle unit");
           ImGui::Text("Create Morph unit");
           ImGui::Text("Create OSC unit");

           ImGui::NewLine();
           ImGui::Text("Select unit");

           ImGui::NewLine();
           ImGui::Text("Mute all");
           ImGui::Text("Activate all units");
        }
        ImGui::End();
    }
}

void Gui::drawMainMenu(){

    if ( hideMainMenu ) { return; }

    if( ImGui::BeginMainMenuBar() ) {
        if( ImGui::BeginMenu("File") ){

            if(ImGui::BeginMenu("New sound map")){
                if ( ImGui::MenuItem( "From a folder", TARGET_OS_MAC ? "Cmd+N" : "Ctrl+N") ) {
                    newSession();
                }
                if(ImGui::MenuItem("Slice a long recording")){
                    if ( !ast->isRunning() ) {
                        ast->startThread();
                    }

                    ofFileDialogResult dialogResult = showLoadDialog("Select audio file",
                                                                     false,
                                                                     ofFilePath::getUserHomeDir());

                    if(dialogResult.bSuccess) {
                        ast->dirPath = dialogResult.getPath();
                        string extension = ofToLower( ofFilePath::getFileExt(ast->dirPath) );
                        if (extension != "wav" && extension != "mp3") {
                            string title = "ERROR";
                            string description = "The selected file is not a supported sound file";
                            showMessage(description,title, false);
                        }
                        else {
                            ast->loadAudioFile(ast->dirPath);
                            ast->reset_zoom = true;
                            haveToDrawSegmentationTool = true;
                            haveToDrawWelcomeScreen = false;
                            ast->doSlice();
                        }
                    }
                }
                ImGui::EndMenu();
            }

            if(ImGui::MenuItem("Open", TARGET_OS_MAC ? "Cmd+O" : "Ctrl+O")){
                SessionManager::datasetLoadOptions opts;
                opts.method = "GUI";
                SessionManager::getInstance()->loadSession(opts);
            }

            if ( !SessionManager::getInstance()->isDefaultSession ) {
                if(ImGui::MenuItem("Save", TARGET_OS_MAC ? "Cmd+S" : "Ctrl+S")){
                    SessionManager::getInstance()->saveSession();
                }
            } else {
                ImGui::PushStyleColor(ImGuiCol_Text,ImVec4(0.5,0.5,0.5,1.0));
                if ( ImGui::MenuItem("Save") ) {
                    showMessage("Cannot save default project, use \"Save as\" instead.");
                }
                ImGui::PopStyleColor();
            }

            if(ImGui::MenuItem("Save as...", TARGET_OS_MAC ? "Cmd+Shift+S" : "Ctrl+Shift+S")){
                SessionManager::getInstance()->saveAsNewSession();
            }

            if(ImGui::BeginMenu("Recent Projects")){
                vector<string> recentProjects = SessionManager::getInstance()->getRecentProjects();
                if(SessionManager::getInstance()->areAnyRecentProjects(recentProjects)){
                    for(unsigned int i = 0; i < recentProjects.size(); i++){
                        if(ImGui::MenuItem(recentProjects[i].c_str())){

                            SessionManager::datasetLoadOptions opts;
                            opts.method = "RECENT";
                            opts.path = recentProjects[i].c_str();
                            SessionManager::getInstance()->loadSession(opts);

                        }
                    }
                }

                if(SessionManager::getInstance()->areAnyRecentProjects(recentProjects)) {
                    ImGui::Separator();

                    if(ImGui::MenuItem("Default project")) {
                        SessionManager::getInstance()->loadDefaultSession();
                    }
                } else {
                    ImGui::MenuItem("No recent projects", NULL, false, false);
                }



                ImGui::EndMenu();
            }

            if(ImGui::MenuItem("Quit")){
                showExitMessage();
            }

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("View")){
            Sounds::getInstance()->drawGui();
            ImGui::Checkbox("Show UI", &drawGui);
            ImGui::Checkbox("Show sound filenames", &Sounds::getInstance()->showSoundFilenamesTooltip);
            ImGui::Checkbox("Show stats", &haveToDrawFPS);
            ImGui::Checkbox("Show MIDI/OSC monitors", &showMIDIOSCMonitor);
//            ImGui::Checkbox("Hide Cursor", &haveToHideCursor); // disabled (mouse hides when idle)
            bool fooFullscreen = getFullscreen();
            if ( ImGui::Checkbox("Fullscreen", &fooFullscreen) ) {
                toggleFullscreen();
            }
            ImGui::Checkbox("Show Tooltips", &Tooltip::enabled);

            if ( ImGui::Checkbox("Focus on clusters", &haveToFocusOnCluster) ) {
                Sounds::getInstance()->allSoundsClusterIsHovered();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SHOW_FOCUS_ON_CLUSTERS);

            ImGui::Separator();
            if (ImGui::MenuItem("Fit to map")) {
                CamZoomAndPan::getInstance()->resetView();
            }

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Settings")){

            if ( ImGui::MenuItem("Audio") ) {
                haveToDrawAudioSettingsScreen = true;
            }

            // With the new preloading we don't need this at the moment
//            if ( ImGui::MenuItem("Performance") ) {
//               haveToDrawPerformanceSettingsScreen = true;
//            }

            if(ImGui::MenuItem("Clustering")){
                haveToDrawClusteringSettingsScreen = true;
            }

            ImGui::Separator();

            if ( ImGui::MenuItem("MIDI") ) {
               haveToDrawMidiSettingsScreen = true;
            }

            if ( ImGui::MenuItem("OSC") ) {
               haveToDrawOscSettingsScreen = true;
            }

            if ( ImGui::MenuItem("Ableton Link") ) {
               haveToDrawAbletonLinkSettingsScreen = true;
            }

            ImGui::EndMenu();
        }

        if ( ImGui::BeginMenu("Tools") ) {

            if ( ImGui::MenuItem("Sounds to original positions") ) {
                Sounds::getInstance()->revertToOriginalPositions();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SOUNDS_ORIGINAL_POSITIONS);

            if(ImGui::MenuItem("Export clusters to folders")) {
               Sounds::getInstance()->exportFiles();
            }

            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_EXPORT_FILES);

            ImGui::EndMenu();
        }

        if(ImGui::BeginMenu("Help")){
            if(ImGui::MenuItem("About")){
                haveToDrawAboutScreen = true;
            }
            if(ImGui::MenuItem("Controls & Shortcuts")){
                haveToDrawShortcutScreen = true;
            }
            if(ImGui::MenuItem("Tutorial")){
                startTutorial();
            }
            if(ImGui::MenuItem("Forums")){
                string URL = "https://forum.audiostellar.xyz/";
                string command = "";

                #ifdef TARGET_LINUX
                command = "xdg-open ";
                #endif
                #ifdef TARGET_OSX
                command = "open ";
                #endif
                #ifdef TARGET_WIN32
                command = "start ";
                #endif

                ofSystem(command + URL);
            }

            ImGui::Separator();

            if(ImGui::MenuItem("Source code")){
                string URL = "https://gitlab.com/ayrsd/audiostellar/";
                string command = "";

                #ifdef TARGET_LINUX
                command = "xdg-open ";
                #endif
                #ifdef TARGET_OSX
                command = "open ";
                #endif
                #ifdef TARGET_WIN32
                command = "start ";
                #endif

                ofSystem(command + URL);
            }
            if(ImGui::MenuItem("OSC Documentation")){
                string URL = "https://gitlab.com/ayrsd/audiostellar/-/blob/units/OSC_Documentation.md";
                string command = "";

                #ifdef TARGET_LINUX
                command = "xdg-open ";
                #endif
                #ifdef TARGET_OSX
                command = "open ";
                #endif
                #ifdef TARGET_WIN32
                command = "start ";
                #endif

                ofSystem(command + URL);
            }
            if(ImGui::MenuItem("OSC Examples")){
                string URL = "https://gitlab.com/ayrsd/audiostellar/-/tree/units/osc_examples";
                string command = "";

                #ifdef TARGET_LINUX
                command = "xdg-open ";
                #endif
                #ifdef TARGET_OSX
                command = "open ";
                #endif
                #ifdef TARGET_WIN32
                command = "start ";
                #endif

                ofSystem(command + URL);
            }
           ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }

    drawMainMenuExtra();
}

void Gui::drawMainMenuExtra()
{
    if ( ImGui::GetTopMostPopupModal() == NULL ) {
        drawMainMenuCenter();
        drawMainMenuRight();
    }
}

void Gui::drawMainMenuCenter()
{
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.1f, 0.1f, 0.1f, 0.00f));
    ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));

    auto mainSettings = ofxImGui::Settings();
    mainSettings.windowPos = ImVec2(ofGetWidth()/2 - 85 , -8);
    mainSettings.lockPosition = true;

    if ( ofxImGui::BeginWindow("winTempo", mainSettings, window_flags) ) {
        ImGui::BringWindowToDisplayFront(ImGui::GetCurrentWindow());
        MasterClock::getInstance()->drawGui();
    }
    ImGui::PopStyleColor(2);
    ofxImGui::EndWindow(mainSettings);
}

void Gui::drawMainMenuRight()
{
    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoTitleBar;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;
    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.1f, 0.1f, 0.1f, 0.00f));
    ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));

    auto mainSettings = ofxImGui::Settings();
    mainSettings.windowPos = ImVec2(ofGetWidth() - 250, -8);
    mainSettings.lockPosition = true;

    if ( ofxImGui::BeginWindow("masterVolume", mainSettings, window_flags) ) {
        ImGui::BringWindowToDisplayFront(ImGui::GetCurrentWindow());

        if ( showMIDIOSCMonitor ) {
            MidiServer::getInstance()->drawMonitor();
            ImGui::SameLine();
            OscServer::getInstance()->drawMonitor();
        } else {
            ImGui::Dummy(ImVec2(73,1));
        }
        ImGui::SameLine();

        Units::getInstance()->drawMasterVolume();
        ImGui::SameLine();
        Units::getInstance()->recorder.drawGui();
    }
    ImGui::PopStyleColor(2);
    ofxImGui::EndWindow(mainSettings);
}


void Gui::drawAboutScreen(){
    if(haveToDrawAboutScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - 200, ofGetHeight()/3));
        if(ImGui::Begin("About", &haveToDrawAboutScreen, window_flags)){
           string text = "Thanks for using AudioStellar v" + ofToString(VERSION) + " \n\n\n"
                         "More info at https://www.audiostellar.xyz";
           ImGui::TextUnformatted(text.c_str());
        }
        ImGui::End();
    } else {
        haveToDrawAboutScreen = false;
    }
}

void Gui::drawDimReductScreen(){
    if(haveToDrawDimReductScreen) {
        ImGuiWindowFlags window_flags = 0;
        int w = 400;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

        if (ImGui::Begin("Create new sound map" , &haveToDrawDimReductScreen, window_flags)){
            //path
            ImGui::Text("Folder location");

            string folder;
            string displayFolder;

            if(adr.dirPath.size()) {
                folder = ofFilePath::getPathForDirectory(adr.dirPath);
                displayFolder = folder;
            }

            ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0,3));
            ImGui::BeginChild("child", ImVec2(208, 19), true);
            ImGui::TextUnformatted( displayFolder.c_str() );
            ImGui::EndChild();
            ImGui::PopStyleVar();

            ImGui::SameLine();
            if(ImGui::Button("Select folder")){
                ofFileDialogResult dialogResult = showLoadDialog("Select Folder",
                                                                 true,
                                                                 ofFilePath::getUserHomeDir());

                if(dialogResult.bSuccess) {
                    adr.dirPath = dialogResult.getPath();

                }
            }

            if(ImGui::TreeNode("Advanced settings")) {
                if(ImGui::TreeNode("Feature settings")) {
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_FEATURE_SETTINGS);

                    ImGui::Checkbox("STFT", &adr.stft);
                    ImGui::Checkbox("MFCC", &adr.mfcc);
                    ImGui::Checkbox("Spectral-Centroid", &adr.spectralCentroid);
                    ImGui::Checkbox("Chromagram", &adr.chromagram);
                    ImGui::Checkbox("RMS", &adr.rms);

                    if ( !adr.stft && !adr.mfcc && !adr.spectralCentroid && !adr.chromagram && !adr.rms ) {
                        adr.stft = true;
                    }

                    ImGui::NewLine();

                    ImGui::InputFloat("Audio length", &adr.targetDuration, 0.1f, 0.5f, "%.1f");
                    adr.targetDuration = ofClamp(adr.targetDuration,0,500);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_LENGTH);

                    ImGui::Combo("Sample rate", &sample_rate_item_current, sample_rate_items, IM_ARRAYSIZE(sample_rate_items));
                    adr.targetSampleRate = sample_rate_values[sample_rate_item_current];
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE);

                    ImGui::Combo("Window size", &window_size_item_current, window_size_items, IM_ARRAYSIZE(window_size_items));
                    adr.stft_windowSize = window_size_values[window_size_item_current];
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_WINDOW_SIZE);

                    ImGui::Combo("Hop size", &hop_size_item_current, hop_size_items, IM_ARRAYSIZE(hop_size_items));
                    adr.stft_hopSize = window_size_values[window_size_item_current] / hop_size_values[hop_size_item_current];
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_STFT_HOP_SIZE);
                    ImGui::NewLine();


                    ImGui::TreePop();
                }

                if(ImGui::TreeNode("Visualization settings")) {
                    ImGui::Combo("Algorithm", &viz_item_current, viz_items, IM_ARRAYSIZE(viz_items));
                    adr.viz_method = viz_values[viz_item_current];

                    if ( adr.viz_method == "tsne" ) {
                        ImGui::NewLine();

                        ImGui::Combo("Metric", &metric_item_current, metric_items, IM_ARRAYSIZE(metric_items));
                        adr.metric = metric_values[metric_item_current];
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_METRIC);

                        ImGui::InputInt("Perplexity", &adr.tsne_perplexity);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_PERPLEXITY);


                        ImGui::InputInt("Learning rate", &adr.tsne_learning_rate);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_LEARNING_RATE);

                        ImGui::InputInt("Iterations", &adr.tsne_iterations);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_TSNE_ITERATIONS);
                    } else if ( adr.viz_method == "umap" ) {
                        ImGui::NewLine();

                        ImGui::Combo("Metric", &metric_item_current, metric_items, IM_ARRAYSIZE(metric_items));
                        adr.metric = metric_values[metric_item_current];
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_METRIC);

                        ImGui::InputInt("N Neighbors", &adr.umap_n_neighbors);
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_UMAP_NEIGHBORS);

                        ImGui::InputFloat("Min distance", &adr.umap_min_dist, 0.05f, 0.1f, "%.2f");
                        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_UMAP_MIN_DISTANCE);
                    }

                    ImGui::NewLine();


                    ImGui::TreePop();
                }

                if(ImGui::TreeNode("Preferences")){

                    ImGui::Checkbox("Save intermediate results", &adr.pca_results);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_SAVE_PCA_RESULTS);
                    ImGui::Checkbox("Force full process", &adr.force_full_process);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::DIMREDUCT_FORCE_FULL_PROCESS);

                    ImGui::TreePop();
                }


                ImGui::TreePop();
            }

            ImGui::NewLine();

            if(ExtraWidgets::Button("Run", ExtraWidgets::GREEN, adr.dirPath.size(), ImVec2(-1,40))) {
                haveToDrawDimReductScreen = false;
                isProcessingFiles = true;
                haveToDrawWelcomeScreen = false;

                adr.startThread();
            }

            ImGui::End();
        }
        ImGui::PopStyleVar();

    }
}

void Gui::drawAudioSettingsScreen() {

    if(haveToDrawAudioSettingsScreen) {
        ImGuiWindowFlags window_flags = 0;
        int w = 400;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, 100));
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

        if (ImGui::Begin("Audio Settings" , &haveToDrawAudioSettingsScreen, window_flags)) {
            // this can get better still
            vector <soundDevice> devices = AudioEngine::getInstance()->listDevices();

            string selectedAudioDevice = "No audio";
            if ( AudioEngine::getInstance()->isDeviceSelected() ) {
                selectedAudioDevice = AudioEngine::getInstance()->getSelectedDeviceName();
            }

            if (ImGui::BeginCombo("Audio Out", selectedAudioDevice.c_str())) {

                for (int i = 0; i < devices.size(); i++) {
                    if (ImGui::Selectable( devices[i].name.c_str())) {
                        AudioEngine::getInstance()->selectAudioOutDevice(i);
                    }
                    if (devices[i].deviceID == AudioEngine::getInstance()->getSelectedDeviceID() ) {
                        ImGui::SetItemDefaultFocus();
                    }
                }
                ImGui::EndCombo();
            }

            if ( AudioEngine::getInstance()->isDeviceSelected() ) {
                if (ImGui::BeginCombo( "Sample Rate", ofToString(AudioEngine::getInstance()->getSelectedSampleRate()).c_str() )) {

                    for ( auto & sampleRate : AudioEngine::getInstance()->getSupportedSampleRates() ) {
                        if (ImGui::Selectable( ofToString(sampleRate).c_str() )) {
                            AudioEngine::getInstance()->selectSampleRate(sampleRate);
                        }
                    }

                    ImGui::EndCombo();
                }

                if (ImGui::BeginCombo( "Buffer Size", ofToString(AudioEngine::getInstance()->getSelectedBufferSize()).c_str() )) {

                    for ( auto & bufferSize : AudioEngine::getInstance()->getSupportedBufferSizes() ) {
                        if (ImGui::Selectable( ofToString(bufferSize).c_str() )) {
                            AudioEngine::getInstance()->selectBufferSize(bufferSize);
                        }
                    }

                    ImGui::EndCombo();
                }

                ImGui::NewLine();

                string applyButton = "Apply settings";
                if ( !AudioEngine::getInstance()->isConfigured() ) {
                    applyButton = "Start audio engine";
                }

                if ( ImGui::Button(applyButton.c_str()) ) {
                    AudioEngine::getInstance()->setup();
                }
            }

            ImGui::NewLine();

            // this should be change with Max Voices per channel
//            ImGui::InputInt("Max Voices",  &Voices::getInstance()->numVoices, NULL, NULL);
//            if(ImGui::IsItemDeactivatedAfterEdit()) {
//                Voices::getInstance()->reset();
//            }
//            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::OSC_RECEIVE_PORT);

//            ImGui::NewLine();

            if ( AudioEngine::getInstance()->isConfigured() ) {
                ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.0f, .8f, .0f, 1.f));
        //        ImGui::Dummy(ImVec2(1.f, 0.5f));
                ImGui::Text("Audio engine has started");
                ImGui::PopStyleColor();

                string countAudioChannels =
                        ofToString( AudioEngine::getInstance()->getSelectedDeviceAudioOutNumChannels() / 2 ) +
                        " stereo output channels";
                ImGui::Text("%s", countAudioChannels.c_str());
            } else {
                ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(.8f, .0f, .0f, 1.f));
        //        ImGui::Dummy(ImVec2(1.f, 0.5f));
                ImGui::Text("Audio engine has not started");
                ImGui::PopStyleColor();
            }

            // this should be readded in the future
            //
            //
//            ImGui::PushStyleVar(ImGuiStyleVar_ChildBorderSize, .0f);
//                ImGui::BeginChild("Child1", ImVec2(0,15), true);
//                    ImGui::Columns(2, "columns1", false);
//                    ImGui::Text("Stereo");
//                    ImGui::NextColumn();
//                    ImGui::Text("Mono");
//                ImGui::EndChild();
//            ImGui::PopStyleVar();

//            ImGui::Columns(1);

//            ImGui::BeginChild("Child2", ImVec2(0,300), true, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
//                ImGui::Columns(2, "columns2", false);

//                for (int i = 0; i < AudioEngine::getInstance()->outputs.size(); i++) {
//                    if (AudioEngine::getInstance()->outputs[i].type == "stereo") {
//                        ImGui::Selectable(AudioEngine::getInstance()->outputs[i].label.c_str(), &AudioEngine::getInstance()->outputs[i].enabled);
//                    }
//                }

//                ImGui::NextColumn();

//                for (int i = 0; i < AudioEngine::getInstance()->outputs.size(); i++) {
//                    if (AudioEngine::getInstance()->outputs[i].type == "mono") {

//                        ImGui::Selectable(AudioEngine::getInstance()->outputs[i].label.c_str(),
//                                          &AudioEngine::getInstance()->outputs[i].enabled,
//                                          NULL,
//                                          ImVec2(40.0f, .0f));

//                        ImGui::SameLine(80);

//                        ImGui::Selectable(AudioEngine::getInstance()->outputs[i + 1].label.c_str(),
//                                          &AudioEngine::getInstance()->outputs[i + 1].enabled,
//                                          NULL,
//                                          ImVec2(40.0f, .0f));

//                        i++; //skip next mono output
//                    }
//                }

//            ImGui::EndChild();

            ImGui::End();
        }
        ImGui::PopStyleVar();
    }
}

void Gui::drawClusteringSettingsScreen() {
    if (haveToDrawClusteringSettingsScreen) {
        
        auto session = SessionManager::getInstance();
        auto colorPalette = ColorPalette::getInstance();
        auto sounds = Sounds::getInstance();
        
        auto sessionFile = session->getSessionFile();
        
        bool clusteringSettingsWindowIsOpen = true;
        
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoScrollWithMouse;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        float w = 235;
        ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.2f);

        ImGui::SetNextWindowSizeConstraints(ImVec2(w, -1.f), ImVec2(w, -1.f));
        ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.2f));

        if (ImGui::Begin("Clustering Settings" , &clusteringSettingsWindowIsOpen, window_flags)) {
            
            
            bool showUnsavedSettingsPopup = false;
            bool showDeleteClusterNamesWarningPopup = false;
            bool showUnsavedPalettePopup = false;
            bool showNameTakenPopup = false;
            bool showDeletePalettePopup = false;
            bool showCannotSaveDefaultSessionPopup = false;
            
            ImGui::PushStyleVar(ImGuiStyleVar_TabRounding, 0);
            
            if (ImGui::BeginTabBar("Clustering", ImGuiTabBarFlags_NoTooltip)) {
                
                
                if (ImGui::BeginTabItem("DBScan")) {
                    
                    sounds->dbScanSettingsEdited = sounds->areDBScanSettingsEdited();
                    
                    ImGui::Dummy(ImVec2(0, 0));
                                        
                    if (sounds->getUseOriginalClusters()) {
                        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
                        ImVec4 disabledColor = ImColor(0.3f, 0.3f, 0.3f, 0.5f);
                        ImVec4 disabledTextColor = ImColor(1.f, 1.0f, 1.f, 0.5f);
                        ImGui::PushStyleColor(ImGuiCol_Text, disabledTextColor);
                        ImGui::PushStyleColor(ImGuiCol_Button, disabledColor);

                        ImGui::PushStyleColor(ImGuiCol_FrameBg, disabledColor);

                    }
                    
                    ImGui::SetNextItemWidth(ImGui::GetWindowWidth() - 60);

                    if (ImGui::SliderInt("Eps", &sounds->epsDbScan, 5, 100)) {
                        sounds->doClustering();
                    }
                    if(ImGui::IsItemHovered())  Tooltip::setTooltip(Tooltip::CLUSTERS_EPS);
                    
                    ImGui::SetNextItemWidth(ImGui::GetWindowWidth() - 60);
                    if (ImGui::SliderInt("MinPts", &sounds->minPts, 3, 20)) {
                        sounds->doClustering();
                    }
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_MIN_PTS);
                    
                    
                    if (sounds->getUseOriginalClusters()) {
                        ImGui::PopItemFlag();
                        ImGui::PopStyleColor(3);
                    }
                    
                    
                    if (sounds->areAnyOriginalClusters()) {
                        ImGui::Checkbox( "Use folders as clusters", &sounds->getUseOriginalClusters() );
                        if (sounds->getUseOriginalClusters()) {
                            sounds->setFoldersAsClusters();
                        }
                        else {
                            sounds->doClustering();
                        }
                    }
                    // Need a tooltip here
//                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::CLUSTERS_MIN_PTS);

                    ImGui::Dummy(ImVec2(0, 0));
                    
                    ImGui::EndTabItem();
                }
                
                if (ImGui::BeginTabItem("Palettes")) {
                                        
                    auto& palettes = colorPalette->getPalettes();
                    
                    ImGui::Dummy(ImVec2(0, 0));
                    
                    //Combo, palette selector
                    if (!colorPalette->isRenamingPalette) {
                        ImGuiComboFlags combo_flags = 0;
                        
                        string name = colorPalette->paletteEdited ? session->colorPalette + " *" : session->colorPalette;
                        
                        ImVec2 InputTextPosition = ImGui::GetCursorPos();
                        
                        ImGui::SetNextItemWidth(197);
                        if (ImGui::BeginCombo("##palettesSelector", name.c_str(), combo_flags)) {
                            for (auto palette : palettes) {
                                bool selected = palette.first == session->colorPalette;
                                string name = colorPalette->paletteEdited && selected ? palette.first + " *" : palette.first;
                                if (ImGui::Selectable(name.c_str(), selected)) {
                                    if (!selected && colorPalette->paletteEdited) {
                                        
                                        if (session->colorPalette == DEFAULT_PALETTE) {
                                            colorPalette->revert();
                                            session->colorPalette = palette.first;
                                            colorPalette->paletteEdited = false;
                                            sounds->generateBackground();
                                        }
                                        else {
                                            showUnsavedPalettePopup = true;
                                            colorPalette->selectedPaletteNext = palette.first;
                                            sounds->generateBackground();
                                        }
                                    }
                                    else {
                                        session->colorPalette = palette.first;
                                        sounds->generateBackground();
                                    }
                                }
                                if (selected) ImGui::SetItemDefaultFocus();
                            }
                            ImGui::EndCombo();
                        }
                    }
                    else {
                        
                        ImGui::PushItemWidth(197.f);
                        
                        ImGuiInputTextFlags flags = 0;
                        flags |= ImGuiInputTextFlags_EnterReturnsTrue;
                        flags |= ImGuiInputTextFlags_AutoSelectAll;
                        
                        if (colorPalette->renameModeKeyboardFocus) {
                            ImGui::SetKeyboardFocusHere();
                            colorPalette->renameModeKeyboardFocus = false;
                        }
                        
                        if (ImGui::InputText("##paletteName", colorPalette->newPaletteName, 64, flags)) {
                            if (ofToString(colorPalette->newPaletteName) != "") {
                                
                                if (colorPalette->paletteExists(colorPalette->newPaletteName) && colorPalette->newPaletteName != session->colorPalette) {
                                    showNameTakenPopup = true;
                                }
                                else {
                                    colorPalette->renamePalette();
                                    session->saveSettingsFile(true);
                                    colorPalette->paletteEdited = false;
                                }
                            }
                            //do not allow blank name
                            else {
                                colorPalette->renameModeKeyboardFocus = true;
                            }
                        }
                        // esc key pressed or clicked outside InputText
                        else {
                            if (ImGui::IsItemDeactivated()) {
                                colorPalette->renameMode(false);
                            }
                            
                        }
                        
                    }
                    
                    auto& selectedColors = palettes[session->colorPalette];
                    
                    ImGui::SameLine();
                    
                    
                    //Palette menu
                    if (ImGui::Button("+")) ImGui::OpenPopup("Palette actions");
                    
                    if (ImGui::BeginPopup("Palette actions")) {
                        
                        if (ImGui::MenuItem("New")) {
                            if (session->colorPalette == DEFAULT_PALETTE && colorPalette->paletteEdited) {
                                colorPalette->revert();
                            }
                            colorPalette->newPalette();
                        }
                        
                        if (ImGui::MenuItem("Duplicate")) {
                            string originalPalette = session->colorPalette;
                            colorPalette->duplicatePalette();
                            if (originalPalette == DEFAULT_PALETTE && colorPalette->paletteEdited) {
                                colorPalette->revert(DEFAULT_PALETTE);
                            }
                            session->saveSettingsFile(true);
                        }
                        
                        if (session->colorPalette != DEFAULT_PALETTE) {
                            
                            if (ImGui::MenuItem("Delete")) {
                                showDeletePalettePopup = true;
                            }
                            
                            if (ImGui::MenuItem("Rename")) {
                                colorPalette->renameMode(true);
                            }
                            
                        }
                        ImGui::EndPopup();
                    }
                    
                    //Color buttons
                    for (int i = 0; i < selectedColors.size(); i++) {
                        
                        ofFloatColor color = selectedColors[i];
                        
                        if ((i % 6) != 0) ImGui::SameLine();
                        else ImGui::Dummy(ImVec2(0, 0));
                        
                        ImVec2 rectStart = ImGui::GetCursorScreenPos();
                        
                        ImGuiColorEditFlags flags = 0;
                        flags |= ImGuiColorEditFlags_NoTooltip;
                        flags |= ImGuiColorEditFlags_NoBorder;
                        if (ImGui::ColorButton( ("##colorSelector" + ofToString(i)).c_str(), color, flags, ImVec2(30, 30) )) {
                            colorPalette->selectedColorForEditing = i;
                        }
                        
                        if (i == colorPalette->selectedColorForEditing) {
                            ImVec2 rectEnd = rectStart + ImVec2(30, 30);
                            ImColor rectBorderColor = ImColor(1.f, 1.f, 1.f, 1.f);
                            ImGui::GetWindowDrawList()->AddRect(rectStart, rectEnd, rectBorderColor, 0, NULL, 2.f);
                        }
                    }
                    
                    ImGui::Dummy(ImVec2(0, 0));
                    
                    ofFloatColor& color = selectedColors[colorPalette->selectedColorForEditing];
                    
                    
                    //Color picker
                    ImGuiColorEditFlags flags = 0;
                    flags |= ImGuiColorEditFlags_NoAlpha;
                    flags |= ImGuiColorEditFlags_NoSidePreview;
                    flags |= ImGuiColorEditFlags_NoTooltip;
                    flags |= ImGuiColorEditFlags_NoDragDrop;
                    flags |= ImGuiColorEditFlags_NoSmallPreview;
                    
                    ImGui::PushItemWidth(220);
                    if (ImGui::ColorPicker4("##ColorPicker", color.v, flags, NULL)) {
                        sounds->generateBackground();
                        colorPalette->paletteEdited = true;
                    }
                    ImGui::PopItemWidth();
                    
                    ImGui::Dummy(ImVec2(0, 0));
                    ImGui::Dummy(ImVec2(27, 0));
                    ImGui::SameLine();
                    
                    if(ExtraWidgets::Button("Save", ExtraWidgets::GREEN, colorPalette->paletteEdited, ImVec2(49, 18))) {
                        if (session->colorPalette == DEFAULT_PALETTE) {
                            colorPalette->duplicatePalette();
                            colorPalette->revert(DEFAULT_PALETTE);
                        }
                        else {
                            session->saveSettingsFile(true);
                            colorPalette->paletteEdited = false;
                        }
                    }
                    
                    ImGui::SameLine();
                    
                    if(ExtraWidgets::Button("Revert", ExtraWidgets::BLUE, colorPalette->paletteEdited, ImVec2(49, 18))) {
                        colorPalette->revert();
                        sounds->generateBackground();
                        colorPalette->paletteEdited = false;
                    }
                    
                    ImGui::SameLine();
                    
                    if(ExtraWidgets::Button("Randomize", ExtraWidgets::BLUE, ImVec2(71, 18))) {
                        colorPalette->randomizePalette();
                        sounds->generateBackground();
                        colorPalette->paletteEdited = true;
                    }
                    
                    ImGui::EndTabItem();
                }
                
                // Clicked on X
                if (!clusteringSettingsWindowIsOpen) {
                    
                    if ( (sounds->dbScanSettingsEdited && sounds->areAnyClusterNames()) || colorPalette->paletteEdited ) {
                        showUnsavedSettingsPopup = true;
                    }
                    else {
                        haveToDrawClusteringSettingsScreen = false;
                    }
                }
                
                
                
                
                //Popups
                
                ImGuiWindowFlags popup_flags = 0;
                popup_flags |= ImGuiWindowFlags_NoScrollbar;
                popup_flags |= ImGuiWindowFlags_NoResize;
                popup_flags |= ImGuiWindowFlags_NoCollapse;
                popup_flags |= ImGuiWindowFlags_NoMove;
                
                ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f,
                              ImGui::GetIO().DisplaySize.y * 0.5f);
                ImGui::SetNextWindowSize(ImVec2(290,0));
                ImGui::SetNextWindowPos(center, 0, ImVec2(0.5f, 0.5f));
                
                
                //Unsaved settings popup
                if(showUnsavedSettingsPopup) ImGui::OpenPopup("Unsaved Settings");
                
                if(ImGui::BeginPopupModal("Unsaved Settings", NULL, popup_flags)) {
                    
                    if ( (sounds->dbScanSettingsEdited && sounds->areAnyClusterNames()) ||
                        ((sounds->dbScanSettingsEdited && sounds->areAnyClusterNames()) && colorPalette->paletteEdited)) {
                        
                        ImGui::TextWrapped("DBScan settings have changed. All cluster names will be deleted.");
                        
                        ImGui::NewLine();
                        
                        ImGui::Dummy(ImVec2(217, 0));
                        ImGui::SameLine();
                        
                        if ( ImGui::Button("OK", ImVec2(49, 18)) ) {
                            (*sessionFile)["sounds"]["DBScan_EPS"] = sounds->epsDbScan;
                            (*sessionFile)["sounds"]["DBScan_minPts"] = sounds->minPts;
                            sounds->dbScanSettingsEdited = false;
                            sounds->deleteClusterNames();
                            if (!colorPalette->paletteEdited) {
                                haveToDrawClusteringSettingsScreen = false;
                                ImGui::CloseCurrentPopup();
                            }
                        }
                        
                    } else if (colorPalette->paletteEdited) {
                        
                        if (session->colorPalette == DEFAULT_PALETTE) {
                            colorPalette->duplicatePalette(false);
                            colorPalette->revert(DEFAULT_PALETTE);
                            colorPalette->paletteEdited = false;
                            haveToDrawClusteringSettingsScreen = false;
                        }
                        else {
                            
                            ImGui::TextWrapped("The active palette has unsaved changes. Do you want to save them?");
                            ImGui::NewLine();
                            
                            ImGui::Dummy(ImVec2(103, 0));
                            ImGui::SameLine();
                            
                            if ( ExtraWidgets::Button("Yes", ExtraWidgets::GREEN, ImVec2(49, 18)) ) {
                                session->saveSettingsFile(true);
                                colorPalette->paletteEdited = false;
                                haveToDrawClusteringSettingsScreen = false;
                                ImGui::CloseCurrentPopup();
                            }
                            
                            ImGui::SameLine();
                            
                            if ( ImGui::Button("No", ImVec2(49, 18)) ) {
                                colorPalette->revert();
                                colorPalette->paletteEdited = false;
                                haveToDrawClusteringSettingsScreen = false;
                                ImGui::CloseCurrentPopup();
                            }
                            
                            ImGui::SameLine();
                            if ( ImGui::Button("Cancel", ImVec2(49, 18)) ) {
                                ImGui::CloseCurrentPopup();
                            }
                            
                        }
                    }
                    
                    ImGui::EndPopup();
                }
                
                //Delete cluster names popup
                ImGui::SetNextWindowSize(ImVec2(290,0));
                ImGui::SetNextWindowPos(center, 0, ImVec2(0.5f, 0.5f));
                
                if(showDeleteClusterNamesWarningPopup) ImGui::OpenPopup("Save DBScan Settings");
                
                if(ImGui::BeginPopupModal("Save DBScan Settings", NULL, popup_flags)) {
                    ImGui::TextWrapped("All cluster names will be deleted. Do you want to save changes ?");
                    ImGui::NewLine();
                    
                    ImGui::Dummy(ImVec2(103, 0));
                    ImGui::SameLine();
                    if ( ExtraWidgets::Button("Yes", ExtraWidgets::GREEN, ImVec2(49, 18)) ) {
                        (*sessionFile)["sounds"]["DBScan_EPS"] = sounds->epsDbScan ;
                        (*sessionFile)["sounds"]["DBScan_minPts"] = sounds->minPts;
                        sounds->deleteClusterNames();
                        sessionFile->save(session->getSessionFileName(), true);
                        sounds->dbScanSettingsEdited = false;
                        ImGui::CloseCurrentPopup();
                    }
                    
                    ImGui::SameLine();
                    if ( ImGui::Button("Cancel", ImVec2(49, 18)) ) {
                        ImGui::CloseCurrentPopup();
                    }
                    
                    ImGui::EndPopup();
                }
                
                //Unsaved palette popup
                ImGui::SetNextWindowSize(ImVec2(290,0));
                ImGui::SetNextWindowPos(center, 0, ImVec2(0.5f, 0.5f));
                
                if(showUnsavedPalettePopup) ImGui::OpenPopup("Unsaved Palette");
                
                if(ImGui::BeginPopupModal("Unsaved Palette", NULL, popup_flags)) {
                    ImGui::TextWrapped("The active palette has unsaved changes. Do you want to save them?");
                    ImGui::NewLine();
                    
                    ImGui::Dummy(ImVec2(103, 0));
                    ImGui::SameLine();
                    
                    if ( ExtraWidgets::Button("Yes", ExtraWidgets::GREEN, ImVec2(49, 18)) ) {
                        session->saveSettingsFile(true);
                        colorPalette->paletteEdited = false;
                        if (colorPalette->selectedPaletteNext != "") {
                            session->colorPalette = colorPalette->selectedPaletteNext;
                            sounds->generateBackground();
                        }
                        else {
                            haveToDrawClusteringSettingsScreen = false;
                        }
                        ImGui::CloseCurrentPopup();
                    }
                    
                    ImGui::SameLine();
                    
                    if ( ImGui::Button("No", ImVec2(49, 18)) ) {
                        colorPalette->revert();
                        colorPalette->paletteEdited = false;
                        if (colorPalette->selectedPaletteNext != "") {
                            session->colorPalette = colorPalette->selectedPaletteNext;
                            sounds->generateBackground();
                        }
                        else {
                            haveToDrawClusteringSettingsScreen = false;
                        }
                        ImGui::CloseCurrentPopup();
                    }
                    
                    ImGui::SameLine();
                    if ( ImGui::Button("Cancel", ImVec2(49, 18)) ) {
                        ImGui::CloseCurrentPopup();
                    }
                    
                    ImGui::EndPopup();
                }
                
                //Palette name already taken popup
                ImGui::SetNextWindowSize(ImVec2(290,0));
                ImGui::SetNextWindowPos(center, 0, ImVec2(0.5f, 0.5f));
                
                if(showNameTakenPopup) ImGui::OpenPopup("Name taken");
                
                if(ImGui::BeginPopupModal("Name taken", NULL, popup_flags)) {
                    
                    string text = "The name \"" + ofToString(colorPalette->newPaletteName) + "\" is already used by another palette. Please try a different name.";
                    ImGui::TextWrapped(text.c_str());
                    ImGui::NewLine();
                    
                    ImGui::Dummy(ImVec2(217, 0));
                    
                    ImGui::SameLine();
                    if ( ImGui::Button("Ok", ImVec2(49, 18)) ) {
                        ImGui::CloseCurrentPopup();
                        colorPalette->renameModeKeyboardFocus = true;
                    }
                    
                    ImGui::EndPopup();
                }
                
                
                //Delete palette warning popup
                ImGui::SetNextWindowSize(ImVec2(290,0));
                ImGui::SetNextWindowPos(center, 0, ImVec2(0.5f, 0.5f));
                
                if(showDeletePalettePopup) ImGui::OpenPopup("Delete Palette");
                
                if(ImGui::BeginPopupModal("Delete Palette", NULL, popup_flags)) {
                    
                    string text = "Do you want to delete the palette named \"" + session->colorPalette + "\"? This can't be undone.";
                    ImGui::TextWrapped(text.c_str());
                    ImGui::NewLine();
                    
                    ImGui::Dummy(ImVec2(160, 0));
                    ImGui::SameLine();

                    if ( ExtraWidgets::Button("Yes", ExtraWidgets::GREEN, ImVec2(49, 18)) ) {
                        colorPalette->deletePalette(session->colorPalette);
                        colorPalette->paletteEdited = false;
                        sounds->generateBackground();
                        ImGui::CloseCurrentPopup();
                    }
                    
                    ImGui::SameLine();
                    if ( ImGui::Button("Cancel", ImVec2(49, 18)) ) {
                        ImGui::CloseCurrentPopup();
                    }
                    
                    ImGui::EndPopup();
                }
                
                ImGui::SetNextWindowSize(ImVec2(290,0));
                ImGui::SetNextWindowPos(center, 0, ImVec2(0.5f, 0.5f));
                
                if(showCannotSaveDefaultSessionPopup) ImGui::OpenPopup("Save Settings");
                
                if(ImGui::BeginPopupModal("Save Settings", NULL, popup_flags)) {
                    
                    string text = "Cannot save default session. Do you want to save to a new session file ?";
                    ImGui::TextWrapped(text.c_str());
                    ImGui::NewLine();
                    
                    ImGui::Dummy(ImVec2(160, 0));
                    ImGui::SameLine();

                    if ( ExtraWidgets::Button("Yes", ExtraWidgets::GREEN, ImVec2(49, 18)) ) {
                        session->saveAsNewSession();
                    }
                    
                    ImGui::SameLine();
                    if ( ImGui::Button("Cancel", ImVec2(49, 18)) ) {
                        ImGui::CloseCurrentPopup();
                    }
                    
                    ImGui::EndPopup();
                }
                
                
                
                ImGui::EndTabBar();
            }
            
            ImGui::PopStyleVar();
        
            ImGui::End();
        }
    }
}
void Gui::drawPerformanceSettingsScreen()
{
    //This is never called at the moment

    if(haveToDrawPerformanceSettingsScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        int w = 250;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, ofGetHeight()/3), ImGuiCond_Appearing);
        ImGui::SetNextWindowSize( ImVec2(w, 100) );
        if(ImGui::Begin("Performance Settings",
                        &haveToDrawPerformanceSettingsScreen,
                        window_flags)){

            ImGui::Checkbox("Preload sounds", &Sounds::getInstance()->doPreloading);
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SOUND_PRELOADING);

            ImGui::End();
        }
    }
}

void Gui::drawMidiSettingsScreen(){
    if(haveToDrawMidiSettingsScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        int w = 400;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, ofGetHeight()/3), ImGuiCond_Appearing);
        if(ImGui::Begin("MIDI Settings",
                        &haveToDrawMidiSettingsScreen,
                        window_flags)){

            MidiServer::getInstance()->drawSettings();

            ImGui::End();
        }
    }
}

void Gui::drawOscSettingsScreen(){
    if(haveToDrawOscSettingsScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoMove;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        int w = 400;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, ofGetHeight()/3));
        if(ImGui::Begin("OSC settings",
                        &haveToDrawOscSettingsScreen,
                        window_flags)){

            OscServer::getInstance()->drawGui();
            ImGui::End();
        }
    }
}

void Gui::drawMIDILearnScreen()
{
    MidiServer::getInstance()->drawMIDILearn();
}

void Gui::drawOSCLearnScreen()
{
    OscServer::getInstance()->drawOSCLearn();
}

void Gui::drawAbletonLinkSettingsScreen()
{
    if(haveToDrawAbletonLinkSettingsScreen){
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        int w = 400;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2 - w/2, ofGetHeight()/3), ImGuiCond_Appearing);
        if(ImGui::Begin("Ableton Link settings",
                        &haveToDrawAbletonLinkSettingsScreen,
                        window_flags)){

            AbletonLinkServer::getInstance()->drawGui();
            ImGui::End();
        }
    }
}

void Gui::drawAudioFilesNotFound()
{
    if ( haveToDrawFilesNotFoundMessage ) {
        ImGui::OpenPopup("Audio files not found");
        haveToDrawFilesNotFoundMessage = false;
    }

    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;

    ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
    int width = 325;
    ImGui::SetNextWindowSize( ImVec2(width,0) );
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

    if ( ImGui::BeginPopupModal("Audio files not found", NULL, window_flags) ) {
        ImGui::PushTextWrapPos(width - 5);
        ImGui::Text( "The audio files from your sound map could not be found.\n\n"
                     "Please select the folder that contains them." );

        ImGui::NewLine();

        if ( ExtraWidgets::Button("Ok, select folder", ExtraWidgets::GREEN) ) {
            ofFileDialogResult result = Gui::getInstance()->showLoadDialog("Audio files not found, please locate the folder",
                                                                    true);
            if(result.bSuccess) {
                SessionManager::getInstance()->setAudioFilesNotFoundPath(result.getPath());
                ImGui::CloseCurrentPopup();
            }
        }
        ImGui::SameLine();
        if ( ImGui::Button("No, load default session") ) {
            SessionManager::getInstance()->loadDefaultSession();
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
}

void Gui::drawFPS()
{
    if ( haveToDrawFPS ) {
//        ofSetColor(255);
//        if ( Voices::getInstance()->numVoices == Voices::getInstance()->loadedVoices ) {
//            ofSetColor(255,0,0);
//        }
//        font.drawString(
//            "# Voices: " + ofToString(Voices::getInstance()->loadedVoices, 0),
//            ofGetWidth() - 110,
//            ofGetHeight() - 25);

        ofSetColor(255);
        font.drawString(
            "FPS: " + ofToString(ofGetFrameRate(), 0),
            ofGetWidth() - 70,
            ofGetHeight() - 10);
    }
}

void Gui::drawMessage()
{
    if ( haveToDrawMessage ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 350;
        int h = 100;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2, (ofGetHeight()/2) - h/2 ));

        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, messageOpacity);

        if(ImGui::Begin( messageTitle.c_str() , &haveToDrawMessage, window_flags)) {
           ImGui::PushTextWrapPos(w);
           ImGui::Text("%s", messageDescription.c_str());
        }
        ImGui::End();

        ImGui::PopStyleVar();

        if ( messageFadeAway ) {
            messageOpacity -= 0.006f;
            if ( messageOpacity <= 0 ) {
                haveToDrawMessage = false;
            }
        }
    }
}

void Gui::drawCrashMessage()
{
    if ( haveToDrawCrashMessage ) {
        ImGui::OpenPopup("Crash recovery");
        haveToDrawCrashMessage = false;
    }

    // a second boolean for not executing this part of code when no crash?

    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;

    ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
    int width = 430;
    ImGui::SetNextWindowSize( ImVec2(width,0) );
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

    if ( ImGui::BeginPopupModal("Crash recovery", NULL, window_flags) ) {
        ImGui::PushTextWrapPos(width);
        ImGui::Text( "Sorry, it looks like we've crashed last time. \n\n"
                     "This is usually caused by a wrong audio configuration. "
                     "Please, check that the sample rate and buffer size "
                     "matches your driver configuration." );

        ImGui::NewLine();
//        ImGui::Indent(25.f);

//        ImGui::Checkbox("Sound preloading",
//                        &Sounds::getInstance()->doPreloading);
//        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SOUND_PRELOADING);

//        ImGui::NewLine();

//        if ( ImGui::Button("Reset all settings") ) {
//            SessionManager::getInstance()->resetSettings();
//            SessionManager::getInstance()->loadInitSession();
//            ImGui::CloseCurrentPopup();
//        }
//        ImGui::SameLine();
        if ( ExtraWidgets::Button("Ok, I'll check that", ExtraWidgets::GREEN) ) {
            SessionManager::getInstance()->loadInitSession();

            if ( !AudioEngine::getInstance()->isConfigured() ) {
                haveToDrawAudioSettingsScreen = true;
            }

            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
}

void Gui::drawStopRecordingScreen()
{
    if ( haveToDrawStopRecordingScreen ) {
        ImGui::OpenPopup("Recording in process");
        haveToDrawStopRecordingScreen = false;
    }

    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;

    ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
    int width = 300;
    ImGui::SetNextWindowSize( ImVec2(width,0) );
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

    if ( ImGui::BeginPopupModal("Recording in process", NULL, window_flags) ) {
        ImGui::PushTextWrapPos(width);
        ImGui::Text("Please stop recording before closing.");

        ImGui::NewLine();

        if ( ImGui::Button("Ok") ) {
            ImGui::CloseCurrentPopup();
        }

        ImGui::EndPopup();
    }
}

void Gui::drawTutorial()
{
    if(haveToDrawTutorial) {
        ImGuiWindowFlags window_flags = 0;
        int w = 400;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoResize;
        //    window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ImGui::SetNextWindowPos(ImVec2(ofGetWidth()/2-w/2, 100), ImGuiCond_Appearing);

        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding,ImVec2(20,20));

        if (ImGui::Begin(tutorialCurrentPage->title.c_str() , &haveToDrawTutorial, window_flags)){
            ImGui::PushTextWrapPos(w);
            ImGui::TextWrapped("%s", tutorialCurrentPage->content.c_str());

            string buttonTitle = "Next";
            if ( tutorial.isLastPage() ) {
               buttonTitle = "Finish";
            }

            if(ImGui::Button(buttonTitle.c_str())){
                tutorialCurrentPage = tutorial.getNextPage();
                if ( tutorialCurrentPage == nullptr ) {
                    haveToDrawTutorial = false;
                }
            }

        }
        ImGui::End();

        ImGui::PopStyleVar();
   }
}

void Gui::drawContextMenu() {

    if (haveToDrawSoundContextMenu) {

        string selectedSoundPath = selectedSound->getFileName();
        string buttonText;
        string command;

        #ifdef TARGET_WIN32
        buttonText = "Show in Explorer";
        std::replace(selectedSoundPath.begin(), selectedSoundPath.end(), '/', '\\');
        command = "explorer /select," + selectedSoundPath;
        #endif

        #ifdef TARGET_OSX
        buttonText = "Show in Finder";
        command = "open -R \"" + selectedSoundPath + "\"";
        #endif

        #ifdef TARGET_LINUX
        buttonText = "Show in File Manager";
        string hoveredSoundEnclosingDirectory = ofFilePath::getEnclosingDirectory(selectedSoundPath, false);
        string script {
            "file_manager=$(xdg-mime query default inode/directory | sed 's/.desktop//g'); "
            "if [ \"$file_manager\" = \"nautilus\" ] || [ \"$file_manager\" = \"dolphin\" ] || [ \"$file_manager\" = \"nemo\" ]; "
            "then command=\"$file_manager\"; path=\"%s\"; "
            "elif [ \"$file_manager\" = \"org.gnome.Nautilus\" ]; "
            "then command=\"nautilus\"; path=\"%s\"; "
            "else command=\"xdg-open\"; path=\"%s\"; "
            "fi; "
            "$command \"$path\" &"
        };


        char buffer[ script.size() + selectedSoundPath.size() * 2 + hoveredSoundEnclosingDirectory.size() ];
        std::sprintf( buffer, script.c_str(), selectedSoundPath.c_str() , selectedSoundPath.c_str(),
                     hoveredSoundEnclosingDirectory.c_str()  );
        command = buffer;
        #endif

        //Sound context menu
        ImGui::OpenPopup("soundMenu");
        if (ImGui::BeginPopup("soundMenu")) {
            ImGui::TextDisabled("Sound Options");

            if (ImGui::MenuItem( "Mute", NULL, selectedSound->mute) ) {
                selectedSound->mute = !selectedSound->mute;
                if ( selectedSound->mute ) {
                    Units::getInstance()->muteSound(selectedSound);
                }
                ImGui::CloseCurrentPopup();
                haveToDrawSoundContextMenu = false;
            }

            ImGui::Separator();

            if (ImGui::MenuItem( buttonText.c_str(), NULL, false) ) {
                ofLog() << command;
                ofSystem(command);
                ImGui::CloseCurrentPopup();
                haveToDrawSoundContextMenu = false;
            }

            if ( selectedClusterID >= 0 ) {
                ImGui::NewLine();
                drawClusterContextMenu();
            }

            ImGui::EndPopup();
        }
    }

    if (haveToDrawClusterContextMenu) {
        ImGui::OpenPopup("clusterMenu");
        if (ImGui::BeginPopup("clusterMenu")) {
            drawClusterContextMenu();
            ImGui::EndPopup();
        } else {
            haveToDrawClusterContextMenu = false;
        }

    }
}

void Gui::drawClusterContextMenu()
{
    vector<shared_ptr<Sound>> cluster = Sounds::getInstance()->getSoundsByCluster(selectedClusterID);

    ImGui::TextDisabled("Cluster Options");
    ImGui::Dummy(ImVec2(0, 3.0f));

    ImGui::AlignTextToFramePadding();
    ImGui::Text("Name:");

    ImGui::SameLine();

    if (!isClusterContextMenuFirstOpen ) {
        string selectedClusterName = Sounds::getInstance()->clusterNames[selectedClusterID];
        strncpy( Sounds::getInstance()->clusterNameBeingEdited, selectedClusterName.c_str(), CLUSTERNAME_MAX_LENGTH - 1 );

        isClusterContextMenuFirstOpen = true;
    }

    ImGui::PushItemWidth(150);
    if (ImGui::InputText("##ClusterName", Sounds::getInstance()->clusterNameBeingEdited, 64, ImGuiInputTextFlags_EnterReturnsTrue |
                                                     ImGuiInputTextFlags_AutoSelectAll )) {

        Sounds::getInstance()->nameCluster(selectedClusterID);
        haveToDrawClusterContextMenu = false;
        haveToDrawSoundContextMenu = false;
    }
    else if (ImGui::IsItemEdited()) {
        Sounds::getInstance()->nameCluster(selectedClusterID);
    }
    isClusterContextMenuFirstOpen = ImGui::IsItemActive();
    ImGui::Separator();

    // check if all sounds in cluster are muted
    bool allMuted = true;
    for (int i = 0; i < cluster.size(); i++) {
        if (!cluster[i]->mute) {
            allMuted = false;
            break;
        }
    }

    if (ImGui::MenuItem("Mute random 10%") ) {
        vector<shared_ptr<Sound>> vecSoundsNotMuted;
        for (int i = 0; i < cluster.size(); i++) {
            if ( !cluster[i]->mute ) {
                vecSoundsNotMuted.push_back( cluster[i] );
            }
        }

        int countToMute = floor(vecSoundsNotMuted.size() * 0.1f);
        for (int i = 0; i < countToMute; i++) {
            int randomIdx = (int)ofRandom(0, vecSoundsNotMuted.size());
            vecSoundsNotMuted[randomIdx]->mute = true;
            Units::getInstance()->muteSound(vecSoundsNotMuted[randomIdx]);

            vecSoundsNotMuted.erase(
                std::remove(vecSoundsNotMuted.begin(),
                vecSoundsNotMuted.end(), vecSoundsNotMuted[randomIdx]), vecSoundsNotMuted.end());
        }

        ImGui::CloseCurrentPopup();
        haveToDrawClusterContextMenu = false;
        haveToDrawSoundContextMenu = false;
    }

    if (ImGui::MenuItem("Mute all", NULL, allMuted) ) {

        for (int i = 0; i < cluster.size(); i++) {
            cluster[i]->mute = !allMuted;

            if ( cluster[i]->mute ) {
                Units::getInstance()->muteSound(cluster[i]);
            }
        }

        ImGui::CloseCurrentPopup();
        haveToDrawClusterContextMenu = false;
        haveToDrawSoundContextMenu = false;
    }
}

void Gui::showMessage(string description, string title, bool fadeAway)
{
    haveToDrawMessage = true;
    messageTitle = title;
    messageDescription = description;
    messageOpacity = 1.0f;
    messageFadeAway = fadeAway;
}

void Gui::hideMessage()
{
    haveToDrawMessage = false;
}

void Gui::showCrashMessage()
{
    haveToDrawCrashMessage = true;
}

void Gui::showSampleLoadingScreen()
{
    haveToDrawLoadingSamplesScreen = true;
    isPreloadingFiles = true;
}

void Gui::showExitMessage()
{
    if ( Units::getInstance()->recorder.isRecording() ) {
        haveToDrawStopRecordingScreen = true;
        return;
    }

    haveToDrawExitMessage = true;
}

void Gui::showFilesNotFoundMessage()
{
    haveToDrawFilesNotFoundMessage = true;
}

void Gui::showWelcomeScreen()
{
//    if ( AudioEngine::getInstance()->isConfigured() ) {
        haveToDrawWelcomeScreen = true;
//    }
}

void Gui::startTutorial()
{
    tutorialCurrentPage = tutorial.start();
    haveToDrawTutorial = true;
}

ofFileDialogResult Gui::showSaveDialog(string defaultName, string messageName)
{
    bool f = getFullscreen();
    setFullscreen(false);
    ofFileDialogResult res = ofSystemSaveDialog(defaultName, messageName);
    setFullscreen(f);
    return res;
}

ofFileDialogResult Gui::showLoadDialog(string windowTitle, bool bFolderSelection, string defaultPath)
{
    bool f = getFullscreen();
    setFullscreen(false);
    ofFileDialogResult res = ofSystemLoadDialog(windowTitle, bFolderSelection, defaultPath);
    setFullscreen(f);
    return res;
}

bool Gui::getShowStats()
{
    return haveToDrawFPS;
}

void Gui::setShowStats(bool v)
{
    haveToDrawFPS = v;
}

bool Gui::getFullscreen()
{
    return ofGetWindowMode() == 1;
//    return isFullscreen;
}

void Gui::setFullscreen(bool v)
{
    //this happens when loading a sound map on setup()
    if ( ofGetFrameNum() == 0 && !fullscreenRequested ) {
        fullscreenRequested = true;
    } else {
        if(v != getFullscreen() ){
            ofToggleFullscreen();
        }
    }
}

void Gui::toggleFullscreen()
{
    setFullscreen( !getFullscreen() );
}

bool Gui::getShowGUI()
{
    return drawGui;
}

void Gui::setShowGUI(bool v)
{
    drawGui = v;
}

bool Gui::getShowMIDIOSCMonitor()
{
    return showMIDIOSCMonitor;
}

void Gui::setShowMIDIOSCMonitor(bool v)
{
    showMIDIOSCMonitor = v;
}

bool Gui::getUserWannaExit()
{
    return userWannaExit;
}

bool Gui::isMouseCursorHidden()
{
    return haveToHideCursor;
}

bool Gui::getFocusOnClusters()
{
    return haveToFocusOnCluster;
}

void Gui::setFocusOnClusters(bool v)
{
    haveToFocusOnCluster = v;
}

bool Gui::isAnyModifierPressed( bool includeAlt )
{
    return (ofGetKeyPressed(OF_KEY_CONTROL) ||
            ofGetKeyPressed(OF_KEY_COMMAND) ||
            ( includeAlt && ofGetKeyPressed(OF_KEY_ALT)) );
}

void Gui::keyPressed(ofKeyEventArgs & e) {

    auto &io = ImGui::GetIO();
    if ( io.WantTextInput ) {
        return;
    }

    Sounds::getInstance()->keyPressed(e);
    Units::getInstance()->keyPressed(e);
    SessionManager::getInstance()->keyPressed(e);

    bool controlOrCommand = (e.hasModifier(OF_KEY_CONTROL)||e.hasModifier(OF_KEY_COMMAND));

    ////////////////
    /// Shortcuts File menu
    ////////////////

    if(e.keycode == 'G' && controlOrCommand) {
        drawGui = !drawGui;
    }
    if(e.keycode == 'F' && controlOrCommand) {
        toggleFullscreen();
    }
    if(e.keycode == 'C' && controlOrCommand) {
        Sounds::getInstance()->toggleShowClusterNames();
    }
//    ofLog() << e.key << " " << e.keycode << " " << controlOrCommand << " " << e.hasModifier(OF_KEY_SHIFT);

    if ( e.keycode == 'S' && controlOrCommand && e.hasModifier(OF_KEY_SHIFT) ) //with shift
        SessionManager::getInstance()->saveAsNewSession();
    else if ( e.keycode == 'S' && controlOrCommand && !e.hasModifier(OF_KEY_SHIFT) )
        if ( !SessionManager::getInstance()->isDefaultSession ) {
            SessionManager::getInstance()->saveSession();
        } else {
            showMessage("Cannot save default session, use \"Save as\" instead");
        }
    else if ( e.keycode == 'N' && controlOrCommand && !e.hasModifier(OF_KEY_SHIFT) )
        newSession();
    else if ( e.keycode == 'O' && controlOrCommand && !e.hasModifier(OF_KEY_SHIFT) ) {
        SessionManager::datasetLoadOptions opts;
        opts.method = "GUI";
        SessionManager::getInstance()->loadSession(opts);
    }

    if ( e.keycode == 'B' && controlOrCommand && e.hasModifier(OF_KEY_SHIFT) ) {
        hideMainMenu = !hideMainMenu;

        if ( hideMainMenu && !hideMainMenuWarningShown ) {
            showMessage("Main menu hidden. Press Ctrl+Shift+B to show it again.");
            hideMainMenuWarningShown = true;
        }
    }

    ////////////////
    /// Shortcut Units
    ////////////////

    if ( controlOrCommand && e.keycode == '1' ) {
        Units::getInstance()->addUnit( shared_ptr<Unit>(new ExplorerUnit()) );
    }
    if ( controlOrCommand && e.keycode == '2' ) {
        Units::getInstance()->addUnit( shared_ptr<Unit>(new SequenceUnit()) );
    }
    if ( controlOrCommand && e.keycode == '3' ) {
        Units::getInstance()->addUnit( shared_ptr<Unit>(new ParticleUnit()) );
    }
    if ( controlOrCommand && e.keycode == '4' ) {
        Units::getInstance()->addUnit( shared_ptr<Unit>(new MorphUnit()) );
    }
    if ( controlOrCommand && e.keycode == '5' ) {
        Units::getInstance()->addUnit( shared_ptr<Unit>(new OscUnit()) );
    }

    if ( !controlOrCommand ) {
        if ( e.keycode == '1' ) {
            Units::getInstance()->selectUnitAtIndex(0);
        } else if ( e.keycode == '2' ) {
            Units::getInstance()->selectUnitAtIndex(1);
        } else if ( e.keycode == '3' ) {
            Units::getInstance()->selectUnitAtIndex(2);
        } else if ( e.keycode == '4' ) {
            Units::getInstance()->selectUnitAtIndex(3);
        } else if ( e.keycode == '5' ) {
            Units::getInstance()->selectUnitAtIndex(4);
        } else if ( e.keycode == '6' ) {
            Units::getInstance()->selectUnitAtIndex(5);
        } else if ( e.keycode == '7' ) {
            Units::getInstance()->selectUnitAtIndex(6);
        } else if ( e.keycode == '8' ) {
            Units::getInstance()->selectUnitAtIndex(7);
        } else if ( e.keycode == '9' ) {
            Units::getInstance()->selectUnitAtIndex(8);
        }
    }

    if ( controlOrCommand && e.keycode == 'M' ) {
        Units::getInstance()->toggleMuteAll();
    }
    if ( controlOrCommand && e.keycode == 'A' ) {
        Units::getInstance()->toggleActiveAll();
    }
}

void Gui::keyReleased(ofKeyEventArgs &e)
{
    auto &io = ImGui::GetIO();
    if ( io.WantTextInput ) {
        return;
    }

    Sounds::getInstance()->keyReleased(e);


}

void Gui::mousePressed(ofVec2f p, int button) {
    //this happens only when a popup is opened but the mouse is not hovering it
    if (isMouseHoveringGUI() && !ImGui::IsAnyWindowHovered()) {
        // I think there is no need for these booleans
        // using openPopup like Units.cpp might be enough
        haveToDrawSoundContextMenu = false;
        haveToDrawClusterContextMenu = false;
        haveToDrawMarkerContextMenu = false;
        isClusterContextMenuFirstOpen = false;
        selectedSlider = nullptr;

        // this feels like a workaround
        // maybe gui should manage input to other modules ??
        // right now ui is managing keyboard input
        Units::getInstance()->mousePressed(p.x, p.y, button);
        UberSlider::clickOutsidePopup();
    }

    if ( !isMouseHoveringGUI() &&
         isAnyModifierPressed(false) ) {

        if ( button == 0 ) {
            Units::getInstance()->selectUnitAtPosition(p);
        } else if ( button == 1 ) {
            shared_ptr<Unit> selectedUnit = Units::getInstance()->getSelectedUnit();
            if ( selectedUnit != nullptr ) {
                selectedUnit->setSolo( !selectedUnit->isSolo() );
            }
        } else if ( button == 2 ) {
            shared_ptr<Unit> selectedUnit = Units::getInstance()->getSelectedUnit();
            if ( selectedUnit != nullptr ) {
                selectedUnit->setMute( !selectedUnit->isMuted() );
            }
        }
    }
}

void Gui::mouseReleased(ofVec2f p, int button)
{
    if (!isMouseHoveringGUI()) {

        if (button == 2 &&
            !Sounds::getInstance()->soundIsBeingMoved &&
            !isAnyModifierPressed()) {

            shared_ptr<Sound> hoveredSound = Sounds::getInstance()->getHoveredSound();

            if ( hoveredSound != nullptr ) {
                haveToDrawSoundContextMenu = true;
                if (!isClusterContextMenuFirstOpen) {
                    selectedClusterID = Sounds::getInstance()->getHoveredClusterID();
                }
                selectedSound = hoveredSound;
            }
            else haveToDrawSoundContextMenu = false;


            if (hoveredSound == nullptr && Sounds::getInstance()->getHoveredClusterID() >= 0) {
                haveToDrawClusterContextMenu = true;
                if (!isClusterContextMenuFirstOpen) {
                    selectedClusterID = Sounds::getInstance()->getHoveredClusterID();
                }
            }
            else haveToDrawClusterContextMenu = false;

        }
        else {
            haveToDrawSoundContextMenu = false;
            haveToDrawClusterContextMenu = false;
            isClusterContextMenuFirstOpen = false;
        }
    }
}

void Gui::mouseScrolled(ofVec2f p, float scrollX, float scrollY)
{
    if ( scrollY != 0 && isAnyModifierPressed(false) ) {
        shared_ptr<Unit> selectedUnit = Units::getInstance()->getSelectedUnit();
        if ( selectedUnit != nullptr ) {
            static const float step = 0.01f;

            float v = selectedUnit->getVolume() + step * scrollY;
            selectedUnit->setVolume( v );
        }
    }
}

bool Gui::isMouseHoveringGUI() {
    return ImGui::GetIO().WantCaptureMouse;
}

void Gui::hideIdleMouse(int idleTime) {

    if ( ofGetMouseX() == previousMouseX && ofGetMouseY() == previousMouseY ) {
        if ( mouseIdleStartTime == -1 ) {
            mouseIdleStartTime = ofGetElapsedTimef();
        }

        haveToHideCursor = ofGetElapsedTimef() - mouseIdleStartTime > idleTime;
    }
    else {
        mouseIdleStartTime = -1;
        haveToHideCursor = false;
    }

    previousMouseX = ofGetMouseX();
    previousMouseY = ofGetMouseY();
}

void Gui::drawSegmentationTool() {
    if(haveToDrawSegmentationTool) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoNav;
        window_flags |= ImGuiWindowFlags_AlwaysAutoResize;

        ast->checkProcess();

        float win_width = ofGetWidth() * 0.9f;
        float win_height = ofGetHeight() * 0.6f;
        int plot_width = win_width * 0.75;
        int plot_height = win_height * 0.9;

        ImGui::SetNextWindowPos( ImVec2(ofGetWidth() * 0.5f - (win_width * 0.5f),
                                        ofGetHeight() * 0.5f - (win_height * 0.5f)), ImGuiCond_Appearing );
        ImGui::SetNextWindowSize(ImVec2( ofGetWidth() * 0.9f, plot_height * 1.2f ));

        string title = "Audio slicer - " + ofFilePath::getFileName(ast->dirPath);

        if (ImGui::Begin(title.c_str(), &haveToDrawSegmentationTool, window_flags)){

            static double min;
            static double max;
            min = ofClamp(min, 0, ast->sample->length - 1);
            max = ofClamp(max, 0, ast->sample->length - 1);

            if (ast->reset_zoom) {
                ImPlot::SetNextAxisToFit(ImAxis_X1);
                ast->reset_zoom = false;
            }

            ImGui::Columns(2, "slicer columns", false);//, "columns", false);
            ImGui::SetColumnWidth(0, plot_width);

            if( ImPlot::BeginPlot("Audio File", ImVec2(plot_width, plot_height), ImPlotFlags_CanvasOnly) ) {

                ImPlot::SetupAxes("X", "Y", ImPlotAxisFlags_NoDecorations, ImPlotAxisFlags_NoDecorations);
                ImPlot::SetupAxisLimits(ImAxis_X1, 0, ast->sample->length - 1);
                ImPlot::SetupAxisLinks(ImAxis_X1, &min, &max);
                ImPlot::SetupAxisLimits(ImAxis_Y1, -1, 1, ImGuiCond_Always);

                int sampleLength = ast->sample->length;
                int plotLimitsMin = ofClamp( (int)ImPlot::GetPlotLimits().X.Min, 0, sampleLength - 1);
                int plotLimitsMax = ofClamp( (int)ImPlot::GetPlotLimits().X.Max, 0, sampleLength - 1);
                int plotLimitsSize = ofClamp ( plotLimitsMax - plotLimitsMin, 1, sampleLength );

                int base_stride = (sampleLength / (int)ImPlot::GetPlotSize().x) / 8;
                int divider = ast->getDivider(sampleLength, plotLimitsSize);
                int stride = MAX(base_stride / divider, 1);
                int start = ofClamp( (plotLimitsMin / stride) * stride, 0, sampleLength - 1 );
                int size = plotLimitsSize / stride;

                ImPlot::PlotLine("Audio", &ast->plot_indexes.data()[start], &ast->sample->buffer[0][start], size, 0, sizeof(float) * stride);

                if (ast->markers.size()) {
                    ImPlotDragToolFlags flags = ImPlotDragToolFlags_Delayed | ImPlotDragToolFlags_NoInputs;
                    float markerOpacity = 0.2f;
                    //ctrl to move markers
                    if ( isAnyModifierPressed(false) ) {
                        flags = ImPlotDragToolFlags_Delayed;
                        markerOpacity = 0.5f;
                        if (ImGui::IsMouseClicked(1)) {
                            ast->getHoveredMarker();
                            ast->newMarkerPosition = ImPlot::PixelsToPlot(ofGetMouseX(), ofGetMouseY()).x;
                            if (ast->hoveredMarker >= 0 || ImPlot::IsPlotHovered()) {
                                haveToDrawMarkerContextMenu = true;
                            }
                        }
                    }

                    for (int i = 0; i < ast->markers.size(); i++) {

                        if( ImPlot::DragLineX(i, &ast->markers[i], ImVec4(1,1,1,markerOpacity), 1, flags) ) {
                            ast->needToSort = true;
                            ast->markers[i] = ofClamp(ast->markers[i], 0, ast->sample->length); // prevent dragging markers outside waveform limits
                        }
                        else {
                            if ( ast->needToSort && !ImGui::IsMouseDown(0)) {
                                sort( ast->markers.begin(), ast->markers.end() );
                                ast->needToSort = false;
                            }
                        }
                    }

                    if (haveToDrawMarkerContextMenu) {
                        ImGui::OpenPopup("markerMenu");
                        if (ImGui::BeginPopup("markerMenu")) {
                            if (ast->hoveredMarker >= 0) {
                                if (ImGui::MenuItem("Remove marker") ) {
                                    ast->markers.erase(ast->markers.begin() + ast->hoveredMarker);
                                    ImGui::CloseCurrentPopup();
                                    haveToDrawMarkerContextMenu = false;
                                }
                            }
                            else {
                                if (ImGui::MenuItem("Add marker") ) {
                                    ast->markers.push_back(ast->newMarkerPosition);
                                    sort( ast->markers.begin(), ast->markers.end() );
                                    ImGui::CloseCurrentPopup();
                                    haveToDrawMarkerContextMenu = false;
                                }
                            }
                            ImGui::EndPopup();
                        }
                    }

                    if (ImPlot::IsPlotHovered() &&
                            ImGui::IsMouseClicked(0) && !ImGui::IsMouseDragging(0) && !ImGui::IsMouseDoubleClicked(0) &&
                            !isAnyModifierPressed(false)) {

                        int previousMarkerPlayed = ast->marker_played;

                        for (int i = 1; i < ast->markers.size(); i++) {
                            if ( ImPlot::GetPlotMousePos().x < ast->markers[i] ) {
                                ast->marker_played = i - 1;
                                break;
                            }
                        }

                        if ( !ast->player.isPlaying() || ast->marker_played != previousMarkerPlayed ) {

                            int start = ast->markers[ast->marker_played];
                            int end = ast->markers[ast->marker_played + 1];
                            int length = (end - start);

                            ast->player.load(ast->sample, start, length, ast->sample->channels);
                            ast->player.play(1.0f);
                        } else {
                            ast->player.stop();
                        }
                    }

                    if (ast->player.isLoaded()) {

                        int start = ast->markers[ast->marker_played];

                        float playerPositionInSamples = start + ast->player.getPlayerPosition() * ast->player.getSampleLength();
                        bool isMarkerPlaying = ast->player.isPlaying()
                                               && playerPositionInSamples >= ast->markers[ast->marker_played]
                                               && playerPositionInSamples < ast->markers[ast->marker_played + 1];

                        if (isMarkerPlaying) {

                            ImVec2 rect_min = ImPlot::PlotToPixels(ImPlotPoint(ast->markers[ast->marker_played], 1));
                            ImVec2 rect_max = ImPlot::PlotToPixels(ImPlotPoint(ast->markers[ast->marker_played + 1], -1));
                            ImPlot::PushPlotClipRect();
                            ImPlot::GetPlotDrawList()->AddRectFilled(rect_min, rect_max, IM_COL32(255,255,255,32));
                            ImVec2 p1 = ImPlot::PlotToPixels(ImPlotPoint(playerPositionInSamples, 1));
                            ImVec2 p2 = ImPlot::PlotToPixels(ImPlotPoint(playerPositionInSamples, -1));
                            ImPlot::GetPlotDrawList()->AddLine(p1, p2, IM_COL32(255,255,255,64));
                            ImPlot::PopPlotClipRect();

                        }
                    }
                }

                // invisible points for proper fitting
                int lineX[] {0, static_cast<int>(ast->sample->length - 1)};
                int lineY[] {1, -1};
                ImPlot::SetNextLineStyle(ImVec4(0, 0, 0, 0));
                ImPlot::PlotLine("##", lineX, lineY, 2);

                ImPlot::EndPlot();
            }

            ImGui::TextDisabled("Need help with controls?");
            if (ImGui::IsItemHovered())
            {
                ImGui::BeginTooltip();
                ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
                ImGui::TextUnformatted("Mouse scroll to zoom \n"
                                       "Mouse drag to move around \n"
                                       "Click to play segment \n"
                                       "Double click to zoom out \n\n"
                                       "Ctrl/Cmd + drag to move markers \n"
                                       "Ctrl/Cmd + right click to add/remove markers");
                ImGui::PopTextWrapPos();
                ImGui::EndTooltip();
            }

            int seconds = ast->sample->length / ast->sample->fileSampleRate;
            int minutes = seconds / 60;
            seconds -= minutes * 60;
            string length = "Duration: " + ofToString(minutes, 0, 2, '0') + ":" + ofToString(seconds, 0, 2, '0');

            ImGui::SameLine(plot_width - ImGui::GetFontSize() * length.size() + 90);

            ExtraWidgets::Text( length, true );

            ImGui::NextColumn();

            ImGui::SliderFloat("Sensitivity", &ast->sensitivity, 0.2f, 0.99f, "%.2f");
            if ( ImGui::IsItemDeactivatedAfterEdit() ) {
                ast->sensitivity = ofClamp(ast->sensitivity,0,1);
                ast->delta = 1 - ast->sensitivity;
                ast->doSlice();
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SLICER_SENSITIVITY);

            ImGui::NewLine();

            if ( ImGui::TreeNode("Advanced settings") ) {
                if ( ImGui::TreeNode("Onset detection parameters") ) {
                    ImGui::NewLine();

                    ImGui::PushItemWidth(130);
                    if ( ImGui::InputFloat("Window max", &ast->window_max, 0.01f, 0.05f, "%.2f") ){
                        ast->window_max = ofClamp(ast->window_max,0,1);
                    }
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SLICER_PARAMETERS);

                    if ( ImGui::InputFloat("Window avg.", &ast->window_avg, 0.1f, 0.5f, "%.2f") ) {
                        ast->window_avg = ofClamp(ast->window_avg,0.1,1);
                    }
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SLICER_PARAMETERS);

                    if ( ImGui::InputFloat("Delta", &ast->delta, 0.01f, 0.05f, "%.2f") ) {
                        ast->delta= ofClamp(ast->delta,0,1);
                        ast->sensitivity = 1 - ast->delta;
                    }
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SLICER_PARAMETERS);

                    ImGui::PopItemWidth();

                    ImGui::Checkbox("Backtrack", &ast->backtrack);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SLICER_BACKTRACK);

                    ImGui::NewLine();

                    if ( ExtraWidgets::Button("Process onsets", !ast->isProcessing) ) {
                        ast->doSlice();
                    }

                    ImGui::NewLine();
                    ImGui::TreePop();
                }
                if ( ImGui::TreeNode("Segmentation parameters") ) {
                    ImGui::NewLine();
                    ImGui::PushItemWidth(130);
                    ImGui::InputInt("Fade (samples)", &ast->fade);
                    ImGui::PopItemWidth();
                    ast->fade = ofClamp(ast->fade,0,2000);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SLICER_FADE);

                    ImGui::Checkbox("Normalize", &ast->normalize);
                    if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::SLICER_NORMALIZE);
                    ImGui::TreePop();
                }
                ImGui::TreePop();
            }

            ImGui::NewLine();

            if ( ExtraWidgets::Button("Create sound map", ExtraWidgets::GREEN, !ast->isProcessing, ImVec2(-1,40)) ) {
                ofFileDialogResult dialogResult = showLoadDialog("Select destination folder",
                                                                 true,
                                                                 ofFilePath::getUserHomeDir());

                if(dialogResult.bSuccess) {
                    ast->resultPath = ofFilePath::addTrailingSlash( dialogResult.getPath() );
                    ast->resultPath += ofFilePath::removeExt( ofFilePath::getFileName(ast->dirPath) ) + " - Slices";
                    ofDirectory dir(ast->resultPath);
                    if(!dir.exists()){
                        dir.create(true);
                    }

                    ast->saveSlices();
                }
            }

            ImGui::TextDisabled("(%d segments)", ast->markers.size());

            ImGui::Columns();
            ImGui::End();
        }

        if ( ast->statusState == "doneAndSave" ) {
            haveToDrawSegmentationTool = false;
            adr.dirPath = ast->resultPath;
            newSession();
        }
        else if ( ast->statusState == "errorLoadingFile" ) {
            haveToDrawSegmentationTool = false;
            showMessage( "The selected audio file could not be loaded.\n"
                         "If the format is mp3 please convert it to wav and try again.", "Error" );
        }
    }
}

void Gui::drawAudioSliceWindow(){
    if ( ast->isProcessing ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoScrollbar;
        window_flags |= ImGuiWindowFlags_NoResize;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoMove;

        ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
        ImGui::SetNextWindowSize(ImVec2(300,0));
        ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

        if ( ast->isRunning() ) {
            if (ast->statusState == "slicingPreprocessing" ||
                ast->statusState == "slicingProcessing") {
                ImGui::OpenPopup("Slicing file", ImGuiPopupFlags_NoOpenOverExistingPopup);

                if(ImGui::BeginPopupModal("Slicing file", NULL, window_flags)) {
                    float progress = (float)ast->statusSliced / (float)ast->statusCountMarkers;
                    ImGui::ProgressBar(progress);
                    if ( ast->statusState == "slicingPreprocessing" ) {
                        ImGui::Text("Preprocessing segments...");
                    } else {
                        ImGui::Text("Creating samples...");
                    }

                    ImGui::EndPopup();
                }
            } else {
                ImGui::OpenPopup("Processing file", ImGuiPopupFlags_NoOpenOverExistingPopup);
                if(ImGui::BeginPopupModal("Processing file", NULL, window_flags)){
                    ImGui::Text("Finding audio onsets, please wait...");

                    ImGui::EndPopup();
                }
            }
        } else {

            /*ImGui::OpenPopup("Error slicing");
            if(ImGui::BeginPopupModal("Error slicing", NULL, window_flags)){
                ImGui::TextWrapped("Unexpected error ocurred while slicing the recording.");
                ImGui::NewLine();
                ImGui::TextWrapped("Try with a shorter recording or less slices. Make sure "
                            "the path to the file doesn't contain special characters.");
                 ImGui::NewLine();

                if ( ImGui::Button("Ok, bummer") ) {
                    ast->isProcessing = false;
                    haveToDrawSegmentationTool = false;
                }

                ImGui::EndPopup();
            }*/
        }
    }
}

void Gui::drawMapIsDone()
{
    if ( haveToDrawMapIsDone ) {
        ImGui::OpenPopup("Good news, commander");
        haveToDrawMapIsDone = false;
    }

    ImGuiWindowFlags window_flags = 0;
    window_flags |= ImGuiWindowFlags_NoMove;
    window_flags |= ImGuiWindowFlags_NoResize;
    window_flags |= ImGuiWindowFlags_NoCollapse;

    ImVec2 center(ImGui::GetIO().DisplaySize.x * 0.5f, ImGui::GetIO().DisplaySize.y * 0.5f);
    int width = 210;
    ImGui::SetNextWindowSize( ImVec2(width,0) );
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

    if ( ImGui::BeginPopupModal("Good news, commander", NULL, window_flags) ) {
        ImGui::PushTextWrapPos(width);

        string strSuccess = "\nHooray! Your map is done. \n\n";

        if ( !adr.failFiles.empty() ) {
            strSuccess += "\n:( Some files couldn't be processed though: ";
            for ( string s : adr.failFiles ) {
                strSuccess += "\n" + s;
            }
        }

        ImGui::Text( strSuccess.c_str() );

        if ( ExtraWidgets::Button("Let's set cluster settings", ExtraWidgets::GREEN) ) {
            ImGui::CloseCurrentPopup();
            haveToDrawClusteringSettingsScreen = true;
        }

        ImGui::NewLine();

        ImGui::EndPopup();
    }
}
