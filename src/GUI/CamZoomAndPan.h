#pragma once

#define DEFAULT_ZOOM_DISTANCE 665
#define ZOOM_LIMIT 50
#define ZOOM_VELOCITY 20
#define ZOOM_OUT_LIMIT 0.35
#define ZOOM_IN_LIMIT 60.0

#include "ofMain.h"
#include "ofxImGui.h"
//
//  2DCam.h
//  This addon is for navigating through a 3D scene using an orthographic projection, so that it looks like 2D, or any 2D scene.
//  This should be used instead of ofEasyCam when using an ortho projection or when drawing 2D, as ofEasyCam's controls are not well suited for such task.
//  The user can change the position from where the scene is being looked. TOP, BOTTOM, LEFT, RIGHT, FRONT (default), BACK.
//
//
//  Created by Roy Macdonald on 27-06-15.
//
//  AudioStellar modified this


#include "ofMain.h"


class CamZoomAndPan {
public:
    static const int MOVE_DELTA =  7;
    static constexpr float ZOOM_DELTA = 0.075f;
    enum LookAt{
        OFX2DCAM_FRONT =0,
        OFX2DCAM_BACK,
        OFX2DCAM_LEFT,
        OFX2DCAM_RIGHT,
        OFX2DCAM_TOP,
        OFX2DCAM_BOTTOM
    };

    static CamZoomAndPan* getInstance();
    ~CamZoomAndPan();

    void init();

    virtual void begin(ofRectangle viewport = ofGetCurrentViewport());
    virtual void end();
    void reset();
    //-------   mouse
    void enableMouseInput(bool e = true);
    void disableMouseInput();
    bool getMouseInputEnabled();

    void mousePressed(ofMouseEventArgs & mouse);
    void mouseReleased(ofMouseEventArgs & mouse);
    void mouseDragged(ofMouseEventArgs & mouse);
    void mouseScrolled(ofMouseEventArgs & mouse);

    void mouseMoved(ofMouseEventArgs & mouse);
    void keyPressed(ofKeyEventArgs & keys);
    void keyReleased(ofKeyEventArgs & keys);

    bool isSpaceBarPressed = false;

    //-------   getters/setters

    ofVec3f getTranslation(){return translation;}

    bool getYFlipped(){return bFlipY;}
    float getScale(){return scale;}

    void setScale(float s);
    void setFlipY(bool bFlipped);
	void setTranslation(ofVec3f t);

    void moveTo(ofVec3f to);
    void moveBy( ofVec3f delta ); //renombrar moveCam
    void moveLeft( float delta = MOVE_DELTA );
    void moveRight( float delta = MOVE_DELTA );
    void moveUp( float delta = MOVE_DELTA );
    void moveDown( float delta = MOVE_DELTA);

    void zoomTo(float to);
    void zoomBy(float delta = ZOOM_DELTA);
    void zoomIn(float delta = ZOOM_DELTA);
    void zoomOut(float delta = ZOOM_DELTA);
    void rotateTo(ofVec3f delta);


    void setLookAt(LookAt l);
    LookAt getLookAt();

    void update();
    void drawDebug();

    void setDragSensitivity(float s);
    float getDragSensitivity(){return dragSensitivity;}

    void  setScrollSensitivity(float s);
    float getScrollSensitivity(){return scrollSensitivity;}

    void setDrag(float drag);
    float getDrag() const;

    void setNearClip(float nc);
    float getNearClip(){return nearClip;}

    void setFarClip(float fc);
    float getFarClip(){return farClip;}

    void setOverrideMouse(bool b);
    bool isMouseOverride(){return bMouseOverride;}
    //-------   parameters
    ofParameterGroup parameters;

    //-------   utils
    ofVec3f screenToWorld(ofVec3f screen);
	ofVec3f worldToScreen(ofVec3f world);
    
    void resetView();

	void save(string path);
	bool load(string path);

    int windowOriginalWidth;
    int windowOriginalHeight;
protected:

    void update(ofEventArgs & args);

    void updateMouse();
    void enableMouseListeners(bool e = true);
    void enableMouseInputCB(bool &e);

    ofMouseEventArgs lastMouseDragged, lastMousePressed, lastMouseReleased, lastMouseScrolled;


    ofRectangle viewport;
    ofVec3f move;
    float currentZ = 0;

    ofVec3f orientation;


    ofParameter<float> scale;

    ofParameterGroup protectedParameters;
    ofParameter<ofVec3f>translation;
    ofParameter<bool> bEnableMouse,bFlipY;
    ofParameter<float> dragSensitivity, scrollSensitivity, drag, farClip, nearClip;

    bool bApplyInertia;
    bool bDoTranslate;
    bool bDoScale;
    bool bDoScrollZoom;
    bool bMouseInputEnabled;
    bool bDistanceSet;
    bool bEventsSet;
    bool bMouseOverride;

    bool bNotifyMouseDragged, bNotifyMousePressed, bNotifyMouseReleased, bNotifyMouseScrolled;
    bool bMouseListenersEnabled;

    float scaleToUpdate;
    ofVec2f prevMouse, centerPointToUpdate;
    ofVec3f translationToUpdate;

    ofVec2f mouseVel;

//    bool isManuallyMoving = false;


    ofMatrix4x4 orientationMatrix;

    unsigned long lastTap;

    ofParameter<int> lookAt;

    static ofMatrix4x4 FM, BM, LM, RM, TM, BoM;


private:
    static CamZoomAndPan* instance;
    CamZoomAndPan();

};
