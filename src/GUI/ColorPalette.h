#pragma once

#include "ofMain.h"
#include "ofxJSON.h"

class ColorPalette {

private:
    
    #define PALETTE_NAME_MAX_LENGTH 64
    #define DEFAULT_PALETTE "Classic"
    

    ColorPalette();
    
    static ColorPalette* instance;
    
    unordered_map<string, vector<ofFloatColor>> palettes;
    
    unsigned int colorCount = 11;
        
    vector<unsigned int> defaultColors = { 0x1f78b4,
        0xff7f00,
        0xfdbf6f,
        0x9e1213,
        0xfb9a99,
        0x4325af,
        0xa6cee3,
        0x0991af,
        0xcab2d6,
        0xa58ac2,
        0xffff99,
        0xc7c7c7 //noiseColor
    };
    
    const ofColor noiseColor = ofColor(0xc7c7c7);

    string selectedColorPalette = "";

public:
    
    static ColorPalette* getInstance();
        
    ofFloatColor getColor(int index);

    Json::Value getDefaultPalette();
    
    void newPalette();
    void deletePalette(string name);
    void renamePalette();
    void duplicatePalette(bool enableRename);
    void duplicatePalette();
    void randomizePalette();

    void renameMode(bool renaming);
    
    void load(Json::Value jsonData);
    Json::Value save();
    void revert();
    void revert(string name);

    bool paletteExists(string name);
        
    unordered_map<string, vector<ofFloatColor>>& getPalettes();
    
    string selectedPaletteForEditing = "";
    
    int selectedColorForEditing = 0;
    
    bool isRenamingPalette = false;

    char newPaletteName[PALETTE_NAME_MAX_LENGTH] = "\0";
    
    bool paletteEdited = false;
    
    string selectedPaletteNext = "";
    
    bool renameModeKeyboardFocus = false;

};
