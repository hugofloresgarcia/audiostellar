#include "UberSlider.h"
#include "ofxImGui.h"

UberSlider::UberSlider(const string &name, const float &v, const float &min, const float &max, const Tooltip::tooltip *tooltip)
    : UberControl (name, v, min, max, tooltip)
{

}

UberSlider::UberSlider(ofParameter<float> &parameter, const Tooltip::tooltip *tooltip)
    : UberControl (parameter, tooltip)
{

}

UberSlider::UberSlider() : UberControl ()
{

}

void UberSlider::draw()
{
    float tmpRef = get();
    if (small) ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.f,0.6f));
    
    ImGuiSliderFlags sliderFlags = 0;
    if (log) sliderFlags |= ImGuiSliderFlags_Logarithmic;

    string finalFormat = format;
    if ( !maxFormat.empty() && get() == getMax() ) {
        finalFormat = maxFormat;
    } else if ( !minFormat.empty() && get() == getMin() ) {
        finalFormat = minFormat;
    }
    
    bool sliderState = ImGui::SliderFloat( ofxImGui::GetUniqueName(getName()),
                                         (float *)&tmpRef,
                                         getMin(),
                                         getMax(),
                                         finalFormat.c_str(),
                                         sliderFlags);

    if (small) ImGui::PopStyleVar();

    addUberFeatures(sliderState, tmpRef);
}
