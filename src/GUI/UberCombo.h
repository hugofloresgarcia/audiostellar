#pragma once

#include "ofMain.h"
#include "UberControl.h"

class UberCombo : virtual public UberControl
{
private:
    vector<string> values;

public:
    UberCombo( const std::string& name,
               const vector<string> values,
               const Tooltip::tooltip * tooltip,
               const int defaultValueIndex = 0);

    void draw();

    int getValueAsIndex(float v = -1);
    float indexToValue(int i);
};
