#include "UberCombo.h"

int UberCombo::getValueAsIndex(float v)
{
    if ( v == -1 ) {
        v = get();
    }

    float segmentSize = 1.f / values.size();
    int intIndex = floor( v/segmentSize );
    return MIN(intIndex, values.size()-1);
}

float UberCombo::indexToValue(int i)
{
    float segmentSize = 1.f / values.size();
    return segmentSize * i;
}

UberCombo::UberCombo(const string &name, const vector<string> values,
                     const Tooltip::tooltip *tooltip, const int defaultValueIndex) :
    UberControl(name, 0.f, 0.f, 1.f, tooltip)
{
    assert(values.size() > 0);
    this->values = values;

    float defaultValue = indexToValue(defaultValueIndex);
    setDefaultValue( defaultValue );
    set(defaultValue);
}

void UberCombo::draw()
{
    auto result = false;
    auto tmpRef = get();
    if (ImGui::BeginCombo(getName().c_str(),
                          values[getValueAsIndex()].c_str()))
    {
        for (size_t i = 0; i < values.size(); ++i)
        {
            bool selected = (i == getValueAsIndex());
            if (ImGui::Selectable(values[i].c_str(), selected))
            {
                tmpRef = indexToValue(i);
                result = true;
            }
            if (selected)
            {
                ImGui::SetItemDefaultFocus();
            }
        }

        ImGui::EndCombo();
    }
    if (result)
    {
        set(tmpRef);
    }

    addUberFeatures(result, tmpRef);
}
