#pragma once

#include "ofMain.h"
#include "UberControl.h"
#include "ofxImGui.h"
/**
 * @brief The UberButton class
 *
 * This is the button version of UberControl
 */
class UberButton : virtual public UberControl
{
public:
    UberButton( const std::string& name, const Tooltip::tooltip * tooltip );
    UberButton( ofParameter<float>& parameter, const Tooltip::tooltip * tooltip );
    UberButton();

    enum Style { Small, Normal, Big };

    Style style = Normal;
    ImVec2 size = ImVec2(0,0);

    void draw(bool disabled = false);
};
