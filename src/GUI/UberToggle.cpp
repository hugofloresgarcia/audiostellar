#include "UberToggle.h"
#include "../Servers/MidiServer.h"

UberToggle::UberToggle(const string &name, const Tooltip::tooltip *tooltip, const float &v)
    : UberControl (name, v, 0.f, 1.f, tooltip)
{
    //I don't think this constructor is working
    UberToggle();
}

UberToggle::UberToggle(ofParameter<float> &parameter, const Tooltip::tooltip *tooltip)
    : UberControl (parameter, tooltip)
{
    UberToggle();
}

UberToggle::UberToggle() : UberControl ()
{

}

void UberToggle::setActive(bool active)
{
    if ( active ) {
        set(1.0f);
    } else {
        set(0.f);
    }

    MidiServer::getInstance()->broadcastUberControlChange(this);
    OscServer::getInstance()->broadcastUberControlChange(this);
}

bool UberToggle::isActive()
{
    return get() >= 1.f;
}

void UberToggle::setStyle(Style s)
{
    style = s;
}

UberToggle::Style UberToggle::getStyle()
{
    return style;
}

void UberToggle::setSmall(bool s)
{
    small = s;
}

bool UberToggle::getSmall()
{
    return small;
}

void UberToggle::setColor(ImVec4 c)
{
    color = c;
}

ImVec4 UberToggle::getColor()
{
    return color;
}

void UberToggle::draw()
{
    float tmpRef = get();
    bool buttonState = false;

    if ( style == BUTTON ) {
        if ( tmpRef >= 1.0f ) {
            if ( currentPalette == BLUE_DEFAULT ) {
                ImGui::PushStyleColor(ImGuiCol_Button, color);
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
                ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1.f, 1.0f, 1.f, 1.f));
            } else { //ignore color use palette
                ImGui::PushStyleColor(ImGuiCol_Button, colors[currentPalette][2]);
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, colors[currentPalette][2]);
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, colors[currentPalette][2]);
                ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0,0,0,1));
            }

            if ( small ) {
                buttonState = ImGui::SmallButton( ofxImGui::GetUniqueName(getName()) );
            } else {
                buttonState = ImGui::Button( ofxImGui::GetUniqueName(getName()) );
            }

            ImGui::PopStyleColor(4);
        } else {
            if ( currentPalette != BLUE_DEFAULT ) {
                ImGui::PushStyleColor(ImGuiCol_Button, colors[currentPalette][0]);
                ImGui::PushStyleColor(ImGuiCol_ButtonHovered, colors[currentPalette][1]);
                ImGui::PushStyleColor(ImGuiCol_ButtonActive, colors[currentPalette][2]);
            }
            if ( small ) {
                buttonState = ImGui::SmallButton( ofxImGui::GetUniqueName(getName()) );
            } else {
                buttonState = ImGui::Button( ofxImGui::GetUniqueName(getName()) );
            }
            if ( currentPalette != BLUE_DEFAULT ) {
                ImGui::PopStyleColor(3);
            }
        }
    } else {
        bool foo = tmpRef >= 1.0f;
        buttonState = ImGui::Checkbox( ofxImGui::GetUniqueName(getName()), &foo );
    }

    if ( buttonState ) {
        if ( tmpRef >= 1.0f ) {
            tmpRef = 0.f;
        } else {
            tmpRef = 1.f;
        }
    }

    addUberFeatures(buttonState, tmpRef);
}
