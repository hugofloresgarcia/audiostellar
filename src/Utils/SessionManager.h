#pragma once

#include "ofMain.h"
#include "Utils.h"
#include "ofxJSON.h"
#include "../Units/Units.h"
#include "../Sound/Sounds.h"
#include "../Servers/MidiServer.h"
#include "ofxImGui.h"
#include "../Servers/OscServer.h"
#include "../GUI/UI.h"
#include "../Sound/AudioEngine.h"
#include "../Sound/Voices.h"

class Units;
class MidiServer;

class SessionManager {

private:
    SessionManager();
    static SessionManager* instance;

    Units * units = nullptr;
    Sounds * sounds = nullptr;
    MidiServer * midiServer = nullptr;
    AudioEngine * audioEngine = nullptr;

    const string STRING_ERROR = "Error";
    const int RECENT_FILES_MAX = 10;

    //SESSION
    struct datasetResult{
        bool success;
        string status;
        ofxJSONElement data;
    };

    string jsonFilename = "";
    ofxJSONElement * jsonFile = nullptr;

    string userSelectJson();
    datasetResult * validateDataset(string datasetPath);

    bool sessionLoaded = false;

    //STELLAR FILE
    string SETTINGS_FILENAME = "stellar.1-0.json"; //path will change depending on OS on constructor
    ofxJSONElement settingsFile;
    bool settingsFileExist();
    void createSettingsFile();
    void loadSettings();

    //RECENT PROJECTS
    ofFile recentProjectsFile;
    vector<string> recentProjects;
    vector<string> loadRecentProjects();
    void saveToRecentProject(string path);
    const string DEFAULT_SESSION = "assets/default_session/default.session.json";
    const string DEFAULT_SESSION_DIRECTORY = "assets/default_session/drumkits.mp3/";
    const string DEFAULT_SESSION_PLACEHOLDER = "DEFAULT_SESSION";

    void setWindowTitleWithSessionName();

    bool lastCrashed = false;
    bool firstTimeOpened = false;

    void finishLoadingSession();

public:
    static SessionManager* getInstance();
    string sessionAudioDeviceName = "";

    struct datasetLoadOptions{
        string method;
        string path;
    };

    void init();
    void loadInitSession();
    void loadSession(datasetLoadOptions opts);
    void loadDefaultSession();
    void saveSession();
    bool isDefaultSession = false;
    void startAudioWithCurrentConfiguration();
    void resetSettings();
    
    void saveSettingsFile( bool crashCheck = false );

    void setAudioFilesNotFoundPath( string path );

    void saveAsNewSession();
    void exit();

    bool areAnyRecentProjects(vector<string> recentProjects);

    //Getters
    bool getSessionLoaded();
    vector<string> getRecentProjects();
    ofxJSONElement getAudioSettings();
    bool getLastCrashed();

    ofxJSONElement& getSettingsFile();
    ofxJSONElement* getSessionFile();
    string getSessionFileName();
    
    void keyPressed(ofKeyEventArgs & e);
    
    string colorPalette = "Default";
};
