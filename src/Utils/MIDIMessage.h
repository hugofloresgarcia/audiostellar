#pragma once

#include "ofMain.h"
#include "ofxMidiConstants.h"

struct MIDIMessage {
  MidiStatus status;
  int channel;
  int pitch;
  int velocity;

  int cc;
  int ccVal;

  int beats = -1;
  double bpm = -1;
};
