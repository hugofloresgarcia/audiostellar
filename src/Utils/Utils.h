#pragma once
#include "ofMain.h"

class Utils {
public:
    static float getMaxVal(float val, float winner) {
        if(val > winner) {
            return val;
        } else {
            return winner;
        }
    }
    static float getMinVal(float val, float winner) {
        if(val < winner) {
            return val;
        } else {
            return winner;
        }
    }

    static string resolvePath(string root, string path){

        if (!ofFilePath::isAbsolute(path)) {
            root = ofFilePath::getEnclosingDirectory(root);
            path = root + path;
        }

        if(path[path.size() - 1] != '/'){
            path = path + "/";
        }

        return path;
    }
};
