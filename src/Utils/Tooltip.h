#pragma once

#include "ofMain.h"
#include "ofxImGui.h"

namespace Tooltip {

    struct tooltip {
        string title = "";
        string content = "";
        
        tooltip(string t, string c){
            title = t;
            content = c;
        }
    };
    
    extern bool enabled;
    extern bool itemHovered;
    extern string tooltipContent;
    extern string tooltipTitle;
    
    void drawGui();
    void setTooltip(tooltip tooltip);
    

    const tooltip NONE =
    {"",""};



    const tooltip UNITS_MASTER_VOLUME =
    {"Master volume","Set the master volume level. This will affect all enabled output channels."};
    const tooltip UNITS_MUTE =
    {"Mute all units","Mute/Unmute all units"};
    const tooltip UNITS_SOLO =
    {"Solo all units","Solo/Unsolo all units"};
    const tooltip UNITS_ACTIVE =
    {"Activate all units","Activate/Deactivate all units"};

    const tooltip UNIT_NAME =
    {"Unit name","Units can be renamed, moved and removed by right clicking on its name"};
    const tooltip UNIT_VOLUME =
    {"Unit volume","Set this unit's volume"};
    const tooltip UNIT_PAN =
    {"Unit pan","Set this unit's pan"};
    const tooltip UNIT_MUTE =
    {"Unit mute","Toggles mute on this unit"};
    const tooltip UNIT_SOLO =
    {"Unit solo","Toggles solo on this unit"};
    const tooltip UNIT_ACTIVE =
    {"Unit activation","Toggles unit activation. Inactive units don't use DSP resources"};
    const tooltip UNIT_ENVELOPE_ENABLE =
    {"Enable","Enable amplitude envelope for the sounds played by this unit."};
    const tooltip UNIT_ENVELOPE_ATTACK_TIME =
    {"Attack","Sets the envelope attack time in milliseconds for the sounds played by this unit."};
    const tooltip UNIT_ENVELOPE_HOLD_TIME =
    {"Hold","Sets the envelope hold time in milliseconds for the sounds played by this unit."};
    const tooltip UNIT_ENVELOPE_RELEASE_TIME =
    {"Release","Sets the envelope release time in milliseconds for the sounds played by this unit."};
    const tooltip UNIT_ENVELOPE_ATTACK_PERCENTAGE =
    {"Attack","Sets the envelope attack time as a percentage of the duration of each sound played by this unit."};
    const tooltip UNIT_ENVELOPE_HOLD_PERCENTAGE =
    {"Hold","Sets the envelope hold time as a percentage of the duration of each sound played by this unit."};
    const tooltip UNIT_ENVELOPE_RELEASE_PERCENTAGE =
    {"Release","Sets the envelope release time as a percentage of the duration of each sound played by this unit."};
    const tooltip UNIT_ENVELOPE_MODE_TIME =
    {"Envelope Mode","Set the envelope parameters in absolute time values."};
    const tooltip UNIT_ENVELOPE_MODE_PERCENTAGE =
    {"Envelope Mode","Set the envelope parameters as a percentage of the duration of each sound."};
    const tooltip UNIT_SAMPLE_PITCH =
    {"Pitch","Sets the pitch for the sounds played by this unit, in semitones."};
    const tooltip UNIT_SAMPLE_START =
    {"Start","Sets the starting position for the sounds played by this unit. A value of 0 will start playback from the begining of the sound, a value of 0.5 will start from the middle."};
    const tooltip UNIT_SAMPLE_REVERSE =
    {"Reverse","Reverses the playback direction for the sounds played by this unit. When enabled the sound will be played backwards from the starting position."};
    const tooltip UNIT_SELECT_OUTPUT =
    {"Unit output channels","Select this unit's output channels."};
    const tooltip UNIT_SELECT_OUTPUT_NO_OUT =
    {"Error on output channels","Check Settings -> Audio settings"};
    const tooltip UNIT_ADD =
    {"Unit add","Adds a unit. To remove a unit, right-click on the name of the unit you want to remove and then click 'Remove unit'."};
    const tooltip UNIT_REMOVE =
    {"Unit remove","Removes selected unit"};
    const tooltip UNIT_EFFECT_ADD =
    {"Add effect","Adds an effect to this unit"};
    const tooltip UNIT_CLUSTER_RESTRICT =
    { "Restrict to cluster",
      "Only sounds members of selected cluster will play. "
      "OSC play/xy messages will be mapped to the cluster's region bounding box." };
    const tooltip UNIT_SPREAD_MODE =
    { "Spread mode",
      "Relative modes will pan sounds depending on their position but related to the other sounds. "
      "Absolute modes will use map's absolute position. Random will pan randomly" };
    const tooltip UNIT_SPREAD_PAN =
    { "Pan spread",
      "Audio clips will sound with different panning related to their position or random, "
      "depending on the selected mode." };

    const tooltip UNIT_SEQUENCE_PROBABILITY =
    {"Probability","Sets the probability of playing an audio clip"};
    const tooltip UNIT_SEQUENCE_OFFSET =
    { "Offset", "Set the offset time from the starting beat in sixteenth notes resolution." };
    const tooltip UNIT_SEQUENCE_BARS =
    { "Bars", "Set the duration of the sequence." };
    const tooltip UNIT_SEQUENCE_CLEAR =
    { "Clear sequence", "Delete all the events in the sequence." };
    const tooltip UNIT_SEQUENCE_PROB_GROW =
    { "Grow probability", "Each time the sequence starts, the probability for a new sound to be added" };
    const tooltip UNIT_SEQUENCE_BTN_GROW =
    { "Grow button", "Adds a new sound to the sequence (tempo synced)" };
    const tooltip UNIT_SEQUENCE_PROB_SHRINK =
    { "Shrink probability", "Each time the sequence starts, the probability for a sound to be removed." };
    const tooltip UNIT_SEQUENCE_BTN_SHRINK =
    { "Shrink button", "Removes a sound from the sequence (tempo synced)" };
    const tooltip UNIT_SEQUENCE_PROB_MUTATE =
    { "Mutate probability", "Each time a sound is played, the probability for that sound to be changed for another one." };
    const tooltip UNIT_SEQUENCE_BTN_MUTATE =
    { "Mutate button", "Changes next sound in the sequence for another one (tempo synced)" };
    const tooltip UNIT_SEQUENCE_USE_CLUSTERS =
    { "Preserve clusters", "When growing, shrinking or mutating, always use a sound from a cluster currently in use" };
    const tooltip UNIT_SEQUENCE_PREVENT_DEATH =
    { "Prevent death",
      "Prevent shrinking if only one sound left. If 'preserve clusters' is on, "
      "it will prevent each cluster for dying" };

    const tooltip UNIT_PARTICLE_LIFESPAN =
    {"Lifespan","Sets how long each particle will live"};
    const tooltip UNIT_PARTICLE_VELOCITY_X =
    {"Velocity X","Sets the velocity in the X axis. Negative values invert direction"};
    const tooltip UNIT_PARTICLE_VELOCITY_Y =
    {"Velocity Y","Sets the velocity in the Y axis. Negative values invert direction"};
    const tooltip UNIT_PARTICLE_SPREAD =
    {"Spread amount","Sets how strong the particles shake"};
    const tooltip UNIT_PARTICLE_DENSITY =
    {"Density","Sets how many particles will be emitted at once"};
    const tooltip UNIT_PARTICLE_SPEED =
    {"Speed","The speed of the explosion"};
    const tooltip UNIT_PARTICLE_EMIT_RATE =
    {"Emit rate","Sets how fast the emitter create particles"};
    const tooltip UNIT_PARTICLE_EMIT_PARTICLE =
    {"Emit","Emitter will create a new particle. Enable by clicking on the map"};
    const tooltip UNIT_PARTICLE_EMIT_AREA_SIZE =
    {"Emitter area size","Particles trespassing emitter area will be instantly killed"};
    const tooltip UNIT_PARTICLE_LOCK =
    {"Lock emitter position","Clicking on the map won't change emitter position"};
    const tooltip UNIT_PARTICLE_EMIT_RANDOM =
    {"Randomize inside area","Particles will be emitted from a random position inside area"};
    const tooltip UNIT_PARTICLE_PROBABILITY =
    {"Probability","Sets the probability of playing an audio clip"};
    const tooltip UNIT_PARTICLE_REBIRTH =
    {"Rebirth","When a particle dies a new one will automatically be emitted"};

    const tooltip UNIT_MORPH_PLAY =
    { "Play","Plays the sounds inside the circle. Enable by clicking on the map" };
	const tooltip UNIT_MORPH_THRESHOLD =
	{ "Threshold","Sets the radius in which sounds get played" };
	const tooltip UNIT_MORPH_SHAPER =
	{ "Morph Shaper","Sets the shape of the morphing curve" };
	const tooltip UNIT_MORPH_PROBABILITY =
	{ "Morph Probability","Sets the probability of the sounds playing" };
	const tooltip UNIT_MORPH_LOCK =
	{ "Lock Morph Centroid","Clicking on the map won't change the morph position" };


    const tooltip UNIT_OSC_PREFIX =
    {"OSC Prefix","Adds a prefix to OSC messages "
     "(check Help > OSC documentation). i.e. setting 'test' will accept "
     "/test/play/xy"};

    const tooltip UNIT_BEHAVIOR_TRAJECTORY_CREATE =
    {"Create trajectory","Draw a trajectory on the map and loop it"};
    const tooltip UNIT_BEHAVIOR_TRAJECTORY_AWAITING =
    {"Create trajectory","Click and drag on the map to draw a trajectory"};
    const tooltip UNIT_BEHAVIOR_TRAJECTORY_SPEED =
    {"Trajectory speed","Sets how fast trajectories are played"};
    const tooltip UNIT_BEHAVIOR_TRAJECTORY_PROBABILITY =
    {"Trajectory probability","Sets the probability of triggering an event"};

    const tooltip UNIT_BEHAVIOR_OSC_LISTEN =
    {"Listen OSC","Listens OSC message with route /play/xy, /play/x or /play/y with parameters x and y. "
     "It will mimic the mouse, triggering an event on selected location."};
    const tooltip UNIT_BEHAVIOR_OSC_RESTRICT_TO_SECTOR =
    {"Restrict to sector","Draw a rectangle on the map so incoming OSC coordinates will be mapped relative"
     "to it instead of the entire map"};
    const tooltip UNIT_BEHAVIOR_OSC_RESTRICT_TO_SECTOR_SHOW =
    {"Show restricted sector","Show the rectangle where OSC coordinates are restricted. It will be shown "
     "only if the unit is selected"};


    const tooltip UNIT_BEHAVIOR_POLYPHONY_MONO =
    {"Monophonic","Only one audio clip will play at a time. If a new audio clip is requested to play, the behavior will be determined by the choke parameter."};
    const tooltip UNIT_BEHAVIOR_POLYPHONY_POLYPHONIC =
    {"Polyphonic","Different audio clips can play simultaneously. If an audio clip is requested to play and it is already playing, the behavior will be determined by the choke parameter."};
    const tooltip UNIT_BEHAVIOR_POLYPHONY_FULLPOLYPHONIC =
    {"Full polyphonic","Audio clips will play simultaneously regardless of whether they are already playing or not."};
    const tooltip UNIT_BEHAVIOR_POLYPHONY_CHOKE =
    {"Choke","Applicable only for Monophonic and Polyphonic modes. If an audio clip is "
     "currently playing and a new one is requested to play, selecting 'choke' "
     "will cause the first audio clip to stop and be replaced by the new one. "
     "If 'choke' is not selected, the new audio clip will not play and will be ignored."};

    const tooltip UNIT_BEHAVIOR_MIDI_KEYBOARD_MIDI_CHANNEL =
    {"MIDI keyboard channel","Select what channel is the MIDI keyboard set to."};
    const tooltip UNIT_BEHAVIOR_MIDI_KEYBOARD_ROOT_NOTE =
    {"MIDI keyboard root note","By setting the root note, you can control how pitch changes when you play other notes. "
     "When you play the selected root note, the pitch will stay the same. "
     "For example, if you choose C as the root note, playing C4 will not affect the pitch, but playing D4 will raise the pitch by one tone."};
    const tooltip UNIT_BEHAVIOR_MIDI_KEYBOARD_ENV_ENABLE =
    {"Enable MIDI keyboard envelope","This volume envelope will be triggered when "
     "playing the MIDI keyboard and will affect the whole resulting Unit sound."};
    const tooltip UNIT_BEHAVIOR_MIDI_KEYBOARD_EMIT_PARTICLES =
    {"MIDI keyboard emit particles","Should the MIDI keyboard emit particles, or should it only change the pitch of whats playing?"};
    const tooltip UNIT_BEHAVIOR_MIDI_KEYBOARD_SAVE_NOTES =
    {"MIDI keyboard save notes",
     "If enabled, the session will store the latest notes played, allowing the pitch to be "
     "adjusted based on those notes even if you disable this module or reopen the session."};

    const tooltip EFFECT_SATURATOR_INPUT_GAIN =
    {"Input gain","Sets input gain"};
    const tooltip EFFECT_SATURATOR_OUTPUT_GAIN =
    {"Output gain","Toggles solo on this unit"};

    const tooltip SHOW_FOCUS_ON_CLUSTERS =
    {"Focus on clusters","When passing the mouse over a cluster, hides all other clusters"};

//    tooltip MASTER_VOLUME;
//    tooltip SOUNDS_REPLAY;
    const tooltip SOUNDS_ORIGINAL_POSITIONS =
    { "Sounds to original positions", "Revert sounds to their original positions." };
//    tooltip SOUNDS_PLAY_ON_HOVER;
    const tooltip CLUSTERS_EPS =
    { "DBScan Eps", "DBScan Eps parameter specifies how close points should "
      "be to each other to be considered as part of a cluster." };
    const tooltip CLUSTERS_MIN_PTS =
    { "DBScan MinPts", "DBScan MinPts specifies the minimum number of points to "
      "form a dense region." };
    const tooltip CLUSTERS_EXPORT_FILES =
    { "Export clusters to folders", "Export sounds in each cluster to folders. Muted sounds won't be exported." };

    const tooltip MIDI_MONITOR =
    { "MIDI Monitor",
      "The monitor will turn yellow when a new MIDI message arrive." };
    const tooltip OSC_MONITOR =
    { "OSC Monitor",
      "The monitor will turn yellow when a new OSC message arrive. Click to inspect incoming OSC messages" };
    const tooltip OSC_MONITOR_ENABLE =
    { "OSC Inspector Enable", "Enable to inspect incoming OSC messages" };
    const tooltip OSC_MONITOR_FIX_TO_BOTTOM =
    { "OSC Inspector - Fix to bottom",
      "Keep scrollbar fixed to the bottom" };
    const tooltip OSC_MONITOR_CLEAR =
    { "OSC Inspector - Clear",
      "Clear log window" };

    const tooltip MIDI_DEVICES_INPUT =
    { "Input MIDI Devices", "Select available MIDI devices." };
    const tooltip MIDI_DEVICES_OUTPUT =
    { "Output MIDI Devices", "Select available MIDI devices." };
    const tooltip MIDI_CLOCK =
    { "MIDI Clock", "Syncronize AudioStellar's internal clock with external hardware or software. "
      "Enable SPP (MIDI Song Position Pointer) for syncing bars." };
    const tooltip MIDI_HANDLE_PLAY_STOP =
    { "Use external Play/Stop MIDI messages",
      "Sequence Unit will follow play/stop MIDI messages from your external device" };
    const tooltip MIDI_LEARN_VELOCITY =
    { "Enable MIDI velocity", "When mapping a MIDI note, should velocity affect mapped parameter?." };
    const tooltip MIDI_LEARN_IGNORE_NOTE_OFF =
    { "Ignore NOTE_OFF", "Ignore NOTE_OFF messages. Useful when mapping velocity and don't want the fader to drop to 0" };

    const tooltip DIMREDUCT_AUDIO_SAMPLE_RATE =
    { "Sample rate", "Your audio samples will be converted to this sample rate for processing. "
      "Original audios will not be affected." };
    const tooltip DIMREDUCT_AUDIO_FEATURES =
    { "Audio feature extraction", "Set features to extract from audio." };
    const tooltip DIMREDUCT_METRIC =
    { "Metric", "Similarity measure you would like to use for the visualization." };
    const tooltip DIMREDUCT_AUDIO_LENGTH =
    { "Audio length", "Set the length of the audio samples in seconds. "
      "If your files are longer they will be chopped otherwise they will "
      "be zero padded to the specified duration. "
      "Only for processing, original audios will not be affected." };
    const tooltip DIMREDUCT_FEATURE_SETTINGS =
    { "Feature settings", "Select the features you would like to process. "
      "This parameters will affect feature extraction and not your original "
      "audios or how they will sound in the application." };
    const tooltip DIMREDUCT_STFT =
    { "Short Time Fourier Transform", "" };
    const tooltip DIMREDUCT_STFT_WINDOW_SIZE =
    { "Window size", "Set the size of the analysis in number of samples. "
      "Smaller windows produce more precise time resolution while larger "
      "windows increase frequency resolution." };
    const tooltip DIMREDUCT_STFT_HOP_SIZE =
    { "Hop size", "Set the offset for the overlapping windows." };
    const tooltip DIMREDUCT_PCA =
    { "Principal Component Analysis", "PCA performs a linear mapping "
      "of data to a lower-dimensional space in such a way that the "
      "variance of the data in the low dimensional representation is maximized." };
    const tooltip DIMREDUCT_SAVE_PCA_RESULTS =
    { "Save intermediate results", "Save feature extraction and PCA results to "
      "disk for greatly reducing process time in future processes." };
    const tooltip DIMREDUCT_FORCE_FULL_PROCESS =
    { "Force full process", "Do full process even if intermediate results are "
      "present. Use this if you change or add new sounds to your folder." };
    const tooltip DIMREDUCT_TSNE_PERPLEXITY =
    { "Perplexity", "Set the number of effective nearest neighbors. "
      "Larger dataset requires a larger perplexity." };
    const tooltip DIMREDUCT_TSNE_LEARNING_RATE =
    { "t-SNE's learning rate.", "Set t-SNE's learning rate. "
      "Usually in the range 10.0, 1000.0." };
    const tooltip DIMREDUCT_TSNE_ITERATIONS =
    { "t-SNE's iteration count.", "Set Maximum number of iterations for the optimization. "
      "Should be at least 250." };
    const tooltip DIMREDUCT_UMAP_NEIGHBORS =
    { "UMAP N Neighbors", "Controls how UMAP balances local versus global structure in the data."
      " Low values will force UMAP to concentrate on very local structure (potentially to the "
      "detriment of the big picture), while large values will push UMAP to look at larger neighborhoods." };
    const tooltip DIMREDUCT_UMAP_MIN_DISTANCE =
    { "UMAP Minimum distance", "Controls how tightly UMAP is allowed to pack points together."
      " Low values will result in clumpier embeddings." };

    const tooltip OSC_RECEIVE_PORT =
    { "Receive Port", "Set the port number for incoming OSC messages." };
    const tooltip OSC_RECEIVE_ADDRESS =
    { "Receive Address", "AudioStellar will listen OSC messages from this IP addresses. "
      "If you want to receive OSC from an application running in this computer "
      "use the first one. From another device, use the second one." };
    const tooltip OSC_SEND_PORT =
    { "Send Port", "Set the port number for outgoing OSC messages." };
    const tooltip OSC_SEND_ADDRESS =
    { "Send Address", "Set the IP address of the device that will receive OSC messages "
      "from AudioStellar. If you are sending OSC to an application running on this "
      "computer set this address to 127.0.0.1" };

    const tooltip RECORDER =
    { "Recorder", "Record main outputs to a stereo wav file" };

    const tooltip SOUND_PRELOADING =
    { "Sound Preloading", "Preload all sounds. "
      "Great for performance but bad for systems with low RAM memory" };

    const tooltip MASTERCLOCK_LINK =
    { "Ableton Link", "Sync AudioStellar with other software using AbletonLink" };
    
    const tooltip ABLETON_LINK =
    { "Ableton Link", "Sync AudioStellar with other software using AbletonLink" };
    const tooltip ABLETON_LINK_HANDLE_PLAY_STOP =
    { "Use external Play/Stop", "Sequence Unit will follow play/stop Ableton Link from your external software" };
    const tooltip ABLETON_LINK_LATENCY =
    { "Ableton Link Latency", "Adjust latency if the beat is not perfectly on tempo" };

    const tooltip SLICER_SENSITIVITY =
    { "Slicer sensitivity", "Higher sensitivity will result in more slices." };
    const tooltip SLICER_PARAMETERS =
    { "Peak pick parameters", "librosa.util.peak_pick parameters" };
    const tooltip SLICER_BACKTRACK =
    { "Slicer backtrack", "Markers snap to zero crossings" };
    const tooltip SLICER_FADE =
    { "Segmentation fade", "How many samples to fade out. Prevents click and pops. "
      "Default is usually fine" };
    const tooltip SLICER_NORMALIZE =
    { "Normalize", "Should slicer normalize audio clips?" };

//    tooltip AUDIO_SETTINGS_MAX_VOICES;
//    tooltip AUDIO_SETTINGS_ACTIVE_CHANNELS;

//    tooltip CONTEXT_MENU_SOUND_OUTPUT_CHANNELS;
//    tooltip CONTEXT_MENU_CLUSTER_OUTPUT_CHANNELS;
};







//#include "Tooltip.h"

//Tooltip::tooltip Tooltip::Tooltip::UNIT_VOLUME =
//{"Unit volume","Set this unit's volume"};
//Tooltip::tooltip Tooltip::Tooltip::UNIT_PAN =
//{"Unit pan","Set this unit's pan"};
//Tooltip::tooltip Tooltip::Tooltip::UNIT_MUTE =
//{"Unit mute","Toggles mute on this unit"};
//Tooltip::tooltip Tooltip::Tooltip::UNIT_SOLO =
//{"Unit pan","Toggles solo on this unit"};

//Tooltip::tooltip Tooltip::Tooltip::MASTER_VOLUME = { "Volume", "Set the master volume. This fader will only affect the first two output channels of the selected audio device in Audio settings." };
//Tooltip::tooltip Tooltip::SOUNDS_REPLAY = { "Replay Sound", "When active allows to retrigger sounds as they are clicked." };
//Tooltip::tooltip Tooltip::SOUNDS_ORIGINAL_POSITIONS =

//Tooltip::tooltip Tooltip::CLUSTERS_EPS =
//Tooltip::tooltip Tooltip::CLUSTERS_MIN_PTS =
//Tooltip::tooltip Tooltip::CLUSTERS_EXPORT_FILES  =
//Tooltip::tooltip Tooltip::CLUSTERS_SET_NAME = { "Set cluster name", "Set a name for this cluster. Holding 'c' on your keyboard will show all cluster names at once." };

//Tooltip::tooltip Tooltip::MODE_EXPLORER = { "Explorer Mode", "Play sounds by cliking or dragging on the map. Right click on each sound to get options." };
//Tooltip::tooltip Tooltip::MODE_PARTICLE = { "Particle Mode", "Click on the map to drop particles. Each time a particle collides with a sound it triggers playback. Choose different particle modes and adjust parameters in the Tools menu to set particle trajectories." };
//Tooltip::tooltip Tooltip::MODE_SEQUENCE = { "Sequence Mode", "Select sounds in the map to play them sequentially, in a loop. Sounds are played in order, as they are added to the sequence. The time interval between sounds in the sequence is related to their distances in the map." };

//Tooltip::tooltip Tooltip::PARTICLE_VOLUME = { "Volume", "Set the volume for sounds triggered by particles." };
//Tooltip::tooltip Tooltip::PARTICLE_LIFESPAN = { "Lifespan", "Set the lifespan of each particle. A value of 10 makes the particle permanent." };
//Tooltip::tooltip Tooltip::PARTICLE_RANDOMIZE_EMITTER = { "Randomize Emitter position", "Randomize the position of each new particle inside a particle region. Only useful if you created a particle region using MIDI Learn." };

//Tooltip::tooltip Tooltip::PARTICLE_SIMPLE = { "Simple", "Simple particles move in straight line." };
//Tooltip::tooltip Tooltip::PARTICLE_SIMPLE_X_ACCELERATION = { "X", "Sets the acceleration in the X axis. Negative values invert direction." };
//Tooltip::tooltip Tooltip::PARTICLE_SIMPLE_Y_ACCELERATION = { "Y", "Sets the acceleration in the Y axis. Negative values invert direction." };
//Tooltip::tooltip Tooltip::PARTICLE_SWARM = { "Swarm", "Swarm particles perform a two dimensional random walk." };
//Tooltip::tooltip Tooltip::PARTICLE_SWARM_X_ACCELERATION = { "X", "" };
//Tooltip::tooltip Tooltip::PARTICLE_SWARM_Y_ACCELERATION = { "Y", "" };
//Tooltip::tooltip Tooltip::PARTICLE_SPREAD_AMOUNT = { "Spread amount", "" };

//Tooltip::tooltip Tooltip::PARTICLE_EXPLOSION = { "Explosion", "Explosion particles move in stright line in all directions away from an emitter. Click on the map to create a particle emitter." };
//Tooltip::tooltip Tooltip::PARTICLE_EXPLOSION_SPEED = { "Speed", "Set the speed for explosion particles." };
//Tooltip::tooltip Tooltip::PARTICLE_EXPLOSION_DENSITY = { "Density", "Set the number of explosion particles created from each emitter." };

//Tooltip::tooltip Tooltip::ATTRACTOR_GRAVITY = { "Gravity", "" };
//Tooltip::tooltip Tooltip::ATTRACTOR_MASS = { "Mass", "" };

//Tooltip::tooltip Tooltip::SEQUENCE_BPM = { "BPM", "Set the tempo in beats per minute." };

//Tooltip::tooltip Tooltip::SEQUENCE_ACTIVE = { "Select sequence track", "Click to edit sequence parameters." };
//Tooltip::tooltip Tooltip::SEQUENCE_VOLUME = { "Sequence Volume", "Set the volume for each sequence track." };
//Tooltip::tooltip Tooltip::SEQUENCE_OFFSET =
//Tooltip::tooltip Tooltip::SEQUENCE_PROBABILITY = { "Probability", "Set the probability to be played for all sounds in the sequence." };
//Tooltip::tooltip Tooltip::SEQUENCE_BARS =
//Tooltip::tooltip Tooltip::SEQUENCE_CLEAR =

//Tooltip::tooltip Tooltip::MIDI_DEVICES =
//Tooltip::tooltip Tooltip::MIDI_LEARN = { "Midi Learn", "In Explorer Mode map a sound to a MIDI note. In Particle Mode, click and drag to draw a particle region and map a MIDI note to it. In any mode, moving any slider in AudioStellar and then moving a fader in your controller will map them together." };
//Tooltip::tooltip Tooltip::MIDI_CLOCK = { "Use MIDI clock", "Get the tempo from an external MIDI device." };

//Tooltip::tooltip Tooltip::DIMREDUCT_AUDIO_SAMPLE_RATE =
//Tooltip::tooltip Tooltip::DIMREDUCT_AUDIO_LENGTH =
//Tooltip::tooltip Tooltip::DIMREDUCT_AUDIO_FEATURES =
//Tooltip::tooltip Tooltip::DIMREDUCT_FEATURE_SETTINGS =
//Tooltip::tooltip Tooltip::DIMREDUCT_METRIC =
//Tooltip::tooltip Tooltip::DIMREDUCT_STFT =
//Tooltip::tooltip Tooltip::DIMREDUCT_STFT_WINDOW_SIZE =
//Tooltip::tooltip Tooltip::DIMREDUCT_STFT_HOP_SIZE =
//Tooltip::tooltip Tooltip::DIMREDUCT_PCA =
//Tooltip::tooltip Tooltip::DIMREDUCT_UMAP_NEIGHBORS =
//Tooltip::tooltip Tooltip::DIMREDUCT_UMAP_MIN_DISTANCE =
//Tooltip::tooltip Tooltip::DIMREDUCT_TSNE_PERPLEXITY =
//Tooltip::tooltip Tooltip::DIMREDUCT_TSNE_ITERATIONS =
//Tooltip::tooltip Tooltip::DIMREDUCT_TSNE_LEARNING_RATE =
//Tooltip::tooltip Tooltip::DIMREDUCT_SAVE_PCA_RESULTS =
//Tooltip::tooltip Tooltip::DIMREDUCT_FORCE_FULL_PROCESS =

//Tooltip::tooltip Tooltip::OSC_RECEIVE_PORT =
//Tooltip::tooltip Tooltip::OSC_RECEIVE_ADDRESS =
//Tooltip::tooltip Tooltip::OSC_SEND_PORT =
//Tooltip::tooltip Tooltip::OSC_SEND_ADDRESS =

//Tooltip::tooltip Tooltip::AUDIO_SETTINGS_MAX_VOICES = { "Max Voices", "Set the maximum number of sounds to be played simultaneously."};
//Tooltip::tooltip Tooltip::AUDIO_SETTINGS_ACTIVE_CHANNELS = { "Active Channels", "Click on a channel to activate or deactivate it. Only active channels will be available. You can select stereo pairs or individual mono outputs."};


//Tooltip::tooltip Tooltip::CONTEXT_MENU_SOUND_OUTPUT_CHANNELS = { "Output Channels", "Select the output channel for individual sounds. Only the active outputs are available in this menu. To activate outputs select them in Audio settings menu."};
//Tooltip::tooltip Tooltip::CONTEXT_MENU_CLUSTER_OUTPUT_CHANNELS = { "Output Channels", "Select the output channel for individual clusters. Only the active outputs are available in this menu. To activate outputs select them in Audio settings menu."};





