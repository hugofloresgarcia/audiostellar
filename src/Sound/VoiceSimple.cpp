#include "VoiceSimple.h"

VoiceSimple::VoiceSimple()
{

}

VoiceSimple::~VoiceSimple()
{
    unpatch();
    unload();
}


void VoiceSimple::patch() {
    if ( isPatched ) return;

    addModuleOutput( "0", out_amp0 );
    addModuleOutput( "1", out_amp1 );

    addModuleInput("speed", sampler0.in_pitch());
    addModuleInput("speed", sampler1.in_pitch());

    sampleTrigger >> sampler0 >>             mix0_amp0 >> out_amp0;
    sampleTrigger >> sampler1 >>             mix0_amp1 >> out_amp1;

                     sampler0 >> env_amp0 >> mix1_amp0 >> out_amp0;
                     sampler1 >> env_amp1 >> mix1_amp1 >> out_amp1;

    envTrigger >> env >> env_amp0.in_mod();
                  env >> env_amp1.in_mod();

    mix0_amp0.set(1.0f);
    mix0_amp1.set(1.0f);
    mix1_amp0.set(0.00000001f);
    mix1_amp1.set(0.00000001f);

    out_amp0.set(0.0f);
    out_amp1.set(0.0f);

    isPatched = true;
}

void VoiceSimple::unpatch()
{
    out_amp0.disconnectAll();
    out_amp1.disconnectAll();

    sampler0.disconnectAll();
    sampler1.disconnectAll();

    sampleTrigger.disconnectAll();
    envTrigger.disconnectAll();

    isPatched = false;
}

void VoiceSimple::load(string path)
{
    if ( sample == nullptr ) {
        sample = new pdsp::SampleBuffer();
    }
    sample->load(path);
    sampler0.setSample( sample, 0, 0 );

    if (sample->channels == 1) {
        sampler1.setSample(sample, 0, 0);
    } else {
        sampler1.setSample(sample, 0, 1);
    }
}

void VoiceSimple::load(pdsp::SampleBuffer * buffer, int start, int length, int channels) {
    
    if ( sample == nullptr ) {
        sample = new pdsp::SampleBuffer();
    }
    
    sample->init(length, channels);
    sample->fileSampleRate = buffer->fileSampleRate;
    
    if (sample->channels == 1) {
        
        for (int i = 0; i < length; i++) {
            sample->buffer[0][i] = buffer->buffer[0][start + i];
        }
        sampler0.setSample(sample, 0, 0);
        sampler1.setSample(sample, 0, 0);
        
    } else {
        
        for (int i = 0; i < length; i++) {
            sample->buffer[0][i] = buffer->buffer[0][start + i];
            sample->buffer[1][i] = buffer->buffer[1][start + i];
        }
        sampler0.setSample(sample, 0, 0);
        sampler1.setSample(sample, 0, 1);
    }
}

void VoiceSimple::unload() {
    //I don't think any of these is needed
    stop();
//    sample.unLoad();
}

void VoiceSimple::play(float volume) {
    setVolume(volume);
    envTrigger.trigger(1.0f);
    sampleTrigger.trigger(1.0f);
    // This is needed if triggering more than one sone
    sampler0.setIsPlaying(true);
//    sampler1.setIsPlaying(true);

    isStopped = false;
}

void VoiceSimple::stop() {
    setVolume(0.0f);
//    envTrigger.off();

    // this does nothing
    // triggering 1.0 will play sample but 0.0 won't stop it
    sampleTrigger.off();

    isStopped = true;

    // i think this isn't needed with isStopped
//    if ( sound != nullptr ) {
//        sound->updatePlaying(false);
//    }
}

bool VoiceSimple::isLoaded() {
    pdsp::SampleBuffer * s = this->sample;
    return s != nullptr && s->loaded();
}

bool VoiceSimple::isPlaying() {
    pdsp::SampleBuffer * s = this->sample;

    if ( s == nullptr || isStopped ) return false;

    // this can return false if sample
    // hasn't been stopped but the sample finished
    return sampler0.is_playing();
}

float VoiceSimple::getPlayerPosition() {
    return sampler0.meter_position();
}

float VoiceSimple::getSoundDuration() {
    pdsp::SampleBuffer * s = this->sample;

    int length = (s->length / s->fileSampleRate) * 1000; // milliseconds
    if (reverse) {
        length = length * sampleStart;
    }
    else {
        length = length * (1 - sampleStart);
    }
    return length;
}

int VoiceSimple::getSampleRate() {
    assert( sample != nullptr );
    return sample->fileSampleRate;
}

int VoiceSimple::getSampleLength() {
    assert( sample != nullptr );
    return sample->length;
}

float** VoiceSimple::getBuffer() {
    assert( sample != nullptr );
    return sample->buffer;
}

void VoiceSimple::setVolume(float volume) {
    out_amp0.set(volume);
    out_amp1.set(volume);
}

void VoiceSimple::enableEnvelope(bool e) {

    if(e) {
        mix0_amp0.set(0.00000001f);
        mix0_amp1.set(0.00000001f);
        mix1_amp0.set(1.0f);
        mix1_amp1.set(1.0f);
    }
    else {
        mix0_amp0.set(1.0f);
        mix0_amp1.set(1.0f);
        mix1_amp0.set(0.00000001f);
        mix1_amp1.set(0.00000001f);
    }
}

void VoiceSimple::enableReverse(bool e) {

    if(e) {
        -1.0f >> sampler0.in_direction();
        -1.0f >> sampler1.in_direction();
    }
    else {
        1.0f >> sampler0.in_direction();
        1.0f >> sampler1.in_direction();
    }
    reverse = e;
}

void VoiceSimple::setEnvelope(float attack, float hold, float release, int envMode) {

    float a, h, r;

    if(envMode == 0) {
        a = attack;
        h = hold;
        r = release;
    }

    if(envMode == 1) {
        float duration = getSoundDuration();
        a = duration * attack / 100;
        h = duration * hold / 100;
        r = duration * release / 100;
    }

    env.set(a, h, r);
}

void VoiceSimple::setPitch(float p) {
    p >> sampler0.in_pitch();
    p >> sampler1.in_pitch();
}

void VoiceSimple::setStart(float s) {
    s >> sampler0.in_start();
    s >> sampler1.in_start();
    sampleStart = s;
}

void VoiceSimple::setSound(shared_ptr<Sound> s)
{
    sampler0.setSample( sample, 0, 0 );

    if (sample->channels == 1) {
        sampler1.setSample(sample, 0, 0);
    } else {
        sampler1.setSample(sample, 0, 1);
    }
}
