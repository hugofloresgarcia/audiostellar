#include "SoundDevice.h"

soundDevice::soundDevice(ofSoundDevice device) {
    this->api = device.api;
    this->name = device.name;
    this-> deviceID = device.deviceID;
    this->inputChannels = device.inputChannels;
    this->outputChannels = device.outputChannels;
    this->isDefaultInput = device.isDefaultInput;
    this->isDefaultOutput = device.isDefaultOutput;
    this->sampleRates = device.sampleRates;
    this->bufferSizes = { 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 7168, 8192 };
}

unsigned int soundDevice::testBufferSize(unsigned int samplerate, unsigned int buffersize) {

    RtAudio::Api RtAudioApi;

    switch (api) {
    case ofSoundDevice::ALSA:
        RtAudioApi = RtAudio::Api::LINUX_ALSA;
    case ofSoundDevice::PULSE:
        RtAudioApi = RtAudio::Api::LINUX_PULSE;
    case ofSoundDevice::OSS:
        RtAudioApi = RtAudio::Api::LINUX_OSS;
    case ofSoundDevice::JACK:
        RtAudioApi = RtAudio::Api::UNIX_JACK;
#ifndef TARGET_LINUX
    case ofSoundDevice::OSX_CORE:
        RtAudioApi = RtAudio::Api::MACOSX_CORE;
    case ofSoundDevice::MS_WASAPI:
        RtAudioApi = RtAudio::Api::WINDOWS_WASAPI;
    case ofSoundDevice::MS_ASIO:
        RtAudioApi = RtAudio::Api::WINDOWS_ASIO;
    case ofSoundDevice::MS_DS:
        RtAudioApi = RtAudio::Api::WINDOWS_DS;
#endif
    default:
        RtAudioApi = RtAudio::Api::UNSPECIFIED;
    }

    RtAudio audio(RtAudioApi);

    RtAudio::StreamParameters outputParameters;
    outputParameters.deviceId = deviceID;
    outputParameters.nChannels = outputChannels;

    RtAudio::StreamOptions options;
    options.flags = RTAUDIO_SCHEDULE_REALTIME;
    options.numberOfBuffers = 4;
    options.priority = 1;

    unsigned int tryBufferSize = buffersize;
    unsigned int realBufferSize = buffersize;

    try {
        audio.openStream(&outputParameters, nullptr, RTAUDIO_FLOAT32, samplerate, &tryBufferSize, nullptr, this, &options);

        if (tryBufferSize != buffersize) {
            realBufferSize = tryBufferSize;
        }

        audio.closeStream();
    }
    catch (RtAudioError e) {
        ofLog() << e.getType() << ": " << e.getMessage();
    }

    return realBufferSize;
}
