#include "Voices.h"

void Voices::die()
{
    ofRemoveListener(ofEvents().update, this, &Voices::update);
    voiceMap.clear();

    for ( auto &v : voices ) {
        v->die();
    }
}

Voices::Voices() {
    loadedVoices = 0;
    setVoicesAmount(8);

    // init voice map
    vector <shared_ptr<Sound>> sounds = Sounds::getInstance()->getSounds();
    for(unsigned int i = 0; i < sounds.size(); i++) {
        voiceMap[ sounds[i] ] = vector <Voice *>();
    }

    ofAddListener(ofEvents().update, this, &Voices::update);
}

//This tells each sound if it is playing or not. It is used for the playing animation
void Voices::update(ofEventArgs &args)
{
    for (auto &m : voiceMap)  {
        bool isPlaying = false;
        if ( m.second.size() > 0 ) {
            for ( Voice * v : m.second ) {
                if ( v->isPlaying() ) {
                    isPlaying = true;
                    break;
                }
            }
            m.first->updatePlaying(isPlaying);
        }
    }
}

Voices::~Voices()
{
    ofRemoveListener(ofEvents().update, this, &Voices::update);
    for ( unsigned int i = 0 ; i < voices.size() ; i++ ) {
        delete voices[i];
    }
}

void Voices::setVoicesType(Voices::VoicesType type)
{
    voiceType = type;
}

Voices::VoicesType Voices::getVoicesType()
{
    return voiceType;
}

void Voices::setChoke(bool c)
{
    choke = c;
}

bool Voices::getChoke()
{
    return choke;
}

Voice * Voices::getNonPlayingVoice(shared_ptr<Sound> sound) {
    for (unsigned int i = 0; i < voiceMap[sound].size(); i++) {
        Voice * voice = voiceMap[sound][i];
        if ( voice->isPatched() && !voice->isPlaying() ) { // voice not playing
            return voice;
        }
    }
    return nullptr;
}

Voice * Voices::getFreeVoice() {
    
    if (loadedVoices < numVoices) {
        Voice * voice = voices[loadedVoices];

        if ( voice->isPatched() ) {
            loadedVoices++;
            return voice;
        }
    }
    else {
        for (int i = 0; i < numVoices; i++) {
            if ( voices[i]->isPatched() && !voices[i]->isPlaying() ) {
                return voices[i];
            }
        }
    }
    return nullptr;
}

Voice * Voices::getReplaceableVoice() {
    float playerPosition = -1;
    Voice * voice = voices[0];
    // get the voice that is nearer to finish
    // isn't this resource heavy??
    for (unsigned int i = 0; i < numVoices; i++) {
        if (voices[i]->isPatched() && voices[i]->getPlayerPosition() > playerPosition) {
            playerPosition = voices[i]->getPlayerPosition();
            voice = voices[i];
        }
    }
    return voice;
}

Voice *Voices::getVoice(shared_ptr<Sound> sound)
{
    Voice * voice = nullptr;

    switch( voiceType ) {
        case Monophonic:
            if ( !choke ) {
                for ( auto &v : voices ) {
                    if ( v->isPlaying() ) {
//                        return nullptr;
                        v->beingReused = true;
                        return v;
                    }
                }
                voice = getNonPlayingVoice(sound);
                if ( voice == nullptr ) {
                    voice = getFreeVoice();
                }
                takeOverVoice(sound, voice);
            } else {
                for ( auto &v : voices ) {
                    if ( v->isPlaying() ) {
                        v->stop();
                        break;
                    }
                }
                voice = getNonPlayingVoice(sound);
                if ( voice == nullptr ) {
                    voice = getFreeVoice();
                }
                takeOverVoice(sound, voice);
            }

            break;

        case Polyphonic:
            if ( voiceMap[sound].size() > 0 ) {
                if ( !choke ) {
                    for (auto &v : voiceMap[sound]) {
                        if ( v->isPlaying() ) {
//                            return nullptr;
                            v->beingReused = true;
                            return v;
                        }
                    }
                    voice = getNonPlayingVoice(sound);
                    if ( voice == nullptr ) {
                        voice = getFreeVoice();
                        if ( voice == nullptr ) {
                            voice = getReplaceableVoice();
                        }
                    }
                    takeOverVoice(sound, voice);
                } else {
                    for (auto &v : voiceMap[sound]) {
                        if ( v->isPlaying() ) {
                            v->stop();
                        }
                    }
                    voice = getNonPlayingVoice(sound);
                    if ( voice == nullptr ) {
                        voice = getFreeVoice();
                        if ( voice == nullptr ) {
                            voice = getReplaceableVoice();
                        }
                    }
                    takeOverVoice(sound, voice);
                }
            } else {
                voice = getFreeVoice();
                if ( voice == nullptr ) {
                    voice = getReplaceableVoice();
                }
                takeOverVoice(sound, voice);
            }
            break;
        case FullPolyphonic:
            voice = getNonPlayingVoice(sound); //voices allocated for each sound
            if (voice == nullptr) { // no voice allocated or all allocated voices are playing
                voice = getFreeVoice();
                if (voice == nullptr) { // no free voice, all voices are playing
//                    return nullptr;
                    voice = getReplaceableVoice();
                    takeOverVoice(sound, voice);
                }
                else { // voice not playing found
                    takeOverVoice(sound, voice);
                }
            }
            break;
    }

    voice->beingReused = false;
    voice->play();
    return voice;
}

void Voices::takeOverVoice(shared_ptr<Sound> sound, Voice * voice) {
    if (voice->getSound() != nullptr) {
        vector <Voice *> * voices = &voiceMap[voice->getSound()];
        voices->erase(remove(voices->begin(), voices->end(), voice), voices->end());
    }
    voiceMap[sound].push_back(voice);
    voice->setSound(sound);
}

void Voices::setVoicesAmount(int newVoiceAmount)
{
    if ( numVoices == newVoiceAmount ) return;
//    ofLog() << "changing voices amount: " << newVoiceAmount;
    if ( newVoiceAmount > numVoices ) {
        int voicesToBeAdded = newVoiceAmount - numVoices;

        for (unsigned int i = 0; i < voicesToBeAdded; ++i) {
            Voice * newVoice = new Voice();
            newVoice->patch();
            voices.push_back( newVoice );
        }
    } else {
        for (unsigned int i = voices.size() - 1; i >= newVoiceAmount ; i--) {
            Voice * voice = voices[i];

            voices.pop_back();
            voice->unpatch();

            if (voice->getSound() != nullptr) {
                vector <Voice *> * voicesFromMap = &voiceMap[ voice->getSound() ];
                voicesFromMap->erase(remove(voicesFromMap->begin(),
                                            voicesFromMap->end(),
                                            voice), voicesFromMap->end());
            }

            // this crashes so it is done when changing project
//            delete voice;
        }
    }

    numVoices = newVoiceAmount;
}

int Voices::getVoicesAmount()
{
    return numVoices;
}

void Voices::muteSound(shared_ptr<Sound> s)
{
    for ( auto &v : voices ) {
        if ( v->getSound() == s ) {
            v->stop();
        }
    }
}

void Voices::enableEnvelope(bool e) {
    for ( auto &v : voices ) {
        v->enableEnvelope(e);
    }
}

void Voices::enableReverse(bool e) {
    for ( auto &v : voices ) {
        v->enableReverse(e);
    }
}

void Voices::setPitch(float p) {
    for ( auto &v : voices ) {
        v->setPitch(p);
    }
}

void Voices::pitchBend(float delta)
{
    for ( auto &v : voices ) {
        v->pitchBend(delta);
    }
}

void Voices::setActive(bool v)
{
    if (!v) {
        for (unsigned int i = 0; i < voices.size(); ++i) {
            voices[i]->stop();
        }
    }
}

