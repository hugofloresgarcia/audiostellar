#include "Sound.h"
#include "../Utils/Utils.h"
#include "../GUI/ColorPalette.h"
#include "../Utils/Tooltip.h"
#include "Voices.h"

ofImage Sound::imgGlow;

ofColor Sound::colorSelected = ofColor(232, 11, 85);
//int Sound::deadSounds = 0;

Sound::Sound(string _name, ofVec3f _position, int _folder) {
    
    name = _name;
    position = _position;
    originalPosition = _position;
    folder = _folder;

    if ( !Sound::imgGlow.isAllocated() ) {
        Sound::imgGlow.load("assets/star0_24.png");
    }
}

Sound::Sound(string _name) {
    name = _name;
    position.set(ofRandom(ofGetWidth() / 3, ofGetWidth() * 2 / 3),
                 ofRandom(ofGetHeight() / 3, ofGetHeight() * 2 / 3));
}

Sound::~Sound()
{
    sample.unLoad();

    // it looks like around 20 sounds never die for some reason
    // should research more on this
    // my guesses are sequence unit
//    deadSounds++;
//    ofLog() << "dead " << deadSounds;
}

void Sound::update() {
    if ( isPlaying && size < SIZE_ORIGINAL * 2 ) {
        size += sizeInc;
    } else if ( !isPlaying ) {
        if ( size > SIZE_ORIGINAL ) {
            size -= sizeInc;
            unglowing = true;
        } else {
            if ( unglowing ) {
                unglowing = false;
                if ( !hovered ) {
                    stopGlow();
                }
            }
        }
    }
}
 void Sound::updatePlaying(bool isPlaying)
{
     this->isPlaying = isPlaying;
}

void Sound::drawGlow() {
    if ( glowOpacity > 0 ) {
        int glowSize = size*12;
        if ( !selected ) {
            ofSetColor( ColorPalette::getInstance()->getColor(cluster), glowOpacity );
        } else {
            ofSetColor(colorSelected.r, colorSelected.g, colorSelected.b, glowOpacity);
        }

        if ( glowSize == SIZE_GLOW ) {
            Sound::imgGlow.draw(position.x - glowSize / 2,
                                position.y - glowSize / 2);
        } else {
            Sound::imgGlow.draw(position.x - glowSize / 2,
                                position.y - glowSize / 2,
                                glowSize,
                                glowSize);
        }

    }
}

void Sound::draw() {
    
    if ( !sample.loaded() ) {
        return;
    }

    ofFill();
    drawGlow();
    
    if (mute) {
        ofColor clusterColor = ColorPalette::getInstance()->getColor(cluster);
        ofSetColor(clusterColor, 10);
    }
    else {
        if ( !selected ) {
            ofColor clusterColor = ColorPalette::getInstance()->getColor(cluster);
            clusterColor.a = floor(colorOpacity);
            ofSetColor( clusterColor );
            
            if ( clusterIsHovered ) {
                if ( colorOpacity < 255 ) {
                    colorOpacity += ofGetLastFrameTime() * 500;
                }
            } else {
                if ( colorOpacity > OPACITY_MIN ) {
                    colorOpacity -= ofGetLastFrameTime() * 500;
                }
            }
            
            colorOpacity = ofClamp(colorOpacity, 0, 255);
            
        } else {
            ofSetColor(colorSelected, 255);
        }
    }
    
    ofDrawCircle(position.x, position.y, size );
}

void Sound::startGlow() {
    glowOpacity = 100;
}
void Sound::stopGlow() {
    glowOpacity = 0;
}
void Sound::setHovered(bool b) {
    if ( b ) {
        startGlow();
    } else {
        if ( !isPlaying ) {
            stopGlow();
        }
    }
    
    hovered = b;
}
bool Sound::getHovered() {
    return hovered;
}

ofVec2f Sound::getPosition() {
    return position;
}

ofVec2f Sound::getOriginalPosition()
{
    return originalPosition;
}

void Sound::setCluster(int cluster)
{
    this->cluster = cluster;
}
string Sound::getFileName() {
    return name;
}
int Sound::getCluster() {
    return cluster;
}

int Sound::getFolder() {
    return folder;
}

bool Sound::getHide() {
    return hide;
}
void Sound::setHide(bool h) {
    hide = h;
}

void Sound::toggleHide(){
    hide = !hide;
}

void Sound::glow() {
    if ( !hovered ) {
        startGlow();
    }
}

pdsp::SampleBuffer *Sound::getSampleBuffer()
{
    if ( !sample.loaded() ) {
        loadSample();
    }
    return &sample;
}

void Sound::loadSample()
{
    if ( !fileNotFound ) {
        sample.load(name);
        if ( !sample.loaded() ) {
            errorLoading = true;
        }
    }
}

bool Sound::isSampleLoaded()
{
    return sample.loaded();
}

bool Sound::getErrorLoading()
{
    return errorLoading;
}

bool Sound::getFileNotFound()
{
    return fileNotFound;
}

bool Sound::hasErrors()
{
    return (errorLoading || fileNotFound);
}

void Sound::useOriginalPosition() {
    position = originalPosition ;
}

void Sound::drawGui() {
    fileName = getFileName();

    string tooltipTitle = "Sound #" + ofToString( id ) + " - ";
    if ( cluster >= 0 ) {
        tooltipTitle += "Cluster #" + ofToString(cluster);
    } else {
        tooltipTitle += "No cluster";
    }

    Tooltip::tooltip x = { tooltipTitle, fileName.c_str() };
    Tooltip::setTooltip(x);
}
