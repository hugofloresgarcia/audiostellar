#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "dr_wav.h"

// Based on ofxSoundRecorderObject from ofxSoundObjects addon, by Roy Macdonald

class ThreadedRecorder : public pdsp::Unit, ofThread {
    
public:
    
    ThreadedRecorder();
    
    ThreadedRecorder(const ThreadedRecorder& other);
    ThreadedRecorder& operator=(const ThreadedRecorder& other);
    
    ~ThreadedRecorder();
        
    ofEvent<string> recordingEndEvent;
    
    bool isRecording();
    void startRecording(const string & filepath);
    void stopRecording();
    
    string getRecStateString();

    
protected:
    
    void threadedFunction() override;
    ofThreadChannel<ofSoundBuffer> writeChannel;
    
    void write(ofSoundBuffer& input);
    
private:

    void prepareUnit (int expectedBufferSize, double sampleRate) override;
    
    void releaseResources () override;

    void process (int bufferSize) noexcept override;
    
    pdsp::InputNode inputL;
    pdsp::InputNode inputR;
    
    pdsp::OutputNode output;
    
    drwav* wav_handle = NULL;
    
    enum RecState{
        IDLE = 0,
        INIT_REC,
        REC_ON,
        DEINIT_REC
    };
    
    std::atomic<RecState> recState;

    string filename;
    string filenameBuffer;
    float recStartTime = 0.0f;
    
    float tempBuffer[8192 * 2]; //max buffer size, 2 channels
    
    ofSoundBuffer writeBuffer;
    ofSoundBuffer recordingBuffer;
    ofSoundBuffer internalRecordingBuffer;
    
};
