#pragma once

#include "ofMain.h"
#include "ofxAnn.h"
#include "ofxJSON.h"
#include "ofxConvexHull.h"
#include "ofxImGui.h"

#include "Sound.h"
#include "SoundLoader.h"
#include "AudioEngine.h"

#include "../Utils/Utils.h"
#include "../GUI/ColorPalette.h"
#include "../ML/DBScan.h"
#include "../Utils/Tooltip.h"
#include "../Servers/OscServer.h"
#include "../Servers/tidalOscListener.h"
#include "../GUI/CamZoomAndPan.h"

class Voices;

struct soundCoordinatesOverride {
    shared_ptr<Sound> sound;
    ofVec2f position;
    int cluster = -1;
};

class Sounds {
private:
#define CLUSTERNAME_MAX_LENGTH 64
#define DEFAULT_DBSCAN_EPS 15
#define DEFAULT_DBSCAN_MIN_PTS 3
    
    Sounds();
    static Sounds * instance;
    
    vector< shared_ptr<Sound> > sounds;
    vector< shared_ptr<Sound> > soundsWithErrors;

    shared_ptr<Sound> hoveredSound = nullptr;
    SoundLoader soundLoader;
    void loadSamples();
    
    ofBuffer tsvFile;

    bool useOriginalPositions = true;
    bool useOriginalClusters = false;
    string currentSoundLabel = "";

    void setSpaceLimits(vector<string> soundsCoords);
    ofVec3f spaceLimits[2];
    const int spacePadding = 30;
    
    // Clusters
    set<int> clusterIds;
    vector<bool> clustersShow;
    int nClusters;
    int selectedCluster;
    vector<int> originalClusters;

    bool showClusterNames = false;
    float clusterNamesOpacity = 0;
    float clusterNamesSpeed = 5;
    void findConvexHulls();
    vector<ofPolyline> hulls;

    int idCount = 0;
    
    ofTrueTypeFont font;
    
#define BG_TRANSLATION 0.3
#define BG_ALPHA_MIN 30
#define BG_ALPHA_MAX 50
#define BG_ALPHA_STEP 0.3
#define BG_GLOW_SIZE 50

    int windowOriginalWidth;
    int windowOriginalHeight;
    ofFbo fboBackground;
    float backgroundAlpha = BG_ALPHA_MIN;
    int backgroundDirection = 1;
    
    //overrided Positions
    vector<soundCoordinatesOverride> soundPositions;
    void updateSoundPosition(shared_ptr<Sound> sound);

    //GUI
    bool isContextMenuHovered = false;

    bool preloading = false;
    void onFinishedPreloading(); //called by SoundLoader

    ////// kNN //////
    class kNNPoint {
    public:
        kNNPoint(shared_ptr<Sound> sound) {
            assert( sound != nullptr );
            this->sound = sound;
        }

        shared_ptr<Sound> sound;

        inline const ofPoint getPoint() const { return sound->getPosition(); }
    };
    vector<kNNPoint> kNNPoints;
    ofxAnn<vector<kNNPoint>> kNN;

    friend class SoundLoader;
public:
    
    static Sounds* getInstance();

    bool showSoundFilenamesTooltip = false;
    bool soundIsBeingMoved = false; //when moving sounds dragging with right click
    
    int epsDbScan = DEFAULT_DBSCAN_EPS;
    int minPts = DEFAULT_DBSCAN_MIN_PTS;
    
    void deleteClusterNames();
    
    //MAIN
    void loadSounds(ofxJSONElement &jsonFile);
    void update();
    void draw();
    void drawGui();
    void reset();
    
    //DRAW
    void drawBackground();
    void generateBackground();
    void animateClusterNames();
    void drawClusterNames();
    
    //SESSION
    Json::Value save();
    void load( Json::Value jsonData );
    
    //CLUSTERING
    void onClusterToggle(int clusterIdx);
    void doClustering();
    void reClusterSound(shared_ptr<Sound> sound);
    bool areAnyClusterNames();
    bool areAnyOriginalClusters();
    
    //MOUSE
    void mouseMoved(int x, int y);
    void mouseDragged(ofVec2f p, int button);
    void mousePressed(int x, int y , int button);
    void mouseReleased(int x, int y , int button);

    //KEYBOARD
    void keyPressed(ofKeyEventArgs &e);
    void keyReleased(ofKeyEventArgs &e);

    void allSoundsSelectedOff();
    void allSoundsHoveredOff(shared_ptr<Sound> except = NULL);
    void allSoundsClusterIsHovered();

    void revertToOriginalPositions();
    void sequenceUnitReprocess();
    
    //SETTERS
    void setFilenameLabel(string fileName);
    void setDBScanClusters( vector<dbscanPoint> points );
    void setFoldersAsClusters();
    void setOriginalClusters();
    void setUseOrigPos(bool v);
    void setShowSoundFilenames(bool v);
    void setShowClusterNames(bool show);
    void toggleShowClusterNames();

    //GETTERS
    shared_ptr<Sound> getNearestSound(ofVec2f position);
    unsigned int getSoundCount();
    shared_ptr<Sound> getHoveredSound();
    shared_ptr<Sound> getSoundByFilename(string filename);
    shared_ptr<Sound> getSoundById(int id);
    const vector<shared_ptr<Sound>> & getSounds();
    vector<shared_ptr<Sound>> getSoundsByClusters(set<int> clustersId);
    vector<shared_ptr<Sound>> getSoundsByClusters(vector<int> clustersId);
    vector<shared_ptr<Sound>> getSoundsByClusters(map<int,int> clustersId);
    vector<shared_ptr<Sound>> getSoundsByCluster(int clusterId);
    vector<shared_ptr<Sound>> getSoundsByCluster(string name, bool includeMuted = true);
    vector<shared_ptr<Sound>> getNeighbors(shared_ptr<Sound> s, float threshold);
    vector<shared_ptr<Sound>> getNeighbors(ofVec2f position, float threshold);
    vector<ofPoint> getSoundsAsPoints( bool originalPositions = true );
    bool getUseOriginalPositions();
    bool& getUseOriginalClusters();
    bool getShowSoundFilenames();
    int getInitialWindowWidth();
    int getInitialWindowHeight();
    int getHoveredClusterID();
    ofRectangle getClusterBB( int clusterID );
    float getSoundLoaderProgress();

    //COORDINATES
    ofVec3f camToSoundCoordinates(ofVec3f camCoordinates);
    ofVec3f camToSoundCoordinates(ofVec2f camCoordinates);
    ofVec3f soundToCamCoordinates(ofVec3f soundCoordinates);
    
    string selectFolder();
    void exportFiles();
    
    char clusterNameBeingEdited[CLUSTERNAME_MAX_LENGTH] = "\0";
    vector<string> clusterNames;
    const string clusterNameDefault = "";
    void nameCluster(unsigned int clusterID);

    bool doPreloading = true;
    bool isPreloading();
    
    bool dbScanSettingsEdited = false;
    
    bool areDBScanSettingsEdited();
};
