#include "Recorder.h"

#include "../Units/Units.h"
#include "../GUI/UI.h"

Recorder::Recorder() {
    ofAddListener( threadedRecorder.recordingEndEvent, this, &Recorder::onRecordingEnd );
}

Recorder::~Recorder() {
    
}

void Recorder::patch() {
    
    Units::getInstance()->masterGain.ch(0) >> threadedRecorder.in("0");
    Units::getInstance()->masterGain.ch(1) >> threadedRecorder.in("1");
    
    threadedRecorder.out("0") >> AudioEngine::getInstance()->getBlackhole();
}

void Recorder::startRecording(const string & filename) {
    threadedRecorder.startRecording(filename);
}

void Recorder::stopRecording() {
    threadedRecorder.stopRecording();
}

bool Recorder::isRecording() {
    return threadedRecorder.isRecording();
}

void Recorder::drawGui() {
    
//    ImGuiWindowFlags window_flags = 0;
//    window_flags |= ImGuiWindowFlags_NoTitleBar;
//    window_flags |= ImGuiWindowFlags_NoMove;
//    window_flags |= ImGuiWindowFlags_NoResize;
//    window_flags |= ImGuiWindowFlags_NoCollapse;
//    window_flags |= ImGuiWindowFlags_AlwaysAutoResize;
    
//    ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.1f, 0.1f, 0.1f, 0.00f));
//    ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(0.0f, 0.0f, 0.0f, 0.00f));
    
//    auto mainSettings = ofxImGui::Settings();
//    mainSettings.windowPos = ImVec2(ofGetWidth() - 36, -7);
//    mainSettings.lockPosition = true;
    
    float buttonSize = 18.f;
    
//    if ( ofxImGui::BeginWindow("Rec", mainSettings, window_flags) ) {
//        ImGui::BringWindowToDisplayFront(ImGui::GetCurrentWindow());
        
    ImVec2 pos = ImGui::GetCursorScreenPos();
    ImDrawList* draw_list = ImGui::GetWindowDrawList();

    ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32(0, 0, 0, 0));

    if ( threadedRecorder.isRecording() ) {
        if (ImGui::Button("##Stop", ImVec2(buttonSize, buttonSize)) ) {
            stopRecording();
        }
        if ( (ofGetElapsedTimeMillis() % 2000) < 1000 ) {
            draw_list->AddRectFilled(ImVec2(pos.x + buttonSize * 0.24, pos.y + buttonSize * 0.25),                                ImVec2(pos.x + buttonSize * 0.75, pos.y + buttonSize * 0.75),
                                     IM_COL32(255, 0, 0, 255)
                                     );
        }
    }
    else {
        if (ImGui::Button("##Start", ImVec2(buttonSize, buttonSize)) ) {
            startRecording( ofFilePath::getUserHomeDir() + "/AudioStellar_" + ofGetTimestampString() + ".wav");
        }
        float radius = buttonSize * 0.25;
        draw_list->AddCircleFilled(ImVec2(pos.x + buttonSize * 0.5, pos.y + buttonSize * 0.5), radius, IM_COL32(255, 0, 0, 255));
    }
    if (ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::RECORDER);
    ImGui::PopStyleColor();
//    }

    
    if (haveToOpenSaveDialog) {
        ofFile myFile(recordedFilename);
        #ifdef TARGET_OS_MAC
        string defaultName = "new_recording.wav";
        #else
        string defaultName = ofFilePath::getUserHomeDir() + "/new_recording.wav";
        #endif
        ofFileDialogResult result = Gui::getInstance()->showSaveDialog(defaultName, "Save audio file");

        if(result.bSuccess) {
            string path = result.getPath();
            if(!ofIsStringInString(path, ".wav")){
                path += ".wav";
            }

            if ( recordedFilename != path ) {
                bool success = myFile.copyTo(path);
                if (success) myFile.remove();
//                myFile.moveTo(path, true, true);
            }
        }
        else {
            myFile.remove();
        }
        haveToOpenSaveDialog = false;
    }
}

void Recorder::onRecordingEnd(string &filename) {
    recordedFilename = filename;
    haveToOpenSaveDialog  = true;
}

void Recorder::reset() {
    threadedRecorder.disconnectAll();
    patch();
}







