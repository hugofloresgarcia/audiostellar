#pragma once

#include "ofMain.h"

class SoundLoader : public ofThread
{
    int loadedCount = 0;
public:
    SoundLoader();

    void threadedFunction();
    int getLoadedCount() const;
    float getProgress();
};
