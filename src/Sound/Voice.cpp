#include "Voice.h"

Voice::Voice()
{
    panL.enableSmoothing(50);
    panR.enableSmoothing(50);
}

Voice::~Voice()
{
    die();
    unpatch();
    unload();
}

void Voice::die()
{
    if ( sound != nullptr ) {
        sound->updatePlaying(false);
    }
}

void Voice::patch() {
    if ( patched ) return;

    addModuleOutput( "0", out_amp0 );
    addModuleOutput( "1", out_amp1 );

    addModuleInput("speed", sampler0.in_pitch());
    addModuleInput("speed", sampler1.in_pitch());
    
    sampleTrigger >> sampler0 >>             mix0_amp0 >> out_amp0;
    sampleTrigger >> sampler1 >>             mix0_amp1 >> out_amp1;

                     sampler0 >> env_amp0 >> mix1_amp0 >> out_amp0;
                     sampler1 >> env_amp1 >> mix1_amp1 >> out_amp1;

    envTrigger >> env >> env_amp0.in_mod();
                  env >> env_amp1.in_mod();

    panL >> out_amp0.in_mod();
    panR >> out_amp1.in_mod();
    
    mix0_amp0.set(1.0f);
    mix0_amp1.set(1.0f);
    mix1_amp0.set(0.f);
    mix1_amp1.set(0.f);
    
    out_amp0.set(0.0f);
    out_amp1.set(0.0f);
    
    patched = true;
    //    sample.setVerbose(true);
}

void Voice::unpatch()
{
    out_amp0.disconnectIn();
    out_amp1.disconnectIn();

    sampler0.disconnectAll();
    sampler1.disconnectAll();

    sampleTrigger.disconnectAll();
    envTrigger.disconnectAll();

    panL.disconnectAll();
    panR.disconnectAll();

    if ( sound != nullptr ) {
        sound->updatePlaying(false);
    }

    patched = false;
}

void Voice::load(string path)
{
    pdsp::SampleBuffer * sample = new pdsp::SampleBuffer();
    sample->load(path);
    sampler0.setSample( sample, 0, 0 );

    if (sample->channels == 1) {
        sampler1.setSample(sample, 0, 0);
    } else {
        sampler1.setSample(sample, 0, 1);
    }
}

void Voice::unload() {
    //I don't think any of these is needed
    stop();
//    sample.unLoad();
}

void Voice::play(float volume) {
    setVolume(volume);
    envTrigger.trigger(1.0f);
    sampleTrigger.trigger(1.0f);
    // This is needed if triggering more than one sone
    sampler0.setIsPlaying(true);
//    sampler1.setIsPlaying(true);

    isStopped = false;
}

void Voice::stop() {
    setVolume(0.0f);
    panL.set(0);
    panR.set(0);

    // this does nothing
    // triggering 1.0 will play sample but 0.0 won't stop it
    sampleTrigger.off();
    envTrigger.off();

    isStopped = true;

    // i think this isn't needed with isStopped
//    if ( sound != nullptr ) {
//        sound->updatePlaying(false);
    //    }
}

bool Voice::isPatched()
{
    return patched;
}

bool Voice::isLoaded() {
    if ( sound == nullptr ) return false;

    pdsp::SampleBuffer * s = sound->getSampleBuffer();
    return s != nullptr && s->loaded();
}

bool Voice::isPlaying() {
    if ( sound == nullptr ) return false;

    pdsp::SampleBuffer * s = sound->getSampleBuffer();

    if ( s == nullptr || isStopped ) return false;

    // this can return false if sample
    // hasn't been stopped but the sample finished
    return sampler0.is_playing();
}

float Voice::getPlayerPosition() {
    return sampler0.meter_position();
}

float Voice::getSoundDuration() {
    if ( sound == nullptr ) return false;

    pdsp::SampleBuffer * s = sound->getSampleBuffer();

    int length = (s->length / s->fileSampleRate) * 1000; // milliseconds
    if (reverse) {
        length = length * sampleStart;
    }
    else {
        length = length * (1 - sampleStart);
    }
    return length;
}

int Voice::getSampleRate() {
    return sound->getSampleBuffer()->fileSampleRate;
}

int Voice::getSampleLength() {
    return sound->getSampleBuffer()->length;
}

float** Voice::getBuffer() {
    return sound->getSampleBuffer()->buffer;
}

void Voice::setVolume(float volume) {
    this->volume = volume;
}

void Voice::setPan(float pan)
{
    panL.set( ofMap( (1.0f - pan)*2, 0.0001f, 1.0f, 0.f, this->volume, true ) );
    panR.set( ofMap( pan*2, 0.0001f, 1.0f, 0.f, this->volume, true ) );

//    panL >> out_amp0.in_mod();
//    panR >> out_amp1.in_mod();
}

void Voice::enableEnvelope(bool e) {
    
    if(e) {
        0.f >> mix0_amp0.in_mod();
        0.f >> mix0_amp1.in_mod();
        1.f >> mix1_amp0.in_mod();
        1.f >> mix1_amp1.in_mod();
    }
    else {
        1.f >> mix0_amp0.in_mod();
        1.f >> mix0_amp1.in_mod();
        0.f >> mix1_amp0.in_mod();
        0.f >> mix1_amp1.in_mod();
    }
}

void Voice::enableReverse(bool e) {
    
    if(e) {
        -1.0f >> sampler0.in_direction();
        -1.0f >> sampler1.in_direction();
    }
    else {
        1.0f >> sampler0.in_direction();
        1.0f >> sampler1.in_direction();
    }
    reverse = e;
}

void Voice::setEnvelope(float attack, float hold, float release, int envMode) {
    
    float a, h, r;
    
    if(envMode == 0) {
        a = attack;
        h = hold;
        r = release;
    }
    
    if(envMode == 1) {
        float duration = getSoundDuration();
        a = duration * attack / 100;
        h = duration * hold / 100;
        r = duration * release / 100;
    }
    
    env.set(a, h, r);
}

void Voice::setPitch(float p) {
    pitch = p;
    p >> sampler0.in_pitch();
    p >> sampler1.in_pitch();
}

void Voice::pitchBend(float delta)
{
    setPitch(pitch + delta);
}

void Voice::setStart(float s) {
    s >> sampler0.in_start();
    s >> sampler1.in_start();
    sampleStart = s;
}

void Voice::setSound(shared_ptr<Sound> s)
{
    if ( sound != nullptr ) {
        sound->updatePlaying(false);
    }
    sound = s;
    pdsp::SampleBuffer * sample = sound->getSampleBuffer();
    //stop();
    sampler0.setSample( sample, 0, 0 );

    if (sample->channels == 1) {
        sampler1.setSample(sample, 0, 0);
    } else {
        sampler1.setSample(sample, 0, 1);
    }

}

shared_ptr<Sound> Voice::getSound()
{
    return sound;
}
