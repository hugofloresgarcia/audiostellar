#include "ThreadedRecorder.h"

ThreadedRecorder::ThreadedRecorder() {
    
    startThread();
    
    addInput("0", inputL);
    addInput("1", inputR);
    
    addOutput("0", output);
    updateOutputNodes();
    
    if(dynamicConstruction){
        prepareToPlay(globalBufferSize, globalSampleRate);
    }
    
    recState = IDLE;

}

ThreadedRecorder::ThreadedRecorder(const ThreadedRecorder & other) : ThreadedRecorder() {
    std::cout<<"[pdsp] warning! SoundRecorder copy constructed\n";
    pdsp::pdsp_trace();
}

ThreadedRecorder& ThreadedRecorder::operator=(const ThreadedRecorder& other) {
    return *this;
    std::cout<<"[pdsp] warning! SoundRecorder moved constructed\n";
    pdsp::pdsp_trace();
}

ThreadedRecorder::~ThreadedRecorder() {
    stopRecording();
    writeChannel.close();
    if( isThreadRunning() ) {
        waitForThread(true);
    }
}

void ThreadedRecorder::prepareUnit ( int expectedBufferSize, double sampleRate ) {

}

void ThreadedRecorder::releaseResources() {
    
}

void ThreadedRecorder::process (int bufferSize) noexcept {
    
    if (recState != IDLE) {
        
        int signalStateL;
        const float* signalBufferL = processInput(inputL, signalStateL);
        
        int signalStateR;
        const float* signalBufferR = processInput(inputR, signalStateR);
        
        switch( signalStateL ) {
            case pdsp::Changed : case pdsp::Unchanged:
                for (int i = 0; i < bufferSize * 2; i++) {
                    tempBuffer[i] = 0.0f;
                }
                break;
            case pdsp::AudioRate :
                for (int i = 0; i < bufferSize; i++) {
                    tempBuffer[i * 2] = signalBufferL[i];
                    tempBuffer[(i * 2) + 1] = signalBufferR[i];
                }
                break;
        }
        
        writeBuffer.copyFrom(tempBuffer, globalBufferSize   , 2, globalSampleRate);
        writeChannel.send(writeBuffer);
        
    }
    
    setOutputToZero(output);
}

void ThreadedRecorder::threadedFunction() {
    
    ofSoundBuffer buffer;
    while(writeChannel.receive(buffer)) {
        write(buffer);
    }
    
}

void ThreadedRecorder::write(ofSoundBuffer& input) {
    
    if (recState == INIT_REC) {
        recState = REC_ON;
        
        drwav_data_format format;
        format.container = drwav_container_riff;
        format.format = DR_WAVE_FORMAT_IEEE_FLOAT;
        format.channels = input.getNumChannels();
        format.sampleRate = input.getSampleRate();
        format.bitsPerSample = 32;
        
        wav_handle = drwav_open_file_write( filenameBuffer.c_str(), &format);
        internalRecordingBuffer = input;
        
    }
    else if (recState == DEINIT_REC) {
        // finished recording
        drwav_uninit(wav_handle);
        wav_handle  = NULL;
        recState = IDLE;
        
        ofNotifyEvent(recordingEndEvent, filenameBuffer);
    }
    
    if (recState == REC_ON && wav_handle != NULL) {
        drwav_uint64 samplesWritten = drwav_write_pcm_frames(wav_handle, input.getNumFrames(), input.getBuffer().data());
        if(samplesWritten != input.getNumFrames()){
            ofLogWarning("SoundRecorder") << "samplesWritten " << samplesWritten << " != input.getNumFrames() " <<  input.getNumFrames() << endl;
        }
    }
}

bool ThreadedRecorder::isRecording() {
    return recState != IDLE;
}

void ThreadedRecorder::startRecording(const string & filename) {

    if (recState == IDLE) {
        if (filename.empty()) {
            this->filename = ofFilePath::getUserHomeDir() + "/AudioStellar_" + ofGetTimestampString() + ".wav";
        }
        else {
            this->filename = filename;
        }
        {
            this->filenameBuffer = this->filename;
            recStartTime = ofGetElapsedTimef();
        }
        recState = INIT_REC;
    }
    else {
        ofLogWarning("SoundRecorder") << "can not start recording when there is already another recording happening. Please stop this recording before beggining a new one";
    }
}

void ThreadedRecorder::stopRecording() {
    if (recState == REC_ON) {
        recState = DEINIT_REC;
    }
}

string ThreadedRecorder::getRecStateString() {
    RecState state;
    state = recState;

    if(state == IDLE)return "IDLE";
    if(state == INIT_REC)return "INIT_REC";
    if(state == REC_ON)return "RECORDING";
    if(state == DEINIT_REC)return "DEINIT_REC";
    return "";
}
