#include "Sounds.h"
#include "Voices.h"
#include "../Units/Units.h"
#include "../Units/SequenceUnit.h"
#include "../GUI/UI.h"

Sounds* Sounds::instance = nullptr;

Sounds* Sounds::getInstance(){
    if( instance == nullptr){
        instance = new Sounds();
    }
    
    return instance;
}

Sounds::Sounds() : kNN(kNNPoints) {
    useOriginalPositions = false;

    windowOriginalWidth = ofGetWidth();
    windowOriginalHeight = ofGetHeight();

    font.load( OF_TTF_MONO, 10 );
}

void Sounds::loadSamples()
{
    //This seems to cancel last preloading and start preloading again
    soundLoader.startThread();
}

void Sounds::loadSounds(ofxJSONElement &jsonFile){
    
    ofFile fileCheck;
    unsigned int filesDontExist = 0;
    unsigned int duplicatedFiles = 0;

    if(!sounds.empty()){
        reset();
    }
    
    string soundsDir = jsonFile["audioFilesPathAbsolute"].asString();
    
    vector<string> soundFiles = ofSplitString(jsonFile["tsv"].asString(), "|");
    
    setSpaceLimits(soundFiles);
    
    for(int i = 0; i<soundFiles.size() - 1; i++) {
        if(soundFiles[i] == "")continue;
        vector<string>lineElements = ofSplitString(soundFiles[i],",");
        
        int folder = ofToInt(lineElements[TSV_FIELD_FOLDER]);
        string name = lineElements[TSV_FIELD_NAME];

        ofVec3f position = soundToCamCoordinates( ofVec3f(ofToFloat(lineElements[TSV_FIELD_X]),
                                                          ofToFloat(lineElements[TSV_FIELD_Y]),
                                                          ofToFloat(lineElements[TSV_FIELD_Z])) );
        
        bool isFileDuplicated = false;
        for (int i = 0; i < sounds.size(); i++) {
            shared_ptr<Sound> s = sounds[i];
            if ( sounds[i]->position.x == position.x &&
                 sounds[i]->position.y == position.y) {
                duplicatedFiles++;
                isFileDuplicated = true;
                break;
            }
        }
        if ( isFileDuplicated ) continue;

        string file = soundsDir + name;
        
        shared_ptr<Sound> sound = make_shared<Sound>(file, position, folder);
        sounds.push_back(sound);
        
        originalClusters.push_back(folder);

        if ( !fileCheck.doesFileExist(file, false) ) {
            filesDontExist++;
            sound->fileNotFound = true;
        }

        kNNPoints.push_back( kNNPoint(sound) );

        sound->id = idCount;
        idCount++;
    }

    if ( filesDontExist > 0 ) {
        string message = "Couldn't find " + ofToString(filesDontExist) + " sound files in folder " + soundsDir + " .";
        Gui::getInstance()->showMessage(message, "Error");
    }
    if ( duplicatedFiles > 0 ) {
        ofLog() << "skipped " << duplicatedFiles << " duplicated files.";
    }

    if ( doPreloading ) {
        loadSamples();
        Gui::getInstance()->showSampleLoadingScreen();
    }

    kNN.buildIndex();
}

void Sounds::reset() {
// AudioEngine should be stopped before calling this   
    kNNPoints.clear();
    kNN.buildIndex();

    sounds.clear();
    clusterNames.clear();
    hulls.clear();
    clusterIds.clear();
    soundPositions.clear();
    idCount = 0;
    backgroundAlpha = BG_ALPHA_MIN;
    epsDbScan = DEFAULT_DBSCAN_EPS;
    minPts = DEFAULT_DBSCAN_MIN_PTS;
}

void Sounds::update() {
    for (int i = 0; i < sounds.size(); i++) {
        sounds[i]->update();
    }

    animateClusterNames();
}

void Sounds::draw() {
    ofFill();
    drawBackground();
    ofSetColor(255,255,255,255);

    if (clusterNamesOpacity > 0) {

        ofSetColor(ofColor::white, clusterNamesOpacity);

        for ( int i = 0 ; i < hulls.size() ; i++ ) {
            ImVec2 textSize = ImGui::CalcTextSize(clusterNames[i].c_str(), NULL, true);
            font.drawString(clusterNames[i], hulls[i].getCentroid2D().x - (textSize.x / 2), hulls[i].getCentroid2D().y);
        }
    }

    float zoomScale = CamZoomAndPan::getInstance()->getScale();
    float circleResolution = int( ofClamp((zoomScale * 1.5f) + 8.5f, 10.f, 100.f) );
    
    ofSetCircleResolution(circleResolution);
    
    for (int i = 0; i < sounds.size(); i++) {
        if(!sounds[i]->getHide()) {
            sounds[i]->draw();

        }
        if ( showSoundFilenamesTooltip && sounds[i]->getHovered() ) {
            sounds[i]->drawGui();
        }
    }

    if ( soundIsBeingMoved ) {
        if ( hoveredSound == nullptr ) return;

        ofPoint pointPosition = hoveredSound->getPosition();
        int count = 2;
        auto kNNResult = kNN.findNeighbors( pointPosition, count );

        if(kNNResult.size() <= 1 ){
            return;
        }

        for ( unsigned int i = 0 ; i < count ; i++ ) {
            shared_ptr<Sound> s = kNNPoints[ kNNResult.indices[i] ].sound;
            ofNoFill();
            ofDrawCircle( s->getPosition(), 5 );
            ofFill();
        }
    }
}

void Sounds::drawGui(){

}

void Sounds::drawBackground()
{
    ofPushMatrix();
    ofTranslate( -windowOriginalWidth*BG_TRANSLATION, -windowOriginalHeight*BG_TRANSLATION );
    ofSetColor(255,255,255,round(backgroundAlpha));
    fboBackground.draw(0,0);
    
    backgroundAlpha += BG_ALPHA_STEP * backgroundDirection * ofGetLastFrameTime() * 40;
    
    if ( backgroundAlpha >= BG_ALPHA_MAX ) {
        backgroundAlpha = BG_ALPHA_MAX;
        backgroundDirection = -1;
    } else if ( backgroundAlpha <= BG_ALPHA_MIN ) {
        backgroundAlpha = BG_ALPHA_MIN;
        backgroundDirection = 1;
    }
    ofPopMatrix();
}

void Sounds::generateBackground()
{
    fboBackground.allocate(windowOriginalWidth * 1.5, windowOriginalHeight * 1.5, GL_RGBA);
    
    fboBackground.begin();
    ofClear(25,0);
    ofPushMatrix();
    ofTranslate( windowOriginalWidth * BG_TRANSLATION, windowOriginalHeight * BG_TRANSLATION );
    
    float glowSize = SIZE_ORIGINAL * BG_GLOW_SIZE;
    
    for ( shared_ptr<Sound> sound : sounds ) {
        if ( sound->getCluster() < 0 ) {
            continue;
        }
        ofSetColor( ColorPalette::getInstance()->getColor(sound->getCluster()) );

        Sound::imgGlow.draw(sound->position.x - glowSize / 2,
                            sound->position.y - glowSize / 2,
                            glowSize,
                            glowSize);
    }
    
    ofNoFill();
    ofSetColor(255,0,0);
    ofPopMatrix();

    fboBackground.end();
}

void Sounds::animateClusterNames() {
    if (showClusterNames){
        if (clusterNamesOpacity < 255){
            clusterNamesOpacity += clusterNamesSpeed;
        }
    } else {
        if (clusterNamesOpacity > 0){
            clusterNamesOpacity -= clusterNamesSpeed;
        }
    }
}

void Sounds::findConvexHulls() {
    ofxConvexHull convexHull;
    vector<ofVec3f> cluster;
//    string strDebug = "clusterIDS: ";
    
    hulls.clear();
    
    for ( auto clusterId : clusterIds ) {
//        strDebug += ofToString(clusterId) + ",";
        cluster.clear();
        for ( shared_ptr<Sound> sound : sounds ) {
            if ( sound->getCluster() == clusterId ) {
                cluster.push_back( sound->getPosition() );
            }
        }
        
        ofPolyline polyHull;
        if ( cluster.size() >= 3 ) {
            vector<ofPoint> hull = convexHull.getConvexHull( cluster );
            
            for ( int i = 0 ; i < hull.size() ; i++ ) {
                polyHull.addVertex( hull[i] );
            }
            polyHull.close();
            
            //        agrando desde el centro
            for( int i = 0 ; i < polyHull.size() ; i++ ) {
                auto &p = polyHull.getVertices()[i];
                
                p.x += polyHull.getNormalAtIndex(i).x * 15;
                p.y += polyHull.getNormalAtIndex(i).y * 15;
            }
        }
        
        hulls.push_back( polyHull );
    }
}

void Sounds::updateSoundPosition(shared_ptr<Sound> sound) {
    soundCoordinatesOverride * found = nullptr;
    for ( int i = 0 ; i < soundPositions.size() ; i++ ) {
        if ( soundPositions[i].sound == sound ) {
            found = &soundPositions[i];
            break;
        }
    }

    if ( found == nullptr ) {
        found = new soundCoordinatesOverride;
        found->sound = sound;
        
        soundPositions.push_back(*found);
    }
    
    found->position.x = sound->position.x;
    found->position.y = sound->position.y;
}

void Sounds::onFinishedPreloading()
{
    soundsWithErrors.clear();

    for ( vector<shared_ptr<Sound>>::iterator it = sounds.begin() ; it != sounds.end() ; ) {
        shared_ptr<Sound> s = *it;
        if ( s->hasErrors() ) {
            soundsWithErrors.push_back(s);
            it = sounds.erase(it); //with ++ it is safe to erase (it++ returns it )
        } else {
            it++;
        }
    }

    if ( !soundsWithErrors.empty() ) {
        kNNPoints.clear();
        for ( auto &s : sounds ) {
            kNNPoints.push_back( kNNPoint(s) );
        }
        kNN.buildIndex();

        for ( auto &s : soundsWithErrors ) {
            if ( s->getErrorLoading() ) {
                ofLog() << "Couldn't load: " << s->getFileName();
            }
            if ( s->getFileNotFound() ) {
                ofLog() << "File not found: " << s->getFileName();
            }
        }
    }
}

void Sounds::mouseMoved(int x, int y) {
    shared_ptr<Sound> nearestSound = getNearestSound( ofVec2f(x,y) );

    if ( nearestSound != hoveredSound ) {
        allSoundsHoveredOff(nearestSound);
        if (nearestSound != nullptr) {
            if ( !nearestSound->getHovered() ) {
                nearestSound->setHovered(true);
                hoveredSound = nearestSound;
            }
        } else {
            hoveredSound = nullptr;
        }
    }


    // this is done on every mousemove
    // maybe something better can be done here
    //Select cluster

    set<int> selectedClusters;
    selectedCluster = -1;
    for ( int i = 0 ; i < hulls.size() ; i++ ) {
        if ( hulls[i].size() >= 3 && hulls[i].inside( x, y) ) {
            selectedClusters.insert(i);
        }
    }

    for ( auto &cluster : selectedClusters ) {
        if ( selectedCluster == -1 ) {
            selectedCluster = cluster;
        } else {
            if ( hulls[cluster].getArea() < hulls[selectedCluster].getArea() ) {
                selectedCluster = cluster;
            }
        }
    }

    if ( Gui::getInstance()->getFocusOnClusters() &&
         !soundIsBeingMoved &&
         !showClusterNames &&
         !Sounds::getInstance()->isPreloading() ) {

        for ( shared_ptr<Sound> sound : sounds ) {
            if ( (selectedCluster == -1 && !showClusterNames) ||
                 sound->getCluster() == selectedCluster ) {
                sound->clusterIsHovered = true;
            } else {
                sound->clusterIsHovered = false;
            }
        }
    }
}

void Sounds::mouseDragged(ofVec2f p, int button) {
    if ( !soundIsBeingMoved ) {
        mouseMoved( p.x, p.y );
    }

    if ( !useOriginalPositions && button == 2 ) {
        if ( hoveredSound != nullptr ) {
            hoveredSound->position = p;
            reClusterSound( hoveredSound );
            updateSoundPosition(hoveredSound);
            soundIsBeingMoved = true;
        }
    }
}

void Sounds::mousePressed(int x, int y, int button) {

}

void Sounds::mouseReleased(int x, int y, int button)
{
    //end of drag
    if ( soundIsBeingMoved ) {
        sequenceUnitReprocess();
        kNN.buildIndex(); // test this
        generateBackground();
        findConvexHulls();
        soundIsBeingMoved = false;
    }
}


void Sounds::allSoundsSelectedOff() {
    for(int i = 0; i < sounds.size(); i++) {
        sounds[i]->selected = false;
    }
}
void Sounds::allSoundsHoveredOff(shared_ptr<Sound> except) {
    for(int i = 0; i < sounds.size(); i++) {
        if ( except != NULL && sounds[i] == except ) {
            continue;
        }
        if ( sounds[i]->getHovered() ) {
            sounds[i]->setHovered(false);
        }
    }
}

void Sounds::allSoundsClusterIsHovered()
{
    for(int i = 0; i < sounds.size(); i++) {
        sounds[i]->clusterIsHovered = true;
    }
}

shared_ptr<Sound> Sounds::getNearestSound(ofVec2f position) {

    if ( sounds.size() == 0 ) {
        return nullptr;
    }

    ofPoint pointPosition ( position.x, position.y );
    auto kNNResult = kNN.findNearest( pointPosition );

    if(kNNResult.size() == 0 ){
        return nullptr;
    }

   shared_ptr<Sound> s = kNNPoints[ kNNResult.indices[0] ].sound;
   if ( kNNResult.distances[0] <= pow(s->size,3.2) ) {
       return s;
   }

   return nullptr;
}

shared_ptr<Sound> Sounds::getHoveredSound() {
    return hoveredSound;
}

void Sounds::setSpaceLimits(vector<string> soundsCoords) {
    if ( soundsCoords.empty() ) return;

    ofVec3f max(-99999999, -99999999, -99999999);
    ofVec3f min(99999999, 99999999, 99999999);
    // spaceLimits = {ofVec2f(0,0),ofVec2f(1500,1500)};
    spaceLimits[0] = max;
    spaceLimits[1] = min;
    
    
    for(int i = 0; i< soundsCoords.size() - 1; i++) {
        if(soundsCoords[i] == "")continue;
        auto lineElements = ofSplitString(soundsCoords[i],",");
        
        ofVec3f position(ofToFloat(lineElements[TSV_FIELD_X]), ofToFloat(lineElements[TSV_FIELD_Y]), ofToFloat(lineElements[TSV_FIELD_Z]));
        
        spaceLimits[0].x = Utils::getMaxVal(position.x, spaceLimits[0].x);
        spaceLimits[0].y = Utils::getMaxVal(position.y, spaceLimits[0].y);
        spaceLimits[0].z = Utils::getMaxVal(position.z, spaceLimits[0].z);
        
        spaceLimits[1].x = Utils::getMinVal(position.x, spaceLimits[1].x);
        spaceLimits[1].y = Utils::getMinVal(position.y, spaceLimits[1].y);
        spaceLimits[1].z = Utils::getMinVal(position.z, spaceLimits[1].z);
        
    }
    
}

void Sounds::drawClusterNames()
{
    if (clusterNamesOpacity > 0) {
        ofSetColor(ofColor::white, clusterNamesOpacity);

        for ( int i = 0 ; i < hulls.size() ; i++ ) {
            ImVec2 textSize = ImGui::CalcTextSize(clusterNames[i].c_str(), NULL, true);
            font.drawString(clusterNames[i], hulls[i].getCentroid2D().x - (textSize.x / 2), hulls[i].getCentroid2D().y);
        }
    }
}

void Sounds::revertToOriginalPositions()
{
    for(unsigned int i = 0; i < soundPositions.size(); i++) {
        soundPositions[i].sound->useOriginalPosition();
        reClusterSound(soundPositions[i].sound);
    }
    soundPositions.clear();

    generateBackground();
    findConvexHulls();
    sequenceUnitReprocess();
    kNN.buildIndex();
}

void Sounds::sequenceUnitReprocess()
{
    //sequence units do reprocess even if the sound that moved is not in the sequence
    for ( auto u : Units::getInstance()->currentUnits ) {
        if ( u->getUnitName() == "Sequence Unit" ) {
            auto seqUnit = dynamic_pointer_cast<SequenceUnit>(u);
            seqUnit->processSequence();
        }
    }
}

void Sounds::onClusterToggle(int clusterIdx) {
    for(unsigned int i = 0; i<sounds.size(); i++) {
        if(sounds[i]->getCluster() == clusterIdx) {
            sounds[i]->toggleHide();
        }
        
    }
}

void Sounds::doClustering()
{
    DBScan dbscan( epsDbScan, minPts, getSoundsAsPoints(true));
    dbscan.run();
    setDBScanClusters(dbscan.points);

    for ( auto &sPos : soundPositions ) {
        reClusterSound(sPos.sound);
    }

    findConvexHulls();
    generateBackground();
}

void Sounds::reClusterSound(shared_ptr<Sound> sound)
{
    if ( sound == nullptr ) return;

    int countNeighbors = 2;
    ofPoint pointPosition = sound->getPosition();

    auto kNNResult = kNN.findNeighbors( pointPosition, countNeighbors );

    if(kNNResult.size() <= 1 ){
        return;
    }

    int minDist = 0;
    shared_ptr<Sound> minSound = nullptr;
    for ( unsigned int i = 0 ; i < countNeighbors ; i++ ) {
        shared_ptr<Sound> s = kNNPoints[ kNNResult.indices[i] ].sound;
        if ( sound == s ) continue;

        if ( minSound == nullptr ) {
            minDist = kNNResult.distances[i];
            minSound = s;
        } else if ( kNNResult.distances[i] < minDist ) {
            minDist = kNNResult.distances[i];
            minSound = s;
        }
    }

    if ( minDist > pow(sound->size * 20,2) ) {
        sound->setCluster( DBScan::NOISE );
    } else {
        sound->setCluster( minSound->getCluster() );
    }


//    vector<int> vecClusters;
//    vector<int> vecCountVotes;
//    float minDist = -1;
//    for ( unsigned int i = 0 ; i < countNeighbors ; i++ ) {
//        shared_ptr<Sound> s = kNNPoints[ kNNResult.indices[i] ].sound;

//        auto itVecCluster = find(vecClusters.begin(),
//                                 vecClusters.end(),
//                                 s->getCluster());
//        if ( itVecCluster == vecClusters.end() ) {
//            vecClusters.push_back( s->getCluster() );
//            //instead of counting votes we will be using the distances as weighted vote

//            vecCountVotes.push_back( kNNResult.distances[i] );
//        } else {
//            vecCountVotes[ itVecCluster - vecClusters.begin() ]++;
//        }

//        if ( sound == s ) continue;

//        if ( minDist == -1 ) {
//            minDist = kNNResult.distances[i];
//        } else if ( kNNResult.distances[i] < minDist ) {
//            minDist = kNNResult.distances[i];
//        }
//    }

//    //If it is too far away for everything then noise
//    if ( minDist > pow(sound->size * 20,2) ) {
//        sound->setCluster( DBScan::NOISE );
//        return;
//    }

//    //else, count votes!
//    int maxValue = 0;
//    int maxIndex = -1;
//    for ( unsigned int i = 0 ; i < vecCountVotes.size() ; i++ ) {
//        if ( vecCountVotes[i] > maxValue ) {
//            maxValue = vecCountVotes[i];
//            maxIndex = i;
//        }
//    }

//    if ( maxIndex > -1 ) {
//        sound->setCluster( vecClusters[ maxIndex ] );
//    } else {
//        sound->setCluster( DBScan::NOISE );
//    }
//    generateBackground();
}

shared_ptr<Sound> Sounds::getSoundByFilename(string filename) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->getFileName() == filename ) {
            return sounds[i];
        }
    }
    return NULL;
}
shared_ptr<Sound> Sounds::getSoundById(int id) {
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->id == id ) {
            return sounds[i];
        }
    }
    return nullptr;
}

const vector<shared_ptr<Sound>> & Sounds::getSounds()
{
    return sounds;
}

vector<shared_ptr<Sound> > Sounds::getSoundsByClusters(set<int> clustersId)
{
    vector<shared_ptr<Sound>> ret;
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( std::find(clustersId.begin(),
                       clustersId.end(),
                       sounds[i]->getCluster())!=clustersId.end() ) {

            ret.push_back( sounds[i] );
        }
    }
    return ret;
}

vector<shared_ptr<Sound> > Sounds::getSoundsByClusters(vector<int> clustersId)
{
    vector<shared_ptr<Sound>> ret;
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( std::find(clustersId.begin(),
                       clustersId.end(),
                       sounds[i]->getCluster())!=clustersId.end() ) {

            ret.push_back( sounds[i] );
        }
    }
    return ret;
}

vector<shared_ptr<Sound> > Sounds::getSoundsByClusters(map<int, int> clustersId)
{
    vector<shared_ptr<Sound>> ret;
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( clustersId.find(sounds[i]->getCluster()) !=clustersId.end() ) {
            ret.push_back( sounds[i] );
        }
    }
    return ret;
}

vector<shared_ptr<Sound>> Sounds::getSoundsByCluster(int clusterId)
{
    vector<shared_ptr<Sound>> ret;
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( sounds[i]->getCluster() == clusterId ) {
            ret.push_back( sounds[i] );
        }
    }
    return ret;
}

vector<shared_ptr<Sound>> Sounds::getSoundsByCluster(string name, bool includeMuted){
    vector<shared_ptr<Sound>> snds;
    int clusterId = -1;

    for ( int i = 0 ; i < clusterNames.size(); i++){
        if (clusterNames[i] == name){
            clusterId = i;
            break;
        }
    }

    if ( clusterId != -1 ) {
        for ( int i = 0 ; i < sounds.size() ; i++ ) {
            if ( sounds[i]->getCluster() == clusterId ) {
                if ( includeMuted || !sounds[i]->mute ) {
                    snds.push_back( sounds[i] );
                }
            }
        }
    }

    return snds;

}

vector<shared_ptr<Sound>> Sounds::getNeighbors(shared_ptr<Sound> s, float threshold)
{
    ofVec2f position = s->getPosition();
    return getNeighbors(position, threshold);
}

vector<shared_ptr<Sound>> Sounds::getNeighbors(ofVec2f position, float threshold)
{
    vector<shared_ptr<Sound>> neighbors;

    ofPoint p (position);
    ofxAnn<vector<kNNPoint>>::result_radius resultRadius = kNN.findWithin( p, threshold );

    for ( auto &n : resultRadius.data ) {
        neighbors.push_back(kNNPoints[n.first].sound);
    }
    
    return neighbors;
}

vector<ofPoint> Sounds::getSoundsAsPoints(bool originalPositions)
{
    vector<ofPoint> soundsAsPoints;
    
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        if ( originalPositions ) {
            soundsAsPoints.push_back( sounds[i]->getOriginalPosition() );
        } else {
            soundsAsPoints.push_back( sounds[i]->getPosition() );
        }
    }
    
    return soundsAsPoints;
}

void Sounds::setDBScanClusters(vector<dbscanPoint> points)
{
    clusterIds.clear();
    
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        sounds[i]->setCluster( points[i].cluster );
        
        if ( points[i].cluster >= 0 ) {
            clusterIds.insert( points[i].cluster );
            //Para saber si cada cluster se está mostrando. Asumo que todos los clusters empiezan siendo mostrados...
            clustersShow.push_back(true);
        }
    }
    nClusters = clusterIds.size();
    for( int i = 0; i < nClusters; i++){
        clusterNames.push_back( clusterNameDefault );
    }
}

void Sounds::setFoldersAsClusters() {
    clusterIds.clear();
    
    for ( int i = 0 ; i < sounds.size() ; i++ ) {
        sounds[i]->setCluster( sounds[i]->getFolder() );
        
        if ( sounds[i]->getFolder() >= 0 ) {
            clusterIds.insert( sounds[i]->getFolder() );
            clustersShow.push_back(true);
        }
    }
    nClusters = clusterIds.size();
    for( int i = 0; i < nClusters; i++){
        clusterNames.push_back( clusterNameDefault );
    }
    
    findConvexHulls();
    generateBackground();
}


void Sounds::setFilenameLabel(string fileName){
    currentSoundLabel = fileName;
}

bool Sounds::getUseOriginalPositions(){
    return useOriginalPositions;
}

bool& Sounds::getUseOriginalClusters() {
    return useOriginalClusters;
}

bool Sounds::getShowSoundFilenames(){
    return showSoundFilenamesTooltip;
}

int Sounds::getInitialWindowWidth()
{
    return windowOriginalWidth;
}

int Sounds::getInitialWindowHeight()
{
    return windowOriginalHeight;
}

ofVec3f Sounds::camToSoundCoordinates(ofVec2f camCoordinates) {
    return camToSoundCoordinates( ofVec3f(camCoordinates.x, camCoordinates.y, 0) );
}

ofVec3f Sounds::camToSoundCoordinates(ofVec3f camCoordinates)
{
    ofVec3f r;
    
    r.x = ofMap(camCoordinates.x,
                0 + spacePadding,
                windowOriginalWidth - spacePadding,
                spaceLimits[1].x,
                spaceLimits[0].x);
    
    
    r.y = ofMap(camCoordinates.y,
                0 + spacePadding,
                windowOriginalHeight - spacePadding,
                spaceLimits[1].y,
                spaceLimits[0].y);
    
    r.z = ofMap(camCoordinates.z,
                0.5,1,
                spaceLimits[1].z,
                spaceLimits[0].z);
    
    return r;
}

ofVec3f Sounds::soundToCamCoordinates(ofVec3f soundCoordinates)
{
    ofVec3f r;
    
    r.x = ofMap(soundCoordinates.x,
                spaceLimits[1].x,
                spaceLimits[0].x,
                0 + spacePadding,
                windowOriginalWidth - spacePadding);
    
    r.y = ofMap(soundCoordinates.y,
                spaceLimits[1].y,
                spaceLimits[0].y,
                0 + spacePadding,
                windowOriginalHeight - spacePadding);
    
    r.z = ofMap(soundCoordinates.z,
                spaceLimits[1].z,
                spaceLimits[0].z,
                0.5,1);
    
    return r;
}

void Sounds::setUseOrigPos(bool v){
    useOriginalPositions = v;
}

void Sounds::setShowSoundFilenames(bool v){
    showSoundFilenamesTooltip = v;
}

void Sounds::setShowClusterNames(bool show)
{
    if ( useOriginalClusters ) return;
    if ( show && !areAnyClusterNames() ) {
        Gui::getInstance()->showMessage(
                    "No clusters have been named. \n\n"
                    "Use Right Click over a cluster to name it.");
        return;

    }
    showClusterNames = show;
    for (unsigned int i = 0; i < sounds.size(); i++) {
        sounds[i]->clusterIsHovered = !showClusterNames;
    }
}

void Sounds::toggleShowClusterNames()
{
    setShowClusterNames(!showClusterNames);
}

unsigned int Sounds::getSoundCount()
{
    return sounds.size();
}

Json::Value Sounds::save() {
    Json::Value root = Json::Value( Json::objectValue );
    Json::Value overridedPositions = Json::Value( Json::arrayValue );
    
    for ( auto &sPos : soundPositions ) {
        Json::Value soundPosition = Json::Value( Json::objectValue );
        soundPosition["id"] = sPos.sound->id;
        soundPosition["position"] = Json::Value( Json::arrayValue );
        soundPosition["position"].append( sPos.position.x );
        soundPosition["position"].append( sPos.position.y );
        soundPosition["cluster"] = sPos.sound->getCluster();
        
        overridedPositions.append(soundPosition);
    }

    //cluster names
    Json::Value jsonClusterNames = Json::Value(Json::arrayValue);
    for (auto clusterName : clusterNames ) {
        Json::Value jsonClusterName = Json::Value(Json::stringValue);
        jsonClusterName = clusterName;
        jsonClusterNames.append(jsonClusterName);
    }

    root["clusterNames"] = jsonClusterNames;
    root["useOriginalClusters"] = useOriginalClusters;
    root["overridedPositions"] = overridedPositions;
    root["DBScan_EPS"] = epsDbScan;
    root["DBScan_minPts"] = minPts;

    //muted sounds
    Json::Value muted = Json::Value(Json::arrayValue);
    for (auto sound : sounds) {
        if (sound->mute) {
            muted.append(sound->id);
        }
    }

    root["muted"] = muted;

    return root;
}

void Sounds::load( Json::Value jsonData ) {
    Json::Value overridedPositions = jsonData["overridedPositions"];
    for ( int i = 0 ; i < overridedPositions.size() ; i++ ) {
        // Sound * sound = getSoundById( overridedPositions[i]["id"] )
        soundCoordinatesOverride newSoundPosition;
        newSoundPosition.sound = getSoundById( overridedPositions[i]["id"].asInt() );
        newSoundPosition.cluster = overridedPositions[i]["cluster"].asInt();

        if ( newSoundPosition.sound == nullptr ) {
            ofLog() << "Sound ID not found when loading session";
            continue;
        }

        newSoundPosition.position.x = overridedPositions[i]["position"][0].asInt();
        newSoundPosition.position.y = overridedPositions[i]["position"][1].asInt();

        bool isFileDuplicated = false;
        for (int i = 0; i < sounds.size(); i++) {
            shared_ptr<Sound> s = sounds[i];
            if ( sounds[i]->position.x == newSoundPosition.position.x &&
                 sounds[i]->position.y == newSoundPosition.position.y) {
                isFileDuplicated = true;
                break;
            }
        }
        if ( isFileDuplicated ) continue;

        newSoundPosition.sound->position = newSoundPosition.position;
//        newSoundPosition.sound->setCluster(newSoundPosition.cluster);
        soundPositions.push_back(newSoundPosition);
    }

    Json::Value names = jsonData["clusterNames"];
    if(names != Json::nullValue){
        for(unsigned int i = 0; i < names.size(); i++){
           clusterNames.push_back(names[i].asString());
        }
    }
    
    if ( jsonData["DBScan_EPS"].isInt() ) {
        epsDbScan = jsonData["DBScan_EPS"].asInt();
    }
    if ( jsonData["DBScan_minPts"].isInt() ) {
        minPts = jsonData["DBScan_minPts"].asInt();
    }
    
    useOriginalClusters = jsonData["useOriginalClusters"].isBool() &&
                          jsonData["useOriginalClusters"].asBool();

    if ( !useOriginalClusters ) {
        doClustering();
    } else {
        setFoldersAsClusters();
    }

    for ( auto &sPos : soundPositions ) {
        sPos.sound->setCluster( sPos.cluster );
    }
    if ( !soundPositions.empty() ) {
        kNN.buildIndex();
        findConvexHulls();
    }

    Json::Value muted = jsonData["muted"];
    if (muted != Json::nullValue) {
        for (int i = 0; i < muted.size(); i++) {
            sounds[muted[i].asInt()]->mute = true;
        }
    }
}

string Sounds::selectFolder() {
    
    ofFileDialogResult dialogResult = Gui::getInstance()->showLoadDialog("Select Folder",
                                                                         true,
                                                                         ofFilePath::getUserHomeDir());
    
    string path;
    
    if(dialogResult.bSuccess) {
        path = dialogResult.getPath();
    }
    
    return path;
}

void Sounds::exportFiles() {
    
    string selectedFolderPath = selectFolder();
    
    if (selectedFolderPath.size() > 0) {
        
        selectedFolderPath = ofFilePath::addTrailingSlash(selectedFolderPath);
        
        for (std::set<int>::iterator it=clusterIds.begin(); it!=clusterIds.end(); ++it) {
            
            string clusterId = to_string(*it);
            string clusterFolderPath = selectedFolderPath + clusterId;
            
            ofDirectory dir(clusterFolderPath);
            if(!dir.exists()){
                dir.create(true);
            }
        }
        
        for(int i = 0; i < sounds.size(); i++) {
            
            if (sounds[i]->getCluster() >= 0 && !sounds[i]->mute ) {
                
                string cluster = to_string( sounds[i]->getCluster() );
                string sourcePath = sounds[i]->getFileName();
                string destinationPath = selectedFolderPath + cluster;
                
                ofFile::copyFromTo(sourcePath, destinationPath, true, false);

            }
        }
        
        for (int i = 0; i < clusterNames.size(); i++) {
            
            if (clusterNames[i] != clusterNameDefault) {
                
                string clusterFolderPath = selectedFolderPath + ofToString(i);
                ofDirectory dir(clusterFolderPath);
                
                if(dir.exists()){
                    string newFolderName = selectedFolderPath + ofToString(i) + " - " + clusterNames[i];
                    dir.renameTo(newFolderName);

                }
            }
        }

        Gui::getInstance()->showMessage("All sounds have been saved to folders", "Information", true);
    }
}

void Sounds::keyPressed(ofKeyEventArgs & e){
//    if(!useOriginalClusters && e.keycode == CLUSTERNAME_SHOW_SHORTCUT && !showClusterNames) {
//        showClusterNames = true;
//        for (unsigned int i = 0; i < sounds.size(); i++) {
//            sounds[i]->clusterIsHovered = false;
//        }
//    }
}

void Sounds::keyReleased(ofKeyEventArgs &e){
//    if(!useOriginalClusters && e.keycode == CLUSTERNAME_SHOW_SHORTCUT) {
//        showClusterNames = false;
//        for (unsigned int i = 0; i < sounds.size(); i++) {
//            sounds[i]->clusterIsHovered = true;
//        }
//    }
}

void Sounds::nameCluster(unsigned int clusterID){
        clusterNames[clusterID] = ofToString(clusterNameBeingEdited);
        clusterNameBeingEdited[0] = '\0';
}

bool Sounds::isPreloading()
{
    return preloading;
}

int Sounds::getHoveredClusterID()
{
    return selectedCluster;
}

ofRectangle Sounds::getClusterBB(int clusterID)
{
    if ( clusterID < hulls.size() ) {
        return hulls[clusterID].getBoundingBox();
    }

    return ofRectangle(-1,-1,0,0);
}

float Sounds::getSoundLoaderProgress()
{
    return soundLoader.getProgress();
}

bool Sounds::areAnyClusterNames(){
    for(unsigned int i = 0; i < clusterNames.size(); i++){
        if(clusterNames[i] != clusterNameDefault) return true;

    }
    return false;
}

bool Sounds::areAnyOriginalClusters() {
    
    auto jsonFile = *SessionManager::getInstance()->getSessionFile();
    
    vector<string> soundFiles = ofSplitString(jsonFile["tsv"].asString(), "|");
    
    for (auto soundFile : soundFiles) {
        if (soundFile == "") continue;
        vector<string>lineElements = ofSplitString(soundFile,",");
        int cluster = ofToInt(lineElements[TSV_FIELD_FOLDER]);
        if (cluster != 0) return true;
    }
    return false;

}

void Sounds::deleteClusterNames(){
    for(unsigned int i = 0; i < clusterNames.size(); i++){
       clusterNames[i] = clusterNameDefault;
    }
}

bool Sounds::areDBScanSettingsEdited() {
    
    auto sessionFile = SessionManager::getInstance()->getSessionFile();
    
    if ((*sessionFile)["sounds"]["DBScan_EPS"] == Json::nullValue ||
        (*sessionFile)["sounds"]["DBScan_minPts"] == Json::nullValue) {
        
        (*sessionFile)["sounds"]["DBScan_EPS"] == DEFAULT_DBSCAN_EPS;
        (*sessionFile)["sounds"]["DBScan_minPts"] == DEFAULT_DBSCAN_MIN_PTS;
    }
    
    if ((*sessionFile)["sounds"]["DBScan_EPS"] == epsDbScan &&
        (*sessionFile)["sounds"]["DBScan_minPts"] == minPts) {
        return false;
    }
    else {
        return true;
    }
}
