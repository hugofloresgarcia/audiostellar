#pragma once

#include "ofMain.h"
#include "RtAudio.h"

class soundDevice {

public:
    soundDevice(ofSoundDevice device);
    
    ofSoundDevice::Api api;
    std::string name{"Unknown"};
    int deviceID = -1;
    unsigned int inputChannels = 0;
    unsigned int outputChannels = 0;
    bool isDefaultInput = false;
    bool isDefaultOutput = false;
    std::vector<unsigned int> sampleRates;
    std::vector<unsigned int> bufferSizes;

    unsigned int testBufferSize( unsigned int samplerate, unsigned int buffersize );

};
