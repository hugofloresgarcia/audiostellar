#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"
#include "ofxJSON.h"
#include "SoundDevice.h"

class Voices;

class AudioEngine {
    
private:
    
    AudioEngine();
    static AudioEngine * instance;

    vector <soundDevice> devices;
    bool configured = false;
    bool started = false;

    vector<unsigned int> bufferSizes;

    int selectedAudioOutDeviceID = -1;
    int selectedAudioOutNumChannels = -1;
    int selectedSampleRate = 44100;
    int selectedBufferSize = 512;

    pdsp::Engine engine;

    void testCurrentDeviceBufferSize();
    
public:
    
    static AudioEngine * getInstance();
    
    struct Output {
        string type;
        int channel0;
        int channel1;
        bool enabled;
        string label;
    };

    ofEvent<int> soundCardInit;
    
    void start();
    void setup();
	void stop();
    bool isConfigured();
    bool hasStarted();

    int getDefaultAudioOutDeviceIndex();
    int getAudioOutNumChannels(int deviceID);
    int getSelectedDeviceAudioOutNumChannels();
    string getSelectedDeviceName();
    int getSelectedDeviceID();
    int getSelectedSampleRate();
    int getSelectedBufferSize();
    bool isDeviceSelected();
    const vector<unsigned int> & getSupportedSampleRates(int deviceID = -1);
    const vector<unsigned int> & getSupportedBufferSizes(int deviceID = -1);

    void selectAudioOutDevice(int deviceID);
    bool selectAudioOutDevice(string deviceName);
    void selectSampleRate(int sampleRate);
    void selectBufferSize(int bufferSize);

    pdsp::Patchable& getAudioOut(int channel);
    pdsp::Patchable& getBlackhole();
    void setTempo(double tempo);

    vector <soundDevice> listDevices();
    void refreshDeviceList();
    
    vector <Output> outputs; //this should be private with public methods for UI
    const vector<string> getEnabledOutputs();
    Output getEnabledOutput(int idx); //this idx is the enabled output index, not the output
    
    void createOutputs();
    
    Json::Value save();
    void load(Json::Value json, bool loadAndConfigure = true);
    
    void exit();
    
};
