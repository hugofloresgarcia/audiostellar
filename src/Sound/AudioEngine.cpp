#include "AudioEngine.h"
#include "Voices.h"
#include "../Utils/SessionManager.h"

AudioEngine * AudioEngine::instance = nullptr;

AudioEngine * AudioEngine::getInstance() {
    if (instance == nullptr) {
        instance = new AudioEngine();
    }
    return instance;
}

AudioEngine::AudioEngine() {
    refreshDeviceList();
}

void AudioEngine::testCurrentDeviceBufferSize()
{
#ifndef TARGET_WIN32
    soundDevice& device = devices[getSelectedDeviceID()];
    unsigned int realBufferSize = device.testBufferSize(getSelectedSampleRate(), getSelectedBufferSize());

    //    ofLog() << "requested buffer size: " << getSelectedBufferSize();
    //    ofLog() << "real buffer size: " << realBufferSize;

    selectedBufferSize = realBufferSize;
#endif 
}

void AudioEngine::setup() {
    
    if ( !isDeviceSelected() ) {
        selectAudioOutDevice( getDefaultAudioOutDeviceIndex() );

        if ( !isDeviceSelected() ) {
            return;
        }
    }
	
    int numChannels = getAudioOutNumChannels(getSelectedDeviceID());
    
    engine.setOutputDeviceID( devices[getSelectedDeviceID()].deviceID );
    engine.setChannels(0, numChannels);

    testCurrentDeviceBufferSize(); // if buffer size is wrong it will set it right
    engine.setup( getSelectedSampleRate() , getSelectedBufferSize(), getSelectedDeviceAudioOutNumChannels() * 2);

    selectedAudioOutNumChannels = numChannels;
    
    configured = true;

    createOutputs();

    ofNotifyEvent(soundCardInit, selectedAudioOutDeviceID );
}

void AudioEngine::stop()
{
    if ( isConfigured() ) {
        //ofLog() << "Stop audio engine";
        engine.stop();
        started = false;
    }
}

bool AudioEngine::isConfigured()
{
    return configured;
}

bool AudioEngine::hasStarted()
{
    return started;
}

void AudioEngine::selectAudioOutDevice(int deviceID) {
    
	if (deviceID >= devices.size()) {
		ofLog() << "Critical error: No sound card found.";
		return;
	}

    selectedAudioOutDeviceID = deviceID;

    ofxJSONElement audioSettings = SessionManager::getInstance()->getAudioSettings();
    if ( audioSettings != Json::nullValue ) {
        string selectedAudioOut = audioSettings["selectedAudioOut"].asString();
        if ( selectedAudioOut == devices[deviceID].name ) {
            if (audioSettings["selectedSampleRate"] != Json::nullValue) {
                int requestedSampleRate = audioSettings["selectedSampleRate"].asInt();
                selectSampleRate(requestedSampleRate);
            }
            if (audioSettings["selectedBufferSize"] != Json::nullValue) {
                selectBufferSize( audioSettings["selectedBufferSize"].asInt() );
            }
            return;
        }
    }

    selectSampleRate(44100);
    selectBufferSize(1024);
}

bool AudioEngine::selectAudioOutDevice(string deviceName) {
    
    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].name == deviceName) {
            selectAudioOutDevice(i);
            return true;
        }
    }

    ofLogWarning () << "Audio device not found. Switching to default device.";
    selectAudioOutDevice(getDefaultAudioOutDeviceIndex());
    return false;
}

void AudioEngine::selectSampleRate(int sampleRate) {
    assert( isDeviceSelected() );

    // check if current sample rate is supported by the device
    vector<unsigned int> & vec = devices[getSelectedDeviceID()].sampleRates ;
    if ( std::find(vec.begin(), vec.end(), sampleRate) != vec.end() ) {
        selectedSampleRate = sampleRate;
    } else {
        vector<unsigned int> defaultSampleRates = { 44100, 48000 };

        for ( auto sr : defaultSampleRates ) {
            if ( std::find(vec.begin(), vec.end(), sr) != vec.end() ) {
                selectedSampleRate = sr;
                ofLogWarning() << "Selected sample rate not supported. Using default sample rate: " << selectedSampleRate;
                return;
            }
        }

        // Sample rate not selected yet !
        selectedSampleRate = devices[getSelectedDeviceID()].sampleRates[0];
        ofLogWarning() << "Selected sample rate not supported. Using default sample rate: " << selectedSampleRate;
    }
}

void AudioEngine::selectBufferSize(int bufferSize) {
    assert( isDeviceSelected() );

    vector<unsigned int> & vec = devices[getSelectedDeviceID()].bufferSizes ;
    if ( std::find(vec.begin(), vec.end(), bufferSize) != vec.end() ) {
        selectedBufferSize = bufferSize;
    } else {
        vector<unsigned int> defaultBufferSizes = { 1024, 512 };

        for ( auto b : defaultBufferSizes ) {
            if ( std::find(vec.begin(), vec.end(), b) != vec.end() ) {
                selectedBufferSize = b;
                ofLogWarning() << "Selected buffer size not supported. Using default: " << selectedBufferSize;
                return;
            }
        }

        // BufferSize not selected yet !
        selectedBufferSize = devices[getSelectedDeviceID()].bufferSizes[0];
        ofLogWarning() << "Selected buffer size not supported. Using: " << selectedBufferSize;
    }
}

pdsp::Patchable &AudioEngine::getAudioOut(int channel)
{
    return engine.audio_out(channel);
}

pdsp::Patchable &AudioEngine::getBlackhole()
{
    return engine.blackhole();
}

void AudioEngine::setTempo(double tempo)
{
    engine.sequencer.setTempo(tempo);
}

void AudioEngine::start(){
    if ( isConfigured() ) {
        //ofLog() << "Start audio engine";
        engine.start();
        started = true;
    }
}

vector<soundDevice> AudioEngine::listDevices()
{
    return devices;
}

void AudioEngine::refreshDeviceList()
{
    devices.clear();
    vector<ofSoundDevice> unfilteredDevices = engine.listDevices();
    for ( auto device : unfilteredDevices ) {
        if ( device.outputChannels > 0 ) {
            //filter duplicated devices
            bool isDeviceInList = false;
            for ( auto &deviceInList : devices) {
                if ( deviceInList.name == device.name ) {
                    isDeviceInList = true;
                    break;
                }
            }

            if (!isDeviceInList) {
                ofLog() << "Using: " << device.name;
                soundDevice dev(device);

                //Should we don't add it to the list or should we give default buffer sizes ?
                if ( dev.bufferSizes.size() > 0 ) {
                    devices.push_back(dev);
                }
            }
        }
    }
}

const vector<string> AudioEngine::getEnabledOutputs()
{
    vector<string> enabledOutputs;

    for (unsigned int i = 0; i < outputs.size(); i++) {
        if ( outputs[i].enabled ) {
            enabledOutputs.push_back( outputs[i].label );
        }
    }

    return enabledOutputs;
}

AudioEngine::Output AudioEngine::getEnabledOutput(int idx)
{
    int currentIdx = -1;

    for (unsigned int i = 0; i < outputs.size(); i++) {
        if ( outputs[i].enabled ) {
            currentIdx++;

            if ( currentIdx == idx ) {
                return outputs[i];
            }
        }
    }

    return {"error",-1,-1,false,"error"};
}

int AudioEngine::getDefaultAudioOutDeviceIndex() {
    
    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].name == "default") {
            return i;
        }
    }

    for (int i = 0; i < devices.size(); i++) {
        if (devices[i].isDefaultOutput) {
            return i;
        }
    }
    
    return 0; //ok first will be fine
}

int AudioEngine::getAudioOutNumChannels(int deviceID) {
    
    if ( deviceID < devices.size() ) {
        int numChannels = devices[deviceID].outputChannels;

        // we should check numChannels != -1 ??
        if (numChannels > PDSP_MAX_OUTPUT_CHANNELS) {
//            ofLogWarning() << "Selected device has " << numChannels << " output channels. Setting output channels to " << PDSP_MAX_OUTPUT_CHANNELS <<".";
            numChannels = PDSP_MAX_OUTPUT_CHANNELS;
        }
        return numChannels;
    }

    return -1;
}

int AudioEngine::getSelectedDeviceAudioOutNumChannels()
{
	if (selectedAudioOutDeviceID == -1) {
		return -1;
	}
	return getAudioOutNumChannels(selectedAudioOutDeviceID);
}

string AudioEngine::getSelectedDeviceName()
{
	if (selectedAudioOutDeviceID == -1) {
		return "";
	}
    return devices[selectedAudioOutDeviceID].name;
}

int AudioEngine::getSelectedDeviceID()
{
    return selectedAudioOutDeviceID;
}

int AudioEngine::getSelectedSampleRate()
{
    return selectedSampleRate;
}

int AudioEngine::getSelectedBufferSize()
{
    return selectedBufferSize;
}

bool AudioEngine::isDeviceSelected()
{
    return getSelectedDeviceID() != -1;
}

const vector<unsigned int> &AudioEngine::getSupportedSampleRates(int deviceID)
{
    if ( deviceID == -1 ) {
        deviceID = getSelectedDeviceID();
        if ( deviceID == -1 ) {
            const vector<unsigned int> ret;
            return ret;
        }
    }

    return devices[deviceID].sampleRates;
}

const vector<unsigned int> &AudioEngine::getSupportedBufferSizes(int deviceID)
{
    if ( deviceID == -1 ) {
        deviceID = getSelectedDeviceID();
        if ( deviceID == -1 ) {
            const vector<unsigned int> ret;
            return ret;
        }
    }

    return devices[deviceID].bufferSizes;
}

void AudioEngine::createOutputs() {

    outputs.clear();
    
    for (int i = 0; i < selectedAudioOutNumChannels; i+=2) {
        Output output;
        output.type = "stereo";
        output.channel0 = i;
        output.channel1 = i + 1;
        output.enabled = true;
        output.label = ofToString(i + 1) + " - " + ofToString(i + 2);
        outputs.push_back(output);
    }

//    no mono outputs for now

//    for (int i = 0; i < selectedAudioOutNumChannels; i++) {
//        Output output;
//        output.type = "mono";
//        output.channel0 = i;
//        output.channel1 = i;
//        output.enabled = false;
//        output.label = ofToString(i + 1);
//        outputs.push_back(output);
//    }
    
    if (outputs.size() > 0) {
        outputs[0].enabled = true;
    }
}

Json::Value AudioEngine::save() {
    Json::Value root = Json::Value(Json::objectValue);
    
	if (selectedAudioOutDeviceID != -1) {
		root["selectedAudioOut"] = devices[selectedAudioOutDeviceID].name;
		root["selectedSampleRate"] = selectedSampleRate;
		root["selectedBufferSize"] = selectedBufferSize;
	}
    
    vector<int> selectedAudioOutChannels;
    for (unsigned int i = 0; i < outputs.size(); i++) {
        if ( outputs[i].enabled ) {
            root["selectedAudioOutChannels"].append(i);
        }
    }
    
    return root;
}

void AudioEngine::load(Json::Value json, bool loadAndConfigure) {
    
    Json::Value audioSettings = json["audioSettings"];

    bool successfulSelectedDevice = false;
    if (audioSettings["selectedAudioOut"] != Json::nullValue) {
        string selectedAudioOut = audioSettings["selectedAudioOut"].asString();
        selectAudioOutDevice(selectedAudioOut);
    } else {
        selectAudioOutDevice( getDefaultAudioOutDeviceIndex() );
    }
    
    if ( loadAndConfigure ) setup();

//    bool successfulSelectedDevice = false;
//    if (audioSettings["selectedAudioOut"] != Json::nullValue) {
//        string selectedAudioOut = audioSettings["selectedAudioOut"].asString();
//        successfulSelectedDevice = selectAudioOutDevice(selectedAudioOut);
//    } else {
//        selectAudioOutDevice( getDefaultAudioOutDeviceIndex() );
//    }

    // this should be readded in the future
//    if (successfulSelectedDevice && audioSettings["selectedAudioOutChannels"] != Json::nullValue) {
//        for ( unsigned int i = 0 ; i < audioSettings["selectedAudioOutChannels"].size() ; i++ ) {
//            int selectedChannel = audioSettings["selectedAudioOutChannels"][i].asInt();
//            if ( selectedChannel < outputs.size() ) {
//                outputs[selectedChannel].enabled = true;
//            }
//        }
//    }
}

void AudioEngine::exit() {
    engine.stop();
}
