#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "dr_wav.h"

#include "ofxImGui.h"

#include "./ThreadedRecorder.h"
#include "./AudioEngine.h"
#include "../Utils/Tooltip.h"

class Recorder {
    
public:
    
    Recorder();
    
    ~Recorder();
    
    void patch();
    void startRecording(const string & filename);
    void stopRecording();
    bool isRecording();
    void drawGui();
    void reset();
    
private:
    
    ThreadedRecorder threadedRecorder;
    
    string recordedFilename = "";
    bool haveToOpenSaveDialog = false;
    
    void openSaveDialog();
    
    void onRecordingEnd(string &filename);

};







