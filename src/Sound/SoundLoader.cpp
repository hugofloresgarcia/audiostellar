#include "SoundLoader.h"
#include "Sounds.h"

SoundLoader::SoundLoader()
{

}

void SoundLoader::threadedFunction()
{
    if ( isThreadRunning() ) {
        Sounds::getInstance()->preloading = true;
        loadedCount = 0;
        for (int i = 0; i < Sounds::getInstance()->sounds.size(); i++) {
            Sounds::getInstance()->sounds[i]->loadSample();
            loadedCount++;
        }
        Sounds::getInstance()->preloading = false;
        Sounds::getInstance()->onFinishedPreloading();
    }
}

int SoundLoader::getLoadedCount() const
{
    return loadedCount;
}

float SoundLoader::getProgress()
{
    return loadedCount / (float)Sounds::getInstance()->sounds.size();
}
