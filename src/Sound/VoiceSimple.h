#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "AudioEngine.h"
#include "Sound.h"


class VoiceSimple : public pdsp::Patchable
{
public:
    VoiceSimple();
    VoiceSimple(const VoiceSimple & other);
    ~VoiceSimple();

    AudioEngine * audioEngine = AudioEngine::getInstance();

    void patch();
    void unpatch();

    void load(string path); // ONLY AudioSegmentionTool should use this
    void load(pdsp::SampleBuffer * buffer, int start, int length, int channels);
    void unload();
    void play(float volume = 1.0f);
    void stop();
    bool isLoaded();
    bool isPlaying();
    float getPlayerPosition();
    float getSoundDuration();
    int getSampleRate();
    int getSampleLength();
    float** getBuffer();
    void setVolume(float volume);
    void enableEnvelope(bool e);
    void setEnvelope(float attack, float hold, float release, int envMode);
    void setPitch(float p);
    void setStart(float s);
    void enableReverse(bool e);


    void setSound(shared_ptr<Sound> s);
    shared_ptr<Sound> getSound();

private:
    pdsp::SampleBuffer * sample = nullptr;

    pdsp::Sampler sampler0;
    pdsp::Sampler sampler1;
    pdsp::AHR env;
    pdsp::Amp env_amp0;
    pdsp::Amp env_amp1;
    pdsp::Amp mix0_amp0;
    pdsp::Amp mix0_amp1;
    pdsp::Amp mix1_amp0;
    pdsp::Amp mix1_amp1;
    pdsp::Amp out_amp0;
    pdsp::Amp out_amp1;
    pdsp::TriggerControl sampleTrigger;
    pdsp::TriggerControl envTrigger;

    pdsp::Parameter attack;
    pdsp::Parameter hold;
    pdsp::Parameter release;

    bool isPatched = false;

    // current implementation of sampler can't be stopped
    // instead of changing that sensitive part of code
    // we "emulate stop". yes, this should change.
    bool isStopped = false;

    bool reverse = false;
    float sampleStart = 0.f;

};
