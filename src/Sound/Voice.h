#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "AudioEngine.h"
#include "Sound.h"

class Voice : public pdsp::Patchable {

public:

    Voice();
    Voice(const Voice & other) { /* this is needed */};
    ~Voice();

    void die();

    AudioEngine * audioEngine = AudioEngine::getInstance();

    void patch();
    void unpatch();

    void load(string path); // ONLY AudioSegmentionTool should use this
    void unload();
    void play(float volume = 1.0f);
    void stop();
    bool isPatched();
    bool isLoaded();
    bool isPlaying();
    float getPlayerPosition();
    float getSoundDuration();
    int getSampleRate();
    int getSampleLength();
    float** getBuffer();
    void setVolume(float volume);
    void setPan(float pan);
    void enableEnvelope(bool e);
    void setEnvelope(float attack, float hold, float release, int envMode);
    void setPitch(float p);
    void pitchBend(float delta);
    void setStart(float s);
    void enableReverse(bool e);

    void setSound(shared_ptr<Sound> s);
    shared_ptr<Sound> getSound();

    //When using getVoice if returning a voice
    //that is playing instead of a fresh new voice
    bool beingReused = false;
    long pitchChangedWhen = 0;

private:
    shared_ptr<Sound> sound = nullptr;

    pdsp::Sampler sampler0;
    pdsp::Sampler sampler1;
    pdsp::AHR env;
    pdsp::AmpFull env_amp0;
    pdsp::AmpFull env_amp1;
    pdsp::AmpFull mix0_amp0;
    pdsp::AmpFull mix0_amp1;
    pdsp::AmpFull mix1_amp0;
    pdsp::AmpFull mix1_amp1;
    pdsp::AmpFull out_amp0;
    pdsp::AmpFull out_amp1;
    pdsp::TriggerControl sampleTrigger;
    pdsp::TriggerControl envTrigger;

    pdsp::ValueControl panL;
    pdsp::ValueControl panR;
    float volume = 1.0f;
    float pitch = 0.0f;
    
    pdsp::Parameter attack;
    pdsp::Parameter hold;
    pdsp::Parameter release;

    bool patched = false;

    // current implementation of sampler can't be stopped
    // instead of changing that sensitive part of code
    // we "emulate stop". yes, this should change.
    bool isStopped = false;
    
    bool reverse = false;
    float sampleStart = 0.f;
};
