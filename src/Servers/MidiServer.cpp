#include "MidiServer.h"
#include "../GUI/CamZoomAndPan.h"
#include "../GUI/UI.h"
#include "../Servers/OscServer.h"

MidiServer* MidiServer::instance = nullptr;
ofEvent<MIDIKeyboardNotes> MidiServer::midiKeyboardEvent = ofEvent<MIDIKeyboardNotes>();

MidiServer::MidiServer() {
    checkForNewMidiDevices();

    for ( unsigned int i = 0 ; i < 128 ; i++ ) {
        arrPolyNoteOn[i] = false;
    }

    for ( unsigned int c = 0 ; c < 16 ; c++ ) {
        midiKeyboardNotes[c].channel = c + 1;
        midiKeyboardNotes[c].velocities.resize(128);
        for ( unsigned int i = 0 ; i < 128 ; i++ ) {
            midiKeyboardNotes[c].velocities[i] = 0.f;
        }
    }

    ofAddListener(UberControl::uberControlIsDead, this, &MidiServer::onUberControlDead);
}

void MidiServer::sendQueuedMidiMessages()
{
    for ( int i = 0 ; i < SEND_MIDI_MAX_MESSAGES_PER_UPDATE ; i++ ) {
        if ( !midiMessagesToSend.empty() ) {
            SendMidiMessage m = midiMessagesToSend.front();
            if ( m.type == NOTE_ON ) {
                if ( m.strMIDIDevice == "" ) {
                    for ( auto &strMIDIDevice : getConnectedOutputDevices() ) {
                        midiDevicesOUT[strMIDIDevice]->sendNoteOn(m.channel,
                                                                  m.id,
                                                                  m.value);
                    }
                } else {
                    midiDevicesOUT[m.strMIDIDevice]->sendNoteOn(m.channel,
                                                              m.id,
                                                              m.value);
                }
            } else if ( m.type == CONTROL_CHANGE ) {
                if ( m.strMIDIDevice == "" ) {
                    for ( auto &strMIDIDevice : getConnectedOutputDevices() ) {
                        midiDevicesOUT[strMIDIDevice]->sendControlChange(m.channel,
                                                                         m.id,
                                                                         m.value);
                    }
                } else {
                    midiDevicesOUT[m.strMIDIDevice]->sendControlChange(m.channel,
                                                                     m.id,
                                                                     m.value);
                }
            }

            midiMessagesToSend.pop();
        } else {
            break;
        }
    }
}

void MidiServer::sendMidiMessage(SendMidiType type, int channel, int id, int value, string strMIDIDevice)
{
    SendMidiMessage m;
    m.type = type;
    m.channel = channel;
    m.id = id;
    m.value = value;
    m.strMIDIDevice = strMIDIDevice;

//    ofLog() << m.type << " " << m.id << " " << m.value << " " << m.channel << " " << m.strMIDIDevice;

    midiMessagesToSend.push(m);
}

void MidiServer::drawSettings(){
    if ( midiDevicesIN.size() ) {
        ImGui::Text("INPUT Devices:");

        checkForNewMidiDevices();
        static const int lineHeight = 27;

        ImGuiWindowFlags window_flags = ImGuiWindowFlags_HorizontalScrollbar;
        int height = MIN(midiDevicesIN.size() * lineHeight, (unsigned long)4 * lineHeight);
        ImGui::BeginChild("Input MIDI Devices", ImVec2(300, height), true, window_flags);
        {
            map<string, ofxMidiIn *>::iterator it;
            for (it = midiDevicesIN.begin(); it != midiDevicesIN.end(); it++) {
                bool isSelected = it->second != nullptr;
                if ( ImGui::Selectable(it->first.c_str(), isSelected) ) {
                    if ( isSelected ) {
                        removeMidiDeviceIn( it->first );
                    } else {
                        addMidiDeviceIn( it->first );
                    }
                }
            }
        }
        ImGui::EndChild();

        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_DEVICES_INPUT);

        if ( isAnyDeviceConnected() ) {
            if ( ImGui::Checkbox("MIDI Clock", &useMIDIClock) ) {
                if ( useMIDIClock ) {
                    MasterClock::getInstance()->setClockType(MasterClock::MIDI);
                } else {
                    MasterClock::getInstance()->setClockType(MasterClock::INTERNAL);
                }
            }
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_CLOCK);

            if ( useMIDIClock ) {
                ImGui::Checkbox("Use external Play/Stop", &handleMIDIPlayStop);
                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_HANDLE_PLAY_STOP);
            }
        }

        ImGui::NewLine();

        ImGui::Text("OUTPUT Devices:");

        height = MIN(midiDevicesOUT.size() * lineHeight, (unsigned long)4 * lineHeight);
        ImGui::BeginChild("Output MIDI Devices", ImVec2(300, height), true, window_flags);
        {
            map<string, ofxMidiOut *>::iterator it;
            for (it = midiDevicesOUT.begin(); it != midiDevicesOUT.end(); it++) {
                bool isSelected = it->second != nullptr;
                string label = it->first;
                if ( ImGui::Selectable(label.c_str(), isSelected) ) {
                    if ( isSelected ) {
                        removeMidiDeviceOut( it->first );
                    } else {
                        addMidiDeviceOut( it->first );
                    }
                }
            }
        }
        ImGui::EndChild();

        if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_DEVICES_OUTPUT);

        ImGui::NewLine();

        ImGui::Text("Controller template:");
        ImGui::SameLine();

        if ( !isControllerTemplateFileEnabled() ) {
            if ( ImGui::Button("...") ) {
                ofFileDialogResult dialogResult = Gui::getInstance()->showLoadDialog("Select controller template",
                                                                                     false,
                                                                                     ofFilePath::getUserHomeDir());
                if(dialogResult.bSuccess) {
                    loadControllerTemplateFile(dialogResult.getPath());
                }
            }
        } else {
            if ( ImGui::Button("x") ) {
                clearControllerTemplateFile();
            }
        }

        if ( !controllerTemplatePath.empty() ) {
            ExtraWidgets::Text( ofFilePath::getBaseName(controllerTemplatePath) );
        }


        //@Todo Send clock to output
        ////
//        if ( isAnyDeviceConnected() ) {
//            if ( ImGui::Checkbox("MIDI Clock", &useMIDIClock) ) {
//                MasterClock::getInstance()->setClockType(MasterClock::MIDI);
//            }
//            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_CLOCK);

//            if ( useMIDIClock ) {
//                ImGui::Checkbox("Use external Play/Stop", &handleMIDIPlayStop);
//                if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_HANDLE_PLAY_STOP);
//            }
//        }

    } else {
        ImGui::Text("No MIDI devices found");
    }
}

void MidiServer::drawMonitor()
{
    if ( ofGetElapsedTimeMillis() - lastMIDIMessageMillis <= 50 ) {
        ImGui::PushStyleColor(ImGuiCol_Button, MONITOR_COLOR_ACTIVE);
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, MONITOR_COLOR_ACTIVE);
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, MONITOR_COLOR_ACTIVE);
        ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0,0,0,1));
    } else {
        ImGui::PushStyleColor(ImGuiCol_Button, MONITOR_COLOR_INACTIVE);
        ImGui::PushStyleColor(ImGuiCol_ButtonHovered, MONITOR_COLOR_INACTIVE);
        ImGui::PushStyleColor(ImGuiCol_ButtonActive, MONITOR_COLOR_INACTIVE);
        ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1,1,1,1));
    }

    btnMonitor.draw();
    ImGui::PopStyleColor(4);
}

void MidiServer::drawMIDILearn()
{
    if ( midiLearn ) {
        ImGuiWindowFlags window_flags = 0;
        window_flags |= ImGuiWindowFlags_NoCollapse;
        window_flags |= ImGuiWindowFlags_NoResize;

        int w = 320;
        int h = 200;

        ImGui::SetNextWindowSize( ImVec2(w,h) );
        ImGui::SetNextWindowPos(ImVec2( (ofGetWidth()/2) - w/2, (ofGetHeight()/2) - h/2 ),
                                ImGuiCond_Appearing);

        if(ImGui::Begin("MIDI Learn", &midiLearn, window_flags)) {
//           ImGui::PushTextWrapPos(w);
            ImGui::Text("Use your MIDI controller to map a control");
            ImGui::NewLine();

            ImGui::Checkbox("Enable MIDI velocity", &midiLearnUsesVelocity);
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_LEARN_VELOCITY);
            ImGui::Checkbox("Ignore NOTE_OFF", &midiLearnIgnoreNoteOff);
            if(ImGui::IsItemHovered()) Tooltip::setTooltip(Tooltip::MIDI_LEARN_VELOCITY);

            ImGui::NewLine();

            float minRange = lastControlMoved->parameter->getMin();
            float maxRange = lastControlMoved->parameter->getMax();
            ImGui::DragFloat2("Range", midiLearnRange, .01f, minRange, maxRange);

            ImGui::NewLine();
            if ( ImGui::Button("Cancel") ) {
                MidiServer::getInstance()->setMIDILearnOff();
            }
        }
        ImGui::End();
    }
}

void MidiServer::update()
{
    sendQueuedMidiMessages();
    moveCam();

    for ( unsigned int i = 0 ; i < MIDI_CHANNELS ; i++ ) {
        if ( lastMIDIKeyboardNoteMillis[i] > 0 &&
            ofGetElapsedTimeMillis() - lastMIDIKeyboardNoteMillis[i] >= midiKeyboardNotesWaitTime ) {
            ofNotifyEvent(midiKeyboardEvent, midiKeyboardNotes[i], this);
            lastMIDIKeyboardNoteMillis[i] = 0;
        }
    }
}

void MidiServer::setMIDILearnOn(UberControl *parameter)
{
    midiLearn = true;
    midiLearnUsesVelocity = false;
    midiLearnIgnoreNoteOff = false;
    lastControlMoved = new MIDIMapping();
    lastControlMoved->parameter = parameter;
    midiLearnRange[0] = lastControlMoved->parameter->getMin();
    midiLearnRange[1] = lastControlMoved->parameter->getMax();
}

void MidiServer::setMIDILearnOff()
{
    midiLearn = false;
    if ( lastControlMoved != nullptr ) {
        delete lastControlMoved; //should i delete it??
        lastControlMoved = nullptr;
    }
}

bool MidiServer::getMIDILearn()
{
    return midiLearn;
}

bool MidiServer::hasUnknownMappings()
{
    if ( unknownMappings ) {
        for(unsigned int i = 0; i < midiMappings.size(); i++){
            if(midiMappings[i].parameter == nullptr ) {
                return true;
            }
        }
    }
    unknownMappings = false;
    return false;
}

vector<MIDIMapping> MidiServer::fixUnknownMapping(UberControl *parameter)
{
    vector<MIDIMapping> mappings;

    for(unsigned int i = 0; i < midiMappings.size(); i++){
        if(midiMappings[i].label == parameter->getName() &&
           midiMappings[i].parameter == nullptr) {
            midiMappings[i].parameter = parameter;
            mappings.push_back( midiMappings[i] );
        }
    }
    return mappings;
}

void MidiServer::removeMapping(UberControl *parameter)
{
    for(unsigned int i = 0; i < midiMappings.size(); i++) {
        if ( midiMappings[i].parameter == parameter &&
             !midiMappings[i].useMIDIKeyboard) {
            midiMappings.erase( midiMappings.begin() + i );
        }
    }
}

void MidiServer::enableMIDIKeyboard(UberControl *parameter, int channel)
{
    MIDIMapping newMapping;
    newMapping.channel = channel;
    newMapping.useMIDIKeyboard = true;
    newMapping.parameter = parameter;
    parameter->usesMIDIKeyboard = true;
    midiMappings.push_back(newMapping);
}

void MidiServer::disableMIDIKeyboard(UberControl *parameter)
{
    for(unsigned int i = 0; i < midiMappings.size(); i++) {
        if ( midiMappings[i].parameter == parameter &&
             midiMappings[i].useMIDIKeyboard) {
            midiMappings.erase( midiMappings.begin() + i );
        }
    }
}

int MidiServer::getMIDIKeyboardChannel(UberControl *parameter)
{
    for(unsigned int i = 0; i < midiMappings.size(); i++) {
        if ( midiMappings[i].parameter == parameter && midiMappings[i].useMIDIKeyboard ) {
            return midiMappings[i].channel;
        }
    }
    return -1;
}

void MidiServer::checkForNewMidiDevices() {
    if ( lastCheckForMidiDevices == 0 ||
         ofGetElapsedTimeMillis() - lastCheckForMidiDevices > 1000 ) {

        //////////////
        // Input
        //////////////

        {
            std::vector<std::string> strDevices = midiIn.getInPortList();
            for ( auto & name : strDevices ) {
                if ( name.substr(0,3) == "ofx" ) {
                    continue;
                }
                if ( midiDevicesIN.find(name) == midiDevicesIN.end() ) {
                    midiDevicesIN[ name ] = nullptr;
                }
            }

            std::vector<std::string> strDevicesToRemove;
            map<string, ofxMidiIn *>::iterator it;
            for (it = midiDevicesIN.begin(); it != midiDevicesIN.end(); it++) {
                if ( std::find(strDevices.begin(), strDevices.end(), it->first) == strDevices.end() ) {
                    removeMidiDeviceIn( it->first );
                    strDevicesToRemove.push_back(it->first);
                }
            }

            for ( auto & name : strDevicesToRemove ) {
                midiDevicesIN.erase(name);
            }
        }

        //////////////
        // Output
        //////////////

        {
            std::vector<std::string> strDevices = midiOut.getOutPortList();
            for ( auto & name : strDevices ) {
                if ( name.substr(0,3) == "ofx" ) {
                    continue;
                }
                if ( midiDevicesOUT.find(name) == midiDevicesOUT.end() ) {
                    midiDevicesOUT[ name ] = nullptr;
                }
            }

            std::vector<std::string> strDevicesToRemove;
            map<string, ofxMidiOut *>::iterator it;
            for (it = midiDevicesOUT.begin(); it != midiDevicesOUT.end(); it++) {
                if ( std::find(strDevices.begin(), strDevices.end(), it->first) == strDevices.end() ) {
                    removeMidiDeviceIn( it->first );
                    strDevicesToRemove.push_back(it->first);
                }
            }

            for ( auto & name : strDevicesToRemove ) {
                midiDevicesOUT.erase(name);
            }
        }

        lastCheckForMidiDevices = ofGetElapsedTimeMillis();
    }
}

void MidiServer::removeMidiDeviceIn(string name)
{
    if ( midiDevicesIN[name] != nullptr ) {
        midiDevicesIN[name]->removeListener(this);
        midiDevicesIN[name]->closePort();
        delete midiDevicesIN[name];
        midiDevicesIN[name] = nullptr;
    }
}

void MidiServer::addMidiDeviceOut(string name)
{
    if ( midiDevicesOUT.find(name) != midiDevicesOUT.end() ) {
        midiDevicesOUT[name] = new ofxMidiOut();
        midiDevicesOUT[name]->openPort(name);

        broadcastAllMidiMappings();
    }
}

void MidiServer::removeMidiDeviceOut(string name)
{
    if ( midiDevicesOUT[name] != nullptr ) {
        midiDevicesOUT[name]->closePort();
        delete midiDevicesOUT[name];
        midiDevicesOUT[name] = nullptr;
    }
}

void MidiServer::addMidiDeviceIn(string name) {
    if ( midiDevicesIN.find(name) != midiDevicesIN.end() ) {
        midiDevicesIN[name] = new ofxMidiIn();
        midiDevicesIN[name]->openPort(name);
        midiDevicesIN[name]->ignoreTypes(true, false, true); //no ignores los de clock
        midiDevicesIN[name]->addListener(this);
    }
}

void MidiServer::newMidiMessage(ofxMidiMessage& msg) {
    MIDIMessage m;

    m.channel = msg.channel;
    m.status = msg.status;

    m.pitch =  msg.pitch;
    m.velocity = msg.velocity;

    m.cc = msg.control;
    m.ccVal = msg.value;

    m.beats = -1;
    m.bpm = -1;

    // is this ever called ? (not from Bitwig)
    if ( m.status == MIDI_START ) {
        midiClockPlaying = true;
        clock.reset();
    }
    if ( m.status == MIDI_STOP ) {
        midiClockPlaying = false;
    }
    if ( m.status == MIDI_CONTINUE ) {
        midiClockPlaying = true;

        m.beats = currentBeat;
        MasterClock::getInstance()->onMIDIClockTick(m);
    }

    if(clock.update(msg.bytes)) {
        unsigned int beats = clock.getBeats();

        if ( beats != currentBeat ) {
            currentBeat = beats;
            m.beats = currentBeat;

            if ( !handleMIDIPlayStop || midiClockPlaying ) {
                MasterClock::getInstance()->onMIDIClockTick(m);
            }
        }

        return;
    }

    if(msg.status == MIDI_CONTROL_CHANGE ||
       msg.status == MIDI_PITCH_BEND ||
       msg.status == MIDI_NOTE_ON ||
       msg.status == MIDI_NOTE_OFF ) {

        lastMIDIMessageMillis = ofGetElapsedTimeMillis();

        //Nasty workaroud, PitchBend as a CC message
        if(msg.status == MIDI_PITCH_BEND){
            m.cc = 128;
        }

        if(midiLearn){
            if(lastControlMoved != nullptr) {
                lastControlMoved->channel = m.channel;

                if ( msg.status == MIDI_CONTROL_CHANGE || msg.status == MIDI_PITCH_BEND ) {
                    lastControlMoved->cc = m.cc;
                } else {
                    lastControlMoved->pitch = m.pitch;
                    lastControlMoved->usesVelocity = midiLearnUsesVelocity;
                    lastControlMoved->ignoreNoteOff = midiLearnIgnoreNoteOff;
                }

                float minRange = lastControlMoved->parameter->getMin();
                float maxRange = lastControlMoved->parameter->getMax();
                lastControlMoved->range[0] = ofMap(midiLearnRange[0], minRange, maxRange, 0.f, 1.f);
                lastControlMoved->range[1] = ofMap(midiLearnRange[1], minRange, maxRange, 0.f, 1.f);

                midiMappings.push_back(*lastControlMoved);
                lastControlMoved->parameter->hasMIDIMapping = true;

                midiLearn = !midiLearn;
                lastControlMoved = nullptr;
            }
        } else {
            midiKeyboardNotes[m.channel - 1].velocities[m.pitch] = msg.status == MIDI_NOTE_ON && msg.velocity != 0 ? msg.velocity : 0.f;
            midiKeyboardNotes[m.channel - 1].status = msg.status == MIDI_NOTE_ON && msg.velocity != 0 ? MIDI_NOTE_ON : MIDI_NOTE_OFF;
            lastMIDIKeyboardNoteMillis[m.channel - 1] = ofGetElapsedTimeMillis();
            //This will be notified on update

            bool parameterChanged = false;
            for(unsigned int i = 0; i < midiMappings.size(); i++) {
                if ( midiMappings[i].channel != -1 && midiMappings[i].channel != m.channel ) {
                    continue;
                }

                if ( msg.status == MIDI_CONTROL_CHANGE || msg.status == MIDI_PITCH_BEND ) {
                    if(midiMappings[i].cc == m.cc) {
                        if ( midiMappings[i].parameter != nullptr ) {
                            int maxInputVal = (msg.status == MIDI_PITCH_BEND) ? 16383 : 127;
                            float middleValue = midiMappings[i].parameter->getMax() + midiMappings[i].parameter->getMin();

                            // This is mainly for ParticleUnit velocity
                            // If the parameter is centered on 0, then 63 and 64 will map to 0
                            // we lose a little resolution but ensure the center is fine
                            // for using it with joysticks
                            if ( middleValue == 0 &&
                                 (m.ccVal == floor(maxInputVal/2.0f) ||
                                  m.ccVal == ceil(maxInputVal/2.0f) ) ) {
                                    midiMappings[i].parameter->set( middleValue );
                                    parameterChanged = true;
                            } else {
                                //range (supports reverse range ;))
                                float min = midiMappings[i].parameter->getMin();
                                float max = midiMappings[i].parameter->getMax();
                                float segment = max - min;
                                min += segment * midiMappings[i].range[0];
                                max -= segment * (1-midiMappings[i].range[1]);

                                float setValue = ofMap(m.ccVal,
                                                       0,
                                                       maxInputVal,
                                                       min,
                                                       max,
                                                       true);

                                midiMappings[i].parameter->set( setValue );
                                parameterChanged = true;
                                // I don't think this is needed for CC
//                                broadcastUberControlChange(midiMappings[i].parameter);
                            }

                        } else {
                            ofLogError("MIDI Server", "Parameter is null");
                        }
                    }
                } else { //if MIDI_NOTE_ON or MIDI_NOTE_OFF
                    arrPolyNoteOn[m.pitch] = msg.status == MIDI_NOTE_ON && msg.velocity != 0 ? true : false;

                    if(midiMappings[i].pitch == m.pitch) {
                        if ( midiMappings[i].parameter != nullptr ) {
                            if ( midiMappings[i].ignoreNoteOff && (msg.status == MIDI_NOTE_OFF || msg.velocity == 0 ) ) {
                                return;
                            }
                            int val = 0;

                            if ( !midiMappings[i].usesVelocity ) {
                                val = msg.status == MIDI_NOTE_ON && msg.velocity != 0 ? 127 : 0;;
                            } else {
                                val = msg.velocity;
                            }

                            //Toggle buttons have special behaviour.
                            //Also, if it is toggle then it doesn´t use velocity, no need to check
                            // is ubertoggle
                            if ( dynamic_cast<UberToggle*>(midiMappings[i].parameter) != nullptr) {
                                if ( val == 0 ) {
                                    return;
                                } else {
                                    if ( midiMappings[i].parameter->get() == midiMappings[i].parameter->getMax() ) {
                                        val = 0;
                                    } else {
                                        val = 127;
                                    }
                                }
                            }

                            if ( msg.status == MIDI_NOTE_OFF ) {
                                val = 0;
                            }

                            //range (supports reverse range ;))
                            float min = midiMappings[i].parameter->getMin();
                            float max = midiMappings[i].parameter->getMax();
                            float segment = max - min;
                            min += segment * midiMappings[i].range[0];
                            max -= segment * (1-midiMappings[i].range[1]);

                            midiMappings[i].parameter->set( ofMap(val,
                                                                0,
                                                                127,
                                                                min,
                                                                max,
                                                                true));
                            parameterChanged = true;

                            //this happens with yaeltex firmware
//                            if ( !midiMappings[i].usesVelocity ) {
                                broadcastUberControlChange(midiMappings[i].parameter);
//                            }
                        } else {
                            ofLogError("MIDI Server", "Parameter is null");
                        }
                    }

                    //uses keyboard
                    if(midiMappings[i].useMIDIKeyboard && midiMappings[i].parameter != nullptr) {
                        float min = midiMappings[i].parameter->getMin();
                        float max = midiMappings[i].parameter->getMax();
                        float val = msg.pitch - 60; //60 CENTRAL_C4 MIDI NOTE

                        // is not uberbutton
                        if ( dynamic_cast<UberButton*>(midiMappings[i].parameter) == nullptr) {

                            if ( msg.status == MIDI_NOTE_ON ) {
                                midiMappings[i].parameter->set(
                                    midiMappings[i].parameter->getDefaultValue() + val
                                );
                                parameterChanged = true;
                            }

                        } else {
                            checkPolyNoteOn();

                            int val = polyNoteOn ? 127 : 0;
//                            float segment = max - min;
//                            min += segment * ccMappings[i].range[0];
//                            max -= segment * (1-ccMappings[i].range[1]);

                            midiMappings[i].parameter->set( ofMap(val,
                                                                0,
                                                                127,
                                                                min,
                                                                max,
                                                                true));
                            parameterChanged = true;
                        }
                    }
                }
            }
            if ( !parameterChanged ) {
                checkControllerMapping(m);
            }
        }

    } else {
        Units::getInstance()->midiMessage(m);
    }

}

void MidiServer::checkPolyNoteOn()
{
    bool allFalse = true;
    for ( unsigned int i = 0 ; i < 128 ; i++ ) {
        if ( arrPolyNoteOn[i] == true ) {
            allFalse = false;
            break;
        }
    }

    if ( !polyNoteOn && !allFalse ) {
        polyNoteOn = true;
    }
    if ( polyNoteOn && allFalse ) {
        polyNoteOn = false;
    }
}

bool MidiServer::checkControllerMapping(const MIDIMessage &m)
{
    const int DEAD_ZONE_THRESHOLD = 2;

    for ( auto & t : controllerTemplate ) {
        if ( t.channel != -1 && t.channel != m.channel ) {
            continue;
        }
        if ( t.cc != m.cc && t.pitch != m.pitch ) {
            continue;
        }

        if ( t.parameterLabel == SELECTED ) {
            Units::getInstance()->selectUnitAtIndex( t.unitIndex );
        } else if ( t.parameterLabel == CAM_X ||
                    t.parameterLabel == CAM_Y) {
            int value = m.ccVal;
            const int MAX_MOVE = 14;

            if ( t.invert ) {
                value = ofMap( value, 0, 127, 127, 0, true );
            }
            if ( t.dead_zone >= 0 ) {
                if ( value >= t.dead_zone - DEAD_ZONE_THRESHOLD &&
                     value <= t.dead_zone + DEAD_ZONE_THRESHOLD) {
                    moveCamDelta = {0,0,0};
                    return true;
                }
            }

            if ( t.parameterLabel == CAM_X ) {
                if ( value < 64 ) {
                    value = ofMap(value, 0, 63, MAX_MOVE, 1);
                    moveCamDelta = ofVec3f(value, moveCamDelta.y, 0);
                } else if ( value > 64 ) {
                    value = ofMap(value, 65, 127, 1, MAX_MOVE);
                    moveCamDelta = ofVec3f(-value, moveCamDelta.y, 0);
                } else {
                    moveCamDelta = {0,moveCamDelta.y,0};
                }
            }
            if ( t.parameterLabel == CAM_Y ) {
                if ( value < 64 ) {
                    value = ofMap(value, 0, 63, MAX_MOVE, 1);
                    moveCamDelta = ofVec3f(moveCamDelta.x, -value, 0);
                } else if ( value > 64 ) {
                    value = ofMap(value, 65, 127, 1, MAX_MOVE);
                    moveCamDelta = ofVec3f(moveCamDelta.x, value, 0);
                } else {
                    moveCamDelta = {moveCamDelta.x,0,0};
                }
            }

        } else if ( t.parameterLabel == CAM_ZOOM_IN ||
                    t.parameterLabel == CAM_ZOOM_OUT ||
                    t.parameterLabel == CAM_ZOOM_OUT_FAST ||
                    t.parameterLabel == CAM_ZOOM_IN_FAST) {

            int value = m.status == MIDI_NOTE_ON && m.velocity != 0 ? 127 : 0;
            const float MAX_ZOOM = 0.15f;
            const float MAX_ZOOM_FAST = 0.3f;

            if ( value == 0 ) {
                moveCamZoom = 0;
                sendMidiMessage(NOTE_ON, m.channel, m.pitch, 0);
            } else {
                if ( t.parameterLabel == CAM_ZOOM_IN ) {
                    moveCamZoom = MAX_ZOOM;
                    sendMidiMessage(NOTE_ON, m.channel, m.pitch, 127);
                } else if ( t.parameterLabel == CAM_ZOOM_OUT ) {
                    moveCamZoom = -MAX_ZOOM;
                    sendMidiMessage(NOTE_ON, m.channel, m.pitch, 127);
                } else if ( t.parameterLabel == CAM_ZOOM_IN_FAST ) {
                    moveCamZoom = MAX_ZOOM_FAST;
                    sendMidiMessage(NOTE_ON, m.channel, m.pitch, 127);
                } else if ( t.parameterLabel == CAM_ZOOM_OUT_FAST ) {
                    moveCamZoom = -MAX_ZOOM_FAST;
                    sendMidiMessage(NOTE_ON, m.channel, m.pitch, 127);
                }
            }
        } else if ( t.parameterLabel == CAM_ZOOM ) {

            const float MIN_ZOOM = 0.75f;
            const float MAX_ZOOM = 10.f;

            float value = m.ccVal;

            if ( t.invert ) {
                value = ofMap( value, 0, 127, 127, 0, true );
            }

            float shaper=3.5f;
            float pct = ofMap(value,  0.f, 127.f, 0.f, 1.f, true);
            pct = powf(pct, shaper);

            value = ofMap(pct, 0, 1, MIN_ZOOM, MAX_ZOOM, true);
            moveCamZoomAbs = value;
        } else {
            UberControl * parameter = getParameterFromControllerTemplate(t);

            if ( parameter == nullptr ) {
                break;
            }

            int value = -1;
            if ( t.cc == m.cc ) {
                value = m.ccVal;
                if ( t.invert ) {
                    value = ofMap( value, 0, 127, 127, 0, true );
                }
            } else {
                value = m.status == MIDI_NOTE_ON && m.velocity != 0 ? 127 : 0;
                //Toggle buttons have special behaviour.
                if ( dynamic_cast<UberToggle*>(parameter) != nullptr) {
                    if ( value == 0 ) {
                        break;
                    } else {
                        if ( parameter->get() == parameter->getMax() ) {
                            value = 0;
                        } else {
                            value = 127;
                        }
                    }
                }
            }

            float mappedValue = ofMap(value,
                                      0,
                                      127,
                                      parameter->getMin(),
                                      parameter->getMax(),
                                      true);

            parameter->set(mappedValue);

            OscServer::getInstance()->broadcastUberControlChange(parameter);
            //only feedback MIDI when NOTE
            if ( m.status != MIDI_CONTROL_CHANGE &&
                 m.status != MIDI_PITCH_BEND ) {
                broadcastUberControlChange(parameter);
            }
        }

        return true;
    }
    return false;
}

UberControl *MidiServer::getParameterFromControllerTemplate(const MIDIMappingControllerTemplate &t)
{
    UberControl * parameter = nullptr;

    if ( t.unitIndex == -1 ) {
        if ( t.parameterLabel == MASTER_VOLUME ) {
            parameter = &Units::getInstance()->masterVolume;
        } else if ( t.parameterLabel == TEMPO ) {
            parameter = &MasterClock::getInstance()->bpm;
        } else if ( t.parameterLabel == MUTE_ALL ) {
            parameter = &Units::getInstance()->btnMuteAll;
        } else if ( t.parameterLabel == SOLO_ALL ) {
            parameter = &Units::getInstance()->btnSoloAll;
        } else if ( t.parameterLabel == ACTIVE_ALL ) {
            parameter = &Units::getInstance()->btnActiveAll;
        }

    } else {
        if ( t.parameterLabel == VOLUME ) {
            parameter = Units::getInstance()->getUnitVolume(t.unitIndex);
        } else if ( t.parameterLabel == PAN ) {
            parameter = Units::getInstance()->getUnitPan(t.unitIndex);
        } else if ( t.parameterLabel == MUTE ) {
            parameter = Units::getInstance()->getUnitMute(t.unitIndex);
        } else if ( t.parameterLabel == SOLO ) {
            parameter = Units::getInstance()->getUnitSolo(t.unitIndex);
        } else if ( t.parameterLabel == ACTIVE ) {
            parameter = Units::getInstance()->getUnitActive(t.unitIndex);
        } else if ( t.parameterLabel == PITCH ) {
            parameter = Units::getInstance()->getUnitPitch(t.unitIndex);
        }
    }

    return parameter;
}

void MidiServer::broadcastAllMidiMappings(string strMIDIDevice)
{
    for(unsigned int i = 0; i < midiMappings.size(); i++) {
        UberControl * parameter = midiMappings[i].parameter;
        if ( parameter != nullptr ) {
            float v = ofMap(parameter->get(), parameter->getMin(), parameter->getMax(),0,127);

            int midiChannel = midiMappings[i].channel;
            if ( midiChannel == -1 ) {
                midiChannel = 1;
            }

            if ( midiMappings[i].cc != -1 ) {
                sendMidiMessage(CONTROL_CHANGE, midiChannel, midiMappings[i].cc, v, strMIDIDevice);
            } else {
                sendMidiMessage(NOTE_ON, midiChannel, midiMappings[i].pitch, v, strMIDIDevice);
            }
        } else {
            ofLog() << "broadcastAllMidiMappings: parameter " << midiMappings[i].label  << " was null";
        }
    }

    for ( auto & t : controllerTemplate ) {
        UberControl * parameter = getParameterFromControllerTemplate(t);
        if ( parameter != nullptr ) {
            float v = ofMap(parameter->get(), parameter->getMin(), parameter->getMax(),0,127);

            int midiChannel = t.channel;
            if ( midiChannel == -1 ) {
                midiChannel = 1;
            }

            if ( t.cc != -1 ) {
                sendMidiMessage(CONTROL_CHANGE, midiChannel, t.cc, v, strMIDIDevice);
            } else {
                sendMidiMessage(NOTE_ON, midiChannel, t.pitch, v, strMIDIDevice);
            }
        }
    }
}

void MidiServer::moveCam()
{
    if ( moveCamDelta != ofVec3f(0,0,0) ) {
        CamZoomAndPan * cam = CamZoomAndPan::getInstance();
        cam->moveBy(moveCamDelta);
    }
    if ( moveCamZoom != 0 ) {
        CamZoomAndPan * cam = CamZoomAndPan::getInstance();
        cam->zoomBy(moveCamZoom);
    }
    if ( moveCamZoomAbs > 0 ) {
        CamZoomAndPan * cam = CamZoomAndPan::getInstance();
        cam->zoomTo(moveCamZoomAbs);
        moveCamZoomAbs=0.f;
    }
}

void MidiServer::broadcastUberControlChange(UberControl *parameter)
{
    for(unsigned int i = 0; i < midiMappings.size(); i++) {
        if( midiMappings[i].parameter == parameter ) {
            float v = ofMap(parameter->get(), parameter->getMin(), parameter->getMax(),0,127);

            int midiChannel = midiMappings[i].channel;
            if ( midiChannel == -1 ) {
                midiChannel = 1;
            }

            if ( midiMappings[i].cc != -1 ) {
                sendMidiMessage(CONTROL_CHANGE, midiChannel, midiMappings[i].cc, v);
            } else {
                sendMidiMessage(NOTE_ON, midiChannel, midiMappings[i].pitch, v);
            }

            return;
        }
    }

    for ( auto & t : controllerTemplate ) {
        UberControl * templateParameter = getParameterFromControllerTemplate(t);
        if ( templateParameter != nullptr && templateParameter == parameter ) {
            float v = ofMap(parameter->get(), parameter->getMin(), parameter->getMax(),0,127);

            int midiChannel = t.channel;
            if ( midiChannel == -1 ) {
                midiChannel = 1;
            }

            if ( t.cc != -1 ) {
                sendMidiMessage(CONTROL_CHANGE, midiChannel, t.cc, v);
            } else {
                sendMidiMessage(NOTE_ON, midiChannel, t.pitch, v);
            }

            return;
        }
    }
}

void MidiServer::onSelectedUnit(int index)
{
    for ( auto & t : controllerTemplate ) {
        if ( t.parameterLabel == SELECTED ) {
            float v = t.unitIndex == index ? 127 : 0;

            int midiChannel = t.channel;
            if ( midiChannel == -1 ) {
                midiChannel = 1;
            }

            if ( t.cc != -1 ) {
                sendMidiMessage(CONTROL_CHANGE, midiChannel, t.cc, v);
            } else {
                sendMidiMessage(NOTE_ON, midiChannel, t.pitch, v);
            }
        }
    }
}

MidiServer *MidiServer::getInstance()
{
    if ( instance == nullptr ) {
        instance = new MidiServer();
    }

    return instance;
}

Json::Value MidiServer::save(){
   Json::Value root = Json::Value(Json::arrayValue);
   for(unsigned int i = 0; i < midiMappings.size(); i++) {
       if ( midiMappings[i].parameter == nullptr ) {
           ofLog() << "MIDI parameter is null, is this a bug?";
           continue;
       }

       Json::Value val = Json::Value(Json::objectValue);
       val["channel"] = midiMappings[i].channel;
       val["cc"] = midiMappings[i].cc;
       val["pitch"] = midiMappings[i].pitch;
       val["label"] = midiMappings[i].parameter->getName();
       val["usesVelocity"] = midiMappings[i].usesVelocity;
       val["ignoreNoteOff"] = midiMappings[i].ignoreNoteOff;

       val["rangeFrom"] = midiMappings[i].range[0];
       val["rangeTo"] = midiMappings[i].range[1];

       val["useMIDIKeyboard"] = midiMappings[i].useMIDIKeyboard;

       root.append(val);
   }
   return root;
}

void MidiServer::load(Json::Value jsonData){
    if(jsonData != Json::nullValue){
        for(unsigned int i = 0; i < jsonData.size(); i++) {
           Json::Value c = jsonData[i];
           MIDIMapping mapping;
           if ( !c["channel"].isNull() ) {
               mapping.channel = c["channel"].asInt();
           } else {
               mapping.channel = -1;
           }
           mapping.label = c["label"].asString();
           mapping.cc = c["cc"].asInt();
           mapping.pitch = c["pitch"].asInt();
           mapping.usesVelocity = c["usesVelocity"].asBool();
           mapping.ignoreNoteOff = c["ignoreNoteOff"].asBool();
           mapping.range[0] = c["rangeFrom"].asFloat();
           mapping.range[1] = c["rangeTo"].asFloat();
           mapping.useMIDIKeyboard = c["useMIDIKeyboard"].asBool();

           ofStringReplace(mapping.label, "\"", "");
           ofStringReplace(mapping.label, "\n", "");
           //ofStringReplace(mapping.label, " ", "");

           midiMappings.push_back(mapping);
        }
        if ( jsonData.size() > 0 ) unknownMappings = true;
    }
}

void MidiServer::reset(){
    midiMappings.clear();
}

void MidiServer::onUberControlDead(UberControl &deadControl)
{
    for(unsigned int i = 0; i < midiMappings.size(); i++) {
        if ( midiMappings[i].parameter == &deadControl ) {
            midiMappings.erase( midiMappings.begin() + i );
        }
    }
}

bool MidiServer::isAnyDeviceConnected() {
    map<string, ofxMidiIn *>::iterator it;
    for (it = midiDevicesIN.begin(); it != midiDevicesIN.end(); it++) {
        if ( it->second != nullptr ) {
            return true;
        }
    }

    return false;
}

vector<string> MidiServer::getConnectedInputDevices()
{
    vector<string> connectedDevices;

    map<string, ofxMidiIn *>::iterator it;
    for (it = midiDevicesIN.begin(); it != midiDevicesIN.end(); it++) {
        if ( it->second != nullptr ) {
            connectedDevices.push_back(it->first);
        }
    }

    return connectedDevices;
}

vector<string> MidiServer::getConnectedOutputDevices()
{
    vector<string> connectedDevices;

    map<string, ofxMidiOut *>::iterator it;
    for (it = midiDevicesOUT.begin(); it != midiDevicesOUT.end(); it++) {
        if ( it->second != nullptr ) {
            connectedDevices.push_back(it->first);
        }
    }

    return connectedDevices;
}

void MidiServer::connectDevicesIn(const vector<string> & devices){
    for ( auto & d : devices ) {
        addMidiDeviceIn(d);
    }
}

void MidiServer::connectDevicesOut(const vector<string> &devices)
{
    for ( auto & d : devices ) {
        addMidiDeviceOut(d);
    }
}

bool MidiServer::loadControllerTemplateFile(string path)
{
    ofxJSONElement data;
    if ( data.open(path) ) {
        if ( data["units"] != Json::nullValue &&
             data["global"] != Json::nullValue) {

            controllerTemplate.clear();
            controllerTemplatePath = path;

            /////////////
            /// CHANNEL
            /////////////

            int controllerMIDIChannel = -1;
            if ( !data["channel"].isNull() ) {
                controllerMIDIChannel = data["channel"].asInt();
            }

            /////////////
            /// UNITS
            /////////////

            int i = 0;
            for ( auto & unit : data["units"] ) {
                int unitMIDIChannel = -2;
                if ( unit["channel"] != Json::nullValue ) {
                    unitMIDIChannel = unit["channel"].asInt();
                }

                for ( Json::Value::iterator it = unit.begin(); it != unit.end(); ++it ) {
                    MIDIMappingControllerTemplate t;
                    ControllerTemplateParameterLabels label = controllerConfigToLabel( it.key().asString() );
                    if ( label == CHANNEL ) continue;
                    if ( label != INVALID ) {
                        Json::Value value = (*it);
                        if ( value != Json::nullValue ) {
                            if ( value["channel"] != Json::nullValue ) {
                                t.channel = value["channel"].asInt();
                            } else {
                                if ( unitMIDIChannel == -2 ) {
                                    t.channel = controllerMIDIChannel;
                                } else {
                                    t.channel = unitMIDIChannel;
                                }
                            }
                            if ( value["cc"] != Json::nullValue ) {
                                t.cc = value["cc"].asInt();
                            } else if ( value["note"] != Json::nullValue ) {
                                t.pitch = value["note"].asInt();
                            }

                            if ( value["dead_zone"] != Json::nullValue ) {
                                t.dead_zone = value["dead_zone"].asInt();
                            }
                            if ( value["invert"] != Json::nullValue ) {
                                t.invert = value["invert"].asBool();
                            }

                            t.unitIndex = i;
                            t.parameterLabel = label;

                            controllerTemplate.push_back(t);
                        } else {
                            ofLog() << "Invalid value for label '" << it.key().asString() << "' on controller template";
                        }
                    } else {
                        ofLog() << "Invalid label '" << it.key().asString() << "' on controller template";
                    }
                }

                i++;
            }

            /////////////
            /// GLOBAL
            /////////////

            for ( Json::Value::iterator it = data["global"].begin(); it != data["global"].end(); ++it ) {
                MIDIMappingControllerTemplate t;
                ControllerTemplateParameterLabels label = controllerConfigToLabel( it.key().asString() );
                if ( label != INVALID ) {
                    Json::Value value = (*it);
                    if ( value != Json::nullValue ) {
                        if ( value["channel"] != Json::nullValue ) {
                            t.channel = value["channel"].asInt();
                        } else {
                            t.channel = controllerMIDIChannel;
                        }

                        if ( value["cc"] != Json::nullValue ) {
                            t.cc = value["cc"].asInt();
                        } else if ( value["note"] != Json::nullValue ) {
                            t.pitch = value["note"].asInt();
                        }

                        if ( value["dead_zone"] != Json::nullValue ) {
                            t.dead_zone = value["dead_zone"].asInt();
                        }
                        if ( value["invert"] != Json::nullValue ) {
                            t.invert = value["invert"].asBool();
                        }

                        t.parameterLabel = label;
                        controllerTemplate.push_back(t);
                    } else {
                        ofLog() << "Invalid value for label '" << it.key().asString() << "' on controller template";
                    }
                } else {
                    ofLog() << "Invalid label '" << it.key().asString() << "' on controller template";
                }
            }

            return true;
        } else {
            ofLog() << "Invalid controller template file";
        }
    } else {
        ofLog() << "Couldn't load controller template file";
    }

    return false;
}

void MidiServer::clearControllerTemplateFile()
{
    controllerTemplate.clear();
    controllerTemplatePath = "";
}

bool MidiServer::isControllerTemplateFileEnabled()
{
    return controllerTemplatePath != "";
}

ControllerTemplateParameterLabels MidiServer::controllerConfigToLabel(string config)
{
    if ( config == "volume" ) {
        return VOLUME;
    } else if ( config == "pan" ) {
        return PAN;
    } else if ( config == "mute" ) {
        return MUTE;
    } else if ( config == "solo" ) {
        return SOLO;
    } else if ( config == "selected" ) {
        return SELECTED;
    } else if ( config == "samplePitch" ) {
        return PITCH;
    } else if ( config == "masterVolume" ) {
        return MASTER_VOLUME;
    } else if ( config == "tempo" ) {
        return TEMPO;
    } else if ( config == "active" ) {
        return ACTIVE;
    } else if ( config == "mute_all" ) {
        return MUTE_ALL;
    } else if ( config == "solo_all" ) {
        return SOLO_ALL;
    } else if ( config == "active_all" ) {
        return ACTIVE_ALL;
    } else if ( config == "cam_x" ) {
        return CAM_X;
    } else if ( config == "cam_y" ) {
        return CAM_Y;
    } else if ( config == "cam_zoom" ) {
        return CAM_ZOOM;
    } else if ( config == "cam_zoom_in" ) {
        return CAM_ZOOM_IN;
    } else if ( config == "cam_zoom_out" ) {
        return CAM_ZOOM_OUT;
    } else if ( config == "cam_zoom_in_fast" ) {
        return CAM_ZOOM_IN_FAST;
    } else if ( config == "cam_zoom_out_fast" ) {
        return CAM_ZOOM_OUT_FAST;
    } else if ( config == "channel" ) {
        return CHANNEL;
    } else {
        return INVALID;
    }
}

const string &MidiServer::getControllerTemplatePath()
{
    return controllerTemplatePath;
}
