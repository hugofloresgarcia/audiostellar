#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxImGui.h"
#include "ofxJSON.h"

#include "../Utils/Tooltip.h"
#include "../GUI/UberControl.h"
#include "../Sound/Sound.h"
#include "../GUI/UberButton.h"

#define OSC_LOG false

struct oscMapping {
   string oscRoute;
   UberControl * parameter = nullptr;
   string label; //for saving to file
   float range[2] = {0.f,1.f};
};

class OscServer {
    
private:
    const string ROUTE_UNITS = "units";
    // /units/<index>/volume [float 0-1]
    const string ROUTE_UNITS_VOLUME = "volume";
    // /units/<index>/pan [float 0-1]
    const string ROUTE_UNITS_PAN = "pan";
    const string ROUTE_UNITS_MUTE = "mute";
    const string ROUTE_UNITS_SOLO = "solo";

    const string OSC_GET_NUM_SOUNDS = "/get/soundCount";
    const string OSC_GET_NEIGHBORS_ID = "/get/neighborsByID";
    const string OSC_GET_NEIGHBORS_XY = "/get/neighborsByXY";
    const string OSC_GET_POSITION_ID = "/get/positionByID";

    const string OSC_GET_CLUSTER_ID = "/get/clusterIDByID";
    const string OSC_GET_CLUSTER_NAME = "/get/clusterNameByID";
    const string OSC_GET_CLUSTER_NAME_BY_CLUSTER_INDEX = "/get/clusterNameByClusterID";

    const string OSC_GET_PLAYED_SOUND_ID = "/get/playedSound";
    bool oscGetPlayedSoundIDEnabled = false;

    const ImVec4 MONITOR_COLOR_ACTIVE = ImVec4(0.919f, 0.814f, 0.240f, 1.0f);
    const ImVec4 MONITOR_COLOR_INACTIVE = ImVec4(0.2,0.2,0.2,1);
    uint64_t lastOSCMessageMillis = 0;
    bool haveToDrawOSCLog = false;
    vector<string> oscLog;
    bool oscLogEnable = false;
    bool oscLogFixToBottom = true;

    static int receivePort;
    string receiveHost;
    static int sendPort;
    static char sendHost[64];
    
    static char hostname[128];

    OscServer();
    static OscServer* instance;

    oscMapping * lastControlMoved = nullptr;
    vector<oscMapping> oscMappings;
    bool unknownMappings = false;

    bool oscLearn = false;
    vector<string> oscLearnAddresses;
    string oscLearnSelectedAddress = "";
    float oscLearnRange[2] = {0.f,1.f};

    UberButton btnMonitor{"OSC##Monitor", &Tooltip::OSC_MONITOR};
public:
    static OscServer* getInstance();

    ofxOscReceiver oscReceiver;
    static ofxOscSender oscSender;
    
    static ofEvent<ofxOscMessage> oscEvent;
    static bool enable;
    
    void update();
    bool processFixedAddresses(ofxOscMessage &m);
    
    void start();
    void stop();
    
    void drawGui();
    void drawMonitor();
    void drawOSCLog();
    void drawOSCLearn();

    void btnMonitorListener(float &v);

    void getInterfaceIP();
    void log(ofxOscMessage m);

    void setOSCLearnOn( UberControl *parameter );
    void setOSCLearnOff();
    bool getOSCLearn();
    void broadcastUberControlChange(UberControl *parameter);
    // when loads from file it will be true
    // then uberslider will take care of this
    bool hasUnknownMappings();
    //if label is in mappings array it will set it
    bool fixUnknownMapping(UberControl *parameter);
    void removeMapping( UberControl *parameter);

    Json::Value save();
    void load(Json::Value jsonData);
    void reset();

    void onUberControlDead( UberControl & deadControl );
    void onPlaySound( Sound & sound );

    void mixerChangedVolume(unsigned int unitIndex, float value);
    void mixerChangedPan(unsigned int unitIndex, float value);
    void mixerChangedMute(unsigned int unitIndex, float value);
    void mixerChangedSolo(unsigned int unitIndex, float value);

    void static sendMessage(ofxOscMessage message);
    void static sendMessage(string address, float value);
    void static sendMessage(string address, int value);
    void static sendMessage(string address, string value);

    string static getSendHost();
    int static getReceivePort();
    int static getSendPort();
    void static setSendHost( string v );
    void static setReceivePort( int v );
    void static setSendPort( int v );
};





