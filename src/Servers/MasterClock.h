#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "../Sound/AudioEngine.h"
#include "../Utils/Utils.h"
#include "../GUI/UI.h"
#include "../Servers/MidiServer.h"

#include "../GUI/UberSlider.h"
#include "../GUI/UberToggle.h"

class Ubertoggle;

class MasterClock
{
public:
    enum ClockType {
        INTERNAL,
        MIDI,
        ABLETON_LINK};

    static MasterClock* getInstance();

    ofFastEvent<int> onTempo;

//    void setUseMidiClock(bool v);
//    bool getUseMidiClock();
    void setClockType(ClockType t);
    ClockType getClockType();

    void onMIDIClockTick( MIDIMessage m );
    void onAbletonLinkClockTick(int p);
    void onLinkClick(float &v);
    void setLinkStatus(bool b);

    void drawGui();

    void play();
    void stop();
    bool isPlaying();

    float getBPM();
    void setBPM( float theBPM );
    UberSlider bpm = { "BPM", 120, 10, 300, &Tooltip::NONE };
private:
    pdsp::Function * pdspFunction = nullptr;
    void initPDSPFunctionClock();
    void destroyPDSPFunctionClock();

//    bool useMIDIClock = false;
    bool playing = true;

    UberToggle btnLink = { "Link", &Tooltip::MASTERCLOCK_LINK };
//    ofParameter<int> bpm;
    void bpmListener(float &v);

    ClockType currentClockType = INTERNAL;

    MasterClock();
    static MasterClock* instance;
    
    uint64_t lastMidiClockTickTime = 0;
    
    vector<float> BPMs = vector<float>(20, 0.f);

    
};
