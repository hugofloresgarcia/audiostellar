#pragma once

#include "ofMain.h"
#include "ofxImGui.h"
#include "Utils/SessionManager.h"
#include "Utils/Utils.h"
#include "GUI/UI.h"
#include "GUI/CamZoomAndPan.h"
#include "GUI/CamAutoPilot.h"
#include "Servers/MidiServer.h"
#include "ML/DBScan.h"
#include "GUI/ColorPalette.h"
#include "ML/AudioDimensionalityReduction.h"
#include "ofxOsc.h"
#include "Servers/OscServer.h"
#include "ofxPDSP.h"
#include "Units/Units.h"
#include "Sound/Voices.h"
#include "Servers/MasterClock.h"
#include "Utils/AudioSegmentationTool.h"
#include "Servers/AbletonLinkServer.h"

class ofApp : public ofBaseApp {
public:
    void setup();
    void update();
    void draw();
    void exit();
    bool tryToExit(ofEventArgs& args);

    void drawFps();

    void keyPressed(ofKeyEventArgs & e);
    void keyReleased(ofKeyEventArgs & e);
    void mousePressed(int x, int y, int button);
    void mouseScrolled(int x, int y, float scrollX, float scrollY);
    void mouseDragged(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void mouseMoved(int x, int y);

    char cwd[1024];
    vector<string> arguments;
    SessionManager * sessionManager = NULL;
    Sounds * sounds = NULL;
    MidiServer * midiServer = NULL;
    OscServer * oscServer = NULL;
    Units * units = nullptr;
    Gui * gui = NULL;
    CamAutoPilot* autoPilot = NULL;
    CamZoomAndPan * cam;
    AudioSegmentationTool * ast = NULL;
};
