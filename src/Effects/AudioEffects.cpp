#include "AudioEffects.h"
#include "./VAFilterUI.h"
#include "./BasiVerbUI.h"
#include "./DimensionChorusUI.h"

// not in use anymore
// patching the whole pipeline leads to crashes
void AudioEffects::patch()
{
    bool wasAudioEngineStarted = AudioEngine::getInstance()->hasStarted();

    if ( wasAudioEngineStarted ) {
        AudioEngine::getInstance()->stop();
    }

    if ( effects.size() == 0 ) {
        nodeIn0.disconnectOut();
        nodeIn1.disconnectOut();

        nodeIn0 >> nodeOut0;
        nodeIn1 >> nodeOut1;
    } else {
        nodeIn1.disconnectOut();
        nodeIn0.disconnectOut();
        nodeOut0.disconnectIn();
        nodeOut1.disconnectIn();

        nodeIn0 >> effects[0]->channelIn(0);
        nodeIn1 >> effects[0]->channelIn(1);

        for ( unsigned int i = 0 ; i < effects.size() - 1 ; i++ ) {
            effects[i]->disconnectOut();
            effects[i]->channelOut(0) >> effects[i+1]->channelIn(0);
            effects[i]->channelOut(1) >> effects[i+1]->channelIn(1);
        }

        effects[ effects.size()-1 ]->channelOut(0) >> nodeOut0;
        effects[ effects.size()-1 ]->channelOut(1) >> nodeOut1;
    }

    if ( wasAudioEngineStarted ) {
        AudioEngine::getInstance()->start();
    }
}

AudioEffects::AudioEffects() : pdsp::Patchable()
{
    addModuleInput("0", nodeIn0);
    addModuleInput("1", nodeIn1);

    nodeIn0 >> nodeOut0;
    nodeIn1 >> nodeOut1;

    addModuleOutput("0", nodeOut0);
    addModuleOutput("1", nodeOut1);
}

AudioEffects::~AudioEffects()
{
    for ( auto effect : effects ) {
        effect->disconnectAll();
        delete effect;
    }

    nodeIn0.disconnectAll();
    nodeIn1.disconnectAll();
    nodeOut0.disconnectAll();
    nodeOut1.disconnectAll();
}

void AudioEffects::addEffect(EffectUI *newEffect)
{
    effects.push_back(newEffect);

    if ( this->indexPrefix != "" ) {
        setIndexesToEffects(this->indexPrefix);
    }

    patch();
//    AudioEngine::getInstance()->engine.stop();
//    //crashes still happen. gonna ask ofxPDSP
//    //instead of patching the whole pipeline, just make the adjustments needed
//    if ( effects.size() == 1 ) {
//        nodeIn0.disconnectOut();
//        nodeIn1.disconnectOut();
//        nodeOut0.disconnectIn();
//        nodeOut1.disconnectIn();

//        nodeIn0 >> effects[0]->channelOut(0) >> nodeOut0;
//        nodeIn1 >> effects[0]->channelOut(1) >> nodeOut1;
//    } else {
//        int last = effects.size() - 1;
//        effects[ last - 1 ]->disconnectOut();
//        nodeOut0.disconnectIn();
//        nodeOut1.disconnectIn();

//        effects[ last - 1 ]->channelOut(0) >> effects[ last ]->channelIn(0);
//        effects[ last - 1 ]->channelOut(1) >> effects[ last ]->channelIn(1);

//        effects[ last ]->channelOut(0) >> nodeOut0;
//        effects[ last ]->channelOut(1) >> nodeOut1;
//    }
//    AudioEngine::getInstance()->engine.start();
}

void AudioEffects::removeEffect(EffectUI *effect)
{
    //instead of patching the whole pipeline, just make the adjustments needed
    int idxToDelete = -1;
    effect->disconnectAll();

    if ( effects.size() == 1 ) {
        idxToDelete = 0;
        nodeIn0 >> nodeOut0;
        nodeIn1 >> nodeOut1;
    } else {
        for ( unsigned int i = 0 ; i < effects.size() ; i++ ) {
            if ( effects[i] == effect ) {
                idxToDelete = i;
                //is first
                if ( i == 0 ) {
                    nodeIn0 >> effects[1]->channelIn(0);
                    nodeIn1 >> effects[1]->channelIn(1);
                //is last
                } else if ( i == effects.size()-1 ) {
                    effects[ effects.size()-2 ]->channelOut(0) >> nodeOut0;
                    effects[ effects.size()-2 ]->channelOut(1) >> nodeOut1;
                //is middle -> at least 3 effects -> previous and next must exist
                } else {
                    effects[i-1]->channelOut(0) >> effects[i+1]->channelIn(0);
                    effects[i-1]->channelOut(1) >> effects[i+1]->channelIn(1);
                }
            }
        }
    }

    std::vector<EffectUI *>::iterator it = effects.begin() + idxToDelete;
    effects.erase(it);

    for (auto & p : effect->parameters) {
        p->removeAllMappings();
    }

    delete effect;

    setIndexesToEffects(indexPrefix);
}

void AudioEffects::removeMappings()
{
    for ( auto & effect : effects ) {
        for ( auto & p : effect->parameters ) {
            p->removeAllMappings();
        }
    }
}

Json::Value AudioEffects::save()
{
    Json::Value root = Json::Value( Json::arrayValue );
    for(unsigned int i = 0; i < effects.size(); i++) {
        EffectUI * effect = effects[i];
        Json::Value jsonUnit = Json::Value( Json::objectValue );
        jsonUnit["name"] = effect->getName();
        jsonUnit["params"] = effect->save();
        root.append(jsonUnit);
    }
    return root;
}

void AudioEffects::load(Json::Value jsonAudioEffects)
{
    for ( unsigned int i = 0 ; i < jsonAudioEffects.size() ; i++ ) {
        Json::Value jsonEffect = jsonAudioEffects[i];

        if ( jsonEffect["name"].asString() == "VAFilter" ) {
            VAFilterUI * effect = new VAFilterUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else if ( jsonEffect["name"].asString() == "BasiVerb" ) {
            BasiVerbUI * effect = new BasiVerbUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else if ( jsonEffect["name"].asString() == "DimensionChorus" ) {
            DimensionChorusUI * effect = new DimensionChorusUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else if ( jsonEffect["name"].asString() == "Saturator" ) {
            SaturatorUI * effect = new SaturatorUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else if ( jsonEffect["name"].asString() == "Delay" ) {
            DelayUI * effect = new DelayUI();
            addEffect( effect );
            effect->load(jsonEffect["params"]);
        } else {
            ofLog() << "AudioEffects load: Unknown effect '" << jsonEffect["name"].asString() << "'";
        }
    }
}

void AudioEffects::setIndexesToEffects(string indexPrefix)
{
    this->indexPrefix = indexPrefix;

    for ( unsigned int i = 0 ; i < effects.size() ; i++ ) {
        effects[i]->setIndex(i, indexPrefix);
    }
}

void AudioEffects::drawGui()
{
    int i = 0;
    for ( EffectUI * effect : effects ) {
        
        ImVec2 startPos = ImGui::GetCursorScreenPos();
        ImVec2 padding = ImVec2(10, 10);
        ImGui::SetCursorScreenPos(startPos + padding);
        string id = effect->getName() + ofToString(i);
        
        ImGui::BeginGroup();
        
        ImGui::TextDisabled( "%s",  effect->getName().c_str() );

        if ( ImGui::IsItemClicked(1) ) {
            ImGui::OpenPopup( id.c_str() );
        }

//        ImGui::SameLine();
//        string id = "-##" + ofToString(i);
//        if ( ImGui::SmallButton(id.c_str()) ) {
//            removeEffect(effect);
//            ImGui::EndGroup();
//            break;
//        }
        ImGui::SameLine(ImGui::GetContentRegionAvail().x - padding.x - 15);
        if(!effect->enable->get()) {
            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.4f);
            effect->drawGui();
            ImGui::PopStyleVar();
        }
        else {
            effect->drawGui();
        }

        ImGui::Dummy(ImVec2(0, padding.y));
        
        ImGui::EndGroup();
        
        if (ImGui::BeginPopup( id.c_str() )) {
            if (ImGui::MenuItem("Remove Effect")) {
                removeEffect(effect);
                ImGui::CloseCurrentPopup();
            }
            ImGui::EndPopup();
        }

        ImVec2 rectStart = startPos;
        ImVec2 rectEnd = ImVec2(ImGui::GetContentRegionMaxAbs().x,
                                ImGui::GetItemRectMax().y);
        ImColor rectBorderColor = ImGui::GetStyle().Colors[ImGuiCol_Border];

        ImGui::GetWindowDrawList()->AddRect(rectStart, rectEnd, rectBorderColor);
        
        i++;
    }
}
