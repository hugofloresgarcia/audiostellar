#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "./EffectUI.h"
#include "../GUI/UI.h"

class SaturatorUI : public EffectUI {
private:
    pdsp::ParameterGain inGain;

    pdsp::Saturator2 saturatorL;
    pdsp::Saturator2 saturatorR;

    pdsp::ParameterGain outGain;
public:
    SaturatorUI();

    string getName();

    pdsp::Patchable & channelIn(size_t index);
    pdsp::Patchable & channelOut(size_t index);
};
