#include "DimensionChorusUI.h"


DimensionChorusUI::DimensionChorusUI()
{
    parameters.push_back( new UberSlider(speed.set("Speed",0.25f,0.f,5.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(depth.set("Depth",10.f,0.f,1000.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(delay.set("Delay",80.f,0.f,1000.f), &Tooltip::NONE) );
    
    switchL.resize(2);
    switchR.resize(2);

    inL >> switchL.input(0);
    inR >> switchR.input(0);
    
    inL >> chorus.ch(0) >> switchL.input(1);
    inR >> chorus.ch(1) >> switchR.input(1);
    
    switchL >> outL;
    switchR >> outR;
    
    1.f >> switchL.in_select();
    1.f >> switchR.in_select();

    speed >> chorus.in_speed();
    depth >> chorus.in_depth();
    delay >> chorus.in_delay();
}

string DimensionChorusUI::getName()
{
    return "DimensionChorus";
}

