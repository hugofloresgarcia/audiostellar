#include "BasiVerbUI.h"

BasiVerbUI::BasiVerbUI()
{   
    mix = new UberSlider(stereoAmpWet.set("Mix", 0.5f, 0.f, 1.f), &Tooltip::NONE);
    mix->addListener(this, &BasiVerbUI::onMix, OF_EVENT_ORDER_AFTER_APP);
    parameters.push_back(mix);

    rt = new UberSlider(rt60.set("RT60",3.33f,0.f,20.f), &Tooltip::NONE);
    rt->log = true;
    parameters.push_back(rt);
    
    parameters.push_back( new UberSlider(density.set("Density",0.5f,0.f,1.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(damping.set("Damping",0.5f,0.f,1.f), &Tooltip::NONE) );
//    parameters.push_back( new UberSlider(hicut.set("Hi-cut",5000.f,0.f,22000.f), &Tooltip::NONE) );

    parameters.push_back( new UberSlider(lfoFreq.set("Mod speed",0.5f,0.f,200.f), &Tooltip::NONE) );
    parameters.push_back( new UberSlider(lfoAmount.set("Mod amount",0.f,0.f,1.f), &Tooltip::NONE) );
    
    switchL.resize(2);
    switchR.resize(2);

    ofAddListener( ofEvents().update, this, &BasiVerbUI::update );
    patch();
}

BasiVerbUI::~BasiVerbUI()
{
    ofRemoveListener( ofEvents().update, this, &BasiVerbUI::update );
}

string BasiVerbUI::getName()
{
    return "BasiVerb";
}

void BasiVerbUI::onMix(float &v)
{
    stereoAmpDry.set(1.f - v);
}

void BasiVerbUI::patch()
{
    inL >> switchL.input(0);
    inR >> switchR.input(0);

    inL >> crossfader.in_A();
    inL >> stereoAmpDry.ch(0);
    inR >> crossfader.in_B();
    inR >> stereoAmpDry.ch(1);

    //im using the crossfader just for converting to mono
    //since this reverb has mono input
    crossfader >> reverb;
                  reverb.ch(0) >> stereoAmpWet.ch(0);
                  reverb.ch(1) >> stereoAmpWet.ch(1);

    stereoAmpDry.ch(0) + stereoAmpWet.ch(0) >> switchL.input(1);
    stereoAmpDry.ch(1) + stereoAmpWet.ch(1) >> switchR.input(1);

    switchL >> bugfixGainOut.ch(0) >> outL;
    switchR >> bugfixGainOut.ch(1) >> outR;
    switchL >> bugfixGainBlackhole.ch(0) >> AudioEngine::getInstance()->getBlackhole();
    switchR >> bugfixGainBlackhole.ch(1) >> AudioEngine::getInstance()->getBlackhole();

    1.f >> switchL.in_select();
    1.f >> switchR.in_select();

    0.5f >> crossfader.in_fade();

    rt60 >> reverb.in_time();
    density >> reverb.in_density();
    damping >> reverb.in_damping();
    //hicut >> reverb.in_hi_cut();
    lfoFreq >> reverb.in_mod_freq();
    lfoAmount >> reverb.in_mod_amount();

    stereoAmpDry.set(0.5f);

    bugfixGainOut.set(-48.f);
    bugfixGainBlackhole.set(0.f);

    patched = true;

}

void BasiVerbUI::update(ofEventArgs &args)
{
    static const int WAIT_TIME = 55;
    static const int FADE_IN_TIME = 20;

    if ( bugfixWait <= WAIT_TIME ) {
        bugfixWait++;
        return;
    }

    if ( bugfixGainOut.get() != 0.f ) {
        if ( bugfixFadeIn == 0 ) {
            bugfixGainBlackhole.set(-48.f);
        }
        if ( bugfixFadeIn <= FADE_IN_TIME ) {
            float currentGainOut = bugfixGainOut.get();
            currentGainOut = ofMap( bugfixFadeIn, 0, FADE_IN_TIME, -47.9f, 0 );
            bugfixGainOut.set( currentGainOut );
            bugfixFadeIn++;
        }
        return;
    }

    ofRemoveListener( ofEvents().update, this, &BasiVerbUI::update );
}
