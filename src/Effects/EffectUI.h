#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxJSON.h"

#include "../GUI/UberControl.h"
#include "../GUI/UberToggle.h"

class EffectUI : public pdsp::Patchable {
protected:
    int index = -1;

    vector<UberControl *> parameters;
    Json::Value * loadedParams = nullptr;
    //this will parse only whats inside parameters
    //each effect should take care of custom params overriding this function
    virtual void parseLoadedParams();
    
    pdsp::PatchNode inL;
    pdsp::PatchNode inR;
    pdsp::PatchNode outL;
    pdsp::PatchNode outR;
    
    pdsp::Switch switchL;
    pdsp::Switch switchR;
    
    UberToggle * enable;
    
    friend class AudioEffects;
    
private:
    

public:
    EffectUI();
    virtual ~EffectUI();
    
    virtual string getName() = 0;

    // if the effect is only floats that can be put in variable parameters
    // then you don't have to implement save
    virtual Json::Value save();
    
    void load(Json::Value jsonAudioEffectsParams);

    virtual void setIndex(int i, string indexPrefix);

    pdsp::Patchable & channelIn(size_t index);
    pdsp::Patchable & channelOut(size_t index);
    
    virtual void drawGui();
    
    void enableListener(float &v);
    
};
