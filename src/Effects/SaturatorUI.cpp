#include "SaturatorUI.h"

SaturatorUI::SaturatorUI()
{
    parameters.push_back( new UberSlider(inGain.set("Input Gain", 0.f, -40.f, 100.f),
                                         &Tooltip::NONE) );
    parameters.push_back( new UberSlider(outGain.set("Output Gain", 0.f, -40.f, 100.f),
                                         &Tooltip::NONE) );

    switchL.resize(2);
    switchR.resize(2);

    inL >> switchL.input(0);
    inR >> switchR.input(0);
    
    inL >> inGain.ch(0) >> saturatorL >> outGain.ch(0) >> switchL.input(1);
    inR >> inGain.ch(1) >> saturatorR >> outGain.ch(1) >> switchR.input(1);
    
    switchL >> outL;
    switchR >> outR;
    
    1.f >> switchL.in_select();
    1.f >> switchR.in_select();
    
}

string SaturatorUI::getName()
{
    return "Saturator";
}


