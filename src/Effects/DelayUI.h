#pragma once

#include "ofMain.h"
#include "ofxPDSP.h"
#include "ofxImGui.h"

#include "./EffectUI.h"
#include "../GUI/UI.h"


class DelayUI : public EffectUI
{
private:
    pdsp::Delay delayL;
    pdsp::Delay delayR;
    
    pdsp::LinearCrossfader crossfaderL;
    pdsp::LinearCrossfader crossfaderR;

    pdsp::Parameter time;
    pdsp::Parameter feedback;
    pdsp::Parameter damping;
    pdsp::Parameter mix;
    

    const float MAX_DELAY_TIME = 1000;
public:
    DelayUI();

    string getName();

    pdsp::Patchable & channelIn(size_t index);
    pdsp::Patchable & channelOut(size_t index);
};
